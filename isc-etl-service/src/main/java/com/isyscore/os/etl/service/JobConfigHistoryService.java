package com.isyscore.os.etl.service;

import com.isyscore.os.etl.model.dto.JobConfigHistoryDTO;

public interface JobConfigHistoryService {
    void insertJobConfigHistory(JobConfigHistoryDTO jobConfigHistoryDTO);
}
