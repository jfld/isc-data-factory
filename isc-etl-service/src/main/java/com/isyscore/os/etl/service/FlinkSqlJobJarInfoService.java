package com.isyscore.os.etl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.etl.model.FlinkSqlJobJarInfo;

public interface FlinkSqlJobJarInfoService extends IService<FlinkSqlJobJarInfo> {
}
