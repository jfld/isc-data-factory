package com.isyscore.os.etl.model.form;

import com.isyscore.os.etl.model.Insert;
import com.isyscore.os.etl.model.Update;
import com.isyscore.os.etl.model.dto.JobComponent;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @description : 可视化配置数据结构
 * @date : 2021/11/23 4:39 下午
 */
@Data
public class JobViewable {

    @NotNull(groups = Update.class, message = "工作流ID不能为空")
    private String jobConfigId;

    /**
     * 任务名称
     */
    @NotNull(groups = Insert.class, message = "工作流名称不能为空")
    @Length(groups = Insert.class, min = 1, max = 32, message = "数据源组件名称长度不符合要求")
    private String jobName;
    /**
     *  部署方式
     */
    @NotNull(groups = Insert.class, message = "运行模式不能为空")
    private String deployMode;


    private String flinkRunConfig;

    /**
     * jar 包依赖地址
     */
    private String extJarPath;

    /**
     * 任务类型
     */
    @NotNull(groups = Insert.class, message = "任务类型不能为空")
    private Integer jobType;


    private String cron;

    private Integer isScheduled;

    @Valid
    private List<JobComponent> components;

    /**
     * 前端现场信息
    */
    private String localConfig;

}
