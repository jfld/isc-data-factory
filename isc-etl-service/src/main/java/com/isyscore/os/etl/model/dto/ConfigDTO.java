package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ConfigDTO {
    private DmcUploadRespDTO dmcConfig;
    private String jarid;
}
