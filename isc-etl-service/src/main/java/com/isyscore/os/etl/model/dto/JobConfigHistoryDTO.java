package com.isyscore.os.etl.model.dto;

import cn.hutool.core.collection.CollectionUtil;
import com.isyscore.os.core.model.entity.JobConfig;
import com.isyscore.os.core.model.entity.JobConfigHistory;
import lombok.Data;
import org.apache.commons.compress.utils.Lists;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Data
public class JobConfigHistoryDTO {
    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * job_config主表Id
     */
    private String jobConfigId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 提交模式: standalone 、yarn 、yarn-session
     */
    private String deployMode;

    /**
     * flink运行配置
     */
    private String flinkRunConfig;


    /**
     * udf地址及连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar
     */
    private String extJarPath;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime editTime;

    private String creator;

    private String editor;

    /**
     * sql语句
     */
    private String flinkSql;

    private String tenantId;

    private String schedule;


    public static JobConfigHistory toEntity(JobConfigHistoryDTO jobConfigHistoryDTO) {
        if (jobConfigHistoryDTO == null) {
            return null;
        }
        JobConfigHistory jobConfigHistory = new JobConfigHistory();
        jobConfigHistory.setId(jobConfigHistoryDTO.getId());
        jobConfigHistory.setJobConfigId(jobConfigHistoryDTO.getJobConfigId());
        jobConfigHistory.setJobName(jobConfigHistoryDTO.getJobName());
        jobConfigHistory.setDeployMode(jobConfigHistoryDTO.getDeployMode());
        jobConfigHistory.setFlinkRunConfig(jobConfigHistoryDTO.getFlinkRunConfig());
        jobConfigHistory.setExtJarPath(jobConfigHistoryDTO.getExtJarPath());
        jobConfigHistory.setVersion(jobConfigHistoryDTO.getVersion());
        jobConfigHistory.setCreatedAt(jobConfigHistoryDTO.getCreateTime());
        jobConfigHistory.setUpdatedAt(jobConfigHistoryDTO.getEditTime());
        jobConfigHistory.setCreator(jobConfigHistoryDTO.getCreator());
        jobConfigHistory.setEditor(jobConfigHistoryDTO.getEditor());
        jobConfigHistory.setFlinkSql(jobConfigHistoryDTO.getFlinkSql());
        jobConfigHistory.setTenantId(jobConfigHistoryDTO.getTenantId());
        jobConfigHistory.setSchedule(jobConfigHistoryDTO.getSchedule());
        return jobConfigHistory;
    }


    public static JobConfigHistoryDTO toDTO(JobConfigHistory jobConfigHistory) {
        if (jobConfigHistory == null) {
            return null;
        }
        JobConfigHistoryDTO jobConfigHistoryDTO = new JobConfigHistoryDTO();
        jobConfigHistoryDTO.setId(jobConfigHistory.getId());
        jobConfigHistoryDTO.setJobConfigId(jobConfigHistory.getJobConfigId());
        jobConfigHistoryDTO.setJobName(jobConfigHistory.getJobName());
        jobConfigHistoryDTO.setDeployMode(jobConfigHistory.getDeployMode());
        jobConfigHistoryDTO.setFlinkRunConfig(jobConfigHistory.getFlinkRunConfig());
        jobConfigHistoryDTO.setExtJarPath(jobConfigHistory.getExtJarPath());
        jobConfigHistoryDTO.setVersion(jobConfigHistory.getVersion());
        jobConfigHistoryDTO.setCreateTime(jobConfigHistory.getCreatedAt());
        jobConfigHistoryDTO.setEditTime(jobConfigHistory.getUpdatedAt());
        jobConfigHistoryDTO.setCreator(jobConfigHistory.getCreator());
        jobConfigHistoryDTO.setEditor(jobConfigHistory.getEditor());
        jobConfigHistoryDTO.setFlinkSql(jobConfigHistory.getFlinkSql());
        jobConfigHistoryDTO.setTenantId(jobConfigHistory.getTenantId());
        jobConfigHistoryDTO.setSchedule(jobConfigHistoryDTO.getSchedule());
        return jobConfigHistoryDTO;
    }

    public static List<JobConfigHistoryDTO> toListDTO(List<JobConfigHistory> jobConfigHistoryList) {
        if (CollectionUtil.isEmpty(jobConfigHistoryList)) {
            return Collections.EMPTY_LIST;
        }

        List<JobConfigHistoryDTO> list = Lists.newArrayList();

        for (JobConfigHistory jobConfigHistory : jobConfigHistoryList) {

            JobConfigHistoryDTO jobConfigHistoryDTO = JobConfigHistoryDTO.toDTO(jobConfigHistory);
            if (jobConfigHistoryDTO != null) {
                list.add(jobConfigHistoryDTO);
            }
        }

        return list;
    }


    public static JobConfigHistoryDTO to(JobConfig jobConfig) {
        if (jobConfig == null) {
            return null;
        }
        JobConfigHistoryDTO jobConfigHistoryDTO = new JobConfigHistoryDTO();
        jobConfigHistoryDTO.setJobConfigId (jobConfig.getId());
        jobConfigHistoryDTO.setJobName(jobConfig.getJobName());
        jobConfigHistoryDTO.setDeployMode(jobConfig.getDeployMode());
        jobConfigHistoryDTO.setFlinkRunConfig(jobConfig.getFlinkRunConfig());
        jobConfigHistoryDTO.setExtJarPath(jobConfig.getExtJarPath());
        jobConfigHistoryDTO.setVersion(jobConfig.getVersion());
        jobConfigHistoryDTO.setCreateTime(jobConfig.getCreatedAt());
        jobConfigHistoryDTO.setEditTime(jobConfig.getUpdatedAt());
        jobConfigHistoryDTO.setCreator(jobConfig.getCreator());
        jobConfigHistoryDTO.setEditor(jobConfig.getEditor());
        jobConfigHistoryDTO.setFlinkSql(jobConfig.getFlinkSql());
        jobConfigHistoryDTO.setTenantId(jobConfig.getTenantId());
        jobConfigHistoryDTO.setSchedule(jobConfig.getCron());
        return jobConfigHistoryDTO;
    }
}
