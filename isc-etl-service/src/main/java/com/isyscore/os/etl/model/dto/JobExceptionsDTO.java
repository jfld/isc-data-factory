package com.isyscore.os.etl.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class JobExceptionsDTO {

        private List<ExceptionDTO> allExceptions;

        private String rootException;

        private Long timestamp;

        private Boolean truncated;


        @Data
        static class ExceptionDTO{

            private String exception;

            private String location;

            private String task;

                private Long timestamp;

        }
}
