package com.isyscore.os.etl.model.flinkviewable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.isyscore.os.etl.model.Insert;
import lombok.Data;
import javax.validation.constraints.NotEmpty;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2021/12/13 11:50 上午
 */
@Data
public class Mapping {
    /**
     * 原表字段
    */
    @NotEmpty(groups = Insert.class, message = "源表字段不能为空")
    @JsonInclude
    private String sourceCol;
    /**
     * 原表字段类型
    */
    @NotEmpty(groups = Insert.class, message = "源表字段类型不能为空")
    @JsonInclude
    private String sourceColType;
    /**
     * 目标表字段类型
    */
    @NotEmpty(groups = Insert.class, message = "目标表字段类型不能为空")
    @JsonInclude
    private String targetColType;
    /**
     * 目标表字段
    */
    @NotEmpty(groups = Insert.class, message = "目标表字段不能为空")
    @JsonInclude
    private String targetCol;
}
