package com.isyscore.os.etl.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.etl.model.dto.LogsInfoDTO;
import com.isyscore.os.etl.model.dto.TaskManagerInfo;
import com.isyscore.os.etl.service.FlinkService;
import com.isyscore.os.etl.service.JobConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;


/**
 * @author liyc
 * @Title: LogRunController
 * @ProjectName isc-data-factory
 * @date 2021/8/83:10 PM
 */
@Api(tags = "Flink任务相关(log 相关功能)")
@Slf4j
@RestController
@RequestMapping("${api-full-prefix}/log")
public class LogRunController {

    @Autowired(required = false)
    private FlinkService flinkService;

    @Autowired
    private JobConfigService jobConfigService;

    @ApiOperation("查询 JobManager 日志列表")
    @GetMapping("/jobmanager/logs/{jobConfigId}")
    public RespDTO<LogsInfoDTO> getJobManagerLogList(@PathVariable("jobConfigId") String jobConfigId) {

        URI uriByConfigId = jobConfigService.getUriByConfigId(jobConfigId);

        return RespDTO.onSuc(flinkService.getJobManagerLogList(uriByConfigId));
    }

    @ApiOperation("查询 JobManager 日志详情")
    @GetMapping("/jobmanager/logs/{name}/{jobConfigId}")
    public RespDTO<String> getJobManagerLog(@PathVariable("name") String name, @PathVariable("jobConfigId") String jobConfigId) {

        URI uriByConfigId = jobConfigService.getUriByConfigId(jobConfigId);

        return RespDTO.onSuc(flinkService.getJobManagerLog(uriByConfigId, name));
    }

    @ApiOperation("查询 taskmanagers 任务列表")
    @GetMapping("/taskmanagers/{jobConfigId}")
    public RespDTO<TaskManagerInfo> getTaskManager(@PathVariable("jobConfigId") String jobConfigId) {

        URI uriByConfigId = jobConfigService.getUriByConfigId(jobConfigId);

        return RespDTO.onSuc(flinkService.getTaskManagers(uriByConfigId));
    }

    @ApiOperation("查询 taskmanagers 任务")
    @GetMapping("/taskmanagers/{taskId}/logs/{jobConfigId}")
    public RespDTO<LogsInfoDTO> getTaskManagersLogList(@PathVariable("taskId") String taskId, @PathVariable("jobConfigId") String jobConfigId) {

        URI uriByConfigId = jobConfigService.getUriByConfigId(jobConfigId);

        return RespDTO.onSuc(flinkService.getTaskManagerLogListByTaskId(uriByConfigId, taskId));
    }

    @ApiOperation("查询 taskmanagers 任务的日志")
    @GetMapping("/taskmanagers/{taskId}/logs/{name}/{jobConfigId}")
    public RespDTO<String> getTaskManagersLogList(@PathVariable("taskId") String taskId, @PathVariable("name") String name, @PathVariable("jobConfigId") String jobConfigId) {

        URI uriByConfigId = jobConfigService.getUriByConfigId(jobConfigId);

        return RespDTO.onSuc(flinkService.getTaskManagerLogByTaskIdAndName(uriByConfigId, taskId, name));
    }

}
