package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
/**
 * @author lyc
 * @Desc:
 */
public class LogsInfoDTO {

    private List<Logs> logs;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Logs {

        private String name;

        private Integer size;
    }

}

