package com.isyscore.os.etl.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * 任务运行状态，可作为runLog和flink端的状态枚举
 * @author wuwx
 */
@Getter
@AllArgsConstructor
public enum JobRunStatus {
    /**
     * flink状态枚举
     */
    INITIALIZING("INITIALIZING", 0),

    CREATED("CREATED", 1),

    RUNNING("RUNNING", 2),

    FAILING("FAILING", 3),

    FAILED("FAILED", 4),

    CANCELLING("CANCELLING", 5),

    CANCELED("CANCELED", 6),

    FINISHED("FINISHED", 7),

    RESTARTING("RESTARTING", 8),

    SUSPENDED("SUSPENDED", 9),

    SCHEDULING("SCHEDULING", 10),

    PRE_SCHEDULING("PRE_SCHEDULING", 11),

    SUCCESS("SUCCESS", 12),

    RECONCILING("RECONCILING", -1);

    private final String status;

    private final Integer code;

    public static JobRunStatus get(String name) {
        return JobRunStatus.valueOf(name);
    }


}
