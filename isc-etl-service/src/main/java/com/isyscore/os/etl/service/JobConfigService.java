package com.isyscore.os.etl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.model.entity.JobConfig;
import com.isyscore.os.etl.model.dto.JobComponent;
import com.isyscore.os.etl.model.dto.JobConfigDTO;
import com.isyscore.os.etl.model.form.JobViewable;

import java.net.URI;
import java.util.List;
import java.util.Map;

public interface JobConfigService extends IService<JobConfig> {
    RespDTO addJob(JobConfig jobConfig);

    RespDTO updateJobConfigById(JobConfigDTO jobConfigDTO);

    RespDTO updateJobConfig(JobConfig jobConfig);

    RespDTO openJob(String id);

    RespDTO closeJob(String id);

    RespDTO deleteJobConfigById(String id);

    JobConfigDTO getJobConfigById(String id);

    RespDTO startJob(String id);

    RespDTO stopJob(String id);


    void checkJobStatus();

    /**
     * 根据 configId 获取fegin请求时的统一资源定位符
     *
     * @param configId
     * @return
     */
    URI getUriByConfigId(String configId);

    void schedule (String id, Integer isScheduled);

    RespDTO<Map> saveViewable(JobViewable form);

    void updateViewable(JobViewable form);

    String preview(List<JobComponent> components);

    void manuallyStop(String id);
}
