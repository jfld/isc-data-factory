package com.isyscore.os.etl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.mapper.JobRunLogMapper;
import com.isyscore.os.core.model.entity.JobRunLog;
import com.isyscore.os.etl.model.dto.JobRunLogDTO;
import com.isyscore.os.etl.service.JobRunLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JobRunLogServiceImpl extends ServiceImpl<JobRunLogMapper, JobRunLog> implements JobRunLogService {
    @Override
    public String insertJobRunLog(JobRunLogDTO jobRunLogDTO) {
        JobRunLog jobRunLog = JobRunLogDTO.toEntity(jobRunLogDTO);
        this.save(jobRunLog);
        return jobRunLog.getId();
    }

    @Override
    public RespDTO updateJobRunLogById(JobRunLogDTO jobRunLogDTO) {
        JobRunLog jobRunLog = JobRunLogDTO.toEntity(jobRunLogDTO);
        this.updateById(jobRunLog);
        return RespDTO.onSuc();
    }

    @Override
    public void updateLogById(String localLog, String id) {
        try {
            JobRunLog jobRunLog = new JobRunLog();
            jobRunLog.setId(id);
            jobRunLog.setLocalLog(localLog);
            this.updateById(jobRunLog);
        } catch (Exception e) {
            log.error("更新日志失败", e);
        }
    }

    @Override
    public void deleteLogByConfigId(String configId) {
        LambdaQueryWrapper<JobRunLog> wrapper = new LambdaQueryWrapper<>();
        this.remove(wrapper.eq(JobRunLog::getJobConfigId, configId));
    }
}
