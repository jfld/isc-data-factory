package com.isyscore.os.etl.model.enums;

import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.core.exception.ErrorCode;
import org.apache.commons.lang3.StringUtils;

public enum DeployModeEnum {
    YARN_PER, STANDALONE, LOCAL;

    public static DeployModeEnum getModel(String model) {
        if (StringUtils.isEmpty(model)) {
            throw new DataFactoryException(ErrorCode.MISSING_SERVLET_REQUEST_PARAMETER);
        }
        for (DeployModeEnum deployModeEnum : DeployModeEnum.values()) {
            if (deployModeEnum.name().equals(model.trim().toUpperCase())) {
                return deployModeEnum;
            }

        }
        throw new DataFactoryException(ErrorCode.PARAM_ERROR);
    }
}
