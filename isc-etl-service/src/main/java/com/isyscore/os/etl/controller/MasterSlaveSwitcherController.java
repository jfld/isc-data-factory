package com.isyscore.os.etl.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.etl.config.JobConfiguration;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api-full-prefix}/masterSlaveSwitcher")
@Api(tags = "master-slave切换")
@Slf4j
public class MasterSlaveSwitcherController {

    @Autowired
    JobConfiguration jobConfiguration;
    @ApiOperation("（内部API）切换当前服务为master")
    @PutMapping("/master")
    public RespDTO<?> switch2Master() throws Exception {
        jobConfiguration.switch2Master();
        return RespDTO.onSuc();
    }

    @ApiOperation("（内部API）切换当前服务为slave")
    @PutMapping("/slave")
    public RespDTO<?> switch2Slave() {
        jobConfiguration.switch2Slave();
        return RespDTO.onSuc();
    }

    @ApiOperation("（内部API）查看当前服务的角色")
    @GetMapping("/role")
    public RespDTO<String> getRole() {
        return RespDTO.onSuc(jobConfiguration.getCurrentRole());
    }

}
