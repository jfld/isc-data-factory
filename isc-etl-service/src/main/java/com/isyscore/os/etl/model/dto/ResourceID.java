package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author liyc
 * @Title: ResourceID
 * @ProjectName isc-data-factory
 * @date 2021/8/85:32 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceID implements Serializable {

    private static final long serialVersionUID = 42L;

    private String resourceId;

    private String metadata;

}
