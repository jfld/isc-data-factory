package com.isyscore.os.etl.model.dto;

import lombok.Data;

@Data
public class JobDTO {

    private String id;

    private String status;

    private String jid;
    private String name;
    private Long startTime;
    private Long endTime;
    private Long duration;
    private Long lastModification;
    private TaskStatistic tasks;

}

