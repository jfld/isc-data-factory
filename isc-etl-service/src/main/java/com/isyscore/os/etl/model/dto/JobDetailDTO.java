package com.isyscore.os.etl.model.dto;


import com.isyscore.os.etl.model.enums.JobIdStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
public class JobDetailDTO {

    private Integer duration;

    private Integer endTime;

    private Boolean isStoppable;

    private String jid;

    private Integer maxParallelism;

    private String name;

    private Integer now;

    private String plan;

    private Integer startTime;

    private JobIdStatus jobIdStatus;

    private Integer statusCounts;

    /*
     "RESTARTING": 0,
    "RECONCILING": 0,
    "RUNNING": 1622008801288,
    "CANCELED": 0,
    "CANCELLING": 0,
    "INITIALIZING": 1622008801205,
    "FINISHED": 1622008844492,
    "SUSPENDED": 0,
    "CREATED": 1622008801220,
    "FAILED": 0,
    "FAILING": 0
     */
    private Map<String, Long> timestamps;

    private List<Iterm> vertices;


}
