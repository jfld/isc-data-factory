package com.isyscore.os.etl.model.dto;

import lombok.Data;

@Data
public class JobStandaloneInfoDTO {
    private String jid;

    private String state;

    private String errors;
}
