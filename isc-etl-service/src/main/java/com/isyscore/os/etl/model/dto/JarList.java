package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.Files;
import lombok.Data;

import java.util.List;

@Data
public class JarList {
    private String address;
    private List<Files> files;
}
