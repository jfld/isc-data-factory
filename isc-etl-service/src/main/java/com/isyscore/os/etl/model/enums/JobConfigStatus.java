package com.isyscore.os.etl.model.enums;

import lombok.Getter;

/**
 * 任务配置状态，仅作为runConfig状态枚举,不参与flink端状态条件判断
 * @author wuwx
 */

@Getter
public enum JobConfigStatus {
    FAILED(-1, "失败"),

    RUNNING(1, "运行中"),

    STOP(0, "未启动"),

    STARTING(2, "启动中"),

    FINISHED(3, "已完成"),

    CANCELED(4, "已取消"),

    ;

    private Integer code;

    private String desc;

    JobConfigStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static JobConfigStatus getJobConfigStatus(Integer code) {
        for (JobConfigStatus jobConfigStatus : JobConfigStatus.values()) {
            if (jobConfigStatus.getCode().equals(code)) {
                return jobConfigStatus;
            }
        }
        return STOP;
    }

    public static JobConfigStatus get(String name) {
        return JobConfigStatus.valueOf(name);
    }

    public static void main(String[] args) {
        System.out.println(JobConfigStatus.get("FINISHED"));
    }
}
