package com.isyscore.os.etl.constant;

/**
 * @author wany
 */
public interface FlinkApiConstant {

    String API_RESULT_STATUS_SUCCESS = "success";

    String API_RESULT_STATUS_PROP_NAME = "status";

    String API_RESULT_JOBID_PROP_NAME = "jobid";

}
