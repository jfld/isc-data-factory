package com.isyscore.os.etl.model.dto;

import cn.hutool.core.io.resource.Resource;
import lombok.Data;

/**
 * @author wany
 */
@Data
public class FlinkSqlJarInfoDTO {

    private String jarVersion;

    private Resource jarResource;

}
