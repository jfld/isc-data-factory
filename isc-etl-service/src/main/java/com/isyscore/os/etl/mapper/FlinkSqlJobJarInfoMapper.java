package com.isyscore.os.etl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.etl.model.FlinkSqlJobJarInfo;

/**
 * @author wany
 */
public interface FlinkSqlJobJarInfoMapper extends BaseMapper<FlinkSqlJobJarInfo> {

}
