package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobVertexSubAccDTO {

    private String id;

    private Integer parallelism;

    private List<Subtasks> subtasks;

    @Data
    @Builder
    @AllArgsConstructor
    static class Subtasks {

        @Data
        @Builder
        @AllArgsConstructor
        static class Items {

            private String attempt;

            private String host;

            private Integer subtask;

            private UserAccumulators userAccumulators;

        }

        @Data
        @Builder
        @AllArgsConstructor
        static class UserAccumulators {

            @Data
            @Builder
            @AllArgsConstructor
            static class Items {

                private String name;

                private String type;

                private String value;

            }

        }

    }


}
