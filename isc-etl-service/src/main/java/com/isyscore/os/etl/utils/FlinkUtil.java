package com.isyscore.os.etl.utils;

import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.core.exception.ErrorCode;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

/**
 * @Description:
 * @date 2021/8/18 16:10
 */

@UtilityClass
@Slf4j
public class FlinkUtil {

    private static RestTemplate restTemplate = new RestTemplate();

    private static String PREFIX = "http://";

    private static String PATH = "/jars";

    public static void ping(String url, boolean splicing) {
        if (url == null)
            return;
        if (!url.startsWith("http")) {
            url = PREFIX + url;
        }
        url = url + (splicing == true ? PATH : "");

        ResponseEntity<String> resp;
        try {
             resp= restTemplate.getForEntity(url, String.class);
        }catch (RestClientException e) {
            log.error("flink 客户端连接出现异常：{}", e);
            throw new DataFactoryException(ErrorCode.FLINK_CONNET_ERROR);
        }
        if (Objects.isNull(resp) || !resp.hasBody())
            throw new DataFactoryException(ErrorCode.FLINK_CONNET_ERROR);
    }
}
