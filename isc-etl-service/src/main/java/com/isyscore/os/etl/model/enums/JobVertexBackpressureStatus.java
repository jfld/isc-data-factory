package com.isyscore.os.etl.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobVertexBackpressureStatus {

    OK("OK"),

    DEPRECATED("DEPRECATED");

    private String status;

}
