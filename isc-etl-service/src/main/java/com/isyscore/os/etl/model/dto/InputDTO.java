package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InputDTO {
    @ApiModelProperty("数量")
    private Integer num;
    private String id;
    @JsonAlias({"shipStrategy", "ship_strategy"})
    private String shipStrategy;
    private String exchange;
}
