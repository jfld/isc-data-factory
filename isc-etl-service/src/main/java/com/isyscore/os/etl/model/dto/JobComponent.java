package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author codezzz
 * @version 0.0.1
 * @Description:
 * @date 2021/12/19 14:24
 */
@Data
public class JobComponent {

    @NotEmpty(message = "节点ID不能为空")
    private String nodeId;

    @NotNull(message = "节点类型不能为空")
    private Integer componentType;

    private JobComponent next;

    private List<String> sourceIds;

    private List<String> targetIds;

    private ViewableConfig config;

}
