package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.enums.RescalingTrigger;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class JobIdRescalingTriggerIdDTO {

    @Data
    @Builder
    @AllArgsConstructor
    static class Operation{

        static class failureCause{}

    }

    @Data
    @Builder
    @AllArgsConstructor
    static class Status{

        private RescalingTrigger status;

    }

}
