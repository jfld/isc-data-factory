package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author liyc
 * @Title: TaskExecutorMemoryConfiguration
 * @ProjectName isc-data-factory
 * @date 2021/8/85:28 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskExecutorMemoryConfiguration {

    private Long frameworkHeap;

    private Long taskHeap;

    private Long frameworkOffHeap;

    private Long taskOffHeap;

    private Long networkMemory;

    private Long managedMemory;

    private Long jvmMetaspace;

    private Long jvmOverhead;

    private Long totalFlinkMemory;

    private Long totalProcessMemory;

}
