package com.isyscore.os.etl.model.enums;

import lombok.Getter;

@Getter
public enum JobType {
    SQL_STREAMING(0), JAR(1), SQL_BATCH(2), VIEW(3), ROW_SQL(4);

    private int code;

    JobType(int code) {
        this.code = code;
    }

    public static JobType getJobType(Integer code) {
        if (code == null) {
            return null;
        }
        for (JobType jobType : JobType.values()) {
            if (code == jobType.getCode()) {
                return jobType;
            }
        }

        return null;
    }
}
