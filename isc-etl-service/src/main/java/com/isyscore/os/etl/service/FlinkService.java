package com.isyscore.os.etl.service;

import com.isyscore.os.etl.model.dto.*;
import com.isyscore.os.etl.model.form.ProcessParamsForm;
import org.apache.flink.runtime.rest.messages.taskmanager.TaskManagerDetailsInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.Map;

@FeignClient(name = "flink-servie", url = "${flink-config.host}:${flink-config.port}")
public interface FlinkService {

    @PostMapping(value = "/jars/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    UploadDTO upload(@RequestPart("jarfile") MultipartFile file);

    @DeleteMapping("/jars/{jobId}")
    Map deleteFile(@PathVariable("jobId") String jobId);

    /**
     * 查看jar包的DAG图
     * NOTE: flink 1.0 NOT SUPPORTED
     *
     * @param jobId
     * @param params
     * @return
     */
    @PostMapping(value = "/jars/{jobId}/plan")
    PlanDTO showPlan(@PathVariable("jobId") String jobId, @RequestBody ProcessParamsForm params);

    @GetMapping(value = "/jars/{jobId}/plan")
    PlanDTOWrapper getPlan(@PathVariable("jobId") String jobId,
                           @NotNull(message = "请输入入口类的信息") @RequestParam(name = "entryClass") String entryClass,
                           @RequestParam(name = "program-args", required = false) String programArgs,
                           @RequestParam(name = "parallelism", required = false) Integer parallelism);

    /***
     * @description 运行job
     * @param jobId
     * @param params
     * @author zhangyn
     * @date 2021/5/26 11:41 上午
     * @return java.lang.String
     */
    @PostMapping(value = "/jars/{jobId}/run", consumes = MediaType.APPLICATION_JSON_VALUE)
    Map<String, String> run(@PathVariable("jobId") String jobId, ProcessParamsForm params);

    /***
     * @description 查看jobmanager日志
     * @param
     * @author zhangyn
     * @date 2021/5/26 11:37 上午 
     * @return java.lang.String
     */
    @GetMapping("/jobmanager/log")
    String getJMLogs();

    /**
     * 查看jobmanager list 日志
     *
     * @return
     */
    @GetMapping("/jobmanager/logs")
    LogsInfoDTO getJobManagerLogList(URI uri);

    /**
     * 查看jobmanager 日志
     *
     * @return
     */
    @GetMapping("/jobmanager/logs/{name}")
    String getJobManagerLog(URI uri,@PathVariable("name") String name);

    /***
     * @description 返回各job的详细信息
     * @param
     * @author zhangyn
     * @date 2021/5/26 11:59 上午
     * @return com.isyscore.os.etl.model.dto.JobDTO
     */
    @GetMapping("/jobs/overview")
    Map<String, List<JobDTO>> getJobOverview();

    /**
     * 仅返回各job的id和状态
     *
     * @param
     * @return com.isyscore.os.etl.model.dto.JobDTO
     * @author zhangyn
     * @date 2021/5/26 11:59 上午
     */
    @GetMapping("/jobs")
    Map<String, List<JobDTO>> getJob();

    /***
     * @description 查看指定job的详情
     * @param jobId
     * @author zhangyn
     * @date 2021/5/26 1:59 下午
     * @return com.isyscore.os.etl.model.dto.JobIdDTO
     */
    @GetMapping("/jobs/{jobId}")
    JobDetailDTO getJob(@PathVariable("jobId") String jobId);

    /**
     * @param jobId
     * @return com.isyscore.os.etl.model.dto.JobIdExceptionsDTO
     * @description 查看指定job的异常日志
     * @author zhangyn
     * @date 2021/5/26 2:00 下午
     */
    @GetMapping("/jobs/{jobId}/exceptions")
    JobExceptionsDTO getJobExceptions(@PathVariable("jobId") String jobId);


    /**
     * @param
     * @return com.isyscore.os.etl.model.dto.JarList
     * @description 获取flink服务上的所有jar包信息
     * @author zhangyn
     * @date 2021/6/1 2:13 下午
     */
    @GetMapping("/jars")
    JarList getJars();


    /**
     * 查看指定job的执行结果
     *
     * @param jobId
     * @return
     */
    @GetMapping("/jobs/{jobId}/execution-result")
    JobIdExeResultDTO getJobExecutionResult(@PathVariable("jobId") String jobId);

    /**
     * cancel正在执行的job
     *
     * @param jobId
     */
    @GetMapping("/jobs/{jobId}/yarn-cancel")
    void cancelJob(@PathVariable("jobId") String jobId);

    /**
     * 返回所有taskmanager的信息
     *
     * @return
     */
    @GetMapping("/taskmanagers")
    TaskManagerInfo getTaskManagers(URI uri);

    /**
     * 返回指定taskmanager的信息
     *
     * @param taskManagerId
     * @return
     */
    @GetMapping("/taskmanagers/{taskManagerId}")
    TaskManagerDetailsInfo getTaskManagersByTaskId(URI uri,@PathVariable("taskManagerId") String taskManagerId);

    /**
     * 查询 flink  taskmanager 日志列表
     *
     * @param taskManagerId
     * @return
     */
    @GetMapping("/taskmanagers/{taskManagerId}/logs")
    LogsInfoDTO getTaskManagerLogListByTaskId(URI uri,@PathVariable("taskManagerId") String taskManagerId);

    /**
     * 查询 flink  taskmanager 具体日志
     *
     * @param taskManagerId
     * @param name
     * @return
     */
    @GetMapping("/taskmanagers/{taskManagerId}/logs/{name}")
    String getTaskManagerLogByTaskIdAndName(URI uri,@PathVariable("taskManagerId") String taskManagerId, @PathVariable("name") String name);

    /**
     * 返回指定taskmanager的日志
     *
     * @param taskManagerId
     * @return
     */
    @GetMapping("/taskmanagers/{taskManagerId}/log")
    String getJobTaskManagersLogsByTaskId(@PathVariable("taskManagerId") String taskManagerId);

    /**
     * \返回指定taskmanager的标准输出
     *
     * @param taskManagerId
     * @return
     */
    @GetMapping("/taskmanagers/{taskManagerId}/stdout")
    String getJobTaskManagersStdoutByTaskId(@PathVariable("taskManagerId") String taskManagerId);


}
