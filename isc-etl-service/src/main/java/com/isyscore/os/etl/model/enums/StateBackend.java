package com.isyscore.os.etl.model.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum StateBackend {
    MEMORY("0"), FILE("1"), ROCKSDB("2");

    private String type;

    StateBackend(String type) {
        this.type = type;
    }

    public static StateBackend getStateBackend(String stateBackendType) {
        if (StringUtils.isEmpty(stateBackendType)) {
            return FILE;
        }

        for (StateBackend stateBackend : StateBackend.values()) {
            if (stateBackend.getType().equalsIgnoreCase(stateBackendType.trim())) {
                return stateBackend;
            }

        }
        throw new  RuntimeException("stateBackendType值只能是 0 1 2 非法参数值"+stateBackendType);
    }
}
