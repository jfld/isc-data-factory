package com.isyscore.os.etl.schedule;

import com.isyscore.boot.login.LoginUserManagerImpl;
import com.isyscore.os.etl.service.impl.JobConfigServiceImpl;
import com.isyscore.os.permission.entity.LoginVO;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;

/**
 * @author zhangyn
 * @version 1.0
 * @date 2021/9/11 2:12 下午
 */
@Slf4j
public class RunJob extends QuartzJobBean {

    @Autowired
    private JobConfigServiceImpl jobConfigService;

    @Autowired
    private LoginUserManagerImpl loginUserManager;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String jobId = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("jobId");
        LoginVO current =  (LoginVO) jobExecutionContext.getJobDetail().getJobDataMap().get("current");
        loginUserManager.setThreadLocalLoginUser(current);
        Boolean isResume = jobExecutionContext.getJobDetail().getJobDataMap().get("isResume") == null ? false : true;
        log.info("开始调度：{}##{}", LocalDateTime.now(),jobId);
        try {
            if (!isResume) {
                jobConfigService.startJobInternalSyn(jobId);
            } else {
                jobConfigService.resumeSchedule(jobId);
            }
        } catch (Exception e) {
            log.error("任务执行失败{}",e.getMessage(),e);
        }
        log.info("调度结束: {}##{}", LocalDateTime.now(), jobId);
    }
}
