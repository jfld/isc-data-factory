package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;

/**
 * @author liyc
 * @Title: HardwareDescription
 * @ProjectName isc-data-factory
 * @date 2021/8/85:29 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardwareDescription implements Serializable {

    private static final long serialVersionUID = 3380016608300325361L;

    /**
     * The number of CPU cores available to the JVM on the compute node.
     */
    private int cpuCores;

    /**
     * The size of physical memory in bytes available on the compute node.
     */
    private long physicalMemory;

    /**
     * The size of the JVM heap memory
     */
    private long freeMemory;

    /**
     * The size of the memory managed by the system for caching, hashing, sorting, ...
     */
    private long managedMemory;
}
