package com.isyscore.os.etl.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import com.isyscore.os.core.entity.Resource;
import com.isyscore.os.etl.service.ResourceService;
import com.isyscore.os.permission.common.constants.PermissionConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import javax.validation.groups.Default;

/**
 * 
 *
 * @author felixu
 * @since 2021-08-03
 */
@RestController
@Api(tags = "资源管理")
@RequiredArgsConstructor
@RequestMapping("${api-full-prefix}/resource")
public class ResourceController {

    private final ResourceService resourceService;

    @GetMapping("/{id}")
    @ApiOperation("查询资源详情")
    public Resource get(@PathVariable("id") String id) {
        return resourceService.getResourceByIdAndCheck(id);
    }

    @GetMapping
    @ApiOperation("分页查询资源列表")
    public RespDTO<IPage<Resource>> page(Resource resource, PageRequest page) {
        return RespDTO.onSuc(resourceService.list(resource, page));
    }

    @ApiOperation("新增资源")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public RespDTO<Resource> create(@Validated({ValidateCreate.class, Default.class}) @RequestBody Resource resource) {
        return RespDTO.onSuc(resourceService.createResource(resource));
    }

    @ApiOperation("修改资源")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public RespDTO<Resource> update(@Validated({ValidateUpdate.class, Default.class}) @RequestBody Resource resource) {
        return RespDTO.onSuc(resourceService.updateResource(resource));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据 id 删除资源")
    public RespDTO<Resource> delete(@PathVariable("id") String id) {
        return RespDTO.onSuc(resourceService.deleteResource(id));
    }
}