package com.isyscore.os.etl.service;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.model.dto.LicenseQueryDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${service.license.name}",path = "/api/core/license", url="${service.license.url}")
public interface LicenseService {
    @GetMapping("/module/valid")
    @ApiOperation("查询模块是否可用")
    RespDTO<Boolean> valid(@RequestParam("appCode") String  appCode);
    @PostMapping("/desc/query")
    @ApiOperation("主动获取license信息")
    RespDTO<List> query(@RequestBody LicenseQueryDTO queryDTO);
}
