package com.isyscore.os.etl.service;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.etl.config.InitCrossServerConfig;
import com.isyscore.os.etl.model.dto.DataSourceDTO;
import com.isyscore.os.permission.common.constants.PermissionConstants;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2021/12/14 5:40 下午
 */
@FeignClient(name = "${service.metadata.name}",path = "/api/udmp/metadata/dataSource", url="${service.metadata.url}", configuration = InitCrossServerConfig.class)
public interface DatasourceService {

    @GetMapping("/{id}")
    @ApiOperation("查询数据源")
    RespDTO<DataSourceDTO> get(@PathVariable("id") Long  id, @RequestHeader(PermissionConstants.Header.CROSS_TENANT_TENANTID_KEY) String tenantId);
}
