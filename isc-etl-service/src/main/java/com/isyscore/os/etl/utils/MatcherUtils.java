package com.isyscore.os.etl.utils;

import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.core.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class MatcherUtils {
    private static String REG_1 = "^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\\\/])+$";


    public static boolean isHttpsOrHttp(String url) {
        Pattern p = Pattern.compile(REG_1);
        Matcher m = p.matcher(url.trim());
        return m.matches();
    }

    public static String lastUrlValue(String url) {
        if (StringUtils.isEmpty(url)) {
            return null;
        }
        if (!isHttpsOrHttp(url)) {
            log.error("非法的url :{}", url);
            throw new DataFactoryException(ErrorCode.PARAM_ERROR);
        }
        String[] val = url.trim().split("/");
        return val[val.length - 1];


    }
}
