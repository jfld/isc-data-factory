package com.isyscore.os.etl.manager;

import com.google.common.collect.ImmutableMap;
import com.isyscore.boot.quartz.QuartzJobInfo;
import com.isyscore.boot.quartz.QuartzTemplate;
import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.etl.constant.JobConstant;
import com.isyscore.os.etl.schedule.RunJob;
import com.isyscore.os.permission.entity.LoginVO;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.isyscore.os.core.exception.ErrorCode.*;

@Service
@Slf4j
public class QuartzJobManager {

    @Autowired
    private QuartzTemplate quartzTemplate;

    public void addJob(String jobId, String cron, LoginVO loginVO,Boolean isResume) {
        boolean existed = hasMetricJobTask(jobId);
        if (existed) {
            throw new DataFactoryException(DUPLICATE_METRIC_JOB);
        }
        Map<String, Object> params = ImmutableMap.of(
                "jobId", jobId,
                "isResume", isResume,
                "current", loginVO);
        try {
            quartzTemplate.addJob(RunJob.class, jobId, JobConstant.JOB_GROUP, cron, params, LocalDateTime.now());
        } catch (Exception e) {
            log.error("Quartz异常：", e);
            throw new DataFactoryException(JOB_SCHEDULER_ERROR);
        }
    }

    public void deleteJob(String jobId) {
        boolean existed = hasMetricJobTask(jobId);
        if (existed) {
            try {
                quartzTemplate.deleteJob(JobConstant.JOB_GROUP, String.valueOf(jobId));
            } catch (SchedulerException e) {
                log.error("Quartz异常：", e);
                throw new DataFactoryException(JOB_SCHEDULER_ERROR);
            }
        }
    }

    public void pauseJob(String jobId) {
        boolean existed = hasMetricJobTask(jobId);
        if (existed) {
            try {
                quartzTemplate.pauseJob(JobConstant.JOB_GROUP, String.valueOf(jobId));
            } catch (Exception e) {
                log.error("Quartz异常：", e);
                throw new DataFactoryException(JOB_SCHEDULER_ERROR);
            }
        }
    }

    public boolean hasMetricJobTask(String jobId) {
        boolean existed = false;
        List<QuartzJobInfo> quartzJobInfos = null;
        try {
            quartzJobInfos = quartzTemplate.schedulers();
        } catch (Exception e) {
            log.error("Quartz异常：", e);
            throw new DataFactoryException(JOB_SCHEDULER_ERROR);
        }
        for (QuartzJobInfo job : quartzJobInfos) {
            if (job.getName().equals(String.valueOf(jobId))) {
                existed = true;
                break;
            }
        }
        return existed;
    }

    public void stopAllMetricJob() {
        try {
            quartzTemplate.deleteJobsByGroup(JobConstant.JOB_GROUP);
        } catch (Exception e) {
            log.error("停止定时任务失败", e);
            throw new DataFactoryException(COMMON_ERROR, "停止定时任务失败");
        }
    }

}
