package com.isyscore.os.etl.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DmcUploadRespDTO {

    private LocalDateTime createdAt;
    private String fileType;
    private boolean isForce;
    private String md5;
    private String module;
    private String resId;
    private String resName;
    private String resPath;
    private Long resSize;
    private LocalDateTime updatedAt;
    private boolean useClientName;
    private String userId;
    private String version;
}
