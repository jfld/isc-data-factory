package com.isyscore.os.etl.constant;

public interface ProcessConstant {

    String RUN_WITH_NOW = "NOW";

    String RUN_WITH_CRON = "CRON";
}
