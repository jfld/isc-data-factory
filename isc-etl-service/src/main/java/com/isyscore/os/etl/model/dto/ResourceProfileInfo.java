package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * @author liyc
 * @Title: ResourceProfileInfo
 * @ProjectName isc-data-factory
 * @date 2021/8/85:31 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceProfileInfo implements Serializable {

    private double cpuCores;

    private int taskHeapMemory;

    private int taskOffHeapMemory;

    private int managedMemory;

    private int networkMemory;

    private Map<String, Double> extendedResources;
}
