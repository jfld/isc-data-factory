package com.isyscore.os.etl.model.form;

import lombok.Data;

@Data
public class SQLParam {
    String flinkSql;
}
