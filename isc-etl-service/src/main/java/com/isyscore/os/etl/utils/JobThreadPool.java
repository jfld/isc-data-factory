package com.isyscore.os.etl.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class JobThreadPool {
    private static int corePoolSize = 10;

    private static int maximumPoolSize = 100;

    private static long keepAliveTime = 10;


    private static ThreadPoolExecutor threadPoolExecutor;

    private static JobThreadPool asyncThreadPool;

    private JobThreadPool() {
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(200, true);
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.MINUTES, workQueue);
    }

    public static synchronized JobThreadPool getInstance() {
        if (null == asyncThreadPool) {
            synchronized (JobThreadPool.class) {
                if (null == asyncThreadPool) {
                    asyncThreadPool = new JobThreadPool();
                }
            }
        }
        log.info("JobThreadPool threadPoolExecutor={}", threadPoolExecutor);
        return asyncThreadPool;
    }

    public synchronized ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

}
