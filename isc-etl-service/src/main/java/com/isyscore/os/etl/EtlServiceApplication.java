package com.isyscore.os.etl;

import com.isyscore.os.core.config.CustomBeanNameGenerator;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author felixu
 * @since 2021.05.06
 */
@SpringBootApplication(scanBasePackages = "com.isyscore.os")
@MapperScan(basePackages = {"com.isyscore.os.core.mapper", "com.isyscore.os.etl.mapper"})
@EnableFeignClients(basePackages = {"com.isyscore.os.etl.service"})
public class EtlServiceApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(EtlServiceApplication.class);
        app.setBeanNameGenerator(new CustomBeanNameGenerator());
        app.run(args);
    }
}
