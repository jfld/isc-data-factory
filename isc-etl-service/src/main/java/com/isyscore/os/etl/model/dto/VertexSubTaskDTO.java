package com.isyscore.os.etl.model.dto;


import com.isyscore.os.etl.model.enums.JobIdStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class VertexSubTaskDTO {

    private Integer attempt;

    private Integer duration;

    private Integer endTime;

    private String host;

    private Metrics metrics;

    private Integer startTime;

    private JobIdStatus status;

    private Integer subtask;

    private String taskmanagerId;

}
