package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


/**
 * @author liyc
 * @Title: TaskManagerInfo
 * @ProjectName isc-data-factory
 * @date 2021/8/85:26 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskManagerInfo implements Serializable {

    private List<TaskManager> taskmanagers;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TaskManager {

        private String id;

        private String path;

        private Integer dataPort;

        private Long jmxPort;

        private Integer slotsNumber;

        private Integer freeSlots;

        private Long timeSinceLastHeartbeat;

        private ResourceProfileInfo totalResource;

        private ResourceProfileInfo freeResource;

        private HardwareDescription hardware;

        private TaskExecutorMemoryConfiguration memoryConfiguration;
    }

}
