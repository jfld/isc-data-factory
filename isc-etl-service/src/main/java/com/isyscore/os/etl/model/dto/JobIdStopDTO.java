package com.isyscore.os.etl.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class JobIdStopDTO {

    @Data
    @Builder
    @AllArgsConstructor
    static class RequestId{}
}
