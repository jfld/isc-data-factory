package com.isyscore.os.etl.constant;

/**
 * @author zhangyn
 * @version 1.0
 * @date 2021/6/9 5:24 下午
 */
public interface JobConstant {
    String JOB_GROUP = "ETL_PROCESS";
    Integer IS_CHECKED = 1;
    Integer NO_CHECKED = 0;
    //解决switch case 的问题
    int MYSQL_TYPE = 0;
    int ORACLE_TYPE = 1;
    int CLICKHOUSE_TYPE = 2;
    int SQLSERVER_TYPE = 3;
    int DM_TYPE = 4;
    int TD_TYPE = 5;
    int PG_TYPE = 6;

    int DATA_SYNC_TYPE = 100;
    String TABLE_NAME_TEMPLATE = "%s_%s_%s";
}
