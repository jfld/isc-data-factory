package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class JMConfigDTO {

    private String key;

    private String value;
}
