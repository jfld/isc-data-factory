package com.isyscore.os.etl.model.dto;

import com.isyscore.os.core.model.entity.JobConfig;
import com.isyscore.os.etl.model.enums.DeployModeEnum;
import com.isyscore.os.etl.model.enums.JobConfigStatus;
import com.isyscore.os.etl.model.enums.JobType;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@ApiModel("任务录入和更新的入参")
public class JobConfigDTO {

    private String id;
    /**
     * 任务名称
     */
    private String jobName;

    private DeployModeEnum deployModeEnum;

    /**
     * flink运行配置
     */
    private String flinkRunConfig;


    /**
     * flink运行配置
     */
    private String jobId;

    /**
     * 1:开启 0: 关闭
     */
    private Integer isOpen;

    /**
     * @see JobConfigStatus
     * 1:运行中 0: 停止中 -1:运行失败
     */
    private JobConfigStatus status;


    /**
     * 三方jar udf、 连接器 等jar如http://xxx.xxx.com/flink-streaming-udf.jar
     */
    private String extJarPath;

    /**
     * 最后一次启动时间
     */
    private LocalDateTime lastStartTime;

    private Integer version;
    /**
     * sql语句
     */
    private String flinkSql;


    /**
     * 任务类型
     */
    private JobType jobTypeEnum;

    /**
     * 启动jar可能需要使用的自定义参数
     */
    private String customArgs;

    /**
     * 程序入口类
     */
    private String customMainClass;

    /**
     * 自定义jar的http地址 如:http://ccblog.cn/xx.jar
     */
    private String customJarUrl;


    private Long lastRunLogId;


    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime editTime;

    private String creator;

    private String editor;

    private String tenantId;

    private String cron;

    private Integer isScheduled;

    private Integer isChecked;

    public static JobConfig toEntity(JobConfigDTO jobConfigDTO) {
        if (jobConfigDTO == null) {
            return null;
        }
        JobConfig jobConfig = new JobConfig();
        jobConfig.setId(jobConfigDTO.getId());
        jobConfig.setJobName(jobConfigDTO.getJobName());
        if (jobConfigDTO.getDeployModeEnum() != null) {
            jobConfig.setDeployMode(jobConfigDTO.getDeployModeEnum().name());
        }
        jobConfig.setFlinkRunConfig(jobConfigDTO.getFlinkRunConfig());
        jobConfig.setJobId(jobConfigDTO.getJobId());
        jobConfig.setIsOpen(jobConfigDTO.getIsOpen());
        jobConfig.setStatus(jobConfigDTO.getStatus().getCode());
        jobConfig.setLastStartTime(jobConfigDTO.getLastStartTime());
        jobConfig.setVersion(jobConfigDTO.getVersion());
        jobConfig.setFlinkSql(jobConfigDTO.getFlinkSql());
        jobConfig.setCreator(jobConfigDTO.getCreator());
        jobConfig.setEditor(jobConfigDTO.getEditor());
        jobConfig.setLastRunLogId(jobConfigDTO.getLastRunLogId());
        jobConfig.setExtJarPath(jobConfigDTO.getExtJarPath());
        jobConfig.setTenantId(jobConfigDTO.getTenantId());
        jobConfig.setCron(jobConfigDTO.getCron());

        if (jobConfigDTO.getJobTypeEnum() != null) {
            jobConfig.setJobType(jobConfigDTO.getJobTypeEnum().getCode());
        }
        jobConfig.setCustomArgs(jobConfigDTO.getCustomArgs());
        jobConfig.setCustomMainClass(jobConfigDTO.getCustomMainClass());
        jobConfig.setCustomJarUrl(jobConfigDTO.getCustomJarUrl());
        jobConfig.setIsScheduled(jobConfigDTO.getIsScheduled());
        jobConfig.setIsChecked(jobConfigDTO.getIsChecked());
        return jobConfig;
    }


    public static JobConfigDTO toDTO(JobConfig jobConfig) {
        if (jobConfig == null) {
            return null;
        }
        JobConfigDTO jobConfigDTO = new JobConfigDTO();
        jobConfigDTO.setId(jobConfig.getId());
        jobConfigDTO.setJobName(jobConfig.getJobName());
        jobConfigDTO.setDeployModeEnum(DeployModeEnum.getModel(jobConfig.getDeployMode()));
        jobConfigDTO.setFlinkRunConfig(jobConfig.getFlinkRunConfig());
        jobConfigDTO.setJobId(jobConfig.getJobId());
        jobConfigDTO.setIsOpen(jobConfig.getIsOpen());
        jobConfigDTO.setStatus(JobConfigStatus.getJobConfigStatus(jobConfig.getStatus()));
        jobConfigDTO.setLastStartTime(jobConfig.getLastStartTime());
        jobConfigDTO.setVersion(jobConfig.getVersion());
        jobConfigDTO.setCreateTime(jobConfig.getCreatedAt());
        jobConfigDTO.setEditTime(jobConfig.getUpdatedAt());
        jobConfigDTO.setCreator(jobConfig.getCreator());
        jobConfigDTO.setEditor(jobConfig.getEditor());
        jobConfigDTO.setFlinkSql(jobConfig.getFlinkSql());
        jobConfigDTO.setLastRunLogId(jobConfig.getLastRunLogId());
        jobConfigDTO.setExtJarPath(jobConfig.getExtJarPath());
        jobConfigDTO.setTenantId(jobConfig.getTenantId());
        jobConfigDTO.setCron(jobConfig.getCron());

        jobConfigDTO.setJobTypeEnum(JobType.getJobType(jobConfig.getJobType()));
        jobConfigDTO.setCustomArgs(jobConfig.getCustomArgs());
        jobConfigDTO.setCustomMainClass(jobConfig.getCustomMainClass());
        jobConfigDTO.setCustomJarUrl(jobConfig.getCustomJarUrl());

        jobConfigDTO.setIsScheduled(jobConfig.getIsScheduled());

        jobConfigDTO.setIsChecked(jobConfig.getIsChecked());
        return jobConfigDTO;
    }


    public static List<JobConfigDTO> toListDTO(List<JobConfig> jobConfigList) {
        if (CollectionUtils.isEmpty(jobConfigList)) {
            return Collections.emptyList();
        }

        List<JobConfigDTO> jobConfigDTOList = new ArrayList<JobConfigDTO>();

        for (JobConfig jobConfig : jobConfigList) {
            jobConfigDTOList.add(toDTO(jobConfig));
        }

        return jobConfigDTOList;
    }


    public static String buildRunName(String jobName) {

        return "flink@" + jobName;
    }


    public static JobConfigDTO buildStop(String id) {
        JobConfigDTO jobConfig = new JobConfigDTO();
        jobConfig.setStatus(JobConfigStatus.STOP);
        jobConfig.setEditor("sys_auto");
        jobConfig.setId(id);
        jobConfig.setJobId("");
        return jobConfig;
    }
}
