package com.isyscore.os.etl.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatabasePair {
    @ApiModelProperty("数据库名称")
    private String databaseName;
    @ApiModelProperty(value = "是否上锁", allowableValues = "0(未上锁),1(已上锁)")
    private Integer isLocked;
}
