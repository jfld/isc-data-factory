package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class NodeDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("并行度")
    private Integer parallelism;
    @ApiModelProperty("算子")
    private String operator;
    @JsonAlias(value = {"operator_strategy", "operatorStrategy", "operator-strategy"})
    private String operatorStrategy;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("输入")
    List<InputDTO> inputs;
}
