package com.isyscore.os.etl.model.dto;

import lombok.Data;

/**
 * @author zhangyn
 * @version 1.0
 * @date 2021/6/1 2:11 下午
 */
@Data
public class Entry {
    private String name;
    private String description;
}
