//package com.isyscore.os.etl.schedule;
//
//import com.isyscore.boot.quartz.QuartzTemplate;
//import com.isyscore.os.core.exception.DataFactoryException;
//import com.isyscore.os.core.exception.ErrorCode;
//import com.isyscore.os.core.model.entity.ProcessInstance;
//import com.isyscore.os.etl.constant.JobConstant;
//import com.isyscore.os.etl.model.enums.JobStatus;
//import com.isyscore.os.etl.model.form.ProcessParamsForm;
//import com.isyscore.os.etl.service.FlinkService;
//import com.isyscore.os.etl.service.impl.ProcessDefinitionServiceImpl;
//import com.isyscore.os.etl.service.impl.ProcessInstanceServiceImpl;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.quartz.SchedulerException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.quartz.QuartzJobBean;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.time.LocalDateTime;
//import java.util.Objects;
//
///**
// *
// * @author zhangyn
// * @version 1.0
// * @date 2021/6/9 5:00 下午
// */
//@Slf4j
//public class RunProcessInstanceJob extends QuartzJobBean {
//    @Autowired
//    private ProcessInstanceServiceImpl processInstanceService;
//
//    @Autowired
//    private ProcessDefinitionServiceImpl processDefinitionService;
//
//    @Autowired
//    private FlinkService flinkService;
//
//    @Autowired
//    private QuartzTemplate quartzTemplate;
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
//        String psid = jobExecutionContext.getJobDetail().getKey().getName();
//        log.debug("开始调度:{}",psid);
//        ProcessParamsForm params = (ProcessParamsForm)jobExecutionContext.getJobDetail().getJobDataMap().get("params");
//        ProcessInstance processInstance = processInstanceService.getById(psid);
//        if (!Objects.equals(processInstance.getStatus(), JobStatus.PRE_SCHEDULING.getStatus()) &&
//                !Objects.equals(processInstance.getStatus(), JobStatus.SCHEDULING.getStatus())) {
//            log.error("工作流{}不处于调度中,调度停止", psid);
//            try {
//                quartzTemplate.deleteJob(JobConstant.JOB_GROUP, psid);
//            } catch (SchedulerException e) {
//                log.error("删除调度任务失败");
//                e.printStackTrace();
//            }
//            processInstance.setStatus(JobStatus.FAILED.getStatus());
//            processInstanceService.updateById(processInstance);
//            return;
//        }
//        String jarid = null;
//        try {
//            log.info("开始检查资源是否存在");
//             jarid = processDefinitionService.checkResource(processInstance.getProcessDefinitionId());
//             if (jarid == null) {
//                 throw new DataFactoryException(ErrorCode.FAIL);
//             }
//        }catch (DataFactoryException e) {
//            log.error(e.getMessage());
//            processInstance.setStatus(JobStatus.FAILED.getStatus());
//            processInstanceService.updateById(processInstance);
//            try {
//                quartzTemplate.deleteJob(JobConstant.JOB_GROUP, psid);
//            } catch (SchedulerException e1) {
//                log.error("删除调度任务失败");
//                e1.printStackTrace();
//            }
//        }
//        params.setStrategy(null);
//        params.setCron(null);
//        params.setJobName(null);
//        String jobid = flinkService.run(jarid, params).get("jobid");
//        if (StringUtils.isEmpty(jobid)) {
//            log.error("任务提交flink端失败,调度停止");
//            processInstance.setStatus(JobStatus.FAILED.getStatus());
//            processInstanceService.updateById(processInstance);
//            try {
//                quartzTemplate.deleteJob(JobConstant.JOB_GROUP, psid);
//            } catch (SchedulerException e) {
//                log.error("删除调度任务失败");
//                e.printStackTrace();
//            }
//            return;
//        }
//        log.debug("更新db工作流状态");
//        processInstance.setJobId(jobid);
//        processInstance.setUpdatedAt(LocalDateTime.now());
//        processInstance.setStatus(JobStatus.SCHEDULING.getStatus());
//        processInstanceService.updateById(processInstance);
//        log.debug("此次调度结束");
//    }
//}
