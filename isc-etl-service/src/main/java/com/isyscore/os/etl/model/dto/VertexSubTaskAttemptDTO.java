package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.enums.JobRunStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class VertexSubTaskAttemptDTO {

    private Integer attempt;

    private Integer duration;

    private Integer endTime;

    private String host;

    private Metrics metrics;

    private Integer starTime;

    private JobRunStatus status;

    private Integer subtask;

    private String taskManagerId;


}
