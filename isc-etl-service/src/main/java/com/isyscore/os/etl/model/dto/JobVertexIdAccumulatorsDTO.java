package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class JobVertexIdAccumulatorsDTO {

    private List<UserAccumulators> userAccumulators;


    @Data
    @Builder
    @AllArgsConstructor
    static class UserAccumulators{

        private Items items;
    }


}
