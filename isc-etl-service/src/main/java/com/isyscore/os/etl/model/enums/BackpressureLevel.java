package com.isyscore.os.etl.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BackpressureLevel {

    OK("OK"),

    LOW("LOW"),

    HIGH("HIGH");

    private String level;

}
