package com.isyscore.os.etl.annotate;

import java.lang.annotation.*;

/**
 * @author codezzz
 * @Description:
 * @date 2021/8/18 16:41
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Ping {
    String uri() default "";
    boolean splicing() default false;
}
