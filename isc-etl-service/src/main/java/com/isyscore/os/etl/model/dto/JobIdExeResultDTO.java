package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.enums.JobIdExeResultStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobIdExeResultDTO {

    private Status status;


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Status {

        private String id;
    }
}
