package com.isyscore.os.etl.aspect;

import com.isyscore.os.etl.annotate.Ping;
import com.isyscore.os.etl.utils.FlinkUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author codezzz
 * @Description:
 * @date 2021/8/18 16:45
 */

@Aspect
@Component
public class PingAspect {

    @Value("${flink-config.host}:${flink-config.port}")
    private String URI;

    @Pointcut("@annotation(com.isyscore.os.etl.annotate.Ping)")
    private void serviceAspect(){}

    @Before("serviceAspect()")
    private void pingFlink(JoinPoint joinPoint) {
        Ping ping = ((MethodSignature)joinPoint.getSignature()).getMethod().getAnnotation(Ping.class);
        String uri;
        boolean splicing;
        if (StringUtils.isEmpty(ping.uri())) {
            uri = URI;
            splicing = true;
        } else {
            uri = ping.uri();;
            splicing = ping.splicing();
        }
        FlinkUtil.ping(uri, splicing);
    }

}
