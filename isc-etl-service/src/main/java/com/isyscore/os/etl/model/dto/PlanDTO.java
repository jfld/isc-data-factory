package com.isyscore.os.etl.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PlanDTO {
    private String jid;
    @ApiModelProperty("对应的名称")
    private String name;
    @ApiModelProperty("节点信息")
    private List<NodeDTO> nodes;
}
