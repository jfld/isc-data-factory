package com.isyscore.os.etl.service;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.etl.config.InitCrossServerConfig;
import com.isyscore.os.etl.model.dto.DmcUploadRespDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "${service.dmc.name}", url = "${service.dmc.url}", path = "/api/device/dmc", configuration = InitCrossServerConfig.class)
public interface DmcService {


    @PostMapping(value = "/res/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RespDTO<DmcUploadRespDTO> upload(@RequestPart("file") MultipartFile file, @RequestPart("fileType") String fileType, @RequestPart("module") String module);

    @DeleteMapping("/file/{id}")
    RespDTO<?> delete(@PathVariable("id") String id);

    @DeleteMapping("/res/path")
    RespDTO<Boolean> deleteFileByPath(@RequestParam("path") String path);
}
