package com.isyscore.os.etl.model.dto;
import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 14:49
 */
@Data
public class DataSource {
    @TableId(type= IdType.ASSIGN_ID)
    @ApiModelProperty("主键")
    private Long id;

    private String userId;

    @TableField(condition = SqlCondition.LIKE)
    private String name;

    private String ip;

    private String port;

    private String databaseName;

    private String userName;

    private String password;

    private Integer type;
    //    @TableLogic
    @Deprecated
    private Integer IsDelete;

    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    private Integer isDataWarehouse;

    @ApiModelProperty("上锁信息：{databaseName: \"\",isLocked: 0}")
    private String databaseConfig;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "连接类型", allowableValues = "BASIC,TNS")
    private String connectType;

    @ApiModelProperty(value = "BASIC下的连接类型", allowableValues = "ServiceName,SID")
    private String basicType;

    @ApiModelProperty(value = "ServiceName,SID的值")
    private String basicValue;

}

