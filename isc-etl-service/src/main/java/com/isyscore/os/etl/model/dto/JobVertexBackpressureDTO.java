package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.enums.BackpressureLevel;
import com.isyscore.os.etl.model.enums.JobVertexBackpressureStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class JobVertexBackpressureDTO {

    private BackpressureLevel level;

    private Integer endTimestamp;

    private JobVertexBackpressureStatus status;

    private List<Subtasks> subtasks;


    @Data
    @Builder
    @AllArgsConstructor
    static class Subtasks{

        @Data
        @Builder
        @AllArgsConstructor
        static class Items{

        private BackpressureLevel levels;

        private Number busyRatio;

        private Number idleRatio;

        private Number ratio;

        private Integer subtask;

        }

        @Data
        @Builder
        @AllArgsConstructor
        static class BackpressureLevel{

            private BackpressureLevel level;

        }

    }


}
