package com.isyscore.os.etl.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Metrics {

    private Integer readBytes;

    private Boolean readBytesComplete;

    private Integer readRecords;

    private Boolean readRecordsComplete;

    private Integer writeBytes;

    private Boolean writeBytesComplete;

    private Integer writeRecords;

    private Boolean writeRecordsComplete;

}
