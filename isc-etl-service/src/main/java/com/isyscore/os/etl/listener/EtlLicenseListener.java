package com.isyscore.os.etl.listener;

import com.isyscore.boot.login.LoginUserManager;
import com.isyscore.os.core.license.LicenseListener;
import com.isyscore.os.core.model.enums.LicenseEnable;
import com.isyscore.os.core.model.enums.LicenseOption;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author wuwx
 */
@Component
@Slf4j
public class EtlLicenseListener extends LicenseListener {
    @Resource
    private LoginUserManager loginUserManager;
    @Override
    public void onLicense(List<Map<String, Integer>> data) {
        log.info("收到license变更{}", data.toString());
        for(Map<String,Integer> map: data){
            if(LicenseOption.UDMP_ENABLE.getCode().equals(map.get("key"))&&map.containsKey("value")){
                boolean udmpCheck = Boolean.FALSE;
                Integer udmpEnable=map.get("value");
                if(LicenseEnable.ENABLE.getCode().equals(udmpEnable)){
                    log.info("License授权认证成功:UDMP授权状态:{}",udmpEnable);
                    udmpCheck=true;
                }else {
                    log.error("License授权认证失败:UDMP授权状态:{}",udmpEnable);
                    udmpCheck=false;
                }
                loginUserManager.setAppLicenseStatus(udmpCheck);
            }
        }

    }
}
