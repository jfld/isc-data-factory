package com.isyscore.os.etl.model.dto;

import lombok.Data;

/**
 * @author zhangyn
 * @version 1.0
 * @date 2021/5/27 3:01 下午
 */
@Data
public class PlanDTOWrapper {
    private PlanDTO plan;
}
