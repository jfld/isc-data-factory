package com.isyscore.os.etl.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.isyscore.os.etl.model.Insert;
import com.isyscore.os.etl.model.Update;
import com.isyscore.os.etl.model.flinkviewable.Mapping;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2021/12/21 4:59 下午
 */
@Data
public class ViewableConfig {

    /******* 数据同步配置 *******/
    //insert 分组 -> 数据同步
    private String sourceId;
    private String targetId;
    @NotEmpty(groups = Insert.class, message = "源表名称不能为空")
    @JsonInclude
    private String sourceTb;
    @NotEmpty(groups = Insert.class, message = "目标表名称不能为空")
    @JsonInclude
    private String targetTb;
    @Valid
    private List<Mapping> mappings;

    /****** 数据源(数据库)配置 *******/
    //Update 分组 -> 数据源配置
    @NotEmpty(groups = Update.class, message = "数据源组件名称不能为空")
    @Length(groups = Update.class, min = 1, max = 32, message = "数据源组件名称长度不符合要求")
    private String name;
    private String datasourceId;
    @NotEmpty(groups = Update.class, message = "IP不能为空")
    private String ip;
    @NotEmpty(groups = Update.class, message = "端口不能为空")
    private String port;
    @NotEmpty(groups = Update.class, message = "用户名不能为空")
    private String userName;
    @NotEmpty(groups = Update.class, message = "密码不能为空")
    private String password;
    @NotEmpty(groups = Update.class, message = "数据库名称不能为空")
    private String databaseName;
    private String basicType;
    private String basicValue;

    /****** mysql 数据任务组件 ********/




}
