package com.isyscore.os.etl.model.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 *
 * @author zhangyn
 * @version 1.0
 * @date 2021/5/28 10:37 上午
 */
@ApiModel("工作流实例列表项")
@Data
public class InstanceItem {

}
