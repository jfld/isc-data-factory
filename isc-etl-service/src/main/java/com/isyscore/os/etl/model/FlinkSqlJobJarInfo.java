package com.isyscore.os.etl.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author wany
 */
@Data
public class FlinkSqlJobJarInfo {

    private String jarVersion;

    private String flinkJarId;

    private String flinkAddress;

    private LocalDateTime uploadTime;

}
