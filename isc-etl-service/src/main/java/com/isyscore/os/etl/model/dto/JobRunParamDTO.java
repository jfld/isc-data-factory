package com.isyscore.os.etl.model.dto;

import com.isyscore.os.core.model.entity.JobConfig;
import lombok.Data;

@Data
public class JobRunParamDTO {

    /**
     * flink bin目录地址
     */
    private String flinkBinPath;

    /**
     * flink 运行参数 如：-yjm 1024m -ytm 2048m -yd -m yarn-cluster
     */
    private String flinkRunParam;

    /**
     * sql语句存放的目录
     */
    private String sqlPath;



    /**
     * flink-streaming-platform-web 所在目录 如：/use/local/flink-streaming-platform-web
     */
    private String sysHome;

    /**
     * 主类jar地址
     */
    private String mainJarPath;


    public JobRunParamDTO(String flinkBinPath,
                          String flinkRunParam,
                          String sqlPath,
                          String sysHome) {
        this.flinkBinPath = flinkBinPath;
        this.flinkRunParam = flinkRunParam;
        this.sqlPath = sqlPath;
        this.sysHome = sysHome;
    }

    public static JobRunParamDTO buildJobRunParam(JobConfig jobConfig, String sqlPath, String clientHome) {

        String flinkBinPath = clientHome + "/bin/flink";

        String flinkRunParam = jobConfig.getFlinkRunConfig();

        return new JobRunParamDTO(
                flinkBinPath,
                flinkRunParam,
                sqlPath,
                clientHome
        );
    }
}
