package com.isyscore.os.etl.constant;

import java.util.HashMap;
import java.util.Map;

public interface CommonConstant {
    String UPLOAD_STATUS = "success";

    String MODULE_NAME = "DATA_FACTORY";

    String OTHER_FILE_TYPE = "OTHER";

    Integer DMC_HTTP_SUCCESS = 0;

    String CONFIG_DMC_FILE_PATH = "dmcConfig.filePath";

    String CONFIG_DMC_FILE_NAME = "dmcConfig.fileName";

    String CONFIG_JAR_ID = "jarid";

    Integer FALSE = 0;

    Integer TRUE = 1;

    /**
     * 是否进行了初始化内置连接器
     */
    String INITIALIZED = "INITIALIZED";

    String INNER_CONNECTOR = "INNER_CONNECTOR";

    String LOCAL = "LOCAL";

    String STANDALONE = "STANDALONE";
    String DEPLOY_MODE_STANDALONE = "standalone";

    String APP_CODE = "udmp";

    Map<String, String> CONNECTOR_MAPPING = new HashMap(){{
        put("kafka-clients-2.4.1.jar", "Kafka 客户端依赖");
        put("flinkx-core-1.12-SNAPSHOT.jar",  "FLINK 通用依赖");
        put("flinkx-connector-sqlserver-1.12-SNAPSHOT.jar", "SQLserver 连接器");
        put("flinkx-connector-postgresql-1.12-SNAPSHOT.jar", "Postgres 连接器");
        put("flinkx-connector-oracle-1.12-SNAPSHOT.jar", "Oracle 连接器");
        put("flinkx-connector-mysql-1.12-SNAPSHOT.jar", "MySQL 连接器");
        put("flinkx-connector-dm-1.12-SNAPSHOT.jar", "达梦数据库连接器");
        put("flinkx-connector-clickhouse-1.12-SNAPSHOT.jar", "click house 连接器");
        put("flink-connector-postgres-cdc-1.3.0.jar", "Postgres CDC 依赖");
        put("flink-connector-mysql-cdc-1.1.1.jar", "MySQL CDC 依赖");
        put("flink-connector-kafka_2.11-1.12.2.jar", "Kafka 连接器");
    }};

}
