package com.isyscore.os.etl.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobIdStatus {

    CREATED("CREATED"),

    SCHEDULED("SCHEDULED"),

    DEPLOYING("DEPLOYING"),

    RUNNING("RUNNING"),

    FINISHED("FINISHED"),

    CANCELING("CANCELING"),

    CANCELED("CANCELED"),

    FAILED("FAILED"),

    RECONCILING("RECONCILING"),

    INITIALIZING("INITIALIZING");

    private String status;


}
