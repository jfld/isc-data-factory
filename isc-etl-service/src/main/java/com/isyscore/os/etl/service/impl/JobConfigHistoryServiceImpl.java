package com.isyscore.os.etl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.isyscore.os.core.mapper.JobConfigHistoryMapper;
import com.isyscore.os.core.model.entity.JobConfigHistory;
import com.isyscore.os.etl.model.dto.JobConfigHistoryDTO;
import com.isyscore.os.etl.service.JobConfigHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JobConfigHistoryServiceImpl extends ServiceImpl<JobConfigHistoryMapper, JobConfigHistory>
        implements JobConfigHistoryService {
    @Override
    public void insertJobConfigHistory(JobConfigHistoryDTO jobConfigHistoryDTO) {
        this.save(JobConfigHistoryDTO.toEntity(jobConfigHistoryDTO));
    }
}
