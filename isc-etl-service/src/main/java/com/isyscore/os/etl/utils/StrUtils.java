package com.isyscore.os.etl.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;

import java.util.List;
/**
 *  字符工具类
 */
public class StrUtils {
    private static   String BASE_PATTERN_TEMPLATE="(?<=[-]{1}{}\\s{1})\\S*";

    /**
     * 通过正则表达式获取第一个满足的文本
     * @param patternText 正则表达式内容
     * @param checkText 原有文本
     * @return 满足正则的第一个文本
     */
    public static String getParamByPattern(String patternText, String checkText){
        List<String> resultFindAll = ReUtil.findAll(patternText,checkText,0);
        if(CollectionUtil.isNotEmpty(resultFindAll)){
            return resultFindAll.get(0);
        }
        return null;
    }

    /**
     * 构建正则表达式 如传入 sql 则是获取 ’-s test -q b’ 参数字符串中参数s的值的正则表达式
     * @param flag 参数名
     * @return 参数值的正则表达式
     */
    public static String getPatternByFlag(String flag){
        return StrUtil.format(BASE_PATTERN_TEMPLATE,flag);
    }
}
