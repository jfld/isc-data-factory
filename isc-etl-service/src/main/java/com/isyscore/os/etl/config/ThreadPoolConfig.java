package com.isyscore.os.etl.config;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author codezzz
 * @version 0.0.1
 * @Description:
 * @date 2021/10/21 17:22
 */

@Configuration
public class ThreadPoolConfig {

    @Bean
    public ThreadPoolExecutor jobExecutor() {
        return new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors() * 3,
                Runtime.getRuntime().availableProcessors() * 3 + 1, 3L, TimeUnit.MINUTES, new ArrayBlockingQueue<>(50),
                ThreadFactoryBuilder.create().setNamePrefix("job-executor-")
                        .setDaemon(true)
                        .build(), new ThreadPoolExecutor.DiscardPolicy());
    }

    @Bean
    public ThreadPoolExecutor statusExecutor() {
        return new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors() * 3,
                Runtime.getRuntime().availableProcessors() * 3 + 1, 3L, TimeUnit.MINUTES, new ArrayBlockingQueue<>(1),
                ThreadFactoryBuilder.create().setNamePrefix("status-synchronize-")
                        .setDaemon(true)
                        .build(), new ThreadPoolExecutor.DiscardPolicy());
    }

}
