package com.isyscore.os.etl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.core.entity.Resource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *  服务类
 *
 * @author felixu
 * @since 2021-08-03
 */
public interface ResourceService extends IService<Resource> {

    Resource getResourceByIdAndCheck(String id);

    Resource createResource(Resource resource);

    Resource updateResource(Resource resource);

    Resource deleteResource(String id);

    IPage<Resource> list(Resource resource, PageRequest page);
}