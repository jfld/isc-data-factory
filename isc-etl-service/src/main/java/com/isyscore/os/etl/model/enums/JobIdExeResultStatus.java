package com.isyscore.os.etl.model.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobIdExeResultStatus {

    IN_PROGRESS("IN_PROGRESS"),
    COMPLETED("COMPLETED");

    private String status;

}
