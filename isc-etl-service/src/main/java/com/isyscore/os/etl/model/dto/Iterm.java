package com.isyscore.os.etl.model.dto;

import com.isyscore.os.etl.model.enums.JobIdStatus;

import java.util.Map;

/**
 *
 * @author zhangyn
 * @version 1.0
 * @date 2021/5/26 2:14 下午
 */
public class Iterm {
    private Integer duration;

    private Integer endTime;

    private String id;

    private Integer maxParallelism;

    private String name;

    private Integer parallelism;

    private Integer startTime;

    private JobIdStatus jobIdStatus;

    /*
     "CREATED": 0,
    "RECONCILING": 0,
    "FINISHED": 6,
    "RUNNING": 0,
    "DEPLOYING": 0,
    "CANCELED": 0,
    "SCHEDULED": 0,
    "CANCELING": 0,
    "FAILED": 0
     */
    private Map<String, Integer> tasks;
}
