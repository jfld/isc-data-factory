package com.isyscore.os.etl.model;

import com.isyscore.os.etl.model.dto.Entry;
import lombok.Data;

import java.util.List;


@Data
public class Files {
    private String id;
    private String name;
    private long uploaded;
    private List<Entry> entry;
}
