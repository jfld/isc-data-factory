package com.isyscore.os.etl.model.form;


import com.isyscore.os.etl.model.dto.JobConfigDTO;
import com.isyscore.os.etl.model.enums.DeployModeEnum;
import com.isyscore.os.etl.model.enums.JobConfigStatus;
import com.isyscore.os.etl.model.enums.JobType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Objects;


@Data
@ApiModel
public class UpsertJobConfigForm {

    @ApiModelProperty("任务id")
    private String id;

    @NotNull(message = "缺失必要参数：jobName")
    @Length(max = 50, message = "任务名称长度超出限制")
    @ApiModelProperty("任务名称")
    private String jobName;

    @NotNull(message = "缺失必要参数：deployMode")
    @ApiModelProperty("运行模式，LOCAL或STANDALONE")
    private String deployMode;

    @ApiModelProperty("任务类型，0:sql流 1:自定义jar 2:sql批")
    @NotNull(message = "任务类型不能为空")
    private Integer jobType;

    @ApiModelProperty("flink运行配置，deployMode为STANDALONE时必需")
    private String flinkRunConfig;


    @ApiModelProperty("sql语句，jobType为0或2时必需")
    private String flinkSql;

    @ApiModelProperty("第三方udf、连接器，仅支持http协议")
    private String extJarPath;

    @ApiModelProperty("启动jar可能需要使用的自定义参数")
    private String customArgs;

    @ApiModelProperty("程序入口类，jobType为1时必需")
    private String customMainClass;

    @ApiModelProperty("自定义jar的http地址，jobType为1时必需")
    private String customJarUrl;

    @ApiModelProperty("任务编辑是否开启，1:开启 0: 关闭")
    private Integer isOpen = 0;

    @ApiModelProperty("任务状态，-1：失败 0：未运行 1：运行中 2：启动中 3：提交成功 4：已取消 5：已完成 6：重启中 -2：未知")
    private Integer status = JobConfigStatus.STOP.getCode();

    @NotNull
    @ApiModelProperty("是否开启定时调度0：不开启  1：开启")
    private Integer isScheduled = 0;

    @ApiModelProperty("调度时间")
    private String cron = "";


    public static JobConfigDTO toDTO(UpsertJobConfigForm upsertJobConfigParam) {
        if (upsertJobConfigParam == null) {
            return null;
        }
        JobConfigDTO jobConfigDTO = new JobConfigDTO();
        jobConfigDTO.setId(upsertJobConfigParam.getId());
        jobConfigDTO.setDeployModeEnum(DeployModeEnum.getModel(upsertJobConfigParam.getDeployMode()));
        jobConfigDTO.setJobName(upsertJobConfigParam.getJobName());

        if (!Objects.isNull(upsertJobConfigParam.getIsScheduled())) {
            jobConfigDTO.setIsScheduled(upsertJobConfigParam.getIsScheduled());
            jobConfigDTO.setCron(upsertJobConfigParam.getCron());
        }

        jobConfigDTO.setIsScheduled(upsertJobConfigParam.getIsScheduled());

        if (StringUtils.isNotEmpty(upsertJobConfigParam.getFlinkRunConfig())) {
            jobConfigDTO.setFlinkRunConfig(upsertJobConfigParam.getFlinkRunConfig());
        } else {
            jobConfigDTO.setFlinkRunConfig("");
        }


        if (StringUtils.isNotEmpty(upsertJobConfigParam.getFlinkSql())
                && (JobType.SQL_STREAMING.getCode() == upsertJobConfigParam.getJobType()
                || JobType.SQL_BATCH.getCode() == upsertJobConfigParam.getJobType())) {
            jobConfigDTO.setFlinkSql(upsertJobConfigParam.getFlinkSql());
        } else {
            jobConfigDTO.setFlinkSql("");
        }

        jobConfigDTO.setJobTypeEnum(JobType.getJobType(upsertJobConfigParam.getJobType()));
        jobConfigDTO.setCustomArgs(upsertJobConfigParam.getCustomArgs());
        jobConfigDTO.setCustomMainClass(upsertJobConfigParam.getCustomMainClass());
        jobConfigDTO.setCustomJarUrl(upsertJobConfigParam.getCustomJarUrl());

        jobConfigDTO.setIsOpen(upsertJobConfigParam.getIsOpen());
        jobConfigDTO.setStatus(JobConfigStatus.getJobConfigStatus(upsertJobConfigParam.getStatus()));
        if (StringUtils.isNotEmpty(upsertJobConfigParam.getExtJarPath())) {
            jobConfigDTO.setExtJarPath(upsertJobConfigParam.getExtJarPath().trim());
        }else{
            jobConfigDTO.setExtJarPath("");
        }

        return jobConfigDTO;
    }
}
