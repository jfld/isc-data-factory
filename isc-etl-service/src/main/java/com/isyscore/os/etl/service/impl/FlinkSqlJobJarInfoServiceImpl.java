package com.isyscore.os.etl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.isyscore.os.etl.mapper.FlinkSqlJobJarInfoMapper;
import com.isyscore.os.etl.model.FlinkSqlJobJarInfo;
import com.isyscore.os.etl.service.FlinkSqlJobJarInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FlinkSqlJobJarInfoServiceImpl extends ServiceImpl<FlinkSqlJobJarInfoMapper, FlinkSqlJobJarInfo> implements FlinkSqlJobJarInfoService {
}
