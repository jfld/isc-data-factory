package com.isyscore.os.etl.model.dto;


import com.isyscore.os.etl.model.enums.JobIdStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class JobVertexIdDTO {

    private String id;

    private Integer maxParallelism;

    private String name;

    private Integer now;

    private Integer parallelism;

    private List<Subtasks> subtasks;


    @Data
    @Builder
    @AllArgsConstructor
    static class Subtasks{

        @Data
        @Builder
        @AllArgsConstructor
        static class Items{

            private Integer attempt;

            private Integer duration;

            private Integer endTime;

            private String host;

            private Metrics metrics ;

            private Integer startTime;

            private JobIdStatus status;

            private Integer subtask;

            private String taskManagerId;


        }

        @Data
        @Builder
        @AllArgsConstructor
        static class Metrics{

            private Integer readBytes;

            private Boolean readBytesComplete;

            private Integer readRecords;

            private Boolean readRecordsComplete;

            private Integer writeBytes;

            private Boolean writeBytesComplete;

            private Integer writeRecords;

            private Boolean writeRecordsComplete;

        }


    }


}
