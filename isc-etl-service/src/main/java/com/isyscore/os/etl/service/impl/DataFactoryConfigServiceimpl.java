package com.isyscore.os.etl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.isyscore.os.core.mapper.DataFactoryConfigMapper;
import com.isyscore.os.core.model.entity.DataFactoryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2022/2/17 4:39 下午
 */
@Service
@Slf4j
public class DataFactoryConfigServiceimpl extends ServiceImpl<DataFactoryConfigMapper, DataFactoryConfig> {

}
