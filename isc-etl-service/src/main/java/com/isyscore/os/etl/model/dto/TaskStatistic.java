package com.isyscore.os.etl.model.dto;

import lombok.Data;

/**
 * 任务统计
 *
 * @author zhangyn
 * @version 1.0
 * @date 2021/5/26 11:56 上午
 */
@Data
public class TaskStatistic {
    private Long total;
    private Long created;
    private Long scheduled;
    private Long deploying;
    private Long running;
    private Long finished;
    private Long canceling;
    private Long canceled;
    private Long failed;
    private Long reconciling;

}
