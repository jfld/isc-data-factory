package com.isyscore.os.etl.model.dto;

import com.zaxxer.nuprocess.NuProcess;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author felixu
 * @since 2021.08.12
 */
@Data
@AllArgsConstructor
public class NuProcessWrapperDTO {

    private NuProcess process;

    private String absolutePath;
}
