package com.isyscore.os.etl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.model.entity.JobConfig;
import com.isyscore.os.core.model.entity.JobRunLog;
import com.isyscore.os.etl.model.dto.JobConfigDTO;
import com.isyscore.os.etl.model.dto.JobRunLogDTO;

public interface JobRunLogService extends IService<JobRunLog> {
    String insertJobRunLog(JobRunLogDTO jobRunLogDTO);

    RespDTO updateJobRunLogById(JobRunLogDTO jobRunLogDTO);

    void updateLogById(String localLog, String id);

    void deleteLogByConfigId(String configId);
}
