package com.isyscore.os.etl.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class ProcessParamsForm {

//    @ApiModelProperty("工作流定义的ID")
//    private String ProcessDefinitionId;

    @NotNull(message = "请输入入口类的信息")
    @ApiModelProperty("入口类")
    private String entryClass;

    @ApiModelProperty("jobId")
    private String jobId;

    @ApiModelProperty("并行数")
    private Integer parallelism;

    @ApiModelProperty("启动参数")
    private  String programArgs;

    @ApiModelProperty("是否允许不进行状态还原")
    private boolean allowNonRestoredState;

    @ApiModelProperty("提交job的名字")
    @Length(max = 40, message = "名字长度超出限制")
    private String jobName;

    @NotNull(message = "请输入任务执行策略")
    @ApiModelProperty("任务执行策略 NOW/CRON")
    private String strategy;

    @ApiModelProperty("cron 表达式")
    private String cron;
}
