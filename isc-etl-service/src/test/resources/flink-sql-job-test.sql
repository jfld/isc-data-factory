CREATE TABLE sink_table_2 (
    id STRING,
    name STRING,
    age STRING,
    PRIMARY KEY (id) NOT ENFORCED
) WITH (
    'connector' = 'print'
);

INSERT INTO sink_table_2 VALUES ('1', 'name1', 'age1'),('2', 'name2', 'age2'),('3', 'name3', 'age3');