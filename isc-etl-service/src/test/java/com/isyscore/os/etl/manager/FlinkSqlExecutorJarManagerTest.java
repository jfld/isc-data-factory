package com.isyscore.os.etl.manager;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.StrUtil;
import com.isyscore.os.etl.EtlServiceApplication;
import com.isyscore.os.etl.service.JobConfigService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collections;

@SpringBootTest(classes = EtlServiceApplication.class)
@ActiveProfiles("local")
public class FlinkSqlExecutorJarManagerTest {

    @Autowired
    private FlinkSqlExecutorJarManager flinkSqlExecutorJarManager;
    @Autowired
    private JobConfigService jobConfigService;

    @Test
    public void testJobSubmit() {
        //List<String> classpaths = Lists.newArrayList();
        //classpaths.add("http://10.30.30.26:38080/api/device/dmc/file/custom/data-factory/other/0610810ae87d404584e79c4a9aafdbc4.jar");
        //classpaths.add("http://10.30.30.26:38080/api/device/dmc/file/custom/data-factory/other/4dfe08ccd4e046bd86165b2dd2f1d159.jar");
        String testSql = ResourceUtil.readUtf8Str("flink-sql-job-test.sql");
        String jobId = flinkSqlExecutorJarManager.submitSqlJob(testSql, Collections.emptyList(), null,1,new StringBuilder(),null);
        Assertions.assertNotNull(jobId);
    }

    @Test
    public void testJobSubmitWithJar(){
        String ss="http://10.30.30.26:38080/api/device/dmc/file/custom/data-factory/other/0610810ae87d404584e79c4a9aafdbc4.jar,http://10.30.30.26:38080/api/device/dmc/file/custom/data-factory/other/0610810ae87d404584e79c4a9aafdbc4.jar";
        String test2Sql = ResourceUtil.readUtf8Str("mysql-to-mysql.sql");
        String jobId = flinkSqlExecutorJarManager.submitSqlJob(test2Sql, StrUtil.split(ss,StrPool.COMMA), null,1,new StringBuilder(),null);
        Assertions.assertNotNull(jobId);
    }

}
