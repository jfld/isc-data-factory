package com.isyscore.os.core.model.enums;

/**
 * 可用
 * @author wuwx
 */
public enum LicenseEnable {
    /**
     *
     */
    NOT_SUPPORT(-1, "授权参数不支持"),
    ENABLE(0, "授权可用"),
    UNABLE(1, "授权不可用");
    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public void setDesc(String desc) {
        this.desc = desc;
    }

    LicenseEnable(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
