package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@TableName("job_config_history")
public class JobConfigHistory {

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "job_config主表Id")
    @NotNull
    private String jobConfigId;

    @ApiModelProperty(value = "任务名称")
    @NotBlank
    private String jobName;

    @ApiModelProperty(value = "提交模式: standalone 、yarn 、yarn-session ")
    @NotBlank
    private String deployMode;

    @ApiModelProperty(value = "flink运行配置")
    @NotBlank
    private String flinkRunConfig;

    @ApiModelProperty(value = "sql语句")
    @NotBlank
    private String flinkSql;


    @ApiModelProperty(value = "udf地址及连接器jar")
    private String extJarPath;

    @ApiModelProperty(value = "版本号")
    @NotNull
    private Integer version;

    @NotNull
    private Boolean isDeleted;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updatedAt;

    private String creator;

    private String editor;

    @ApiModelProperty(value = "租户Id")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private String tenantId;

    @ApiModelProperty("调度时间")
    private String schedule;
}
