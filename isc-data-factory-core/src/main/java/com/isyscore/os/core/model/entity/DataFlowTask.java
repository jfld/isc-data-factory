package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("data_flow_task")
public class DataFlowTask implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("唯一编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;


    @ApiModelProperty("批次ID")
    private String kettleBatchId;

    @ApiModelProperty("定义ID")
    private Long definitionId;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("触发方式:1：手动触发，2：自动触发 ")
    private Integer triggerMethod;

    @ApiModelProperty("任务开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty("任务结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
