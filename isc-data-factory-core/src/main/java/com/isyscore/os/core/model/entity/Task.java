package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * (task)实体类
 *
 * @author zhangyn
 * @since 2021-05-17 11:14:29
 * @description 
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("task")
@Builder
@AllArgsConstructor
public class Task extends Model<Task> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @ApiModelProperty("任务ID")
    @TableId(type = IdType.ASSIGN_UUID)
	private String id;
    /**
     * task name
     */
    @ApiModelProperty("任务名称")
    private String name;
    /**
     * task type
     */
    @ApiModelProperty("任务节点类型")
    private String taskType;
    /**
     * process definition id
     */
    @ApiModelProperty("对应的工作流实例的ID")
    private String processInstanceId;
    /**
     * task content json
     */
    @ApiModelProperty("任务节点的内容信息")
    private String taskJson;
    /**
     * 状态信息
     */
    @ApiModelProperty("状态信息")
    private Integer state;
    /**
     * task start time
     */
    @ApiModelProperty("创建信息")
    private LocalDateTime createdAt;
    /**
     * task end time
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updatedAt;
    /**
     * 任务的上游节点
     */
    @ApiModelProperty("上有任务节点的ID")
    private String pids;
    /**
     * 任务的下游节点
     */
    @ApiModelProperty("下游节点的ID")
    private String cids;
    /**
     * 任务的配置信息
     */
    @ApiModelProperty("任务节点的配置信息")
    private String configJson;
    /**
     * task所需要的资源位置
     */
    @ApiModelProperty("任务节点执行所需要的资源路径")
    private String path;

}