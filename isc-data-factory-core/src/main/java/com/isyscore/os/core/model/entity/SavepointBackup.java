package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@TableName("savepoint_backup")
public class SavepointBackup {

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    @ApiModelProperty("savepoint id")
    private String id;

    @ApiModelProperty("任务配置id")
    private String jobConfigId;

    @ApiModelProperty("savepoint保存路径")
    private String savepointPath;

    private Integer isDeleted;

    private Date backupTime;

    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "修改时间")
    private Date updatedAt;

    private String creator;

    private String editor;

    @ApiModelProperty("租户Id")
    private String tenantId;

    private static final long serialVersionUID = 1L;
}
