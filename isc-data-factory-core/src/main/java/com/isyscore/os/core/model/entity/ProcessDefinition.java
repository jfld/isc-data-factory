package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * (process_definition)实体类
 *
 * @author zhangyn
 * @since 2021-05-17 11:14:29
 * @description 
 */
@Data
@NoArgsConstructor
@TableName("process_definition")
public class ProcessDefinition extends Model<ProcessDefinition> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @ApiModelProperty("工作流定义的ID")
    @TableId(type = IdType.ASSIGN_UUID)
	private String id;
    /**
     * process definition name
     */
    @ApiModelProperty("工作流定义的name")
    @TableField(condition = SqlCondition.LIKE)
    private String name;
    /**
     * process definition version
     */
    @ApiModelProperty("版本号（目前用不到）")
    private String version;
    /**
     * tenant id
     */
    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;
    /**
     * process definition json content
     */
    @ApiModelProperty("工作流的信息（预留字段）")
    private String processJson;
    /**
     * description
     */
    @ApiModelProperty("描述")
    private String description;
    /**
     * 前端回显字段
     */
    @ApiModelProperty("现场信息")
    private String locations;
    /**
     * Node connection information
     */
    @ApiModelProperty("节点之间的连接信息（目前单节点模式）")
    private String connects;
    /**
     * create time
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT_UPDATE )
    private LocalDateTime createdAt;
    /**
     * update time
     */
    @ApiModelProperty("修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE )
    private LocalDateTime updatedAt;
    /**
     * modify user
     */
    @ApiModelProperty("最后修改人")
    private String modifyBy;
    /**
     * config
     */
    @ApiModelProperty("工作流配置信息")
    private String config;

}