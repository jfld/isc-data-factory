package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.DataFlowTask;

public interface DataFlowTaskMapper extends BaseMapper<DataFlowTask> {
}
