package com.isyscore.os.core.sqlcore;

public interface Builder {

    Builder createTable(String tableName);

    Builder addCol(String colName, String colType, String comment);

    Builder insert(String table);

    Builder addValue(String col, Object val);

    String build();
}
