package com.isyscore.os.core.logger;

import com.isyscore.boot.logger.model.AccessLoggerInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author felixu
 * @since 2021.11.16
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OperationLogger extends AccessLoggerInfo {

    /**
     * 目标方法
     */
    private String strMethod;

    /**
     * 目标类
     */
    private String strTarget;

    /**
     * 请求时间
     */
    private LocalDateTime requestTime;

    /**
     * 请求耗时
     */
    private Long duration;

    /**
     * 异常信息
     */
    private String errorMsg;

    /**
     * 请求 ip
     */
    private String ip;

    /**
     * 请求路径
     */
    private String url;

    /**
     * 请求头信息
     */
    private Map<String, String> httpHeaders;

    /**
     * 请求方式
     */
    private String httpMethod;
}
