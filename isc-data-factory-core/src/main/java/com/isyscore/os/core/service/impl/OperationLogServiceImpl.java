package com.isyscore.os.core.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.isyscore.os.core.mapper.OperationLogMapper;
import com.isyscore.os.core.model.entity.OperationLog;
import com.isyscore.os.core.service.OperationLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@DS("#{T(com.isyscore.os.core.logger.DatasourceHolder).getDs(T(com.isyscore.os.core.util.ApplicationUtils).getProperty('spring.application.name'))}")
@RequiredArgsConstructor
public class OperationLogServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements OperationLogService {

}
