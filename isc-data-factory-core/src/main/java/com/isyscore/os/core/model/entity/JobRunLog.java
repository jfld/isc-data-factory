package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@TableName("job_run_log")
public class JobRunLog {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("任务日志id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty("任务配置id")
    private String jobConfigId;

    @ApiModelProperty("任务名称")
    @TableField(condition = SqlCondition.LIKE)
    private String jobName;

    @ApiModelProperty("运行模式")
    private String deployMode;

    @ApiModelProperty("flink端任务id")
    private String jobId;

    @ApiModelProperty("远程查看日志url")
    private String remoteLogUrl;

    @ApiModelProperty("启动时间")
    private LocalDateTime startTime;

    @ApiModelProperty("停止时间")
    private LocalDateTime endTime;

    @ApiModelProperty("任务状态")
    private String jobStatus;

    private Integer isDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updatedAt;

    private String creator;

    private String editor;

    @ApiModelProperty("日志详情")
    private String localLog;

    private String runIp;

    @ApiModelProperty("租户Id")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private String tenantId;
}
