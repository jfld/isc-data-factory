package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.JobConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobConfigMapper extends BaseMapper<JobConfig> {
}
