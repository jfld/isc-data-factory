package com.isyscore.os.core.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.isyscore.device.common.model.BaseEntity;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

/**
 *
 *
 * @author felixu
 * @since 2021-08-03
 */
@Data
@TableName("etl_resource")
@ApiModel(description = "资源表")
public class Resource implements BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键 id")
    @NotNull(groups = ValidateUpdate.class)
    @Null(groups = ValidateCreate.class)
    private String id;

    @ApiModelProperty(value = "资源名称")
    @NotBlank
    @Length(min = 1, max = 32)
    @TableField(condition = SqlCondition.LIKE)
    @Pattern(regexp = "[\u4e00-\u9fa5A-Za-z0-9_#@().-]{1,32}", message = "支持中文、英文字母、数字、和特殊字符._-@()#，长度限制1~32")
    private String name;

    @ApiModelProperty(value = "文件名")
    @NotBlank
    private String fileName;

    @ApiModelProperty(value = "文件路径")
    @NotBlank
    private String url;

    @ApiModelProperty(value = "资源描述")
    @Length(max = 64)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String remark;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;


    @ApiModelProperty(value = "连接类型")
    private Integer connectType;
}
