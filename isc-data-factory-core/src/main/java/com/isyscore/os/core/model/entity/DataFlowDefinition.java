package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("data_flow_definition")
public class DataFlowDefinition  implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("唯一编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("以JSON格式保存原始的用户配置信息")
    private String userConfig;

    @ApiModelProperty("根据用户配置信息解析后的kettle的xml配置文件")
    private String kettleDefinition;

    @ApiModelProperty("是否定时调度，0否1是")
    private Integer scheduled;

    @ApiModelProperty("是否重复运行，0否1是")
    private Integer repeatRun;

    @ApiModelProperty("是否重试运行，0否1是")
    private Integer retryRun;


    @ApiModelProperty("定时调度的开始时间")
//    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private LocalDateTime scheduleStartTime;

    @ApiModelProperty("定时调度的时间间隔单位，包括：year,month,day,hour,minute")
//    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String scheduleIntervalTimeUnit;

    @ApiModelProperty("定时调度的时间间隔")
//    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer scheduleInterval;

    @ApiModelProperty("租户ID")
    private String tenantId;

    @ApiModelProperty("创建者")
    private String creator;

    @ApiModelProperty("修改者")
    private String updator;

    @ApiModelProperty("创建时间")
//    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @ApiModelProperty("修改时间")
//    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;


    @ApiModelProperty("是否为增量任务YES/NO")
    private String isIncre;
    @ApiModelProperty("增量表")
    private String incrTable;
    @ApiModelProperty("增量字段")
    private String increField;
    @ApiModelProperty("当前增量值")
    private String increValue;
    @ApiModelProperty("增量字段类型")
    private String increFieldType;
    @ApiModelProperty("分组ID")
    private String groupId;
    @ApiModelProperty("任务描述")
    private String description;
    @ApiModelProperty("发布状态 0 已保存，1 已发布")
    private Integer publishStatus;

    @ApiModelProperty("日志大小")
    private Integer logSize;
}
