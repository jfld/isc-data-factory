package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.isyscore.os.core.model.entity.DataFactoryConfig;
import org.apache.ibatis.annotations.Param;

/**
 * 微服务配置表 Mapper 接口
 *
 * @author starzyn
 * @since 2022-02-17
 */
public interface DataFactoryConfigMapper extends BaseMapper<DataFactoryConfig> {
}
