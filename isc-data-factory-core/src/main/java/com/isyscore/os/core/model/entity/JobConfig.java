package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@TableName("job_config")
public class JobConfig {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("任务配置id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty("任务名称")
    @TableField(condition = SqlCondition.LIKE)
    @NotNull(message = "缺失必要参数：jobName")
    @Length(max = 50, message = "任务名称长度超出限制")
    private String jobName;

    @ApiModelProperty("运行模式LOCAL或STANDALONE")
//    @NotNull(message = "缺失必要参数：deployMode")
    private String deployMode;

    @ApiModelProperty("flink运行配置")
    private String flinkRunConfig;

    @ApiModelProperty("flink端任务id")
    private String jobId;

    @ApiModelProperty("是否开启编辑，1开启0关闭")
    private Integer isOpen;

    @ApiModelProperty("任务状态，-1：失败 0：未运行 1：运行中 2：启动中 3：已完成 4：已取消")
//    @TableField(typeHandler = EnumOrdinalTypeHandler.class)
    private Integer status;

    @ApiModelProperty("第三方udf、连接器，仅支持http协议")
    private String extJarPath;

    @ApiModelProperty("最后一次启动时间")
    private LocalDateTime lastStartTime;

    @ApiModelProperty("最后一次运行时的日志id")
    private Long lastRunLogId;

    @ApiModelProperty("版本号")
    private Integer version;

    @ApiModelProperty("任务类型，0：sql流 1：jar 2：sql批 3: view  4: row sql")
    @NotNull(message = "任务类型不能为空")
    private Integer jobType;

    @ApiModelProperty("启动jar可能需要使用的自定义参数")
    private String customArgs;

    @ApiModelProperty("程序入口类")
    private String customMainClass;

    @ApiModelProperty("自定义jar的http地址")
    private String customJarUrl;

    @ApiModelProperty("是否删除")
    private Integer isDeleted;

    @ApiModelProperty("创建时间")
    @TableField(updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty("修改时间")
    private LocalDateTime updatedAt;

    @ApiModelProperty("创建者")
    private String creator;

    @ApiModelProperty("编辑者")
    private String editor;

    @ApiModelProperty("sql语句")
    private String flinkSql;

    @ApiModelProperty("租户Id")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private String tenantId;

    @ApiModelProperty("调度时间")
    private String cron;

    @ApiModelProperty("是否处在调度中")
    private Integer isScheduled;

    @ApiModelProperty("后段可视化组件的配置信息json")
    private String viewableConfig;

    @ApiModelProperty("是否检查通过")
    private Integer isChecked;

    @ApiModelProperty("前端现场信息")
    private String localConfig;

    //row sql 任务配置信息

    /**
     *  <p>
     *      数据源ID
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "源ID 不能为空")
    private String sourceId;
    /**
     *  <p>
     *      源数据库
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "源数据库不能为空")
    private String sourceDatabase;
    /**
     *  <p>
     *      目标数据源ID
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "目标数据源 ID 不能为空")
    private String targetId;
    /**
     *  <p>
     *      目标数据库
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "目标数据库不能为空")
    private String targetDatabase;
    /**
     *  <p>
     *      目标数据表
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "目标数据表不能为空")
    private String targetTable;
    /**
     *  <p>
     *      SQL 文本
     *  </p>
     */
    @NotEmpty(groups = RowSQL.class, message = "SQL 文本不能为空")
    private String rowSql;

    /**
     *  <p>字段映射信息</p>
     */
    @NotEmpty(groups = RowSQL.class, message = "字段映射不能为空")
    private String mappings;

}
