package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.JobRunLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobRunLogMapper extends BaseMapper<JobRunLog> {
}
