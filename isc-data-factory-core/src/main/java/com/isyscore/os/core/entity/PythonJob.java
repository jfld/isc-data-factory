package com.isyscore.os.core.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.isyscore.device.common.model.BaseEntity;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import javax.validation.constraints.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * 
 *
 * @author felixu
 * @since 2021-08-09
 */
@Data
@TableName("etl_python_job")
@ApiModel(description = "Python 任务")
public class PythonJob implements BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键 id")
    @NotNull(groups = ValidateUpdate.class)
    @Null(groups = ValidateCreate.class)
    private String id;

    @ApiModelProperty(value = "任务名称")
    @NotBlank
    @Length(min = 1, max = 32)
    @TableField(condition = SqlCondition.LIKE)
    @Pattern(regexp = "[\u4e00-\u9fa5A-Za-z0-9_#@().-]{1,32}", message = "支持中文、英文字母、数字、和特殊字符._-@()#，长度限制1~32")
    private String name;

    @ApiModelProperty(value = "调度表达式")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String schedule;

    @ApiModelProperty(value = "脚本内容")
    @NotBlank
    private String script;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "进程 pid")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer pid;

    @ApiModelProperty(value = "资源描述")
    @Length(max = 64)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String remark;

    @ApiModelProperty(value = "租户 id")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;
}
