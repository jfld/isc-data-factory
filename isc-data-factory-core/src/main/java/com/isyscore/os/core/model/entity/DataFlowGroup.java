package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dc
 * @Type DataFlowGroup.java
 * @Desc etl任务分组
 * @date 2022/10/21 10:38
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("data_flow_group")
public class DataFlowGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("唯一编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("分组名称")
    private String name;

    @ApiModelProperty("父节点id")
    private String parentId;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("租户ID")
    private String tenantId;

    @ApiModelProperty("下级分组")
    @TableField(exist = false)
    private List<DataFlowGroup> children;
}
    
