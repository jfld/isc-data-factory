package com.isyscore.os.core.model.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * (process_instance)实体类
 *
 * @author zhangyn
 * @since 2021-05-17 11:14:29
 * @description 
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("process_instance")
public class ProcessInstance extends Model<ProcessInstance> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @TableId(type = IdType.ASSIGN_UUID)
    @ApiModelProperty("工作流实例ID")
	private String id;
    /**
     * process instance name
     */
    @ApiModelProperty("工作流实力的名称")
    private String name;
    /**
     * process definition id
     */
    @ApiModelProperty("对应的工作流定义的ID")
    private String processDefinitionId;

    @ApiModelProperty(allowableValues = "RUNNING,CANCELED,FAILED,SCHEDULING")
    private String  status;
    /**
     * process instance start time
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createdAt;
    /**
     * process instance end time
     */
    @ApiModelProperty("修改时间")
    private LocalDateTime updatedAt;
    /**
     * schedule time
     */
    @ApiModelProperty("调度时间")
    private LocalDateTime scheduleTime;
    /**
     * schedule type NOW/CRON
     */
    @ApiModelProperty("调度类型")
    private String scheduleType;
    /**
     * process instance json(copy的process definition 的json)
     */
    @ApiModelProperty("工作流实力的参数信息")
    private String processInstanceJson;
    /**
     * Node location information
     */
    @ApiModelProperty("现场信息")
    private String locations;
    /**
     * Node connection information
     */
    @ApiModelProperty("节点的连接信息（目前是单节点模式）")
    private String connects;
    /**
     * tenant id
     */
    @ApiModelProperty("租户ID")
    private String tenantId;
    /**
     * jobId
     */
    @ApiModelProperty("对应的jobID")
    private String jobId;

}