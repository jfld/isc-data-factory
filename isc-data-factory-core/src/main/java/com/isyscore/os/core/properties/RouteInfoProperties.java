package com.isyscore.os.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author wuwx
 * @since 2022-05-30
 */
@Data
@ConfigurationProperties(prefix = "route")
public class RouteInfoProperties {

    /**
     * 路由服务地址
     */
    private String routerService="http://isc-route-service:31000";


    /**
     * 路由刷新token的路径
     */
    private String refreshRoutePath="/api/route/refreshRoute";


    /**
     * 路由排除地址
     */
    private List<String> excludeUrl;

    /**
     * 服务ID
     */
    private String serviceId;


    private String registerUrl;

    private String registerAclUrl;

}
