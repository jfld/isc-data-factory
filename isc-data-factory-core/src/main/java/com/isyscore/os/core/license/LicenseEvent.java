package com.isyscore.os.core.license;

import org.springframework.context.ApplicationEvent;

import java.util.List;
import java.util.Map;

/**
 * @author wuwx
 */
public class LicenseEvent extends ApplicationEvent {
    private List<Map<String,Integer>> data;

    public List<Map<String, Integer>> getData() {
        return data;
    }

    public void setData(List<Map<String, Integer>> data) {
        this.data = data;
    }

    public LicenseEvent(Object source, List<Map<String,Integer>> data) {
        super(source);
        this.data=data;
    }
}
