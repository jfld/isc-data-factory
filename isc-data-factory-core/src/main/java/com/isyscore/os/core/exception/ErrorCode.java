package com.isyscore.os.core.exception;

import com.isyscore.device.common.exception.BaseErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author felixu
 * @since 2021.05.10
 */
@Getter
@AllArgsConstructor
public enum ErrorCode implements BaseErrorCode {

    /*-------------------------------- 成功 ------------------------------*/
    OK(0, "成功", ErrorGroup.GENERAL),

    /*-------------------------------- 失败(大多为没有被处理到的特殊异常) ------------------------------*/
    FAIL(-1, "出现未知错误，请稍后再试", ErrorGroup.GENERAL),
    MISSING_CODE(11999, "当前错误信息: {0}，缺失真正 code，请联系开发者处理", ErrorGroup.GENERAL),

    /*-------------------------------- 通用异常 ------------------------------*/
    PARAM_ERROR(11900, "您提交的数据不符合要求", ErrorGroup.GENERAL),
    LOGIN_REQUIRED(11901, "请(重新)登录", ErrorGroup.GENERAL),
    AUTHORIZATION_REQUIRED(11902, "您没有执行该操作的权限", ErrorGroup.GENERAL),
    MISSING_SERVLET_REQUEST_PARAMETER(11903, "不能为空", ErrorGroup.GENERAL),
    PATH_PARAM_DATA_TYPE_ERROR(11904, "路径参数数据类型出错", ErrorGroup.GENERAL),
    INPUT_PARAM_ILLEGAL(11905, "入参非法,请检查输入参数或格式", ErrorGroup.GENERAL),
    LICENSE_EXCEED_LIMIT(11906, "{0}超出授权限制，请联系license升级服务", ErrorGroup.GENERAL),
    FILE_TYPE_NOT_SUPPORT(11907, "文件类型不支持", ErrorGroup.GENERAL),
    FILE_UPLOAD_FAILED(11908, "文件上传失败", ErrorGroup.GENERAL),
    FILE_SIZE_EXCEED_LIMIT(11909, "文件大小超出限制", ErrorGroup.GENERAL),
    RESOURCE_NOT_FOUND(11910, "要查询的资源不存在", ErrorGroup.GENERAL),
    EXCEL_PARSE_ERROR(11911, "Excel文件解析失败", ErrorGroup.GENERAL),
    VALIDATE_ERROR(11912, "校验失败: {0}", ErrorGroup.GENERAL),
    UNKNOWN_ORDER_COLUMN(11913, "不存在的排序列: {0}", ErrorGroup.GENERAL),
    ERROR_TENANT_INFO(11914, "路径中包含租户信息有误，导入失败", ErrorGroup.GENERAL),
    TASK_EXIST(11915, "已有导出/导入任务在进行中", ErrorGroup.GENERAL),
    TENANT_INFO_ERROR(11916, "导入数据中租户信息错误", ErrorGroup.GENERAL),
    INTERNAL_ERROR(11917, "发生内部错误：{0}", ErrorGroup.GENERAL),
    VALIDATE_ERROR_NO_WORDS(11918, "{0}", ErrorGroup.GENERAL),
    ERROR_SIGN(11919, "无效签名：{0}", ErrorGroup.GENERAL),
    LICENSE_DATASOURCE_LIMIT(11920, "数据源数量已达上限，若需新增，请联系管理员！", ErrorGroup.GENERAL),

    SYNC_OPENAPI_ERROR(11921, "同步OpenApi信息到能力中心异常", ErrorGroup.GENERAL),
    /*-------------------------------- ETL 异常 ------------------------------*/
    DELETE_FAILED(11101, "要删除的资源不存在", ErrorGroup.ETL),
    BOOT_FAILED(11102, "启动失败", ErrorGroup.ETL),
    REQUEST_FAILED(11103, "请求服务失败", ErrorGroup.ETL),
    PROCESS_RUNNING(11104, "要删除的工作流正在运行中", ErrorGroup.ETL),
    SYNC_STATUS_FAILED(11105, "状态更新失败", ErrorGroup.ETL),
    CRON_VALID(11106, "cron 表达式不合法", ErrorGroup.ETL),
    STATUS_ERROR(11107, "工作流的状态错误", ErrorGroup.ETL),
    TYPE_ERROR(11108, "工作流不是调度类型", ErrorGroup.ETL),
    SCRIPT_FILE_ERROR(11109, "Python 脚本文件类型不支持", ErrorGroup.ETL),
    JOB_NOT_EXIST(11110, "当前任务不存在", ErrorGroup.ETL),
    JOB_IS_RUNNING(11111, "当前任务正在运行中或者正在启动中，无法重复运行", ErrorGroup.ETL),
    RESOURCE_NAME_EXIST(11112, "当前资源名称已存在", ErrorGroup.ETL),
    JOB_START_ERROR(11113, "任务启动失败", ErrorGroup.ETL),
    JOB_DELETE_AFTER_CANCEL(11114, "请先停止任务，再尝试删除任务", ErrorGroup.ETL),
    JOB_NOT_NEEDED_CANCEL(11115, "不在运行中和启动中的任务，无需取消", ErrorGroup.ETL),
    JOB_IS_SCHEDULING(11116, "当前任务正在调度中，无法进行当前操作", ErrorGroup.ETL),
    JOB_NAME_OUT_OF_LENGTH(11117, "任务名称不能超过50个字符", ErrorGroup.ETL),
    JOB_NAME_DUPLICATE(11118, "任务名称已存在", ErrorGroup.ETL),
    JOB_DOES_NOT_EXISTS(11119, "任务不存在", ErrorGroup.ETL),
    JOB_STATUS_ERROR(11120, "任务状态错误", ErrorGroup.ETL),
    MISSING_PARAMS(11121, "缺少必要参数：{0}", ErrorGroup.ETL),
    DOES_NOT_SUPPORT(11122, "不支持该用法", ErrorGroup.ETL),
    CANCEL_ERROR(11123, "取消失败", ErrorGroup.ETL),
    SAVEPOINT_ERROR(11124, "savepoint失败", ErrorGroup.ETL),
    ILLEGAL_PATH(11125, "路径错误: {0}", ErrorGroup.ETL),
    CHECKPOINT_ERROR(11126, "checkpoint错误", ErrorGroup.ETL),
    WRITE_ERROR(11127, "写入失败", ErrorGroup.ETL),
    FLINK_CONNET_ERROR(11128, "flink 服务端连接失败，请检查flink服务", ErrorGroup.ETL),
    FLINK_SQL_ERROR(11129, "不支持该语法使用{0}", ErrorGroup.ETL),
    LOG_NOT_FOUND(111130, "日志记录查询失败", ErrorGroup.ETL),
    CANNOT_DELETE(11131, "资源正在被{0}引用，无法删除", ErrorGroup.ETL),
    CANNOT_STOP(11132, "任务不在运行中，无法停止，请稍后再试", ErrorGroup.ETL),
    STOP_FAILED(11133, "定时任务停止失败，请稍后再试", ErrorGroup.ETL),
    ALREADY_SCHEDULED(11134, "任务已经在调度中，无法重复调度", ErrorGroup.ETL),
    SCHEDULED_FAILED(11135, "调度开启失败，请稍后再试", ErrorGroup.ETL),
    RESOURCE_DELETE_FAILED(11136, "资源删除失败，请稍后再试", ErrorGroup.ETL),
    PRECHECK_ERROR(11137, "预校验错误{0}", ErrorGroup.ETL),
    NO_CHECKED(11138, "可视化任务配置未完成", ErrorGroup.ETL),
    PARSE_ERROR(11139, "Flink SQL 解析错误", ErrorGroup.ETL),
    RESOURCE_NOT_ALLOWED(11140, "内置连接器不允许删除", ErrorGroup.ETL),
    MYSQL_ROWSQL_CHECKED(11141, "MySQL任务配置未完成", ErrorGroup.ETL),
    FLINK_CONFIG_ERROR(11142, "FLINK配置错误", ErrorGroup.ETL),
    VIS_CONFIG_ERROR_NODE_TYPE_NOT_FOUND(11143, "可视化配置错误，节点类型未找到", ErrorGroup.ETL),
    VIS_CONFIG_ERROR_NODE_TYPE_NOT_REGISTER(11144, "可视化配置错误，节点类型未注册解析器{0}", ErrorGroup.ETL),
    VIS_CONFIG_PARSE_ERROR(11145, "可视化配置错误，节点【{0}】解析错误：{1}", ErrorGroup.ETL),

    KETTLE_ETL_CREATE_FAIL(11146, "ETL创建失败,{0}", ErrorGroup.ETL),
    KETTLE_ETL_CHECK_FAIL(11147, "ETL流程校验失败", ErrorGroup.ETL),
    KETTLE_ETL_GET_TABLE_FIELDS_FAIL(11148, "获得表字段失败", ErrorGroup.ETL),
    KETTLE_ETL_GET_INPUT_OUTPUT_FIELDS_FAIL(11149, "获得输入或输出字段失败", ErrorGroup.ETL),
    KETTLE_ETL_ERROR_TIME_UNIT(11150, "不正确的日期间隔格式", ErrorGroup.METRIC),
    KETTLE_ETL_SCHEDULE_PROHIBIT_PARAM(11151, "定时调度任务禁止使用动态参数", ErrorGroup.METRIC),
    DATA_FLOW_ID_NOT_FOUND(11152, "未找到任务定义ID：{0}", ErrorGroup.METRIC),
    TASK_IS_FINISHED(11153, "任务已运行结束", ErrorGroup.METRIC),
    NODE_ID_NOT_FOUND(11154, "节点ID未找到:{0}", ErrorGroup.METRIC),
    NODE_NOT_UNIQUE(11155, "节点名重复", ErrorGroup.METRIC),
    REPEAT_RUN_PARAM_ERROR(11156, "重复运行必须设置开始时间和间隔", ErrorGroup.METRIC),
    IS_NOT_DATE(11157, "时间格式有误:{0}", ErrorGroup.METRIC),
    INCR_SQL_BUILD_ERROR(11158, "增量SQL构建异常", ErrorGroup.METRIC),
    KETTLE_ETL_CODEC_FAIL(11159, "ETL编解码失败,{0}", ErrorGroup.ETL),
    /*-------------------------------- 元数据异常 ------------------------------*/
    DATA_SOURCE_CONNECTION_ERROR(11200, "数据源建立连接错误", ErrorGroup.METADATA),
    INDICATOR_NAME_REPETITION(11203, "指标库名称重复", ErrorGroup.METADATA),
    MODEL_NAME_REPETITION(11205, "模型名称重复", ErrorGroup.METADATA),
    API_URL_REPETITION(11206, "接口url不能重复", ErrorGroup.METADATA),
    DATA_QUERY_ERROR(11207, "数据查询失败:{0}", ErrorGroup.METADATA),
    DATABASE_NOT_EXIST(11208, "{0}数据库不存在", ErrorGroup.METADATA),
    TABLE_UPDATE_FAILED(11209, "{0}表结构更新失败", ErrorGroup.METADATA),
    DATABASE_PROHIBIT_DELETE(11210, "业务库不允许删除", ErrorGroup.METADATA),
    DATASOURCE_NOT_FOUND(11211, "数据源不存在", ErrorGroup.METADATA),
    TABLE_ALREADY_EXIST(11212, "该数据表已存在", ErrorGroup.METADATA),
    MISSING_ORDER_BY_FIELD(11213, "缺少排序字段", ErrorGroup.METADATA),
    SQL_EXEC_FAILED(11214, "SQL语句执行失败:{0}", ErrorGroup.METADATA),
    MUST_CHOOSE_FIELD(11215, "至少选择一个字段进行导入", ErrorGroup.METADATA),
    DATABASE_PROHIBIT_ADD_TABLE(11216, "业务库不允许新增表", ErrorGroup.METADATA),
    FIELD_NAME_CAN_NOT_EMPTY(11217, "字段名称不能为空", ErrorGroup.METADATA),
    TABLE_NOT_EXIST(11218, "该数据表不存在", ErrorGroup.METADATA),
    DELETE_TABLE_FAILED(11219, "删除表失败", ErrorGroup.METADATA),
    ADD_TABLE_FAILED(11220, "新增表失败", ErrorGroup.METADATA),
    PARAM_METHOD_ILLEGAL(11221, "参数method类型非法", ErrorGroup.METADATA),
    TASK_IS_NULL(11222, "任务过期或者不存在", ErrorGroup.METADATA),
    TASK_IS_STOP(11223, "任务异常停止：{0}", ErrorGroup.METADATA),
    CONNECT_TYPE_NOT_SUPPORT(11224, "暂不支持的连接类型", ErrorGroup.METADATA),
    NULL_DATABASE(11225, "该数据源未配置数据库", ErrorGroup.METADATA),
    DATABASE_LOCKED(11226, "该数据库已上锁或该数据库不存在", ErrorGroup.METADATA),
    ALREADY_LOCKED(11227, "该数据库已经上锁或解锁无法再次上锁或解锁", ErrorGroup.METADATA),
    DATA_IMPORT_FAILED_NEW(11228, "导入数据失败,请查看是否有建表权限", ErrorGroup.METADATA),
    DATA_IMPORT_FAILED_CLEAR(11229, "导入数据失败,请查看是否有建表删表权限", ErrorGroup.METADATA),
    DATA_IMPORT_FAILED_APPEND(11230, "导入数据失败,请查看目标表与文件结构是否一致", ErrorGroup.METADATA),
    RENAME_TABLE_FAILED(11231, "表格名称修改失败", ErrorGroup.METADATA),
    UPDATE_NOT_FOUNT(11232, "更新的内容ID不存在", ErrorGroup.METADATA),
    API_NOT_FOUND(11233, "API不存在", ErrorGroup.METADATA),

    DATASOURCE_TYPE_ERROR(11234, "数据源类型错误", ErrorGroup.METADATA),

    NOT_SUPPORTED(11235, "该数据源类型暂不支持{0}", ErrorGroup.METADATA),
    SELECT_NOT_SUPPORT(11236, "该数据库类型暂不支持查询", ErrorGroup.METADATA),
    DBTYPE_NOT_SUPPORT_PAGE(11238, "该数据源类型暂不支持分页查询：{0}", ErrorGroup.METADATA),
    COMMON_ERROR(11299, "{0}", ErrorGroup.METADATA),

    AGGREGATE_FUNCTION_NULL(11300, "该聚合函数不支持", ErrorGroup.METADATA),
    DEVICE_IDENTIFIER_NOT_FOUND(11301, "该设备标识符无法识别", ErrorGroup.METADATA),
    DEVICE_ATTRIBUTE_AGGREGATE_NOT_SUPPORT(11302, "该设备属性不支持这个聚合函数", ErrorGroup.METADATA),
    ID_EXIST(11303, "ID 已存在", ErrorGroup.METADATA),
    DATA_TEXT_ERROR(11304, "SQL 校验未通过", ErrorGroup.METADATA),
    SQL_QUERY_PARAM_ERROR(11305, "SQL查询缺少参数{0}", ErrorGroup.METADATA),
    DATA_PREVIEW_ERROR(11306, "数据预览失败{0}", ErrorGroup.METADATA),
    SQL_FAST_CREATE_ERROR(11307, "SQL快速创建失败", ErrorGroup.METADATA),
    ONLY_SUPORT_SELECTSQL_ERROR(11308, "只支持查询语句", ErrorGroup.METADATA),
    DATABASE_REFERENCED_BY_KETTLE(11309, "数据源被数据任务使用，删除失败", ErrorGroup.METADATA),
    TASK_ID_NOT_FOUND(11310, "TaskId未找到：{0}", ErrorGroup.METADATA),
    DEFINITION_ID_NOT_FOUND(11311, "DefinitionId未找到：{0}", ErrorGroup.METADATA),
    DATABASE_REFERENCED_BY_SQL_QUERY(11312, "数据源被SQL查询使用，删除失败", ErrorGroup.METADATA),
    KETTLE_TASK_HAS_START(11313, "数据任务已启动，启动失败", ErrorGroup.METADATA),
    KETTLE_TASK_HAS_STOP(11314, "数据任务已启动，停止失败", ErrorGroup.METADATA),
    KETTLE_TASK_NEED_REPEAT_RUN(11315, "数据任务未开启重复运行，启动失败", ErrorGroup.METADATA),
    DATA_SOURCE_PASS_ENCRYPT_ERROR(11316, "数据源密码加密失败", ErrorGroup.METADATA),
    DATA_SOURCE_PASS_DECRYPT_ERROR(11317, "数据源密码解密失败", ErrorGroup.METADATA),
    /*-------------------------------- 数据指标异常 ------------------------------*/
    FORMULA_FORMAT_ERROR(11000, "计算公式格式有误：{0}", ErrorGroup.METRIC),
    FORMULA_RUN_FAILED(11001, "运行计算公式时发生异常", ErrorGroup.METRIC),
    DATA_NOT_FOUND(11003, "未找到对应的数据：{0}", ErrorGroup.METRIC),
    DATA_NOT_ALLOWED_DELETE(11005, "数据不允许删除：{0}", ErrorGroup.METRIC),
    INITIALLY_DATA_NOT_ALLOWED_DELETE(11005, "默认数据不允许删除", ErrorGroup.METRIC),
    METRIC_JOB_CRON_INVALID(11008, "填写的JOB定时任务表达式不满足格式需求，请检查", ErrorGroup.METRIC),
    DATAPERIOD_EXCEED_LIMIT(11010, "数据期超过最大限制：当前最大值为{0}", ErrorGroup.METRIC),
    SQL_FORMAT_ERROR(11014, "指标或模型的SQL格式存在错误", ErrorGroup.METRIC),
    DUPLICATE_METRIC_JOB(11016, "当前数据指标的计算任务已存在，请勿重复添加", ErrorGroup.METRIC),
    JOB_SCHEDULER_ERROR(11018, "定时任务调度器发生错误，任务无法操作", ErrorGroup.METRIC),
    INVALID_TIME_PERIOD(11020, "传入的时间期字段不满足要求，请检查格式是否正确", ErrorGroup.METRIC),
    INVALID_DATETIME_TYPE(11020, "该字段不是被支持的日期时间类型，无法定义为时间维度：{0}", ErrorGroup.METRIC),
    DATAMODEL_NOT_ALLOWED_MODIFY(11022, "数据模型的字段已经被指标引用，无法修改", ErrorGroup.METRIC),
    METRIC_CALCULATE_TIMEOUT(11024, "指标计算超时，请使用异步计算的方式重新提交请求", ErrorGroup.METRIC),
    DIOP_IMPORT_ERROR(11026, "数据导入失败", ErrorGroup.METRIC),
    INVALID_ORDER_TYPE(11028, "请输入正确的排序类型", ErrorGroup.METRIC),
    MEASURE_AGG_INCONSISTENCY(11030, "数据指标不支持同时选择聚合与非聚合的度量值", ErrorGroup.METRIC),
    METRIC_CALCULATE_ERROR(11031, "数据指标计算异常", ErrorGroup.METRIC);

    /**
     * 返回的 code，非 0 表示错误
     */
    private final int code;

    /**
     * 发生错误时的描述
     */
    private final String message;

    /**
     * 错误码分组
     */
    private final ErrorGroup group;

    /**
     * 根据ErrorCode的值搜索对应的枚举对象
     */
    public static ErrorCode getErrorCodeByCodeVal(Integer code) {
        if (code == null) {
            return null;
        }
        ErrorCode[] codes = ErrorCode.values();
        for (ErrorCode errorCode : codes) {
            if (errorCode.getCode() == code) {
                return errorCode;
            }
        }
        return null;
    }
}
