package com.isyscore.os.core.model.enums;

/** 授权等级
 * @author wuwx
 */
public enum LicenseLevel {
    /**
     *
     */
    COMMUNITY_EDITION(0, "社区版"),
    LIGHT_EDITION(1, "轻量版"),
    ULTIMATE_EDITION(2, "旗舰版"),
    INTERNAL_EDITION(3, "内部版"),
    OFFLINE_EDITION(4, "离线版");
    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    LicenseLevel(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
