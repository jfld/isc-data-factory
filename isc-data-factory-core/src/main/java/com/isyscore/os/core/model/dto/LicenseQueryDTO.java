package com.isyscore.os.core.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LicenseQueryDTO {
    private List<Integer> ids;
}
