package com.isyscore.os.core.model.enums;

/**
 * 授权选项
 * @author wuwx
 */
public enum LicenseOption {
    /**
     *
     */
    LICENSE_TYPE(1, "授权类型"),
    DATASOURCE_QUANTITY(5, "外部数据库数量"),
    UDMP_ENABLE(23, "通用数据管理是否可用");
    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    LicenseOption(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
