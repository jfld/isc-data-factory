package com.isyscore.os.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.core.model.entity.OperationLog;

public interface OperationLogService extends IService<OperationLog> {

}
