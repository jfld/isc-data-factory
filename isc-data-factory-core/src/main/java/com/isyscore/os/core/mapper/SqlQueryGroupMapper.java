package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.isyscore.os.core.entity.SqlQueryGroup;
import org.apache.ibatis.annotations.Param;

/**
 *  Mapper 接口
 *
 * @author starzyn
 * @since 2022-03-16
 */
public interface SqlQueryGroupMapper extends BaseMapper<SqlQueryGroup> {
    /**
     * 不做空值检查，根据主键强制更新除“created_at”之外的所有字段（可将字段更新为null）
     *
     * @param entity 目标实体对象
     * @return 受影响的数据量
     */
    int alwaysUpdateSomeColumnById(@Param(Constants.ENTITY) SqlQueryGroup entity);
}
