package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.*;
import org.apache.ibatis.annotations.Mapper;

;

/**
 * (process_definition)数据Mapper
 *
 * @author zhangyn
 * @since 2021-05-17 11:14:29
 * @description 
*/
@Mapper
public interface ProcessDefinitionMapper extends BaseMapper<ProcessDefinition> {

}
