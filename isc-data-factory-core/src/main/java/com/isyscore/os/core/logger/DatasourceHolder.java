package com.isyscore.os.core.logger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author felixu
 * @since 2021.11.23
 */
public class DatasourceHolder {

    private static final Map<String, String> APP_DS_NAME = new HashMap<String, String>() {{
        put("isc-etl-service", "clickhouse-etl");
        put("isc-metadata-service", "clickhouse-metadata");
    }};

    private static final Map<String, String> APP_DB_NAME = new HashMap<String, String>() {{
        put("isc-etl-service", "isc_etl");
        put("isc-metadata-service", "isc_metadata");
    }};

    public static String getDs(String application) {
        return APP_DS_NAME.get(application);
    }

    public static String getDb(String application) {
        return APP_DB_NAME.get(application);
    }
}
