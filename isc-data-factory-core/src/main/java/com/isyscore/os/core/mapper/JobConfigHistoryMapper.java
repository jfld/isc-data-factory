package com.isyscore.os.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.JobConfigHistory;
import org.mapstruct.Mapper;

@Mapper
public interface JobConfigHistoryMapper extends BaseMapper<JobConfigHistory> {
}
