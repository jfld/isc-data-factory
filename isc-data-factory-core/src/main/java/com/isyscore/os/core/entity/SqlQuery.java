package com.isyscore.os.core.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.isyscore.device.common.model.BaseEntity;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;

import java.time.LocalDateTime;
import javax.validation.constraints.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author starzyn
 * @since 2022-03-16
 */
@Data
@ApiModel(description = "")
public class SqlQuery implements BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @NotNull(groups = ValidateUpdate.class)
    @Null(groups = ValidateCreate.class)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "sql名称")
    @NotBlank(groups = {ValidateUpdate.class,ValidateCreate.class},message = "sql名称不能为空")
    @Length(max = 32,message="sql名称应为1~32个字符")
    private String name;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "SQL文本")
    @NotBlank(groups = {ValidateUpdate.class,ValidateCreate.class},message = "sql文本不能为空")
    private String sqlText;

    @ApiModelProperty(value = "状态量 0 -> 未通过。1 -> 已通过")
    private Integer status;

    @ApiModelProperty(value = "是否为私有")
    private String isPrivate;

    @ApiModelProperty(value = "分组ID")
    private Long groupId;

    @ApiModelProperty("数据源 ID")
    @NotNull(message = "数据源不能为空")
    private Long dataSourceId;

    @ApiModelProperty("选定的数据库")
    @NotBlank(message = "选定的数据库不能为空")
    private String databaseName;

}
