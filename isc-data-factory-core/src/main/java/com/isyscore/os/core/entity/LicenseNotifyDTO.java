package com.isyscore.os.core.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 *  授权通知内容
 */
@Data
public class LicenseNotifyDTO {
    // 可以是true/false，表示该应用是否已被授权
    private Boolean licensed;
    // 过期时间，0为永久可用，若是要设置过期时间，此处值为 13 位的 Java 时间戳
    private Long expire;
    // 13位时间戳
    private Long timestamp;
    // 验签签名
    private String sign;
    //授权详情
    private List<Map<String,Integer>> data;
}
