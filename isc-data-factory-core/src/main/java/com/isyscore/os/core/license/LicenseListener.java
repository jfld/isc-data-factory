package com.isyscore.os.core.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;

import java.util.List;
import java.util.Map;

/**
 * @author wuwx
 */
public abstract class LicenseListener implements ApplicationListener<LicenseEvent> {
    private static final Logger log = LoggerFactory.getLogger(LicenseListener.class);

    public LicenseListener() {
    }

    public void onApplicationEvent(LicenseEvent event) {
        this.onLicense(event.getData());
    }

    public abstract void onLicense(List<Map<String,Integer>> data);
}
