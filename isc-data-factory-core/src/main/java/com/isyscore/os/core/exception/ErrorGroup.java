package com.isyscore.os.core.exception;

import com.isyscore.device.common.enums.DescribableEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author felixu
 * @since 2021.05.10
 */
@Getter
@AllArgsConstructor
public enum ErrorGroup implements DescribableEnum {

    GENERAL("通用信息", true),
    ETL("工作流", true),
    METADATA("元数据", true),
    METRIC("数据指标", true),
    ;

    private final String desc;

    private final boolean accessible;
}
