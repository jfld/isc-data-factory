package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.isyscore.device.common.model.BaseEntity;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import java.time.LocalDateTime;
import javax.validation.constraints.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 微服务配置表
 *
 * @author starzyn
 * @since 2022-02-17
 */
@Data
@ApiModel(description = "微服务配置表")
public class DataFactoryConfig implements BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @NotNull(groups = ValidateUpdate.class)
    @Null(groups = ValidateCreate.class)
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "微服务名称")
    @NotBlank
    private String serviceName;

    @ApiModelProperty(value = "key")
    @NotBlank
    private String paramKey;

    @ApiModelProperty(value = "value")
    @NotBlank
    private String paramValue;

    @ApiModelProperty(value = "参数名")
    @NotBlank
    private String paramName;

    @ApiModelProperty(value = "默认值")
    @NotBlank
    private String defaultValue;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "数据类型, 0不对外开放 ,1开放")
    private Boolean type;

    @ApiModelProperty(value = "扩展字段,存储数据类型")
    private String extra;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;

}
