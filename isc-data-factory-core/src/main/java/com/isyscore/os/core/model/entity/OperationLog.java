package com.isyscore.os.core.model.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

@Data
@NoArgsConstructor
@TableName(value = "operation_log", autoResultMap = true)
public class OperationLog {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("操作")
    private String action;

    @ApiModelProperty("操作描述")
    private String describe;

    @ApiModelProperty("请求方法名")
    private String strMethod;

    @ApiModelProperty("请求类名")
    private String strTarget;

    @ApiModelProperty("请求参数")
    private String parameters;

    @ApiModelProperty("请求结果")
    private String response;

    @ApiModelProperty("请求时间")
    private LocalDateTime requestTime;

    @ApiModelProperty("请求是否成功")
    private boolean success;

    @ApiModelProperty("拓展内容")
    private String ext;

    @ApiModelProperty("请求耗时")
    private Long duration;

    @ApiModelProperty("错误信息")
    private String errorMsg;

    @ApiModelProperty("访问者 ip")
    private String ip;

    @ApiModelProperty("请求路径")
    private String url;

    @ApiModelProperty("请求头")
    private String httpHeaders;

    @ApiModelProperty("请求方式")
    private String httpMethod;

    @ApiModelProperty("租户 id")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private String tenantId;

    @ApiModelProperty("操作者")
    @TableField(updateStrategy = FieldStrategy.NEVER)
    private String operator;
}
