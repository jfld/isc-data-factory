CREATE TABLE IF NOT EXISTS {0}.operation_log on cluster replicated
(
    `id` UInt64,
    `action` String,
    `describe` String,
    `str_method` String,
    `str_target` String,
    `parameters` String,
    `response` String,
    `request_time` DateTime64(3),
    `success` Int8,
    `ext` String,
    `duration` UInt64,
    `error_msg` String,
    `ip` String,
    `url` String,
    `http_headers` String,
    `http_method` String,
    `tenant_id` String
)
ENGINE = MergeTree()
ORDER BY id
SETTINGS index_granularity = 8192;

ALTER TABLE {1}.operation_log on cluster replicated ADD COLUMN IF NOT EXISTS operator String;
