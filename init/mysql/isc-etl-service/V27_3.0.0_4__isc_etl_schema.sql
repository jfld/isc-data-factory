USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `data_factory_config` (
                                       `id` varchar(32) NOT NULL COMMENT 'id',
                                       `service_name` varchar(32) NOT NULL COMMENT '微服务名称',
                                       `param_key` varchar(64) NOT NULL COMMENT 'key',
                                       `param_value` varchar(64) NOT NULL COMMENT 'value',
                                       `param_name` varchar(64) NOT NULL COMMENT '参数名',
                                       `default_value` varchar(64) NOT NULL COMMENT '默认值',
                                       `remark` varchar(64) DEFAULT NULL COMMENT '备注',
                                       `type` tinyint(1) DEFAULT '1' COMMENT '数据类型, 0不对外开放 ,1开放',
                                       `extra` text COMMENT '扩展字段,存储数据类型',
                                       `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                       `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='微服务配置表';


INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('0', 'isc-etl-service', 'INITIALIZED', '0', 'INITIALIZED', '0', '内置连接器是否全部初始化完成', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('1', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'kafka-clients-2.4.1.jar', '0', 'Kafka 客户端依赖', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('10', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flink-connector-mysql-cdc-1.1.1.jar', '0', 'MySQL CDC 依赖', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('11', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flink-connector-kafka_2.11-1.12.2.jar', '0', 'Kafka 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('2', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-core-1.12-SNAPSHOT.jar', '0', 'FLINK 通用依赖', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('3', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-sqlserver-1.12-SNAPSHOT.jar', '0', 'SQLserver 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('4', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-postgresql-1.12-SNAPSHOT.jar', '0', 'Postgres 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('5', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-oracle-1.12-SNAPSHOT.jar', '0', 'Oracle 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('6', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-mysql-1.12-SNAPSHOT.jar', '0', 'MySQL 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('7', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-dm-1.12-SNAPSHOT.jar', '0', '达梦数据库连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('8', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flinkx-connector-clickhouse-1.12-SNAPSHOT.jar', '0', 'click house 连接器', 1);
INSERT INTO `isc_data_factory`.`data_factory_config` (`id`, `service_name`, `param_key`, `param_value`, `param_name`, `default_value`, `remark`, `type`) VALUES ('9', 'isc-etl-service', 'INNER_CONNECTOR', '0', 'flink-connector-postgres-cdc-1.3.0.jar', '0', 'Postgres CDC 依赖', 1);

UPDATE `job_config` SET `tenant_id` = 'system' WHERE ISNULL(`tenant_id`) OR `tenant_id` = '';
UPDATE `job_config_history` SET `tenant_id` = 'system' WHERE ISNULL(`tenant_id`) OR `tenant_id` = '';
UPDATE `etl_resource` SET `tenant_id` = 'system' WHERE ISNULL(`tenant_id`) OR `tenant_id` = '';
UPDATE `job_run_log` SET `tenant_id` = 'system' WHERE ISNULL(`tenant_id`) OR `tenant_id` = '';




SET FOREIGN_KEY_CHECKS = 1;