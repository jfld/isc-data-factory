USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE IF NOT EXISTS isc_data_factory.flink_sql_job_jar_info (
	jar_version varchar(32) NOT NULL COMMENT 'jar包的版本',
	flink_jar_id varchar(256) NOT NULL COMMENT '上传jar包后flink给定的jarId',
	flink_address varchar(256) NOT NULL COMMENT 'flink服务端地址',
	upload_time DATETIME NOT NULL COMMENT '上报jar包的时间',
	primary key(flink_jar_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci
COMMENT='保存远程执行flink sql的jar包上传到flink session集群后的相关信息'
ROW_FORMAT = DYNAMIC;

alter table isc_data_factory.etl_resource
    add connect_type int null comment '连接类型';

-- 设置基础配置的连接类型属性
UPDATE isc_data_factory.data_factory_config t SET t.extra = '{"connectType":"0"}',param_value='0' WHERE t.id = '6' ;
UPDATE isc_data_factory.data_factory_config t SET t.extra = '{"connectType":"1"}',param_value='0' WHERE t.id = '5' ;
UPDATE isc_data_factory.data_factory_config t SET t.extra = '{"connectType":"2"}',param_value='0' WHERE t.id = '8' ;
UPDATE isc_data_factory.data_factory_config t SET t.extra = '{"connectType":"3"}',param_value='0' WHERE t.id = '3' ;
UPDATE isc_data_factory.data_factory_config t SET t.extra = '{"connectType":"4"}',param_value='0' WHERE t.id = '7' ;

-- 设置data_factory_config中param_value为0，重新上传升级jar包
UPDATE isc_data_factory.data_factory_config  SET param_value='0' where id='0';

SET FOREIGN_KEY_CHECKS = 1;
