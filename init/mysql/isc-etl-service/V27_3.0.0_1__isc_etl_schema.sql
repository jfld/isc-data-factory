USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `job_config`;
CREATE TABLE `job_config` (
                              `id` varchar(128) NOT NULL,
                              `job_name` varchar(64) NOT NULL COMMENT '任务名称',
                              `deploy_mode` varchar(64) NOT NULL DEFAULT '' COMMENT '提交模式: standalone 、yarn 、yarn-session ',
                              `flink_run_config` varchar(512) NOT NULL DEFAULT '' COMMENT 'flink运行配置',
                              `flink_sql` mediumtext NOT NULL COMMENT 'sql语句',
                              `flink_checkpoint_config` varchar(512) DEFAULT NULL COMMENT 'checkPoint配置',
                              `job_id` varchar(64) DEFAULT NULL COMMENT '运行后的任务id',
                              `is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:开启 0: 关闭',
                              `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:运行中 0: 停止中 -1:运行失败',
                              `ext_jar_path` varchar(2048) DEFAULT NULL COMMENT 'udf地址已经连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar',
                              `last_start_time` datetime DEFAULT NULL COMMENT '最后一次启动时间',
                              `last_run_log_id` bigint(11) DEFAULT NULL COMMENT '最后一次日志',
                              `version` int(11) NOT NULL DEFAULT '0' COMMENT '更新版本号 用于乐观锁',
                              `job_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '任务类型 0:sql 1:自定义jar',
                              `custom_args` varchar(128) DEFAULT NULL COMMENT '启动jar可能需要使用的自定义参数',
                              `custom_main_class` varchar(128) DEFAULT NULL COMMENT '程序入口类',
                              `custom_jar_url` varchar(128) DEFAULT NULL COMMENT '自定义jar的http地址 如:http://ccblog.cn/xx.jar',
                              `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                              `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                              `creator` varchar(32) DEFAULT 'sys',
                              `editor` varchar(32) DEFAULT 'sys',
                              `tenant_id` varchar(32) DEFAULT NULL,
                              `schedule` varchar(128) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `uk_index` (`job_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='flink任务配置表';

DROP TABLE IF EXISTS `job_config_history`;
CREATE TABLE `job_config_history` (
                                      `id` varchar(128) NOT NULL,
                                      `job_config_id` varchar(128) NOT NULL COMMENT 'job_config主表Id',
                                      `job_name` varchar(64) NOT NULL COMMENT '任务名称',
                                      `deploy_mode` varchar(64) NOT NULL COMMENT '提交模式: standalone 、yarn 、yarn-session ',
                                      `flink_run_config` varchar(512) NOT NULL COMMENT 'flink运行配置',
                                      `flink_sql` mediumtext NOT NULL COMMENT 'sql语句',
                                      `flink_checkpoint_config` varchar(512) DEFAULT NULL COMMENT 'checkPoint配置',
                                      `ext_jar_path` varchar(2048) DEFAULT NULL COMMENT 'udf地址及连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar',
                                      `version` int(11) NOT NULL DEFAULT '0' COMMENT '更新版本号',
                                      `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                                      `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                      `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                      `creator` varchar(32) DEFAULT 'sys',
                                      `editor` varchar(32) DEFAULT 'sys',
                                      `tenant_id` varchar(32) DEFAULT NULL,
                                      `schedule` varchar(128) DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      KEY `index_job_config_id` (`job_config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='flink任务配置历史变更表';

DROP TABLE IF EXISTS `job_run_log`;
CREATE TABLE `job_run_log` (
                               `id` varchar(128) NOT NULL,
                               `job_config_id` varchar(128) NOT NULL,
                               `job_name` varchar(64) NOT NULL COMMENT '任务名称',
                               `deploy_mode` varchar(64) NOT NULL DEFAULT '' COMMENT '提交模式: standalone 、yarn 、yarn-session ',
                               `job_id` varchar(64) DEFAULT NULL COMMENT '运行后的任务id',
                               `local_log` mediumtext COMMENT '启动时本地日志',
                               `run_ip` varchar(64) DEFAULT NULL COMMENT '任务运行所在的机器',
                               `remote_log_url` varchar(128) DEFAULT NULL COMMENT '远程日志url的地址',
                               `start_time` datetime DEFAULT NULL COMMENT '启动时间',
                               `end_time` datetime DEFAULT NULL COMMENT '启动时间',
                               `job_status` varchar(32) DEFAULT NULL COMMENT '任务状态',
                               `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                               `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                               `creator` varchar(32) DEFAULT 'sys',
                               `editor` varchar(32) DEFAULT 'sys',
                               `tenant_id` varchar(32) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='运行任务日志';

DROP TABLE IF EXISTS `savepoint_backup`;
CREATE TABLE `savepoint_backup` (
                                    `id` varchar(128) NOT NULL,
                                    `job_config_id` varchar(128) NOT NULL,
                                    `savepoint_path` varchar(2048) NOT NULL COMMENT '地址',
                                    `backup_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '备份时间',
                                    `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                                    `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                    `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                    `creator` varchar(32) DEFAULT 'sys',
                                    `editor` varchar(32) DEFAULT 'sys',
                                    `tenant_id` varchar(32) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `index` (`job_config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='savepoint备份地址';

DROP TABLE IF EXISTS `etl_python_job`;
CREATE TABLE `etl_python_job` (
                                  `id` varchar(32) NOT NULL COMMENT '主键 id',
                                  `name` varchar(32) NOT NULL COMMENT '任务名称',
                                  `schedule` varchar(255) DEFAULT NULL COMMENT '调度表达式',
                                  `script` longtext NOT NULL COMMENT '脚本内容',
                                  `status` tinyint(2) NOT NULL COMMENT '状态',
                                  `pid` int(11) DEFAULT NULL COMMENT '进程 pid',
                                  `remark` varchar(64) DEFAULT NULL COMMENT '任务描述',
                                  `tenant_id` varchar(32) NOT NULL COMMENT '租户 id',
                                  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `etl_resource`;
CREATE TABLE `etl_resource` (
                                `id` varchar(32) NOT NULL COMMENT '主键 id',
                                `name` varchar(32) NOT NULL COMMENT '资源名称',
                                `file_name` varchar(64) NOT NULL COMMENT '文件名',
                                `url` varchar(526) NOT NULL COMMENT '文件路径',
                                `remark` varchar(64) DEFAULT NULL COMMENT '资源描述',
                                `tenant_id` varchar(32) NOT NULL COMMENT '租户 id',
                                `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;