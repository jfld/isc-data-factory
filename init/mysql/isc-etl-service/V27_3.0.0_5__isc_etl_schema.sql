USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `job_config` ADD (
	`source_id` VARCHAR ( 255 ) DEFAULT NULL COMMENT '数据源ID',
	`source_database` VARCHAR ( 255 ) DEFAULT NULL COMMENT '源数据库',
	`target_id` VARCHAR ( 255 ) DEFAULT NULL COMMENT '目标数据源ID',
	`target_database` VARCHAR ( 255 ) DEFAULT NULL COMMENT '目标数据库',
	`target_table` VARCHAR ( 255 ) DEFAULT NULL COMMENT '目标数据表',
    `row_sql` text COMMENT '原生SQL',
    `mappings` text COMMENT '字段映射信息'
);

SET FOREIGN_KEY_CHECKS = 1;