USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `job_config`;
CREATE TABLE `job_config` (
                              `id` varchar(128) NOT NULL,
                              `job_name` varchar(64) NOT NULL COMMENT '任务名称',
                              `deploy_mode` varchar(64) NOT NULL DEFAULT '' COMMENT '提交模式: standalone 、yarn 、yarn-session ',
                              `flink_run_config` varchar(512) NOT NULL DEFAULT '' COMMENT 'flink运行配置',
                              `flink_sql` mediumtext NOT NULL COMMENT 'sql语句',
                              `flink_checkpoint_config` varchar(512) DEFAULT NULL COMMENT 'checkPoint配置',
                              `job_id` varchar(64) DEFAULT NULL COMMENT '运行后的任务id',
                              `is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:开启 0: 关闭',
                              `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:运行中 0: 停止中 -1:运行失败',
                              `ext_jar_path` varchar(2048) DEFAULT NULL COMMENT 'udf地址已经连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar',
                              `last_start_time` datetime DEFAULT NULL COMMENT '最后一次启动时间',
                              `last_run_log_id` bigint(11) DEFAULT NULL COMMENT '最后一次日志',
                              `version` int(11) NOT NULL DEFAULT '0' COMMENT '更新版本号 用于乐观锁',
                              `job_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '任务类型 0:sql 1:自定义jar',
                              `custom_args` varchar(128) DEFAULT NULL COMMENT '启动jar可能需要使用的自定义参数',
                              `custom_main_class` varchar(128) DEFAULT NULL COMMENT '程序入口类',
                              `custom_jar_url` varchar(128) DEFAULT NULL COMMENT '自定义jar的http地址 如:http://ccblog.cn/xx.jar',
                              `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                              `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                              `creator` varchar(32) DEFAULT 'sys',
                              `editor` varchar(32) DEFAULT 'sys',
                              `tenant_id` varchar(32) DEFAULT NULL,
                              `cron` varchar(128) DEFAULT NULL,
                              `is_scheduled` varchar(128) DEFAULT NULL COMMENT '调度状态',
                              PRIMARY KEY (`id`),
                              KEY `uk_index` (`job_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='flink任务配置表';

SET FOREIGN_KEY_CHECKS = 1;