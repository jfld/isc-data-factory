USE `isc_data_factory` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
-- 2022-03-25
-- ETL相关表注释补充，目前data_factory_config，etl_resource，job_config，job_run_log四表有用，其他表皆为弃用表
ALTER TABLE `isc_data_factory`.`data_factory_config` COMMENT = '微服务配置表\n目前用于服务初始化连接器的标识';

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务配置 ID' FIRST;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `deploy_mode` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '提交模式: STANDALONE  LOCAL' AFTER `job_name`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `flink_run_config` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'flink运行配置: -m xxxxx' AFTER `deploy_mode`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `flink_sql` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'flink sql语句' AFTER `flink_run_config`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `flink_checkpoint_config` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'checkPoint配置(弃用)' AFTER `flink_sql`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `job_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'flink 测的任务id' AFTER `flink_checkpoint_config`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '-1, 失败；1, 运行中；0, 未启动；2, 启动中；3, 已完成；4, 已取消' AFTER `is_open`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `ext_jar_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'udf地址连接器jar 如http://xxx.xxx.com/flink-streaming-udf.jar' AFTER `status`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `last_start_time` datetime NULL DEFAULT NULL COMMENT '最后一次启动时间（弃用）' AFTER `ext_jar_path`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `last_run_log_id` bigint NULL DEFAULT NULL COMMENT '最后一次日志（弃用）' AFTER `last_start_time`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `version` int NOT NULL DEFAULT 0 COMMENT '更新版本号 用于乐观锁（弃用）' AFTER `last_run_log_id`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `job_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'SQL_STREAMING(0), JAR(1), SQL_BATCH(2), VIEW(3), ROW_SQL(4)' AFTER `version`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `custom_jar_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'main 方法的jar包地址' AFTER `custom_main_class`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '弃用' AFTER `custom_jar_url`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'sys' COMMENT '弃用' AFTER `updated_at`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `editor` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'sys' COMMENT '弃用' AFTER `creator`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'system' COMMENT '租户ID' AFTER `editor`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `cron` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式' AFTER `tenant_id`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `is_scheduled` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '调度状态0，未调度；1，调度中' AFTER `cron`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `viewable_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '可视化配置json字符串' AFTER `is_scheduled`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `is_checked` int NULL DEFAULT NULL COMMENT '是否通过预校验（0，未通过；1，通过）' AFTER `viewable_config`;

ALTER TABLE `isc_data_factory`.`job_config` MODIFY COLUMN `local_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '弃用' AFTER `is_checked`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `deploy_mode` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '提交模式: standalone 、local' AFTER `job_name`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `run_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务运行所在的机器(弃用)' AFTER `local_log`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `remote_log_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '远程日志url的地址（弃用）' AFTER `run_ip`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `start_time` datetime NULL DEFAULT NULL COMMENT '启动时间（弃用）' AFTER `remote_log_url`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `end_time` datetime NULL DEFAULT NULL COMMENT '启动时间（弃用）' AFTER `start_time`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `job_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务状态INITIALIZING/SUCCESS/FAILED' AFTER `end_time`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '弃用' AFTER `job_status`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'sys' COMMENT '弃用' AFTER `updated_at`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `editor` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'sys' COMMENT '弃用' AFTER `creator`;

ALTER TABLE `isc_data_factory`.`job_run_log` MODIFY COLUMN `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户ID' AFTER `editor`;

SET FOREIGN_KEY_CHECKS = 1;
