-- isc_udmp.data_flow_definition definition

CREATE TABLE IF NOT EXISTS isc_udmp.data_flow_definition (
    `id` bigint(20) NOT NULL,
    `name` varchar(128) NOT NULL COMMENT '任务名称',
    `user_config` mediumtext COMMENT '以JSON格式保存原始的用户配置信息',
    `kettle_definition` mediumtext COMMENT '根据用户配置信息解析后的kettle的xml配置文件',
    `scheduled` int(1) NOT NULL DEFAULT '0' COMMENT '是否定时调度，0否1是',
    `repeat_run` int(1) NOT NULL DEFAULT '0' COMMENT '是否重复运行，0否1是',
    `schedule_start_time` datetime DEFAULT NULL COMMENT '定时调度的开始时间',
    `schedule_interval_time_unit` varchar(64) DEFAULT NULL COMMENT '定时调度的时间间隔单位，包括：year,month,hour,minute',
    `schedule_interval` int(11) DEFAULT NULL COMMENT '定时调度的时间间隔',
    `tenant_id` varchar(255) NOT NULL,
    `created_at` datetime DEFAULT NULL,
    `updated_at` datetime DEFAULT NULL,
    `creator` varchar(255) DEFAULT NULL,
    `updator` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存ETL数据处理流程的相关配置信息';


-- isc_udmp.data_flow_task definition

CREATE TABLE IF NOT EXISTS isc_udmp.data_flow_task (
    `id` bigint(20) NOT NULL,
    `kettle_batch_id` varchar(64) NOT NULL COMMENT 'kettle启动任务时生成的批次ID',
    `definition_id` bigint(20) NOT NULL,
    `status` int(11) NOT NULL COMMENT '任务状态，0：失败  1：运行中  2：已完成',
    `start_time` datetime DEFAULT NULL COMMENT '任务开始时间',
    `end_time` datetime DEFAULT NULL COMMENT '任务结束时间',
    `tenant_id` varchar(64) DEFAULT NULL,
    `trigger_method` int(11) DEFAULT NULL COMMENT '触发方式:1：手动触发，2：自动触发',
    PRIMARY KEY (`id`) USING BTREE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据处理流程的任务实例表';
