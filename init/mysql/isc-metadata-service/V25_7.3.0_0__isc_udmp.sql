create table if not exists isc_udmp.sys_menu (
                            `code` varchar(255) NOT NULL COMMENT '权限编码',
                            `parent_code` varchar(255) DEFAULT NULL COMMENT '父级编码',
                            `name` varchar(32) NOT NULL COMMENT '权限名称',
                            `seq` int(11) DEFAULT NULL COMMENT '序号',
                            `url` varchar(255) NOT NULL COMMENT '访问前端地址',
                            `type` int(2) NOT NULL DEFAULT '1' COMMENT '权限类型： 1.菜单 2.按钮 3.其他',
                            `remark` varchar(255) DEFAULT NULL COMMENT '备注',
                            `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统功能菜单表';

alter table isc_udmp.data_flow_definition add log_size int(11)  comment '日志条数';


INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('data-construct', null, '数据建设', 20, '/data-construct', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('data-factory-new', null, '数据工厂', 10, '/data-factory-new', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('data-setting', 'metadata', '数据源配置', 0, '/data-setting', 1, null, '2022-11-30 18:54:45', '2022-11-30 18:55:01');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('indicator-compute-watch', 'data-construct', '指标计算监控', 30, '/indicator-compute-watch', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('indicator-library', 'data-construct', '指标库', 20, '/indicator-library', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('kettle-log-manage', 'data-factory-new', '日志管理', 10, '/kettle-log-manage', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('kettle-task', 'data-factory-new', '数据任务', 0, '/kettle-task', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('kettle-tool-download', 'data-factory-new', '数据迁移工具', 20, '/kettle-tool-download', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('metadata', null, '元数据管理', 0, '/metadata', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('model-library', 'data-construct', '模型库', 10, '/model-library', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('sql-search', 'data-construct', 'SQL 查询', 0, '/sql-search', 1, null, '2022-11-30 18:51:46', '2022-11-30 18:51:46');
INSERT INTO isc_udmp.sys_menu (code, parent_code, name, seq, url, type, remark, created_at, updated_at) VALUES ('table-manage', 'metadata', '表管理', 10, '/table-manage', 1, null, '2022-11-30 18:54:45', '2022-11-30 18:55:01');