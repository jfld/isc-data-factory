USE `isc_udmp` ;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

UPDATE `data_source` SET `tenant_id` = 'system' WHERE ISNULL(`tenant_id`) OR `tenant_id` = '';
UPDATE `data_source` SET `tenant_id` = 'initially' WHERE `id` = '26';

UPDATE isc_udmp.metric_group SET tenant_id = 'initially' WHERE id =-1;
