create table if not exists isc_udmp.data_flow_group
(
    id          varchar(32)                   not null
        primary key,
    name        varchar(100)                  null comment '分组名',
    parent_id   varchar(32)                   null comment '父分组ID',
    create_time datetime                      null,
    tenant_id   varchar(100) default 'system' null,
    constraint data_flow_group_UN
        unique (parent_id, name)
)
    comment '对etl任务进行分组管理';

replace into isc_udmp.data_flow_group (id, name, parent_id, create_time, tenant_id) values ('0', '默认', null, '2022-10-21 17:57:11', 'system');

alter table isc_udmp.data_flow_definition add group_id varchar(32) null comment '所属分组ID';
alter table isc_udmp.data_flow_definition add description varchar(500) null comment '任务描述';
alter table isc_udmp.data_flow_definition add publish_status varchar(10) null comment '任务发布状态 0 已保存 1 已发布';

update isc_udmp.data_flow_definition set group_id ='0' where group_id is null;
