USE `isc_udmp` ;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

create table IF NOT EXISTS isc_udmp.sql_query
(
    id             bigint       not null comment 'id'
        primary key,
    name           varchar(255) null comment 'sql名称',
    remark         varchar(255) null comment '备注',
    create_time    datetime     null comment '创建时间',
    update_time    datetime     null comment '更新时间',
    tenant_id      varchar(32)  null comment '租户ID',
    sql_text       text         null comment 'SQL文本',
    status         int(1)       null comment '状态量 0 -> 未通过。1 -> 已通过',
    is_private     varchar(32)  null comment '是否为私有',
    group_id       bigint       null comment '分组ID',
    data_source_id bigint       null comment '数据源ID',
    database_name  varchar(255) null,
    constraint id_name
        unique (id, name)
)
    comment 'sql查询表';

create table IF NOT EXISTS isc_udmp.sql_query_group
(
    id          bigint       not null
        primary key,
    name        varchar(255) null,
    tenant_id   varchar(32)  null,
    remark      varchar(255) null,
    type        int(1)       null,
    parent_id   bigint       null,
    create_time datetime     null,
    update_time datetime     null,
    constraint id_name
        unique (id, name)
)
    comment 'sql查询分组表';




INSERT INTO isc_udmp.sql_query_group (id, name, tenant_id, remark, type, parent_id, create_time, update_time)
VALUES (-1, '默认分组', 'initially', '默认分组', 0, null, '1997-01-01 00:00:00', '1997-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
