USE `isc_udmp` ;
-- 修复datamodel_column表的索引错误
drop index index_datamodel_id on isc_udmp.datamodel_column;

create index index_datamodel_id on isc_udmp.datamodel_column
(
   datamodel_ref_id
);