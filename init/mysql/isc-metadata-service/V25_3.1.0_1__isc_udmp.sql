USE `isc_udmp` ;

drop table if exists datamodel;

drop table if exists datamodel_column;

drop table if exists er_relation;

drop table if exists metric;

drop table if exists metric_dim_filter;

drop table if exists metric_group;

drop table if exists metric_job_batch;

drop table if exists metric_job_batch_result;

drop table if exists metric_measure;

drop table if exists sql_statement;

create table datamodel
(
   id                   bigint(20) not null comment '唯一编号',
   tenant_id            varchar(32) comment '租户ID',
   sql_ref_id           bigint(20) comment '用于定义数据模型的数据范围的sql',
   er_ref_id            bigint(20) comment '用于定义数据模型的数据范围的ER关系模型',
   ds_ref_id            bigint(20) comment '该数据模型关联的数据源',
   db_name              varchar(32),
   name                 varchar(32) comment '数据模型的名称',
   description          varchar(512) comment '数据模型的说明信息',
   create_time          datetime comment '创建时间',
   create_type          int(1) comment '1: 通过SQL创建，2：通过ER图创建',
   primary key (id)
);

create index index_tenant_id on datamodel
(
   tenant_id
);

create table datamodel_column
(
   id                   bigint(20) not null,
   datamodel_ref_id     bigint(20) comment '字段所属的数据模型',
   col_name             varchar(64) comment '字段列名',
   col_label            varchar(64) comment '字段展示名',
   col_type             int(1) comment '数据列类型：1：维度列  2：度量列  3: 时间范围',
   col_data_type        varchar(64) comment '字段的数据类型，例如string,integer等等',
   tenant_id            varchar(32) comment '租户ID',
   primary key (id)
);

alter table datamodel_column comment '定义数据模型中包含的数据列';

create index index_datamodel_id on datamodel_column
(
   id
);

create table er_relation
(
   id                   bigint(20) not null,
   er_layout_data       json comment '以JSON格式存储ER图涉及的表字段及相互关系',
   tenant_id            varchar(32) comment '租户ID',
   primary key (id)
);

alter table er_relation comment '存储ER关系';

create table metric
(
   id                   bigint(20) not null,
   tenant_id            varchar(32) comment '租户ID',
   sql_ref_id           bigint(20) comment '指标对应的SQL计算语句',
   group_ref_id         bigint(20) comment '指标分组编号',
   datamodel_ref_id     bigint(20) comment '指标所属的数据模型',
   extend_path          varchar(512) comment '定义指标的派生路径，使用“/”对父指标的code值进行分割，例如：xxx/aaaa/dddd/cccc ',
   name                 varchar(64) comment '指标名称',
   description          varchar(512) comment '指标说明',
   dimension            varchar(64) comment '该指标对应的统计维度列名',
   formula              varchar(1024) comment '该指标对应的计算公式，如果是原子指标，此项为空,例如：(A1+A2)/A3',
   time_period_dim      varchar(64) comment '该指标对应的统计周期字段列名',
   time_period_unit     varchar(16) comment '该指标对应的统计周期时间单位',
   time_period_number   int(11) comment '该指标对应的统计周期跨度',
   task_cron            varchar(64) comment '指标计算任务调度表达式，采用quartz语法',
   create_time          datetime comment '创建时间',
   primary key (id)
);

create index index_tenant_id on metric
(
   tenant_id
);

create index index_extend_path on metric
(
   extend_path
);

create table metric_dim_filter
(
   id                   bigint(20) not null,
   tenant_id            varchar(32) comment '租户ID',
   metric_ref_id        bigint(20) comment '过滤条件对应的指标',
   dim_col_name         varchar(64) comment '维度字段名称',
   dim_condition_value  varchar(256) comment '设定的维度过滤条件值',
   operator             varchar(64) comment '比较操作符，支持equal,between,in,like,lessthan,greaterthan等类型',
   primary key (id)
);

alter table metric_dim_filter comment '保存指标的维度过滤条件,该表仅保存指标自身设定的过滤条件，对于派生指标需要进行级联查询';


create index index_metric_id on metric_dim_filter
(
   metric_ref_id
);

create table metric_group
(
   id                   bigint(20) not null auto_increment,
   group_name           varchar(64),
   tenant_id            varchar(32),
   primary key (id)
);

alter table metric_group comment '数据指标分组';

create table metric_job_batch_result
(
   id                   bigint(20) not null auto_increment,
   tenant_id            varchar(32) comment '租户ID',
   job_batch_id         bigint(20) comment 'job批次任务ID',
   metric_ref_id        bigint(20) comment '结果集对应的指标id',
   dim                  varchar(256),
   measure              varchar(128),
   data_period          varchar(256),
   create_time          datetime,
   primary key (id)
);

create index index_metric_period_id on metric_job_batch_result
(
   tenant_id,
   metric_ref_id,
   data_period
);

create table metric_measure
(
   id                   bigint(20) not null auto_increment,
   metric_ref_id        bigint(20) comment '度量值对应的指标id',
   measure_col_name     varchar(64) comment '度量值对应的字段名',
   measure_agg_type     varchar(16) comment '度量值对应的聚合方式',
   tenant_id            varchar(32) comment '租户ID',
   primary key (id)
);

alter table metric_measure comment '保存指标的所涉及的度量值，以及度量值所对应的聚合方式';

create index index_metric_id on metric_measure
(
   metric_ref_id
);

create table sql_statement
(
   id                   bigint(20) not null,
   encoded_sql          varchar(10000) comment 'sql语句',
   tenant_id            varchar(32) comment '租户ID',
   primary key (id)
);

alter table sql_statement comment '存储数据模型和数据指标对应的SQL语句';

INSERT INTO isc_udmp.metric_group (id,group_name) VALUES (-1,'默认分组');
