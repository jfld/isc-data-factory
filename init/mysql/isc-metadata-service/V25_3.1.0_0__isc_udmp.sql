USE `isc_udmp` ;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


ALTER TABLE `data_source`
    ADD COLUMN `database_config` longtext,
    ADD COLUMN `tenant_id` varchar(255) DEFAULT NULL,
    ADD COLUMN `connect_type` varchar(255) DEFAULT NULL,
    ADD COLUMN `basic_type` varchar(255) DEFAULT NULL,
    ADD COLUMN `basic_value` varchar(255) DEFAULT NULL