ALTER TABLE isc_udmp.data_flow_definition ADD is_incre varchar(5) NULL COMMENT '是否开启增量';
ALTER TABLE isc_udmp.data_flow_definition ADD incr_table varchar(100) NULL COMMENT '增量表';
ALTER TABLE isc_udmp.data_flow_definition ADD incre_field varchar(100) NULL COMMENT '增量字段';
ALTER TABLE isc_udmp.data_flow_definition ADD incre_value varchar(100) NULL COMMENT '当前增量值';
ALTER TABLE isc_udmp.data_flow_definition ADD incre_field_type varchar(100) NULL COMMENT '增量字段类型';


CREATE TABLE isc_udmp.table_alias (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `source_id` varchar(100) DEFAULT NULL COMMENT '数据源id',
                               `table_name` varchar(100) DEFAULT NULL COMMENT '表名称',
                               `alias` varchar(100) DEFAULT NULL COMMENT '表别名',
                               `database_name` varchar(100) DEFAULT NULL COMMENT '数据库',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1569584125019459588 DEFAULT CHARSET=utf8mb4;