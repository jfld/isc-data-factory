USE `isc_udmp` ;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Records of data_source
-- ----------------------------
ALTER TABLE isc_udmp.data_flow_definition ADD retry_run int(1) DEFAULT 0 COMMENT '是否重试运行 0否1是';