package org.pentaho.di.trans.steps.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dc
 * @Type RestParam.java
 * @Desc restParam
 * @date 2022/9/19 10:28
 */
@Data
public class RestParam {
    private String url;
    private String method;
    private List<Info> headers;
    private List<Info> params;
    private String body;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Info {
        private String name;
        private String field;
    }

}
    
