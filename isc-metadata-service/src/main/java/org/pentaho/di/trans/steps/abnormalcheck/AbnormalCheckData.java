package org.pentaho.di.trans.steps.abnormalcheck;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.steps.univariatestats.FieldIndex;

/**
 * @author dc
 * @Type AbnormalCheckData.java
 * @Desc 存储上下节点输出、输入字段
 * @date 2022/10/19 16:29
 */
public class AbnormalCheckData extends BaseStepData implements StepDataInterface {

    public RowMetaInterface outputRowMeta;
    public RowMetaInterface inputRowMeta;
    /**
     * contains the FieldIndexs - one for each UnivariateStatsMetaFunction
     */
    protected AbnormalCheckFunction[] functions;

    public AbnormalCheckData(){
        super();
    }

    public RowMetaInterface getOutputRowMeta() {
        return outputRowMeta;
    }

    public void setOutputRowMeta(RowMetaInterface outputRowMeta) {
        this.outputRowMeta = outputRowMeta;
    }

    public RowMetaInterface getInputRowMeta() {
        return inputRowMeta;
    }

    public void setInputRowMeta(RowMetaInterface inputRowMeta) {
        this.inputRowMeta = inputRowMeta;
    }

    public AbnormalCheckFunction[] getFunctions() {
        return functions;
    }

    public void setFunctions(AbnormalCheckFunction[] functions) {
        this.functions = functions;
    }
}
    
