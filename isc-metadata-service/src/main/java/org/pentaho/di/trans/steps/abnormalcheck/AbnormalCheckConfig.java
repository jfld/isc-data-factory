package org.pentaho.di.trans.steps.abnormalcheck;

import org.apache.commons.lang3.StringUtils;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.metastore.api.IMetaStore;
import org.w3c.dom.Node;

import java.util.Objects;

/**
 * @author dc
 * @Type AbnormalCheckConfig.java
 * @Desc
 * @date 2022/10/19 13:47
 */
public class AbnormalCheckConfig implements Cloneable {
    public static final String XML_TAG = "abnormal_check_config";

    private boolean medianFlag;
    private boolean fixedFlag;
    private boolean meanFlag;

    private String sourceFieldName;
    private String compareSymbols;

    //此处可以是倍数或者是固定值
    private double multiple;

    private String judgeName;


    public AbnormalCheckConfig(boolean medianFlag, boolean fixedFlag, boolean meanFlag, String sourceFieldName,
                               String compareSymbols, double multiple, String judgeName) {
        this.medianFlag = medianFlag;
        this.fixedFlag = fixedFlag;
        this.meanFlag = meanFlag;
        this.sourceFieldName = sourceFieldName;
        this.compareSymbols = compareSymbols;
        this.multiple = multiple;
        this.judgeName = judgeName;
    }

    public AbnormalCheckConfig(Node uniNode) {
        String temp;
        sourceFieldName = XMLHandler.getTagValue(uniNode, "source_field_name");
        temp = XMLHandler.getTagValue(uniNode, "median");
        if ("Y".equalsIgnoreCase(temp)) {
            medianFlag = true;
        }
        temp = XMLHandler.getTagValue(uniNode, "fixed");
        if ("Y".equalsIgnoreCase(temp)) {
            fixedFlag = true;
        }
        temp = XMLHandler.getTagValue(uniNode, "mean");
        if ("Y".equalsIgnoreCase(temp)) {
            meanFlag = true;
        }
        temp = XMLHandler.getTagValue(uniNode, "compare_symbols");
        if (StringUtils.isNotEmpty(temp)) {
            compareSymbols = temp;
        }
        temp = XMLHandler.getTagValue(uniNode, "multiple");
        if (StringUtils.isNotEmpty(temp)) {
            multiple = Double.valueOf(temp);
        } else {
            multiple = 1;
        }
        temp = XMLHandler.getTagValue(uniNode, "judge_name");
        if (StringUtils.isNotEmpty(temp)) {
            judgeName = temp;
        }
    }

    public AbnormalCheckConfig(Repository rep, ObjectId id_step, int nr) throws KettleException {
        sourceFieldName = rep.getStepAttributeString(id_step, nr, "source_field_name");
        meanFlag = rep.getStepAttributeBoolean(id_step, nr, "mean");
        medianFlag = rep.getStepAttributeBoolean(id_step, nr, "median");
        fixedFlag = rep.getStepAttributeBoolean(id_step, nr, "fixed");
        compareSymbols = rep.getStepAttributeString(id_step, nr, "compare_symbols");
        String temp = rep.getStepAttributeString(id_step, nr, "multiple");
        try {
            multiple = Double.valueOf(temp);
        } catch (Exception ex) {
            multiple = 1;
        }
        judgeName = rep.getStepAttributeString(id_step, nr, "judge_name");
    }

    public void saveRep(Repository rep, IMetaStore metaStore, ObjectId id_transformation, ObjectId id_step, int nr) throws KettleException {
        rep.saveStepAttribute(id_transformation, id_step, nr, "source_field_name", sourceFieldName);
        rep.saveStepAttribute(id_transformation, id_step, nr, "mean", meanFlag);
        rep.saveStepAttribute(id_transformation, id_step, nr, "median", medianFlag);
        rep.saveStepAttribute(id_transformation, id_step, nr, "fixed", fixedFlag);
        rep.saveStepAttribute(id_transformation, id_step, nr, "compareSymbols", compareSymbols);
        rep.saveStepAttribute(id_transformation, id_step, nr, "multiple", multiple);
        rep.saveStepAttribute(id_transformation, id_step, nr, "judge_name", judgeName);
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj != null) && (obj.getClass().equals(this.getClass()))) {
            AbnormalCheckConfig mf = (AbnormalCheckConfig) obj;
            return (getXML().equals(mf.getXML()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(medianFlag, fixedFlag, meanFlag, sourceFieldName, compareSymbols, multiple, judgeName);
    }

    public String getXML() {
        String xml = ("<" + XML_TAG + ">");

        xml += XMLHandler.addTagValue("source_field_name", sourceFieldName);
        xml += XMLHandler.addTagValue("mean", meanFlag==true?"Y":"N");
        xml += XMLHandler.addTagValue("median", medianFlag==true?"Y":"N");
        xml += XMLHandler.addTagValue("fixed", fixedFlag==true?"Y":"N");
        xml += XMLHandler.addTagValue("compare_symbols", compareSymbols);
        xml += XMLHandler.addTagValue("multiple", multiple);
        xml += XMLHandler.addTagValue("judge_name", judgeName);
        xml += ("</" + XML_TAG + ">");

        return xml;
    }

    @Override
    public Object clone() {
        try {
            AbnormalCheckConfig retval = (AbnormalCheckConfig) super.clone();
            return retval;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public boolean isMedianFlag() {
        return medianFlag;
    }

    public void setMedianFlag(boolean medianFlag) {
        this.medianFlag = medianFlag;
    }

    public boolean isFixedFlag() {
        return fixedFlag;
    }

    public void setFixedFlag(boolean fixedFlag) {
        this.fixedFlag = fixedFlag;
    }

    public boolean isMeanFlag() {
        return meanFlag;
    }

    public void setMeanFlag(boolean meanFlag) {
        this.meanFlag = meanFlag;
    }

    public String getSourceFieldName() {
        return sourceFieldName;
    }

    public void setSourceFieldName(String sourceFieldName) {
        this.sourceFieldName = sourceFieldName;
    }

    public String getCompareSymbols() {
        return compareSymbols;
    }

    public void setCompareSymbols(String compareSymbols) {
        this.compareSymbols = compareSymbols;
    }

    public double getMultiple() {
        return multiple;
    }

    public void setMultiple(float multiple) {
        this.multiple = multiple;
    }

    public String getJudgeName() {
        return judgeName;
    }

    public void setJudgeName(String judgeName) {
        this.judgeName = judgeName;
    }
}
    
