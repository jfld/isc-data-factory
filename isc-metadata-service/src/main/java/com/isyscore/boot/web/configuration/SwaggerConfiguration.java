//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.isyscore.boot.web.configuration;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.isyscore.boot.web.controller.SystemEnumController;
import com.isyscore.device.common.util.Joiners;
import com.isyscore.device.common.util.Splitters;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.swagger2.web.Swagger2Controller;

@Configuration
@EnableSwagger2
@ConditionalOnClass({Docket.class, Swagger2Controller.class})
@ConditionalOnProperty(
        prefix = "isyscore.web",
        name = {"swagger-scan-package"}
)
@Profile({"local"})
@EnableConfigurationProperties({IsyscoreWebProperties.class})
public class SwaggerConfiguration {
    public SwaggerConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean
    public Docket docket(IsyscoreWebProperties isyscoreWebProperties, @Autowired(required = false) ApiInfo apiInfo) {
        List<String> pack = new ArrayList(isyscoreWebProperties.getSwaggerScanPackage());
        pack.add(SystemEnumController.class.getPackage().getName());
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        if (apiInfo != null) {
            docket.apiInfo(apiInfo);
        }

        return docket.select().apis(basePackage(pack)).paths(PathSelectors.any()).build();
    }

    private static Predicate<RequestHandler> basePackage(List<String> pack) {
        String basePackage = Joiners.SEMICOLON.join(pack);
        return (input) -> {
            return (Boolean)declaringClass(input).transform(handlerPackage(basePackage)).or(true);
        };
    }

    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage) {
        return (input) -> {
            Iterator var2 = Splitters.SEMICOLON.split(basePackage).iterator();

            boolean isMatch;
            do {
                if (!var2.hasNext()) {
                    return false;
                }

                String strPackage = (String)var2.next();
                isMatch = input.getPackage().getName().startsWith(strPackage);
            } while(!isMatch);

            return true;
        };
    }

    private static Optional<? extends Class<?>> declaringClass(RequestHandler input) {
        return Optional.fromNullable(input.declaringClass());
    }
}
