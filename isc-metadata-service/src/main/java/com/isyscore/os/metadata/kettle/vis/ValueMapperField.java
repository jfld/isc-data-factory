package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ValueMapperField {
    @ApiModelProperty("源值")
    private String sourceValue;
    @ApiModelProperty("目标值")
    private String targetValue;
}
