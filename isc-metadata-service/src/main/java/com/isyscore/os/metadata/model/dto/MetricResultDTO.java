package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetricResultDTO {

    @ApiModelProperty("统计维度值")
    private String dimension;

    @ApiModelProperty("指标度量值")
    private String measure;

    @ApiModelProperty("该指标对应的统计时间周期")
    private String dataPeriod;

    @ApiModelProperty("该指标的计算时间")
    private LocalDateTime createTime;

}
