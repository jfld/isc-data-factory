package com.isyscore.os.metadata.model.vo;

import com.isyscore.os.metadata.model.dto.ColumnIndexDTO;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 针对于 openAPI {@link }接口返参进行定制的结构体
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2022/2/17 1:56 下午
 */
@Data
public class DataRecordVo {
    private List<ColumnIndexDTO> columns;

    private List<Map<String, Object>> records;

    private Integer current;

    private Integer size;

    private Long total;
}
