package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class GetInputOutputFieldsDTO {

    @ApiModelProperty("任务流中的所有节点配置")
    private List<Map<String, Object>> nodes;

    @ApiModelProperty("任务流中的所有边配置")
    private List<Map<String, Object>> edges;

    @ApiModelProperty("当前节点名")
    private String stepName;

    @ApiModelProperty("输入或输出节点名")
    private String fromOrToStepName;

    @ApiModelProperty("true表示获得输入字段：false表示获得输出字段")
    private Boolean before;

}
