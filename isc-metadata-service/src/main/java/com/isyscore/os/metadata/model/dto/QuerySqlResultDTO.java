package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class QuerySqlResultDTO {

    @ApiModelProperty("查询的结果")
    private List<Map<String, Object>> results;

    @ApiModelProperty("总记录数")
    private Long total;

}
