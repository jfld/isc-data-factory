package com.isyscore.os.metadata.kettle.base;


import com.isyscore.os.metadata.kettle.vis.VisNode;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class FlowConfig {
    private FlowInfo transInfo;
    private List<FlowConnection> transConnections;
    private List<FlowHup> transHups;
    private List<VisNode> steps;
    private String otherInfo;
    private Map<String,String> otherParam = new HashMap<>();

    public VisNode getNodeByName(String name){
       return steps.stream().filter(s->s.getNodeName().equals(name)).findFirst().get();
    }

    public VisNode getPrNodeByName(String name){
        return steps.stream().filter(s->s.getNodeName().equals(name)).findFirst().get();
    }
}
