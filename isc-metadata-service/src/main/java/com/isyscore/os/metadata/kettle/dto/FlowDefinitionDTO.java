package com.isyscore.os.metadata.kettle.dto;

import com.isyscore.os.core.model.entity.DataFlowDefinition;
import com.isyscore.os.metadata.kettle.vis.VisGraph;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class FlowDefinitionDTO {

    @ApiModelProperty("任务流ID")
    private String definitionId;

    @ApiModelProperty("任务流名称")
    @NotEmpty(message = "任务流名称不能为空")
    private String flowName;

    @ApiModelProperty("是否开启定时调度")
    private Boolean scheduled;

    @ApiModelProperty("是否重复运行")
    private Boolean repeatRun;

    @ApiModelProperty("定时调度的起始时间")
    private LocalDateTime scheduleStartTime;

    @ApiModelProperty("定时调度的间隔时间")
    private Integer scheduleInterval;

    @ApiModelProperty("定时调度的间隔时间单位,包含year,month,day,hour,minute")
    private String scheduleIntervalTimeUnit;

    @ApiModelProperty("流程图配置信息")
    @NotNull(message = "流程图配置信息不能为空")
    private VisGraph userConfig;

    @ApiModelProperty("创建时间")
    private LocalDateTime createdAt;

    @ApiModelProperty("创建人账户名")
    private String creator;

    @ApiModelProperty("修改时间")
    private LocalDateTime updatedAt;

    @ApiModelProperty("修改人账户名")
    private String updator;

    @ApiModelProperty("最后运行时间")
    private LocalDateTime lastRunTime;


    @ApiModelProperty("最后运行时长(s)")
    private Long lastRunDuration;

    @ApiModelProperty("运行状态,0 失败，1运行中，2成功")
    private Integer status;

    @ApiModelProperty("任务说明")
    private String description;
    @ApiModelProperty("分组名")
    private String groupName;
    @ApiModelProperty("分组ID")
    private String groupId;
    @ApiModelProperty("发布状态 0 已保存 1 已发布")
    private String publishStatus;

    @ApiModelProperty("日志大小")
    private Integer logSize;

    @ApiModelProperty("是否开启任务重试")
    private Integer retryRun;

    public void valueOf(DataFlowDefinition dataFlowDefinition) {
        this.setDefinitionId(String.valueOf(dataFlowDefinition.getId()));
        this.setFlowName(dataFlowDefinition.getName());
        this.setScheduled(dataFlowDefinition.getScheduled().equals(1));
        this.setRepeatRun(dataFlowDefinition.getRepeatRun().equals(1));
        this.setScheduleStartTime(dataFlowDefinition.getScheduleStartTime());
        this.setScheduleInterval(dataFlowDefinition.getScheduleInterval());
        this.setScheduleIntervalTimeUnit(dataFlowDefinition.getScheduleIntervalTimeUnit());
        VisGraph visGraph = new VisGraph();
        visGraph.setRenderConfig(dataFlowDefinition.getUserConfig());
        this.setUserConfig(visGraph);
        this.setCreatedAt(dataFlowDefinition.getCreatedAt());
        this.setCreator(dataFlowDefinition.getCreator());
        this.setLogSize(dataFlowDefinition.getLogSize());
        this.setRetryRun(dataFlowDefinition.getRetryRun());
    }
}
