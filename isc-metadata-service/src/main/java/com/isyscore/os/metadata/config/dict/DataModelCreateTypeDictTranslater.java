package com.isyscore.os.metadata.config.dict;

import com.isyscore.os.metadata.common.dict.CachedDictTranslater;

public class DataModelCreateTypeDictTranslater extends CachedDictTranslater {

    @Override
    public String doGetLabelByCode(String dictType, Object dictCode) {
        if (dictCode == null) {
            return "";
        } else if (Integer.valueOf(dictCode.toString()) == 1) {
            return "SQL";
        } else if (Integer.valueOf(dictCode.toString()) == 2) {
            return "ER图";
        }
        return "未知";
    }

}
