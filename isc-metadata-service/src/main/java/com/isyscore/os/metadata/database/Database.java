package com.isyscore.os.metadata.database;

import com.isyscore.os.metadata.enums.DataSourceTypeEnum;
import com.isyscore.os.metadata.manager.DataSourceManager;

import java.util.Map;
import java.util.Set;

public interface Database {

    String escapeTbName(String originTableName, String databaseName);

    String escapeDbName(String databaseName);

    String escapeColName(String originColName);

    String withOutEscapeColName(String escapeColName);
    String withOutEscapeTableName(String escapeTableName);

    String escapeColAliasName(String originAliasName);

    /**
     * 数据库是否支持该时间类型
     *
     * @param colDataType 时间类型
     * @return
     */
    boolean isLegalDateType(String colDataType);

    /**
     * 数据库支持的时间类型
     *
     * @return
     */
    Set<String> getLegalDateTypes();

    /**
     * 时间转换函数
     *
     * @param dateVal
     * @return
     */
    String getParseStr2DateEl(String dateVal);

    String getParseDate2StrEl(String dateVal);

    String getParseStr2IntEl(String dateVal);

    /**
     * 数据库支持的字段类型
     *
     * @return
     */
    Map<String, String> getColumnType();

    /**
     * 获取url
     *
     * @param ip
     * @param port
     * @param dbName
     * @param basicType  oracle,需要判断basic下的 sid 还是 servicename
     * @param basicValue ServiceName,SID的值
     * @return
     * @see DataSourceManager#getDataSource(com.isyscore.os.metadata.model.dto.DataSourceDTO)
     */
    String jdbcUrl(String ip, int port, String dbName, String basicType, String basicValue);

    /**
     * sql: 获取表结构
     *
     * @param tableName 表名
     * @param dbName    数据库名
     * @return sql
     */
    String tableStructSql(String tableName, String dbName);

    /**
     * sql: 获取指定数据库下表列表
     *
     * @param dbName 数据库名
     * @return sql
     */
    String tableListSql(String dbName);

    /**
     * sql: 获取指定数据库下关联表
     *
     * @param columns
     * @param dbName  数据库名
     * @return sql
     */
    String linkTableSql(String columns, String dbName);

    /**
     * sql: 重命名表
     *
     * @param oldTableName 旧表名
     * @param newTableName 新表名
     * @return sql
     */
    String renameTableSql(String oldTableName, String newTableName);

    /**
     * sql: 分页查询
     *
     * @return sql
     */
    String pageSql(String sql, int offset, int limit);

    /**
     * sql: 查询总数
     *
     * @param sql 原sql
     * @return sql
     */
    String countSql(String sql);

    /**
     * sql: 删除表
     *
     * @param tableName 表名
     * @return
     */
    String dropTableSql(String tableName);

    /**
     * 返回当前数据源的类型
     */
    DataSourceTypeEnum getDbType();

    String getQuote();

    /**
     * sql: 获取复制表结构sql
     *
     * @param fromTable 来源表
     * @param newTable  目标表
     * @return sql
     */
    String copyStructSql(String fromTable, String newTable);
}
