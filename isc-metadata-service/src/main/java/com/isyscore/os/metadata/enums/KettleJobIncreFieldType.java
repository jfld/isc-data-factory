package com.isyscore.os.metadata.enums;

import lombok.Getter;

/**
 * 增量任务字段类型
 * @author liy
 */

@Getter
public enum KettleJobIncreFieldType {
    number("number", "数字"),
    time("time", "时间");




    private String code;

    private String desc;

    KettleJobIncreFieldType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static KettleJobIncreFieldType get(String name) {
        return KettleJobIncreFieldType.valueOf(name);
    }

}
