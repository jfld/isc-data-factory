package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.metadata.model.entity.TableAlias;

import java.util.Map;

public interface TableAliasService extends IService<TableAlias> {


    void setTableAlias(Long dataSourceId, String databaseName, String tableName, String alias);
    Map<String,String> getAllAliasCache();
}
