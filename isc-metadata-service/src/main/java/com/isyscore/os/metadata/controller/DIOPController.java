package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.exception.ErrorCode;
import com.isyscore.os.metadata.model.dto.diop.DiopDataTypeRes;
import com.isyscore.os.metadata.model.dto.diop.DiopExportReq;
import com.isyscore.os.metadata.model.dto.diop.DiopImportReq;
import com.isyscore.os.metadata.model.dto.diop.DiopResourceDto;
import com.isyscore.os.metadata.service.diop.DiopApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("${api-full-prefix}/diop")
@Api(tags = "DIOP")
public class DIOPController {

    @Autowired
    private DiopApiService diopApiService;

    @ApiOperation("（内部API）DIOP导出")
    @PostMapping("/export")
    public RespDTO<?> exportResources(HttpServletRequest req, @Validated @RequestBody DiopExportReq diopExportReq) {
        String token = req.getHeader("token");
        boolean success = diopApiService.exportResources(token, diopExportReq.getDataTypes(), diopExportReq.getCallbackUrl());
        if (success) {
            return RespDTO.onSuc(success);
        } else {
            return RespDTO.onFail(ErrorCode.TASK_EXIST.getCode(), ErrorCode.TASK_EXIST.getMessage());
        }
    }

    @ApiOperation("（内部API）DIOP导入")
    @PostMapping("/import")
    public RespDTO<?> importResources(HttpServletRequest req, @Validated @RequestBody DiopImportReq diopImportReq) {
        String token = req.getHeader("token");
        boolean success = diopApiService.importResource(token, diopImportReq.getDirPath(), diopImportReq.getCallbackUrl());
        if (success) {
            return RespDTO.onSuc(success);
        } else {
            return RespDTO.onFail(ErrorCode.TASK_EXIST.getCode(), ErrorCode.TASK_EXIST.getMessage());
        }
    }

    @ApiOperation("（内部API）DIOP获取动态资源")
    @GetMapping("/dataTypes")
    public RespDTO<DiopDataTypeRes> getDataTypes(HttpServletRequest req) {
        List<DiopResourceDto> resources = diopApiService.getDIOPResources();
        DiopDataTypeRes res = new DiopDataTypeRes();
        res.setDataTypes(resources);
        return RespDTO.onSuc(res);
    }

}
