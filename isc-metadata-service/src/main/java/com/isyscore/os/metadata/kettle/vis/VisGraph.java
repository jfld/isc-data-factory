package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class VisGraph {

    @ApiModelProperty("任务流中的所有节点配置")
    private List<Map<String, Object>> nodes;

    @ApiModelProperty("任务流中的所有边配置")
    private List<Map<String, Object>> edges;

    private Map<String,String> otherParam;

    @ApiModelProperty("用于前端页面渲染的相关配置信息")
    private String renderConfig;

}
