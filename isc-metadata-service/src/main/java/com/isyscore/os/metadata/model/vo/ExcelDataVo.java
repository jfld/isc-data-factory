package com.isyscore.os.metadata.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class ExcelDataVo {
    /**
     * 表ID
     */
    private Long tableId;
    /**
     * 表名
     */
    @ApiModelProperty("excel表名")
    private String tableName;
    /**
     * 表别名
     */
    @ApiModelProperty("表别名")
    private String aliasName;
    /**
     * 表头信息
     */
    @ApiModelProperty("表头信息")
    private List<ColumnInfoVo> columnInfoList;
    /**
     * 数据列表
     */
    private List<Object[]> columnValueList;

    /**
     * 写入数据的方式: new / clear /append
     */
    @NotNull(message = "写入数据方式不允许为空")
    @ApiModelProperty("写入数据的方式: NEW / CLEAR /APPEND")
    private String method;

    @NotNull
    @ApiModelProperty("数据源ID")
    private Long dataSourceId;

    @NotNull
    @ApiModelProperty("数据库名称")
    private String databaseName;

    @NotNull
    @ApiModelProperty("需要导入的目标表")
    private String dataTableName;

}
