package com.isyscore.os.metadata.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.isyscore.os.metadata.common.CommonService;
import com.isyscore.os.metadata.dao.MetricJobBatchMapper;
import com.isyscore.os.metadata.model.entity.MetricJobBatch;
import org.springframework.stereotype.Service;

import static com.isyscore.os.metadata.constant.CommonConstant.DS_CK_METADATA;

/**
 * <p>
 * 记录指标每一次计算的批次信息 服务实现类
 * </p>
 *
 * @author wany
 * @since 2021-08-11
 */
@Service
@DS(DS_CK_METADATA)
public class MetricJobBatchService extends CommonService<MetricJobBatchMapper, MetricJobBatch> {
}
