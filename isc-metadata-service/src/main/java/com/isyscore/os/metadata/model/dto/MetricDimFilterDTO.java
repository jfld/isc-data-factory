package com.isyscore.os.metadata.model.dto;

import com.isyscore.os.metadata.common.dict.AutoGenDictLabel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

import static com.isyscore.os.metadata.config.dict.DictTypeConst.FILTER_OPERATOR;

@Data
public class MetricDimFilterDTO {

    @ApiModelProperty("条件字段名称")
    @NotEmpty
    private String dimColName;

    @ApiModelProperty("设定的维度过滤条件值")
    @NotEmpty
    private String dimConditionValue;

    @ApiModelProperty("比较操作符，支持lte，gte，eq，neq，like，in")
    @AutoGenDictLabel(FILTER_OPERATOR)
    @NotEmpty
    private String operator;

}
