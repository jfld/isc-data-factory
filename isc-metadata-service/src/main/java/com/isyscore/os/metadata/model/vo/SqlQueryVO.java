package com.isyscore.os.metadata.model.vo;

import com.isyscore.os.core.entity.SqlQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SqlQueryVO extends SqlQuery {
    @ApiModelProperty(value = "分组名称")
    private String groupName;
    @ApiModelProperty(value = "数据源名称")
    private String dataSourceName;
    @ApiModelProperty(value = "数据源类型")
    private Integer dataSourceType;
}
