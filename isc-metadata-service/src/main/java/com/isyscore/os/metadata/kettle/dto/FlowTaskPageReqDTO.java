package com.isyscore.os.metadata.kettle.dto;

import com.isyscore.boot.mybatis.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FlowTaskPageReqDTO extends PageRequest {

    @ApiModelProperty("查询参数，流程名称")
    private String flowName;

    @ApiModelProperty("查询参数，流程ID")
    private String definitionId;

    @ApiModelProperty("查询参数,任务状态：0：失败  1：运行中  2：已完成")
    private Integer status;

    @ApiModelProperty("查询参数，任务运行时间-起")
    private LocalDateTime startTime;

    @ApiModelProperty("查询参数，任务运行时间-止")
    private LocalDateTime endTime;

}
