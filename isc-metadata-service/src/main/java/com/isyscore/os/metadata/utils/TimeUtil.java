package com.isyscore.os.metadata.utils;

import cn.hutool.core.util.StrUtil;
import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.core.exception.ErrorCode;

import java.time.LocalDateTime;

/**
 * @author wuwx
 */
public class TimeUtil {

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY = "day";
    public static final String HOUR = "hour";
    public static final String MINUTE = "minute";


    /**
     * 获取cron表达式
     *
     * @param interval         时间间隔
     * @param intervalTimeUnit 时间间隔表达式
     * @return cron表达式
     */
    public static String  getCronExpression(Integer interval, String intervalTimeUnit, LocalDateTime startTime){
        if (StrUtil.containsAny(intervalTimeUnit, YEAR, MONTH, DAY,HOUR, MINUTE)) {
            if(YEAR.equals(intervalTimeUnit)){
                return CronUtils.everyYear(startTime,interval);
            }else if(MONTH.equals(intervalTimeUnit)){
                return CronUtils.everyMonth(startTime,interval);
            }else if(DAY.equals(intervalTimeUnit)){
                return CronUtils.everyDay(startTime,interval);
            }else if(HOUR.equals(intervalTimeUnit)){
                return CronUtils.everyHour(startTime,interval);
            }else if(MINUTE.equals(intervalTimeUnit)){
                return CronUtils.everyMinute(startTime,interval);
            }
        } else {
            throw new DataFactoryException(ErrorCode.KETTLE_ETL_ERROR_TIME_UNIT);
        }
        return "";
    }
}
