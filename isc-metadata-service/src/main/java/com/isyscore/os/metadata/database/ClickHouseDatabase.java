package com.isyscore.os.metadata.database;

import com.isyscore.os.metadata.enums.DataSourceTypeEnum;

/**
 * @Description :
 * @Author: qkc
 * @Date: 2021/9/29 11:35
 */
public class ClickHouseDatabase extends AbstractDatabase {

    {
        dbType = DataSourceTypeEnum.CLICKHOUSE;
        //时间类型
        DATE_TYPE.add("DATE");
        DATE_TYPE.add("DATETIME");
        DATE_TYPE.add("DATETIME64");

        //字段类型
        COLUMN_TYPE.put("String", "String");
        COLUMN_TYPE.put("Int8", "Int8");
        COLUMN_TYPE.put("Int16", "Int16");
        COLUMN_TYPE.put("Int32", "Int32");
        COLUMN_TYPE.put("Int64", "Int64");
        COLUMN_TYPE.put("UInt8", "UInt8");
        COLUMN_TYPE.put("UInt16", "UInt16");
        COLUMN_TYPE.put("UInt32", "UInt32");
        COLUMN_TYPE.put("UInt64", "UInt64");
        COLUMN_TYPE.put("Float32", "Float32");
        COLUMN_TYPE.put("Float64", "Float64");
        COLUMN_TYPE.put("Date", "Date");
        COLUMN_TYPE.put("DateTime", "DateTime");
    }

    @Override
    public String getParseStr2DateEl(String dateVal) {
        return "toDateTime64(" + dateVal + ",3,'Asia/Shanghai')";
    }

    @Override
    public String getParseDate2StrEl(String dateVal) {
        return null;
    }

    @Override
    public String getParseStr2IntEl(String dateVal) {
        return "toInt32("+dateVal+")";
    }

    @Override
    public String jdbcUrl(String ip, int port, String dbName, String basicType, String basicValue) {
        return String.format(DataSourceTypeEnum.CLICKHOUSE.getUrl(), ip, port, dbName);
    }

    @Override
    public String tableStructSql(String tableName, String dbName) {
        String SELECT_TABLE_COL = "SELECT " +
                "  CASE " +
                "WHEN LENGTH(comment) > 0 THEN " +
                "  comment " +
                "ELSE " +
                "  name " +
                "END AS columnComment, " +
                " replaceRegexpAll ( " +
                "  `type`, " +
                "  'Nullable\\\\\\\\((.*)\\\\\\\\)', " +
                "  '\\\\\\\\1' " +
                ") AS columnType, " +
                " replaceRegexpAll ( " +
                "  `type`, " +
                "  'Nullable\\\\\\\\((.*)\\\\\\\\)', " +
                "  '\\\\\\\\1' " +
                ") AS dataType, " +
                " name AS columnName, " +
                " name AS oldName, " +
                " is_in_primary_key AS columnKey, " +
                " is_in_sorting_key AS isOrderCol, " +
                " table AS tableName " +
                "FROM " +
                "  `system`.columns " +
                "WHERE " +
                "  `table` = '%s' " +
                "AND database = '%s'";
        return String.format(SELECT_TABLE_COL, tableName, dbName);
    }

    @Override
    public String tableListSql(String dbName) {
        String SELECT_TABLE = "SELECT " +
                "  name AS tableName " +
                "FROM " +
                "  `system`.tables t " +
                "WHERE " +
                "  `system`.tables.database = '%s'";
        return String.format(SELECT_TABLE, dbName);
    }

    @Override
    public String linkTableSql(String columns, String dbName) {
        String SELECT_TABLE_COLUMN_LINK = "SELECT " +
                "  CASE " +
                "WHEN LENGTH(comment) > 0 THEN " +
                "  comment " +
                "ELSE " +
                "  name " +
                "END AS columnComment, " +
                " `type` AS columnType, " +
                " name AS columnName, " +
                " is_in_primary_key AS columnKey, " +
                " table AS tableName " +
                "FROM " +
                "  `system`. columns " +
                "WHERE " +
                "  table IN ( " +
                "    select " +
                "      table " +
                "    from " +
                "      `system`.columns " +
                "    where " +
                "      name in (%s) " +
                "  ) " +
                "and database = '%s'";
        return String.format(SELECT_TABLE_COLUMN_LINK, columns, dbName);
    }

    @Override
    public String renameTableSql(String oldTableName, String newTableName) {
        String RENAME_TABLE = "RENAME TABLE `%s` to `%s`";
        return String.format(RENAME_TABLE, oldTableName, newTableName);
    }

    @Override
    public String dropTableSql(String tableName) {
        String DROP_TABLE = "DROP table %s";
        return String.format(DROP_TABLE, tableName);
    }

    @Override
    public String copyStructSql(String fromTable, String newTable) {
        String sql = "create table %s as %s";
        return String.format(sql,newTable,fromTable);
    }

    public static void main(String[] args) {
        String SELECT_TABLE_COL = "SELECT " +
                "  CASE " +
                "WHEN LENGTH(comment) > 0 THEN " +
                "  comment " +
                "ELSE " +
                "  name " +
                "END AS columnComment, " +
                " replaceRegexpAll ( " +
                "  `type`, " +
                "  'Nullable\\\\\\\\((.*)\\\\\\\\)', " +
                "  '\\\\\\\\1' " +
                ") AS columnType, " +
                " replaceRegexpAll ( " +
                "  `type`, " +
                "  'Nullable\\\\\\\\((.*)\\\\\\\\)', " +
                "  '\\\\\\\\1' " +
                ") AS dataType, " +
                " name AS columnName, " +
                " name AS oldName, " +
                " is_in_primary_key AS columnKey, " +
                " is_in_sorting_key AS isOrderCol, " +
                " table AS tableName " +
                "FROM " +
                "  `system`.columns " +
                "WHERE " +
                "  `table` = '%s' " +
                "AND database = '%s'";
        System.out.println(String.format(SELECT_TABLE_COL, "kettle_step_log_local2", "isc_metadata"));
    }
}
