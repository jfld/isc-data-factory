package com.isyscore.os.metadata.model.dto.diop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DiopDataTypeRes {

    @ApiModelProperty("可导出的动态列表")
    private List<DiopResourceDto> dataTypes;

    @ApiModelProperty("占位符")
    private List<String> placeHolders;

}
