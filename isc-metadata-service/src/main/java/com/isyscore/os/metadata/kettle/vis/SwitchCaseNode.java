package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.pentaho.di.trans.steps.switchcase.SwitchCaseTarget;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dc
 * @Type SwitchCaseNode.java
 * @Desc switch-case
 * @date 2022/10/20 15:31
 */
@Data
@ApiModel("switch-case组件")
public class SwitchCaseNode extends VisNode {

    @ApiModelProperty("字段名称")
    private String fieldName;
    @ApiModelProperty("默认分支名称")
    private String defaultTargetStepName;
    @ApiModelProperty("case分组")
    private List<SwitchCaseTarget> caseTargets;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        List<SwitchCaseTarget> caseTargets = jsonObject.getJSONArray("caseTargets").toJavaList(SwitchCaseTarget.class);
        this.caseTargets=caseTargets;
        this.fieldName=jsonObject.getString("fieldName");
        this.defaultTargetStepName=jsonObject.getString("defaultTargetStepName");
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.SwitchCase;
    }


}
    
