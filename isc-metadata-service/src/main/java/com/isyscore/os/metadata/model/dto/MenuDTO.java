package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.util.List;


/**
 * 菜单信息【权限模块使用】
 *
 */
@Data
public class MenuDTO {
    /**
     * 应用编号
     */
    private String appCode;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用菜单树
     */
    private List<BusinessSysMenuDTO> acls;


}
