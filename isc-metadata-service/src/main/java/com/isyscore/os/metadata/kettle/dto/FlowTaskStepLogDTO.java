package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FlowTaskStepLogDTO {

    @ApiModelProperty("步骤名称")
    private String stepName;

    @ApiModelProperty("记录时间")
    private LocalDateTime logDate;

    @ApiModelProperty("读记录条数")
    private Long readLines;

    @ApiModelProperty("写记录条数")
    private Long writtenLines;

    @ApiModelProperty("输入记录条数")
    private Long inputLines;

    @ApiModelProperty("输出记录条数")
    private Long outputLines;

    @ApiModelProperty("舍弃的记录条数")
    private Long rejectedLines;

    @ApiModelProperty("错误数")
    private Long errors;

}
