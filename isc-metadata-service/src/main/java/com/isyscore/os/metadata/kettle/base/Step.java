package com.isyscore.os.metadata.kettle.base;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;

import java.util.List;

public interface Step {

    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta,FlowConfig transGraph) throws Exception;

    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph)  throws Exception;
}
