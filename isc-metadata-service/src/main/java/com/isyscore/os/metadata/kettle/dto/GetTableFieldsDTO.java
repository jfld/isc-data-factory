package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GetTableFieldsDTO {

    @ApiModelProperty("数据源ID")
    private Long dataSourceId;

    @ApiModelProperty("数据库名")
    private String databaseName;

    @ApiModelProperty("表名")
    private String tableName;

}
