package com.isyscore.os.metadata.config;

import com.isyscore.os.metadata.service.DataSourceService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author wuwx
 */
@Slf4j
@Component
@Data
public class LicenseLimitSupport {
    @Resource
    private DataSourceService dataSourceService;

    /**
     * 是否开启license检查
     */
    private Boolean enableLicenseCheck;

    private Integer dataSourceLimit;

    /**
     * 检查数据数据源是否达到限制，没有达到返回true,否则返回false
     * @return
     */
    public Boolean checkDataSourceLimit(){
        if(enableLicenseCheck){
            Integer currentCount = dataSourceService.getOutDataSourceCount();
            if(currentCount>=dataSourceLimit){
                return Boolean.FALSE;
            }else {
                return Boolean.TRUE;
            }
        }else {
            return Boolean.TRUE;
        }
    }
}
