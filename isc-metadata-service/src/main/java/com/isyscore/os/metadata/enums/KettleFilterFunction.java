package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum KettleFilterFunction {
    lt("2", "小于"),
    gt("4", "大于"),

    lte("3", "小于等于"),
    gte("5", "大于等于"),
    eq("0", "等于"),
    neq("1", "不等于"),
    like("13", "类似于%%"),
    not_null("8", "不为空");

    private String symbol;

    private String label;

    KettleFilterFunction(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;

    }


}
