package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum FormulaSymbol {

    plus("+"), subtract("-"), multiply("*"), divide("/"), left_bracket("("), right_bracket(")");

    private String symbol;

    FormulaSymbol(String symbol) {
        this.symbol = symbol;
    }

}
