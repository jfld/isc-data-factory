package com.isyscore.os.metadata.common.dict;

public interface DictTranslater {

    String getLabelByCode(String dictType, Object dictCode);

}
