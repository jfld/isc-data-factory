package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.metadata.common.page.CompareOperator;
import com.isyscore.os.metadata.common.page.QueryField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MetricPageQuery extends PageRequest {

    @ApiModelProperty("查询条件，数据指标名称，支持模糊查询")
    @QueryField(operator = CompareOperator.LIKE)
    private String name;

    @ApiModelProperty("查询条件，数据指标所对应的模型id")
    @QueryField(name = "datamodel_ref_id", operator = CompareOperator.EQ)
    private String datamodelId;

}
