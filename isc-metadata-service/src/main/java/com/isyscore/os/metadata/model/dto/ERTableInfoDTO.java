package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
public class ERTableInfoDTO {

    @ApiModelProperty("表名")
    @NotBlank
    private String tableName;

    @ApiModelProperty("涉及到的该表的字段名")
    @NotEmpty
    private Set<String> columns;

}
