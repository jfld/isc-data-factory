package com.isyscore.os.metadata.model.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <p>
 * 保存指标的维度过滤条件,该表仅保存指标自身设定的过滤条件，对于派生指标需要进行级联查询
 * </p>
 *
 * @author 
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("metric_dim_filter")
public class MetricDimFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("过滤条件对应的指标")
    private Long metricRefId;

    @ApiModelProperty("维度字段名称")
    private String dimColName;

    @ApiModelProperty("设定的维度过滤条件值")
    private String dimConditionValue;

    @ApiModelProperty("比较操作符，支持equal,between,in,like,lessthan,greaterthan等类型")
    private String operator;

}
