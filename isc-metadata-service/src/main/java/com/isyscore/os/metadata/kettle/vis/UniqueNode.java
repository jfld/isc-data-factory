package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@ApiModel("去重组件")
@Data
public class UniqueNode extends VisNode {
    @ApiModelProperty("去重字段")
    @NotEmpty(message = "去重字段不能为空")
    private List<String> uniqueFields;

    @ApiModelProperty("忽略大小写")
    private List<Boolean> caseInsensitives;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.uniqueFields =  Optional.ofNullable(jsonObject.getJSONArray("uniqueFields")).orElse(new JSONArray()).stream().map(String::valueOf).collect(Collectors.toList());
        Boolean caseInsensitive = jsonObject.getBoolean("caseInsensitives");
        this.caseInsensitives = Collections.nCopies(uniqueFields.size(), caseInsensitive);
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.Unique;
    }

    static Boolean getBoolean(Object o){
        return  new Boolean(o.toString());
    }
}
