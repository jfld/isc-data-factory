package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.util.List;

/**
 *  @author: ynzhang
 *  @Date: 2020/11/19 5:47 下午
 *  @Description: 数据源下面的数据库
 */
@Data
public class DatabaseDTO {
    private String DatabaseName;
    private List<String> tableNameList;
}
