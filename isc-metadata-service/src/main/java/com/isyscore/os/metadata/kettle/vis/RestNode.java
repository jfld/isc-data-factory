package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.enums.RequestType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author dc
 * @Type RestNode.java
 * @Desc
 * @date 2022/9/13 9:23
 */
@ApiModel("rest请求组件")
@Data
@EqualsAndHashCode(callSuper = false)
public class RestNode extends VisNode {

    @ApiModelProperty(value = "请求url")
    @NotBlank(message = "url不能为空")
    private String url;

    @ApiModelProperty(value = "请求方法 GET POST PUT DELETE")
    @NotNull(message = "请求方法不能为空")
    private RequestType requestType;

    @ApiModelProperty(value = "get请求参数")
    private List<Params> params;

    @ApiModelProperty(value = "请求headers")
    private List<Params> headers;

    @ApiModelProperty(value = "表单信息")
    private List<Params> formData;

    @ApiModelProperty(value = "raw json")
    private String raw;

    @ApiModelProperty(value = "结果名称")
    @NotBlank(message = "结果名称不能为空")
    private String resultName;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        Object param = jsonObject.get("params");
        if(Objects.nonNull(param)){
            String paramStr = JSONArray.toJSONString(param);
            this.setParams(JSONArray.parseArray(paramStr, Params.class));
        }
        Object headStr = jsonObject.get("headers");
        if(Objects.nonNull(headStr)){
            String headers = JSONArray.toJSONString(headStr);
            this.setHeaders(JSONArray.parseArray(headers, Params.class));
        }
        Object formData = jsonObject.get("formData");
        if(Objects.nonNull(formData)){
            String paramStr = JSONArray.toJSONString(formData);
            this.setParams(JSONArray.parseArray(paramStr, Params.class));
        }
        this.setRaw(jsonObject.getString("raw"));
        this.setRequestType(RequestType.valueOf(jsonObject.getString("requestType")));
        this.setUrl(jsonObject.getString("url"));
        this.setResultName(jsonObject.getString("resultName"));
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.OTHER;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.Rest;
    }
}

