package com.isyscore.os.metadata.kettle.vis;

import com.isyscore.os.metadata.enums.KettleAggregationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class GroupByMeasure {
    @NotBlank(message = "度量名不能为空")
    @ApiModelProperty(value = "度量名")
    private String measureName;
    @NotBlank(message = "字段名不能为空")
    @ApiModelProperty(value = "字段名")
    private String fieldName;
    @NotNull(message = "聚合类型不能为空")
    @ApiModelProperty(value = "聚合类型")
    private KettleAggregationType aggregationType;
}
