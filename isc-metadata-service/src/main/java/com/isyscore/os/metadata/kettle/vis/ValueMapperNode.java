package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author wuwx
 */
@ApiModel("值映射组件")
@Data
public class ValueMapperNode extends VisNode{
    @ApiModelProperty("使用的字段名称")
    @NotBlank(message = "使用的字段名称不能为空")
    private String fieldToUse;

    @ApiModelProperty("不匹配时的默认值")
//    @NotBlank(message = "不匹配时的默认值不能为空")
    private String nonMatchDefault;

    @ApiModelProperty("映射字段")
    @NotEmpty(message = "排序字段不能为空")
    @Valid
    private List<ValueMapperField> mapperFields;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.fieldToUse = jsonObject.getString("fieldToUse");
        this.nonMatchDefault = jsonObject.getString("nonMatchDefault");
        this.mapperFields =  Optional.ofNullable(jsonObject.getJSONArray("mapperFields")).orElse(new JSONArray()).stream().map(o ->
                JSON.parseObject(JSON.toJSONString(o), ValueMapperField.class)).collect(Collectors.toList());
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.ValueMapper;
    }
}
