package com.isyscore.os.metadata.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.isyscore.os.metadata.common.CommonService;
import com.isyscore.os.metadata.dao.KettleStepLogMapper;
import com.isyscore.os.metadata.model.entity.KettleStepLog;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.isyscore.os.metadata.constant.CommonConstant.DS_CK_METADATA;

/**
 * <p>
 * Kettle步骤日志 服务实现类
 * </p>
 *
 * @author wany
 * @since 2021-08-11
 */
@Service
@DS(DS_CK_METADATA)
public class KettleStepLogService extends CommonService<KettleStepLogMapper, KettleStepLog> {
    public List<KettleStepLog> listGroup(String taskId){
        return baseMapper.listGroup(taskId);
    }
}
