package com.isyscore.os.metadata.database;

import com.alibaba.druid.util.JdbcConstants;
import com.google.common.base.Strings;
import com.isyscore.os.core.sqlcore.DMBuilder;
import com.isyscore.os.metadata.enums.DataSourceTypeEnum;
import com.isyscore.os.metadata.model.vo.ResultVO;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description :
 * @Author: qkc
 * @Date: 2021/9/29 11:35
 */
public class DmDatabase extends AbstractDatabase {

    {
        dbType = DataSourceTypeEnum.DM;
        //时间类型
        DATE_TYPE.add("DATE");
        DATE_TYPE.add("DATETIME");
        DATE_TYPE.add("TIMESTAMP");

        //字段类型
        COLUMN_TYPE.put("CHAR", "CHAR(255)");
        COLUMN_TYPE.put("VARCHAR", "VARCHAR(8188)");
        COLUMN_TYPE.put("INT", "INT");
        COLUMN_TYPE.put("NUMBER", "NUMBER");
        COLUMN_TYPE.put("DECIMAL", "DECIMAL");
        COLUMN_TYPE.put("FLOAT", "FLOAT");
        COLUMN_TYPE.put("DOUBLE", "DOUBLE");
        COLUMN_TYPE.put("DATE", "DATE");
        COLUMN_TYPE.put("TIME", "TIME");
        COLUMN_TYPE.put("TIMESTAMP", "TIMESTAMP");
        COLUMN_TYPE.put("TEXT", "TEXT");

        builder = new DMBuilder();
    }

    private final String RENAME_TABLE = "ALTER TABLE %s rename to \"%s\"";

    private final String SELECT_TABLE_COL =
            "select" +
                    " '表备注' AS \"tableComment\"," +
                    " COLUMN_NAME AS \"columnName\"," +
                    " COLUMN_NAME AS \"oldName\"," +
                    " COLUMN_NAME AS \"columnComment\"," +
                    " DATA_TYPE AS \"dataType\"," +
                    " DATA_TYPE || '(' || DATA_LENGTH || ')' AS \"columnType\"," +
                    " TABLE_NAME AS \"tableName\" " +
                    "from" +
                    " all_tab_columns " +
                    "where Table_Name = '%s' " +
                    " and owner = '%s'";

    private final String SELECT_TABLE =
            "SELECT " +
                    " OBJECT_NAME \"tableName\" " +
                    "FROM " +
                    " all_objects " +
                    "WHERE " +
                    " OBJECT_TYPE = 'TABLE' " +
                    "AND OWNER = '%s'";

    private final String SELECT_TABLE_COLUMN_LINK =
            "select " +
                    " '表备注' AS \"tableComment\"," +
                    " COLUMN_NAME AS \"columnName\"," +
                    " COLUMN_NAME AS \"oldName\"," +
                    " DATA_TYPE AS \"dataType\"," +
                    " DATA_TYPE || '(' || DATA_LENGTH || ')' AS \"columnType\"," +
                    " TABLE_NAME AS \"tableName\" " +
                    "from" +
                    " all_tab_columns " +
                    "where" +
                    " Table_Name in ( SELECT TABLE_NAME FROM ALL_TAB_COLUMNS WHERE COLUMN_NAME IN ( %s ) ) " +
                    " and OWNER = '%s'";

    private String quote = "\"";

    @Override
    public String escapeTbName(String originTableName, String databaseName) {
        if (Strings.isNullOrEmpty(databaseName)) {
            return this.quote + originTableName + quote;
        }
        return databaseName + "." + this.quote + originTableName + quote;
    }

    @Override
    public String escapeColName(String originColName) {
        return this.quote + originColName + quote;
    }

    @Override
    public String escapeColAliasName(String originAliasName) {
        return this.quote + originAliasName + quote;
    }

    @Override
    public String withOutEscapeColName(String escapeColName) {
        return escapeColName.replaceAll(quote,"");
    }
    @Override
    public String withOutEscapeTableName(String escapeTableName) {
        return escapeTableName.replaceAll(quote,"");
    }

    @Override
    public String getParseStr2DateEl(String dateVal) {
        return "to_date(" + dateVal + ",'yyyy-mm-dd hh24:mi:ss')";
    }

    @Override
    public String getParseDate2StrEl(String dateVal) {
        return null;
    }

    @Override
    public String getParseStr2IntEl(String dateVal) {
        return "cast("+dateVal+" as int )";
    }

    @Override
    public String jdbcUrl(String ip, int port, String dbName, String basicType, String basicValue) {
        return String.format(DataSourceTypeEnum.DM.getUrl(), ip, port, dbName);
    }

    @Override
    public String tableStructSql(String tableName, String dbName) {
        return String.format(SELECT_TABLE_COL, tableName, dbName);
    }

    @Override
    public String tableListSql(String dbName) {
        return String.format(SELECT_TABLE, dbName);
    }

    @Override
    public String linkTableSql(String columns, String dbName) {
        return String.format(SELECT_TABLE_COLUMN_LINK, columns, dbName);
    }

    @Override
    public String renameTableSql(String oldTableName, String newTableName) {
        return String.format(RENAME_TABLE, oldTableName, newTableName);
    }

    @Override
    public String dropTableSql(String tableName) {
        String DROP_TABLE = "DROP TABLE %s";
        return String.format(DROP_TABLE, tableName);
    }

    @Override
    public String getQuote() {
        return quote;
    }

    @Override
    public void wrapTableData(ResultVO resultVO) {
        resultVO.setHead(resultVO.getHead().stream()
                .filter(col -> !Objects.equals(col.getColumnName(), OracleDatabase.ROW_ID))
                .collect(Collectors.toList()));

        List<Map<String, Object>> body = resultVO.getContent();
        for (Map<String, Object> map : body) {
            map.remove(OracleDatabase.ROW_ID);
        }
    }


}
