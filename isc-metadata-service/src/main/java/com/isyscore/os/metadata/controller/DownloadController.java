package com.isyscore.os.metadata.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;

/**
 * @author liy
 */
@RestController
@Api(tags = "文件下载服务")
@RequestMapping("${api-full-prefix:}/downLoad")
public class DownloadController {

    private static  String usagePdfPath = "/file/usage-数据迁移.pdf";
    private static  String usageTool = "/file/Studio.zip";

    @GetMapping("/usagePdf")
    @ApiOperation("usage-数据迁移.pdf")
    public void usagePdf(HttpServletResponse response) throws Exception {

        InputStream inputStream = this.getClass().getResourceAsStream(usagePdfPath);
        response.reset();
        response.setContentType("application/octet-stream");
        String filename = new File(usagePdfPath).getName();
        response.addHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(filename, "UTF-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] b = new byte[1024];
        int len;
        while ((len = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, len);
        }
        inputStream.close();
    }

    @GetMapping("/usageTool")
    @ApiOperation("usage工具包")
    public void usageTool(HttpServletResponse response) throws Exception {

        InputStream inputStream = this.getClass().getResourceAsStream(usageTool);
        response.reset();
        response.setContentType("application/octet-stream");
        String filename = new File(usageTool).getName();
        response.addHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(filename, "UTF-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] b = new byte[1024];
        int len;
        while ((len = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, len);
        }
        inputStream.close();
    }
}
