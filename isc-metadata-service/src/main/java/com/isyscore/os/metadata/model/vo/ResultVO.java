package com.isyscore.os.metadata.model.vo;

import com.isyscore.os.metadata.model.dto.ColumnIndexDTO;
import com.isyscore.os.metadata.model.dto.TableRelationDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/20 14:23
 */
@Data
public class ResultVO implements Serializable {

    private List<ColumnIndexDTO> head;

    private List<Map<String, Object>> content;

    private String tableName;

    private String tableComment;

    private long count;

    private List<TableRelationDTO> tableRelationList;

    public ResultVO(){};

    public ResultVO(List<Map<String, Object>> list) {
        this.content = list;
    }

    public ResultVO(List<ColumnIndexDTO> columnList, List<Map<String, Object>> list) {
        this.head = columnList;
        this.content = list;
    }
}
