package com.isyscore.os.metadata.kettle.base;

import com.google.common.collect.Maps;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import org.springframework.stereotype.Component;

import java.util.Map;

public class PluginFactory {
	private static Map<KettleDataFlowNodeType, Class<? extends Step>> stepCodecMap = Maps.newHashMap();

	public  Class<? extends Step> getPlugin(KettleDataFlowNodeType type) {
		return stepCodecMap.get(type);
	}

	public void register(KettleDataFlowNodeType type,Class<? extends Step> plugin) {
		stepCodecMap.put(type,plugin);
	}
}
