package com.isyscore.os.metadata.kettle.base;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.repository.filerep.KettleFileRepositoryMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class KettleRunner implements ApplicationRunner {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        KettleEnvironment.init();

        jdbcTemplate.execute("update data_flow_task set status=0 where status=1");
    }
}
