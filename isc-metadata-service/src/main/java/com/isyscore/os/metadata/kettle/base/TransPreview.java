/*! ******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2018 by Hitachi Vantara : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package com.isyscore.os.metadata.kettle.base;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.step.RowAdapter;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 数据预览捕获对象
 *
 * @author liy
 * @date 2022-07-13 10:50
 */
@Slf4j
public class TransPreview {

    private Map<String, List<RowMetaAndData>> previewDataMap;
    public int previewSize = 20;
    private PreviewMode previewMode;

    /**
     *
     */
    public TransPreview() {
        previewDataMap = new LinkedHashMap<>();
        previewMode = PreviewMode.LAST;
    }

    public Map<String, List<RowMetaAndData>> getPreviewDataMap() {
        return previewDataMap;
    }

    /**
     * 捕获预览数据
     */
    public void capturePreviewData(final Trans trans, List<StepMeta> stepMetas) {
        previewDataMap.clear();

        try {
            for (final StepMeta stepMeta : stepMetas) {
                final List<RowMetaAndData> rowsData;
                if (previewMode == PreviewMode.LAST) {
                    rowsData = new LinkedList<>();
                } else {
                    rowsData = new ArrayList<>();
                }

                previewDataMap.put(stepMeta.getName(), rowsData);

                StepInterface step = trans.findRunThread(stepMeta.getName());

                if (step != null) {
                    if (previewMode == PreviewMode.LAST) {
                        step.addRowListener(new RowAdapter() {
                            @Override
                            public void rowWrittenEvent(RowMetaInterface rowMeta, Object[] row) {
                                try {
                                    rowsData.add(new RowMetaAndData(rowMeta, rowMeta.cloneRow(row)));
                                } catch (KettleValueException e) {
                                    throw new RuntimeException(e);
                                }
                                if (rowsData.size() > previewSize) {
                                    rowsData.remove(0);
                                }
                            }
                        });
                    } else {
                        step.addRowListener(new RowAdapter() {

                            @Override
                            public void rowWrittenEvent(RowMetaInterface rowMeta, Object[] row) {
                                if (rowsData.size() < previewSize) {
                                    try {
                                        rowsData.add(new RowMetaAndData(rowMeta, rowMeta.cloneRow(row)));
                                    } catch (KettleValueException e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            trans.getLogChannel().logError("数据预览缓存数据失败.", e.getMessage());
        }
    }

    /**
     * 根据步骤获取预览数据
     */
    public List<String[]> getData(String stepName) {
        List<RowMetaAndData> rowData = previewDataMap.get(stepName);
        if (rowData == null) {
            return null;
        }

        List<String[]> result = new ArrayList<>(rowData.size());

        for (RowMetaAndData rowDatum : rowData) {
            RowMetaInterface dataRowMeta = rowDatum.getRowMeta();
            Object[] data = rowDatum.getData();
            String[] row = new String[dataRowMeta.size()];
            result.add(row);

            for (int dataIndex = 0; dataIndex < dataRowMeta.size(); dataIndex++) {
                String string;
                ValueMetaInterface valueMetaInterface;
                try {
                    valueMetaInterface = dataRowMeta.getValueMeta(dataIndex);
                    if (valueMetaInterface.isStorageBinaryString()) {
                        Object nativeType = valueMetaInterface.convertBinaryStringToNativeType((byte[]) data[dataIndex]);
                        string = valueMetaInterface.getStorageMetadata().getString(nativeType);
                    } else {
                        string = dataRowMeta.getString(data, dataIndex);
                    }
                } catch (Exception e) {
                    string = "Conversion error: " + e.getMessage();
                }

                row[dataIndex] = string;
            }
        }

        return result;
    }

    public List<String[]> getLastStepData() {
        List<RowMetaAndData> rowData = null;
        List<String[]> result =  new ArrayList<>();
        try {
            Field tail = previewDataMap.getClass().getDeclaredField("tail");
            tail.setAccessible(true);
            rowData = (List<RowMetaAndData>) ((Map.Entry) tail.get(previewDataMap)).getValue();

            if (rowData == null) {
                return null;
            }
            for (RowMetaAndData rowDatum : rowData) {
                RowMetaInterface dataRowMeta = rowDatum.getRowMeta();
                Object[] data = rowDatum.getData();
                String[] row = new String[dataRowMeta.size()];
                result.add(row);

                for (int dataIndex = 0; dataIndex < dataRowMeta.size(); dataIndex++) {
                    String string;
                    ValueMetaInterface valueMetaInterface;
                    valueMetaInterface = dataRowMeta.getValueMeta(dataIndex);
                    if (valueMetaInterface.isStorageBinaryString()) {
                        Object nativeType = valueMetaInterface.convertBinaryStringToNativeType((byte[]) data[dataIndex]);
                        string = valueMetaInterface.getStorageMetadata().getString(nativeType);
                    } else {
                        string = dataRowMeta.getString(data, dataIndex);
                    }

                    row[dataIndex] = string;
                }
            }
        } catch (Exception e) {
            log.error("获得预览数据失败", e);
        }

        return result;
    }

    /**
     * 获取预览字段名集合
     */
    public List<String> getFieldNames(String stepName) {
        List<RowMetaAndData> rowData = previewDataMap.get(stepName);
        if (rowData == null || rowData.isEmpty()) {
            return null;
        }

        final RowMetaInterface rowMeta = rowData.get(0).getRowMeta();
        List<String> previewFieldNames = new ArrayList<>(rowMeta.size());

        for (int i = 0; i < rowMeta.size(); i++) {
            final ValueMetaInterface valueMeta = rowMeta.getValueMeta(i);
            previewFieldNames.add(valueMeta.getName());
        }

        return previewFieldNames;
    }

    public List<String> getLastFieldNames() {
        Field tail = null;
        List<String> previewFieldNames = null;
        try {
            tail = previewDataMap.getClass().getDeclaredField("tail");
            tail.setAccessible(true);
            List<RowMetaAndData> rowData = previewDataMap.get( ((Map.Entry) tail.get(previewDataMap)).getKey());
            if (rowData == null || rowData.isEmpty()) {
                return null;
            }
            RowMetaInterface rowMeta = rowData.get(0).getRowMeta();
            previewFieldNames = new ArrayList<>(rowMeta.size());

            for (int i = 0; i < rowMeta.size(); i++) {
                final ValueMetaInterface valueMeta = rowMeta.getValueMeta(i);
                previewFieldNames.add(valueMeta.getName());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return previewFieldNames;
    }



    public enum PreviewMode {
        FIRST, LAST, OFF,
    }
}
