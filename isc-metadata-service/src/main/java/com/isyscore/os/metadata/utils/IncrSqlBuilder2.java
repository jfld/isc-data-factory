package com.isyscore.os.metadata.utils;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;
import com.alibaba.druid.util.JdbcConstants;
import com.isyscore.os.metadata.database.AbstractDatabase;
import com.isyscore.os.metadata.enums.KettleJobIncreFieldType;

import java.util.*;

public class IncrSqlBuilder2 extends OracleASTVisitorAdapter {
    private final static String dbType = JdbcConstants.ORACLE;
    private String sql;
    private String incrTable;
    private String incrField;
    private String origIncrField;
    private String incStartValue;
    private String incEndValue;
    private boolean first;
    private AbstractDatabase db;
    private String fieldType;
    private String incrFieldType;

    @Override
    public boolean visit(SQLExprTableSource x) {
        String table = x.getExpr().toString();
        String cleanTable = db.withOutEscapeTableName(table);
        if (cleanTable.equals(incrTable)) {
//            if(!db.isLegalDateType(fieldType) && KettleJobIncreFieldType.valueOf(incrFieldType)!=KettleJobIncreFieldType.time){
//                incrField = db.getParseStr2IntEl(incrField);
//            }
            if(KettleJobIncreFieldType.valueOf(incrFieldType)==KettleJobIncreFieldType.time){
                incStartValue = db.getParseStr2DateEl("'"+incStartValue+"'");
                incEndValue = db.getParseStr2DateEl("'"+incEndValue+"'");
            }

            if (x.getAlias() == null) {
                if (first) {
                    x.setExpr("(select * from " + table + "  where " + incrField + ">=" + incStartValue + " and " + incrField + "<=" + incEndValue + ") " + (table.contains(".")?table.split("\\.")[1]:table));
                } else {
                    x.setExpr("(select * from " + table + "  where " + incrField + ">" + incStartValue + " and " + incrField + "<=" + incEndValue + ") " + (table.contains(".")?table.split("\\.")[1]:table));
                }
            } else {
                if (first) {
                    x.setExpr("(select * from " + table + "  where " + incrField + ">=" + incStartValue + " and " + incrField + "<=" + incEndValue + ")");
                } else {
                    x.setExpr("(select * from " + table + "  where " + incrField + ">" + incStartValue + " and " + incrField + "<=" + incEndValue + ")");
                }
            }
        }

        return true;
    }

    public String generateIncrSql(String sql, String incrTable, String incrFieldType, String incrField,String origIncrField, String incStartValue, String incEndValue, boolean first, AbstractDatabase db, String fieldType) {
        this.sql = sql;
        this.incrTable = incrTable;
        this.incrField = incrField;
        this.origIncrField = origIncrField;
        this.incStartValue = incStartValue;
        this.incEndValue = incEndValue;
        this.first = first;
        this.db = db;
        this.fieldType = fieldType;
        this.incrFieldType = incrFieldType;

        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);
        for (SQLStatement stmt : stmtList) {
            stmt.accept(this);
        }
        String convertedSql = SQLUtils.toSQLString(stmtList, dbType);
        System.out.println(convertedSql);

        return convertedSql;
    }

    public static void main(String[] args) {
        IncrSqlBuilder2 t = new IncrSqlBuilder2();
        String build = t.generateIncrSql("SELECT `id`, `rule_name`, `rule_group_name`, `severity`, `message`\n" +
                "\t, `labels`, `period`, `enterprise`, `ip`, `node`\n" +
                "\t, `silence_duration`, `alert_resource`, `alert_persons`, `alert_groups`, `alert_methods`\n" +
                "\t, `alert_time_range`, `sendable`, `unsendable_reason`, `created_by`, `created_at`\n" +
                "\t, `updated_by`, `updated_at`, `scope_cn`, `ext_info`, `strategy_name_json`\n" +
                "\t, `idd`\n" +
                "FROM `alert_record`", "alert_record", "number", "id", "id","10", "100", false, null, null);
//        List<SQLStatement> stmtList = SQLUtils.parseStatements("select * from (select * from m) m left join m_1  on m_1.id = m.id", dbType);
        System.out.println();
    }

}

