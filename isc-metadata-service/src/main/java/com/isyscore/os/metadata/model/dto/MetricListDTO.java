package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetricListDTO {

    @ApiModelProperty("指标ID")
    private Long id;

    @ApiModelProperty("指标分组ID")
    private Long groupId;

    @ApiModelProperty("指标名称")
    private String name;

    @ApiModelProperty("指标描述")
    private String description;

    @ApiModelProperty("指标创建时间")
    private LocalDateTime createTime;

}
