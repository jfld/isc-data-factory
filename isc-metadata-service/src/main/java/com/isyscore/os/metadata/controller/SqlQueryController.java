package com.isyscore.os.metadata.controller;

import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import com.isyscore.os.core.entity.SqlQuery;
import com.isyscore.os.metadata.constant.CommonConstant;
import com.isyscore.os.metadata.model.dto.SqlTreeItemDTO;
import com.isyscore.os.metadata.model.dto.req.ChangeSqlQueryGroupReq;
import com.isyscore.os.metadata.model.dto.req.ChangeSqlQueryNameReq;
import com.isyscore.os.metadata.model.vo.ResultVO;
import com.isyscore.os.metadata.model.vo.SqlQueryVO;
import com.isyscore.os.metadata.service.SqlQueryGroupService;
import com.isyscore.os.metadata.service.SqlQueryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.groups.Default;
import java.util.List;

/**
 * @author starzyn
 * @since 2022-03-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("${api-full-prefix}/sql-query")
@Api(tags = "SQL查询接口")
public class SqlQueryController {

    private final SqlQueryService sqlQueryService;
    private final SqlQueryGroupService sqlQueryGroupService;

    @ApiOperation("修改SQL查询的分组")
    @PutMapping("/changeGroup")
    public RespDTO<?> groupChange(@Validated @RequestBody ChangeSqlQueryGroupReq changeSqlQueryGroupReq) {
        SqlQuery sqlQuery = sqlQueryService.getById(changeSqlQueryGroupReq.getSqlQueryId());
        if (changeSqlQueryGroupReq.getGroupId() == null) {
            sqlQuery.setGroupId(CommonConstant.DEFAULT_SQL_GROUP_ID);
        } else {
            sqlQuery.setGroupId(changeSqlQueryGroupReq.getGroupId());
        }
        sqlQueryService.updateById(sqlQuery);
        return RespDTO.onSuc();
    }

    @ApiOperation("SQL查询重命名")
    @PutMapping("/rename")
    public RespDTO<?> renameMetric(@Validated @RequestBody ChangeSqlQueryNameReq changeSqlQueryNameReq) {
        SqlQuery sqlQuery = sqlQueryService.getById(changeSqlQueryNameReq.getSqlQueryId());
        sqlQuery.setName(changeSqlQueryNameReq.getSqlQueryName());
        sqlQueryService.updateById(sqlQuery);
        return RespDTO.onSuc();
    }


    @ApiOperation("查看单个SQL查询")
    @GetMapping("/{id}")
    public RespDTO<SqlQueryVO> view(@PathVariable("id") String id) {
        return RespDTO.onSuc(sqlQueryGroupService.getSqlQueryVO(id));
    }


    @ApiOperation("查询SQL树")
    @GetMapping("/tree")
    public RespDTO<List<SqlTreeItemDTO>> page(@RequestParam(value = "name", required = false) String name) {
        return RespDTO.onSuc(sqlQueryGroupService.getSqlQueryTree(name));
    }

    @ApiOperation("录入SQL查询")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public RespDTO create(@Validated({ValidateCreate.class, Default.class}) @RequestBody SqlQuery sqlQuery) {
        return sqlQueryService.add(sqlQuery);
    }

    @ApiOperation("更新SQL查询")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public RespDTO update(@Validated({ValidateUpdate.class, Default.class}) @RequestBody SqlQuery sqlQuery) {
        return sqlQueryService.updateDataText(sqlQuery);
    }

    @ApiOperation("删除SQL查询")
    @DeleteMapping("/{id}")
    public RespDTO<Void> delete(@PathVariable("id") String id) {
        sqlQueryService.removeById(id);
        return RespDTO.onSuc();
    }

    @ApiOperation("SQL执行接口")
    @GetMapping("/exec/{id}")
    public RespDTO<ResultVO> getData(@PathVariable("id") String id,PageRequest page, HttpServletRequest request) {
        return RespDTO.onSuc(sqlQueryService.getData(id, page, request));
    }

}
