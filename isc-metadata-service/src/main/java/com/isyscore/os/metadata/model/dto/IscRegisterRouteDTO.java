package com.isyscore.os.metadata.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 应用注册路由请求参数
 *
 * @author qiany@isyscore.com
 * @date 2021-06-18
 */
@Data
@AllArgsConstructor
public class IscRegisterRouteDTO {
    /**
     * 服务ID
     */
    private String serviceId;

    /**
     * 服务前缀
     */
    private String path;

    /**
     * 服务的svc+端口
     */
    private String url;
    /**
     * ws|http,可空，默认为http
     */
    private String protocol;

    /**
     * 登录拦截白名单，多个url用分号隔开
     */
    private String excludeUrl;
}
