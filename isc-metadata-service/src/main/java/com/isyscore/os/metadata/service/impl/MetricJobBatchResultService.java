package com.isyscore.os.metadata.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.isyscore.boot.login.LoginUserManager;
import com.isyscore.os.metadata.common.CommonService;
import com.isyscore.os.metadata.dao.MetricJobBatchResultMapper;
import com.isyscore.os.metadata.model.entity.MetricJobBatchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.isyscore.os.metadata.constant.CommonConstant.DS_CK_METADATA;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author
 * @since 2021-08-11
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
@DS(DS_CK_METADATA)
public class MetricJobBatchResultService extends CommonService<MetricJobBatchResultMapper, MetricJobBatchResult> {
    @Autowired
    private LoginUserManager userManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${metadata.cluster-name}")
    private String clusterName;

    public List<MetricJobBatchResult> getResultsByMetricAndPeriod(Long metricId, String period, String dimension) {
        List<Object> parameters = new ArrayList<>();
        parameters.add(metricId);
        parameters.add(period);
        parameters.add(userManager.getCurrentTenantId());
        String sql = "select id,metric_ref_id,dim,measure,data_period,tenant_id,create_time from metric_job_batch_result_all final where  metric_ref_id = ? and data_period = ? and tenant_id = ?";
        if (StrUtil.isNotEmpty(dimension)) {
            sql += " and dim = ?";
            parameters.add(dimension);
        }
        return jdbcTemplate.query(sql, new ResultRowMapper(), parameters.toArray());
    }

    public void removeResultsByMetricAndPeriod(Long metricId, String periodKey) {
        LambdaQueryWrapper<MetricJobBatchResult> qw = new LambdaQueryWrapper<>();
        qw.eq(MetricJobBatchResult::getDataPeriod, periodKey);
        qw.eq(MetricJobBatchResult::getMetricRefId, metricId);
        this.remove(qw);
    }

    public void removeResultsByMetric(Long metricId) {
        String sql = "ALTER TABLE metric_job_batch_result_local ON CLUSTER '"+clusterName+"' delete where metric_ref_id='"+metricId+"'";
        jdbcTemplate.execute(sql);
    }

    public class ResultRowMapper implements RowMapper<MetricJobBatchResult> {
        @Override
        public MetricJobBatchResult mapRow(ResultSet rs, int rowNum) throws SQLException {
            MetricJobBatchResult result = new MetricJobBatchResult();
            result.setId(rs.getLong("id"));
            result.setMetricRefId(rs.getLong("metric_ref_id"));
            result.setDim(rs.getString("id"));
            result.setMeasure(rs.getString("measure"));
            result.setDataPeriod(rs.getString("data_period"));
            result.setCreateTime(rs.getTimestamp("create_time").toLocalDateTime());
            result.setTenantId(rs.getString("tenant_id"));
            return result;
        }
    }
}
