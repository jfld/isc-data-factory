package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 记录指标每一次计算的批次信息
 * </p>
 *
 * @author wany
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("metric_job_batch_all")
public class MetricJobBatch implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("任务关联的指标ID")
    private Long metricRefId;

    @ApiModelProperty("任务关联的指标名称")
    private String metricName;

    @ApiModelProperty("任务对应的统计周期")
    private String dataPeriod;

    @ApiModelProperty("任务执行结果：0:执行中,  1：成功   -1：失败")
    private Integer status;

    @ApiModelProperty("任务消息")
    private String message;

    @ApiModelProperty("任务实际执行的SQL语句")
    private String executedSql;

    @ApiModelProperty("任务开始时间")
    private String startTime;

    @ApiModelProperty("任务结束时间")
    private String endTime;

    @ApiModelProperty("任务执行的方式：1：自动执行  2：手动执行")
    private Integer type;

    @ApiModelProperty("本次计算生成的结果集数据量")
    private Integer resultQuantity;

}
