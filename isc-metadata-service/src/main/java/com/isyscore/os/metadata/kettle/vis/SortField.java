package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortField {
    @ApiModelProperty("字段名称")
    @NotBlank(message = "字段名称不能为空")
    private String name;
    @ApiModelProperty("升序")
    @NotNull(message = "升序不能为空")
    private Boolean ascending;
}
