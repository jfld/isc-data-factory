package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DynamicFilterDTO {

    @ApiModelProperty("动态条件的字段名")
    @NotBlank
    private String column;

    @ApiModelProperty("比较操作符")
    @NotBlank
    private String operator;

    @ApiModelProperty("条件值")
    @NotBlank
    private String value;

}
