package com.isyscore.os.metadata.service;

import com.isyscore.os.metadata.kettle.dto.FlowNodeValidatedResultDTO;
import com.isyscore.os.metadata.kettle.vis.VisGraph;
import com.isyscore.os.metadata.model.dto.NodeNotAllowsDTO;

import java.util.List;

public interface ETLFlowNodeService {
    List<NodeNotAllowsDTO> accept();
    List<FlowNodeValidatedResultDTO> validateGraph(VisGraph userConfig);
}
