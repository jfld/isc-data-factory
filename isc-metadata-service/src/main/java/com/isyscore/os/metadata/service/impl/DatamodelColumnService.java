package com.isyscore.os.metadata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Sets;
import com.isyscore.os.metadata.common.CommonService;
import com.isyscore.os.metadata.dao.DatamodelColumnMapper;
import com.isyscore.os.metadata.model.entity.DatamodelColumn;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;

/**
 * <p>
 * 定义数据模型中包含的数据列 服务实现类
 * </p>
 *
 * @author
 * @since 2021-08-11
 */
@Service
public class DatamodelColumnService extends CommonService<DatamodelColumnMapper, DatamodelColumn> {


    public List<DatamodelColumn> getColumnsByDataModelId(Long datamodelId) {
        LambdaQueryWrapper<DatamodelColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(DatamodelColumn::getDatamodelRefId, datamodelId);
        return this.list(qw);
    }

    public void removeColumnsByDataModelId(Long datamodelId) {
        LambdaQueryWrapper<DatamodelColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(DatamodelColumn::getDatamodelRefId, datamodelId);
        this.remove(qw);
    }

    public DatamodelColumn getColumnByName(Long datamodelId, String columnName) {
        LambdaQueryWrapper<DatamodelColumn> qw = new LambdaQueryWrapper<>();
        qw.eq(DatamodelColumn::getColName, columnName);
        qw.eq(DatamodelColumn::getDatamodelRefId, datamodelId);
        return this.getOne(qw);
    }

    public void importData(List<DatamodelColumn> datas) {
        if (datas.isEmpty()) {
            return;
        }
        Set<Long> ids = Sets.newConcurrentHashSet();
        for (DatamodelColumn data : datas) {
            ids.add(data.getId());
        }
        LambdaQueryWrapper<DatamodelColumn> qw = new LambdaQueryWrapper<>();
        qw.in(DatamodelColumn::getId, ids);
        this.remove(qw);
        this.saveBatch(datas);
    }

    public List<DatamodelColumn> getColumnsByDatamodelIds(Set<Long> datamodelIds) {
        if (datamodelIds.isEmpty()) {
            return emptyList();
        }
        LambdaQueryWrapper<DatamodelColumn> qw = new LambdaQueryWrapper<>();
        qw.in(DatamodelColumn::getDatamodelRefId, datamodelIds);
        return this.list(qw);
    }

}
