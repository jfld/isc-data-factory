package com.isyscore.os.metadata.enums;

import lombok.Getter;

/**
 * 任务触发类型
 * @author liy
 */

@Getter
public enum KettleJobTriggerType {
    MANUAL(1, "手动"),
    AUTO(2, "自动");




    private Integer code;

    private String desc;

    KettleJobTriggerType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static KettleJobTriggerType get(String name) {
        return KettleJobTriggerType.valueOf(name);
    }

}
