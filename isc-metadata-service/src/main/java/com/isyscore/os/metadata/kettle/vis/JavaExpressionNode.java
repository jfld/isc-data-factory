package com.isyscore.os.metadata.kettle.vis;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author liyang
 */
@ApiModel("java表达式")
@Data
public class JavaExpressionNode extends VisNode{

    @ApiModelProperty("表达式列表")
    private List<Expression> expressions;
    @ApiModelProperty("错误处理方式 SKIP:跳过数据行\n" +
            "    IGNORE:忽略错误设置为NULL\n" +
            "    INTERRUPT:中断程序")
    private JavaExpressionErrorDealType errorDealType;


    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.expressions = jsonObject.getJSONArray("expressions").stream().map(o -> JSON.parseObject(JSON.toJSONString(o), Expression.class)).collect(Collectors.toList());
        this.errorDealType = JavaExpressionErrorDealType.valueOf(jsonObject.getString("errorDealType"));
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.JavaExpression;
    }

    @Override
    public String validate() {
        String va= super.validate();

        for (Expression expression : expressions) {
            Pattern p = Pattern.compile("new(.*?)\\(");
            Matcher m = p.matcher(expression.getExpression());

            while(m.find()) {
                Class<?> aClass = null;
                try {
                    if(m.group(1).contains(".")){
                        aClass = Class.forName(m.group(1).trim());
                        String name = aClass.getPackage().getName();
                        if(!name.equals("java.long")){
                            return "字段"+expression.getField()+"的表达式:"+expression.getExpression()+"不支持";
                        }
                    }
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return va;
    }



    @Data
    public static class Expression{
        @ApiModelProperty("字段")
        public String field;
        @ApiModelProperty("返回值类型BigNumber,\n" +
                "    Number,\n" +
                "    Boolean,\n" +
                "    Integer,\n" +
                "    String,\n" +
                "    Date,\n" +
                "    Timestamp;")
        public JavaExpressionFieldType type;
        @ApiModelProperty("表达式")
        public String expression;


    }

}
