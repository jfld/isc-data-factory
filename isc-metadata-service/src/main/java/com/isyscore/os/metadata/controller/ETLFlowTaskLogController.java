package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.kettle.dto.FlowTaskStepLogDTO;
import com.isyscore.os.metadata.service.ETLFlowTaskLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${api-full-prefix}/etlflow/task/log")
@Api(tags = "ETL流程任务实例日志")
@RequiredArgsConstructor
public class ETLFlowTaskLogController {
private final ETLFlowTaskLogService etlFlowTaskLogService;
    @ApiOperation("按照步骤获取任务的运行结果日志")
    @GetMapping("/step")
    public RespDTO<List<FlowTaskStepLogDTO>> getTaskStepLog(@RequestParam("taskId") String taskId) {
        return RespDTO.onSuc(etlFlowTaskLogService.getTaskStepLog(taskId));
    }

    @ApiOperation("获取任务执行的原始日志输出")
    @GetMapping("/raw")
    public RespDTO<String> getTaskRawLog(@RequestParam("taskId") String taskId) {
        return RespDTO.onSuc(etlFlowTaskLogService.getTaskRawLog(taskId));
    }

    @ApiOperation("获取任务执行每个步骤的运行状态")
    @GetMapping("/status")
    public RespDTO<List<Map<String,String>>> getTaskStepStatus(@RequestParam("taskId") String taskId) {
        return RespDTO.onSuc(etlFlowTaskLogService.getTaskStepStatus(taskId));
    }

}
