package com.isyscore.os.metadata.model.dto;

import com.isyscore.os.metadata.model.entity.MetricJobBatchResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class TaskQueryResultDTO {

    @ApiModelProperty("是否执行完成:-1:失败，1：成功，0：正在执行")
    public Integer success;

    @ApiModelProperty("错误信息")
    public String message;

    @ApiModelProperty("返回数据，success为1时才有值")
    public List<MetricJobBatchResult> results;

}
