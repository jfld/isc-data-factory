package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.kettle.dto.FlowTaskPreviewDataDTO;
import com.isyscore.os.metadata.kettle.dto.FlowTaskStepLogDTO;
import com.isyscore.os.metadata.service.ETLFlowTaskLogService;
import com.isyscore.os.metadata.service.ETLFlowTaskPreviewDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liy
 * @date 2022-07-13 10:50
 */
@RestController
@RequestMapping("${api-full-prefix}/etlflow/task/previewData")
@Api(tags = "ETL流程任务实例数据预览")
@RequiredArgsConstructor
public class ETLFlowTaskPreviewDataController {
private final ETLFlowTaskPreviewDataService etlFlowTaskPreviewDataService;
    @ApiOperation("获取指定节点的预览数据")
    @GetMapping("/step")
    public RespDTO<FlowTaskPreviewDataDTO> getTaskStepPreviewData(@RequestParam("taskId") String taskId, @RequestParam("stepName") String stepName) {
        return RespDTO.onSuc(etlFlowTaskPreviewDataService.getTaskStepPreviewData(taskId,stepName));
    }


    @ApiOperation("获得最后一个节点的预览数据")
    @GetMapping("/last")
    public RespDTO<FlowTaskPreviewDataDTO> getTaskLastStepPreviewData(@RequestParam("taskId")  String taskId) {
        return RespDTO.onSuc(etlFlowTaskPreviewDataService.getTaskLastStepPreviewData(taskId));
    }

    @ApiOperation("获得数据源的预览数据")
    @PostMapping("/dataSourece")
    public RespDTO<FlowTaskPreviewDataDTO> getDataSourecePreviewData(@RequestParam("sql") String sql,@RequestParam("dataSourceId") Long dataSourceId,@RequestParam("databaseName") String databaseName) {
        return RespDTO.onSuc(etlFlowTaskPreviewDataService.previewData(sql,dataSourceId,databaseName));
    }

}
