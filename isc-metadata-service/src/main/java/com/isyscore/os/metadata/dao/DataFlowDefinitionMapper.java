package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.isyscore.os.core.model.entity.DataFlowDefinition;
import com.isyscore.os.metadata.kettle.dto.FlowDefinitionDTO;
import com.isyscore.os.metadata.kettle.dto.FlowDefinitionPageReqDTO;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface DataFlowDefinitionMapper extends BaseMapper<DataFlowDefinition> {

    IPage<FlowDefinitionDTO> list(IPage<FlowDefinitionDTO> page, @Param("pageRequest") FlowDefinitionPageReqDTO pageRequest);

    void insertSelective( @Param("dataFlowDefinition")DataFlowDefinition dataFlowDefinition);

    void updateByPrimaryKeySelective(@Param("dataFlowDefinition")DataFlowDefinition dataFlowDefinition);
}
