package com.isyscore.os.metadata.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
public class TableVO {
    @NotNull
    private  Long dataSourceId;
    @NotNull
    private String newTableName;
    @NotNull
    private String oldTableName;
    private String tableComment;
    @NotNull
    private String databaseName;
    private List<Map<String,String>> fields;

    private List<String> delColumns;
}
