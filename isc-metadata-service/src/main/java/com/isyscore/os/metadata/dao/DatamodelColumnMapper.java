package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.DatamodelColumn;


/**
 * <p>
 * 定义数据模型中包含的数据列 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface DatamodelColumnMapper extends BaseMapper<DatamodelColumn> {

}
