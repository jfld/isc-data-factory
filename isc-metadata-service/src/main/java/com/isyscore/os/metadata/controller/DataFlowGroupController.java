package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.model.entity.DataFlowGroup;
import com.isyscore.os.metadata.model.dto.req.GroupDTO;
import com.isyscore.os.metadata.service.DataFlowGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author dc
 * @Type DataFlowGroupController.java
 * @Desc 1.添加分组（子分组）  2.重命名分组 3.删除分组（下面无挂载任务） 4.全部 查询所有任务， 默认查询默认分组下的任务
 * 5.查询下一级分组                 6.挂载任务 7.编辑任务  8.复制任务
 * @date 2022/10/21 11:24
 */
@Api(tags = "数据任务分组管理")
@RestController
@RequestMapping("${api-full-prefix}/etlflow/group")
@RequiredArgsConstructor
@Slf4j
public class DataFlowGroupController {
    private final DataFlowGroupService dataFlowGroupService;

    @ApiOperation("v7.2.0 添加分组（子分组）")
    @PostMapping("addGroup")
    public RespDTO<DataFlowGroup> addGroup(@Validated @RequestBody GroupDTO groupDTO) {
        return RespDTO.onSuc(dataFlowGroupService.addGroup(groupDTO));
    }

    @ApiOperation("v7.2.0 重命名分组")
    @PostMapping("updateGroup")
    public RespDTO<Boolean> updateGroup(@Validated @RequestBody GroupDTO groupDTO) {
        return RespDTO.onSuc(dataFlowGroupService.updateGroup(groupDTO));
    }

    @ApiOperation("v7.2.0 删除分组")
    @DeleteMapping("/{id}")
    public RespDTO<Boolean> deleteGroup(@PathVariable("id")String id) {
        return RespDTO.onSuc(dataFlowGroupService.deleteGroup(id));
    }

    @ApiOperation("v7.2.0 查询分组(parentId为空查询)")
    @GetMapping("getGroup")
    public RespDTO<List<DataFlowGroup>> getGroup(@RequestParam(value = "parentId",required = false)String parentId) {
        return RespDTO.onSuc(dataFlowGroupService.getGroup(parentId));
    }

    @ApiOperation("v7.2.0 获取分组树)")
    @GetMapping("listTreeGroup")
    public RespDTO<List<DataFlowGroup>> listTreeGroup() {
        return RespDTO.onSuc(dataFlowGroupService.listTreeGroup());
    }
}
    
