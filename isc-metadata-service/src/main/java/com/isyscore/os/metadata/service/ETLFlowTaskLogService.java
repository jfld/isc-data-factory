package com.isyscore.os.metadata.service;

import com.isyscore.os.metadata.kettle.dto.FlowTaskStepLogDTO;

import java.util.List;
import java.util.Map;

public interface ETLFlowTaskLogService {
    public List<FlowTaskStepLogDTO> getTaskStepLog(String taskId);
    public String getTaskRawLog(String taskId);

    List<Map<String,String>> getTaskStepStatus(String taskId);
}
