package com.isyscore.os.metadata.service.quartz;


import com.isyscore.boot.login.LoginUserManager;
import com.isyscore.os.core.model.entity.DataFlowDefinition;
import com.isyscore.os.core.util.InitiallyUtils;
import com.isyscore.os.metadata.enums.KettleJobTriggerType;
import com.isyscore.os.metadata.service.DataFlowDefinitionService;
import com.isyscore.os.metadata.service.DataFlowTaskService;
import com.isyscore.os.permission.entity.LoginVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 用于定时执行指标的计算任务
 *
 * @author wany
 */
@AllArgsConstructor
@Slf4j
public class DataFlowJobTask extends QuartzJobBean {

    private final DataFlowDefinitionService dataFlowDefinitionService;
    private final DataFlowTaskService dataFlowTaskService;

    private final LoginUserManager loginUserManager;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        String dataFlowId = jobExecutionContext.getJobDetail().getJobDataMap().get("id").toString();
        DataFlowDefinition definition = InitiallyUtils.markInitially(() -> dataFlowDefinitionService.getById(dataFlowId));
        LoginVO loginVO = new LoginVO();
        loginVO.setTenantId(definition.getTenantId());
        //将当前租户设置为对应指标的租户ID
        loginUserManager.executeWithAssignedLoginUser(loginVO, () -> {
            dataFlowTaskService.runTask(dataFlowId, null, KettleJobTriggerType.AUTO,null);
            return null;
        });
    }

}
