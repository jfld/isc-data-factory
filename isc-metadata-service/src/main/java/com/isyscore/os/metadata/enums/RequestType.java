package com.isyscore.os.metadata.enums;

/**
 * @author dc
 * @Type RequestType.java
 * @Desc rest组件支持的method枚举
 * @date 2022/9/13 9:44
 */
public enum RequestType {
    /**
     * get
     */
    GET,
    /**
     * post
     */
    POST,
    /**
     * put
     */
    PUT,
    /**
     * delete
     */
    DELETE;
}
