package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.MetricMeasure;

/**
 * <p>
 * 保存指标的所涉及的度量值，以及度量值所对应的聚合方式 Mapper 接口
 * </p>
 *
 * @author wany
 * @since 2021-10-12
 */
public interface MetricMeasureMapper extends BaseMapper<MetricMeasure> {

}
