package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum TimePeriodUnit {

    week("week", "自然周"),
    day("day", "自然日"),
    month("month", "自然月"),
    quarter("quarter", "季度"),
    year("year", "自然年");

    private String symbol;

    private String label;

    TimePeriodUnit(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;
    }
}
