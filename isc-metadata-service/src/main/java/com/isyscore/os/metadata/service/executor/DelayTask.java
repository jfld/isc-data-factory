package com.isyscore.os.metadata.service.executor;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author liy
 * @date 2023-09-05 14:38
 */
public class DelayTask<T> implements Delayed {
    private long start = System.currentTimeMillis();
    private Long delayTime;
    private T task;

    @Override
    public long getDelay(@NotNull TimeUnit unit) {
        return unit.convert((start + this.delayTime) - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(@NotNull Delayed delayed) {
        DelayTask msg = (DelayTask) delayed;
        return (int)(((start + this.delayTime) - System.currentTimeMillis()) -((msg.start + msg.delayTime) - System.currentTimeMillis())) ;
    }

    public T getTask() {
        return this.task;
    }

    public DelayTask(long delayTime, T body) {
        this.delayTime = delayTime;
        this.task = body;
    }
}
