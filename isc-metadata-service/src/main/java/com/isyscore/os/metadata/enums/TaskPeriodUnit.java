package com.isyscore.os.metadata.enums;

import lombok.Getter;

import java.util.Random;

@Getter
public enum TaskPeriodUnit {

    minute("minute", "每分钟",10,1),
    hour("hour", "每小时",10*60,60),
    day("day", "每天",10*60,60),
    month("month", "每月",10*60,60),
    other("other", "其他",0,0);

    private String symbol;

    private String label;

    /**
     * 最大延迟时长(秒)
     */
    private Integer maxDelayTime;
    private Integer factor;

    TaskPeriodUnit(String symbol, String label,Integer maxDelayTime,Integer factor) {
        this.symbol = symbol;
        this.label = label;
        this.maxDelayTime = maxDelayTime;
        this.factor = factor;
    }

    public Integer getDelayTime(){
        return new Random().nextInt(maxDelayTime)+factor;
    }

    public Integer getMaxDelayTime() {
        return maxDelayTime;
    }
}
