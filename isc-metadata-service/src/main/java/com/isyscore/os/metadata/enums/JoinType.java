package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum JoinType {

    INNER("INNER", "内连接"),
    LEFT_OUTER("LEFT OUTER", "左外连接"),
    RIGHT_OUTER("RIGHT OUTER", "右重计数"),
    FULL_OUTER("FULL OUTER", "全外连接");

    private String symbol;

    private String label;

    JoinType(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;
    }

}
