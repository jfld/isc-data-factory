package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
public class GetFastSqlDTO {
    @NotNull
    @ApiModelProperty("数据源ID")
    private String dataSourceId;

    @NotNull
    @ApiModelProperty("数据库名")
    private String databaseName;

    @NotNull
    @ApiModelProperty("表明")
    private String tableName;

}
