package com.isyscore.os.metadata.kettle.config;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.PluginFactory;
import com.isyscore.os.metadata.kettle.step.*;
import com.isyscore.os.metadata.kettle.vis.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KettleConfig {

    @Bean
    public FlowParser flowParser() {
        FlowParser flowParser = new FlowParser();
        flowParser.registerParser(KettleDataFlowNodeType.TableInput, TableInputNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.TableOutput, TableOutputNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.MergeJoin, MergeJoinNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.GroupBy, GroupByNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.FilterRows, FilterRowsNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.InsertUpdate, InsertUpdateNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.Constant, ConstantNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.ValueMapper, ValueMapperNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.Unique, UniqueNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.SortRows, SortRowsNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.BlockingStep, BlockingNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.ExecSQL, ExecSQLNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.Rest, RestNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.JsonInput, JsonInputNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.EmptyValueDeal, EmptyValueDealNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.JavaExpression, JavaExpressionNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.AbnormalCheck, AbnormalCheckNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.SwitchCase, SwitchCaseNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.ReservoirSampling, ReservoirSamplingNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.MergeRows, MergeRowsNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.StreamLookup, StreamLookupNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.Append, AppendNode.class);
        flowParser.registerParser(KettleDataFlowNodeType.Metric, MetricNode.class);
        return flowParser;
    }

    @Bean
    public PluginFactory pluginFactory(){
        PluginFactory pluginFactory = new PluginFactory();
        pluginFactory.register(KettleDataFlowNodeType.TableInput,TableInputStep.class);
        pluginFactory.register(KettleDataFlowNodeType.TableOutput,TableOutputStep.class);
        pluginFactory.register(KettleDataFlowNodeType.MergeJoin, MergeJoinStep.class);
        pluginFactory.register(KettleDataFlowNodeType.GroupBy, GroupByStep.class);
        pluginFactory.register(KettleDataFlowNodeType.FilterRows, FilterRowsStep.class);
        pluginFactory.register(KettleDataFlowNodeType.InsertUpdate, InsertUpdateStep.class);
        pluginFactory.register(KettleDataFlowNodeType.Constant, ConstantStep.class);
        pluginFactory.register(KettleDataFlowNodeType.ValueMapper, ValueMapperStep.class);
        pluginFactory.register(KettleDataFlowNodeType.Unique, UniqueStep.class);
        pluginFactory.register(KettleDataFlowNodeType.SortRows, SortRowsStep.class);
        pluginFactory.register(KettleDataFlowNodeType.BlockingStep, BlockingStep.class);
        pluginFactory.register(KettleDataFlowNodeType.ExecSQL, ExecSQLStep.class);
        pluginFactory.register(KettleDataFlowNodeType.Rest, RestStep.class);
        pluginFactory.register(KettleDataFlowNodeType.JsonInput, JsonInputStep.class);
        pluginFactory.register(KettleDataFlowNodeType.EmptyValueDeal, EmptyValueDealStep.class);
        pluginFactory.register(KettleDataFlowNodeType.JavaExpression, JavaExpressionStep.class);
        pluginFactory.register(KettleDataFlowNodeType.AbnormalCheck, AbnormalCheckStep.class);
        pluginFactory.register(KettleDataFlowNodeType.SwitchCase, SwitchCaseStep.class);
        pluginFactory.register(KettleDataFlowNodeType.ReservoirSampling, ReservoirSamplingStep.class);
        pluginFactory.register(KettleDataFlowNodeType.StreamLookup, StreamLookupStep.class);
        pluginFactory.register(KettleDataFlowNodeType.Append, AppendStep.class);
        pluginFactory.register(KettleDataFlowNodeType.MergeRows, MergeRowsStep.class);
        pluginFactory.register(KettleDataFlowNodeType.Metric, MetricStep.class);
        return pluginFactory;
    }

}
