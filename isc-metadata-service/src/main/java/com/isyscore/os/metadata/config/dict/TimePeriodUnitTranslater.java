package com.isyscore.os.metadata.config.dict;

import com.isyscore.os.metadata.common.dict.CachedDictTranslater;
import com.isyscore.os.metadata.enums.TimePeriodUnit;

public class TimePeriodUnitTranslater extends CachedDictTranslater {
    @Override
    public String doGetLabelByCode(String dictType, Object dictCode) {
        TimePeriodUnit[] types = TimePeriodUnit.values();
        for (TimePeriodUnit type : types) {
            if (type.getSymbol().equals(dictCode)) {
                return type.getLabel();
            }
        }
        return "未知";
    }
}
