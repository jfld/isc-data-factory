package com.isyscore.os.metadata.model.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("datamodel")
public class Datamodel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("唯一编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("用于定义数据模型的数据范围的sql")
    private Long sqlRefId;

    @ApiModelProperty("用于定义数据模型的数据范围的ER关系模型")
    private Long erRefId;

    @ApiModelProperty("该数据模型关联的数据源")
    private Long dsRefId;

    private String dbName;

    @ApiModelProperty("数据模型的名称")
    private String name;

    @ApiModelProperty("数据模型的说明信息")
    private String description;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("1: 通过SQL创建，2：通过ER图创建")
    private Integer createType;

}
