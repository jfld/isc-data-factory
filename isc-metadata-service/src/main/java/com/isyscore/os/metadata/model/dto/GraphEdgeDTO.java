package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GraphEdgeDTO {

    @ApiModelProperty("源节点的ID")
    private String sourceId;

    @ApiModelProperty("目标节点的ID")
    private String targetId;

    @ApiModelProperty("关系说明")
    private String relation;

}
