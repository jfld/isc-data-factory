package com.isyscore.os.metadata.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.isyscore.os.metadata.model.entity.Datamodel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataModelDTO {

    @ApiModelProperty("数据模型id")
    private Long id;

    @ApiModelProperty("用于定义数据模型的数据范围的sql (列表查询时该项为空)")
    private String sql;

    @ApiModelProperty("该数据模型关联的数据源ID")
    private Long dsId;

    @ApiModelProperty("该数据模型关联的数据库名称")
    private String dbName;

    @ApiModelProperty("数据模型的名称")
    private String name;

    @ApiModelProperty("数据模型的说明信息")
    private String description;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("数据模型的创建类型：1: 通过SQL创建，2：通过ER图创建")
    private Integer createType;

    @ApiModelProperty("ER图的配置数据，用于在前端还原ER关系图 (列表查询时该项为空)")
    private String erLayoutJSON;

    @ApiModelProperty("数据模型所属字段 (列表查询时该项为空)")
    private List<DataModelColumnDTO> columns;

    public void fromDataModleEntity(Datamodel datamodel) {
        if (datamodel != null) {
            this.id = datamodel.getId();
            this.createTime = datamodel.getCreateTime();
            this.createType = datamodel.getCreateType();
            this.dbName = datamodel.getDbName();
            this.dsId = datamodel.getDsRefId();
            this.description = datamodel.getDescription();
            this.name = datamodel.getName();
        }
    }

}
