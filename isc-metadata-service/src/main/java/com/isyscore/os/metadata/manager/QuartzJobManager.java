package com.isyscore.os.metadata.manager;

import com.google.common.collect.ImmutableMap;
import com.isyscore.boot.quartz.QuartzJobInfo;
import com.isyscore.boot.quartz.QuartzTemplate;
import com.isyscore.os.core.exception.DataFactoryException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.isyscore.os.core.exception.ErrorCode.*;

@Service
@Slf4j
public class QuartzJobManager {

    @Autowired
    private QuartzTemplate quartzTemplate;

    public void addJob(Long id, String cron,String group, Class<? extends QuartzJobBean> jobBean) {
        boolean existed = hasTask(id,group);
        if (existed) {
            throw new DataFactoryException(DUPLICATE_METRIC_JOB);
        }
        Map<String, Object> params = ImmutableMap.of("id", String.valueOf(id));
        try {
            quartzTemplate.addJob(jobBean, String.valueOf(id), group, cron, params, LocalDateTime.now());
        } catch (Exception e) {
            log.error("Quartz异常：", e);
            throw new DataFactoryException(JOB_SCHEDULER_ERROR);
        }
    }

    public void deleteJob(Long id,String group) {
        boolean existed = hasTask(id,group);
        if (existed) {
            try {
                quartzTemplate.deleteJob(group, String.valueOf(id));
            } catch (Exception e) {
                log.error("Quartz异常：", e);
                throw new DataFactoryException(JOB_SCHEDULER_ERROR);
            }
        }
    }

    public boolean hasTask(Long id,String group) {
        boolean existed = false;
        List<QuartzJobInfo> quartzJobInfos;
        try {
            quartzJobInfos = quartzTemplate.schedulers();
        } catch (Exception e) {
            log.error("Quartz异常：", e);
            throw new DataFactoryException(JOB_SCHEDULER_ERROR);
        }
        for (QuartzJobInfo job : quartzJobInfos) {
            if (job.getName().equals(String.valueOf(id)) && job.getGroup().equals(group)) {
                existed = true;
                break;
            }
        }
        return existed;
    }

    public void stopMetricJob(String group) {
        try {
            quartzTemplate.deleteJobsByGroup(group);
        } catch (Exception e) {
            log.error("停止定时任务失败", e);
            throw new DataFactoryException(COMMON_ERROR, "停止定时任务失败");
        }
    }

    public void stopAllMetricJob() {
        try {
            List<QuartzJobInfo> quartzJobInfos = quartzTemplate.schedulers();
            for (QuartzJobInfo job : quartzJobInfos) {
                quartzTemplate.deleteJob(job.getGroup(),job.getName());
            }
        } catch (Exception e) {
            log.error("Quartz异常：", e);
            throw new DataFactoryException(JOB_SCHEDULER_ERROR);
        }

    }

}
