package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

@ApiModel("指标组件")
@Data
public class MetricNode extends VisNode {
    @ApiModelProperty("指标ID")
    @NotEmpty(message = "指标不能为空")
    private String metricId;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.metricId =  jsonObject.getString("metricId");
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.INPUTS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.Metric;
    }

}
