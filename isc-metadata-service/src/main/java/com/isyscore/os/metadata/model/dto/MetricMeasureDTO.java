package com.isyscore.os.metadata.model.dto;

import com.isyscore.os.metadata.model.entity.MetricMeasure;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MetricMeasureDTO {

    @ApiModelProperty("度量值对应的字段名")
    @NotBlank
    private String measureColName;

    @ApiModelProperty("度量值对应的聚合方式")
    private String measureAggType;

    public void copyFromEnity(MetricMeasure entity) {
        this.setMeasureColName(entity.getMeasureColName());
        this.setMeasureAggType(entity.getMeasureAggType());
    }

    public MetricMeasure toEntity() {
        MetricMeasure entity = new MetricMeasure();
        entity.setMeasureColName(this.getMeasureColName());
        entity.setMeasureAggType(this.getMeasureAggType());
        return entity;
    }

}
