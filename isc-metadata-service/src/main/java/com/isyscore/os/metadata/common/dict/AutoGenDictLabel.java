package com.isyscore.os.metadata.common.dict;


import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于字典值的翻译，可在bean序列化为JSON时自动根据字典值生成字典的文本字段
 * eg:
 *
 * @AutoGenDictLabel(dictType = "unit")
 * private String unitCode;
 * 这样的注解设置可自动生成一个unitCodeLabel的字段，值为单位名称
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(
        using = AutoGenDictLabelSerializer.class
)
public @interface AutoGenDictLabel {

    /**
     * 字典类型，需要为每个类型注册对应的DictTranslater
     */
    String value();

    /**
     * 生成的label的字段名，默认为字典项的属性名+”Label“
     */
    String propName() default "";

}
