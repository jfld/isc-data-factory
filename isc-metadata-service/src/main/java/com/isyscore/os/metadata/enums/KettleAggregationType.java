package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum KettleAggregationType {

    max("6", "求最大值"),
    min("5", "求最小值"),
    count("7", "计数"),
    distinctCount("17", "去重计数"),
    sum("1", "求和"),
    avg("2", "求平均值");

    private String symbol;

    private String label;


    KettleAggregationType(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;
    }

}
