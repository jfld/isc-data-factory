package com.isyscore.os.metadata.service.diop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "diop")
@Component
public class DIOPConfiguration {

    private String importUrl;

    private String exportUrl;

    private String dataTypesUrl;

    private String registerUrl;

    private String appVersion;

    private String compatibleVersionMin;

    private String compatibleVersionMax;

    private String appCode;

    private String serviceId;

    private String exportFileStoragePath;

}
