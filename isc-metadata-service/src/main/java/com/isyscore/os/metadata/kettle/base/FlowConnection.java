package com.isyscore.os.metadata.kettle.base;


import com.isyscore.os.metadata.enums.KettleDataSourceType;
import com.isyscore.os.metadata.model.dto.DataSourceDTO;
import com.isyscore.os.metadata.service.DataSourceService;
import com.isyscore.os.metadata.service.impl.DataSourceServiceImpl;
import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Data
@Accessors(chain = true)
public class FlowConnection {
    private Long id;
    private String name;
    private String server;
    private String type;
    private String database;
    private String port;
    private String username;
    private String password;
    private String basicType;
    private String basicValue;

    public FlowConnection fill(DataSourceDTO dataSource,String  databaseName){
        this.id = dataSource.getDataSourceId();
        this.setName(dataSource.getName());
        this.setServer(dataSource.getIp());
        this.setType(KettleDataSourceType.getType(dataSource.getType()).getName());
        this.setDatabase(databaseName);
        this.setPort(dataSource.getPort());
        this.setUsername(dataSource.getUserName());
        this.setPassword(dataSource.getPlaintextPassword());
        this.setBasicType(dataSource.getBasicType());
        this.setBasicValue(dataSource.getBasicValue());
        return  this;
    }

}
