package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.AbnormalCheckNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.abnormalcheck.AbnormalCheckConfig;
import org.pentaho.di.trans.steps.abnormalcheck.AbnormalCheckMeta;
import java.util.List;

/**
 * @author dc
 * @Type AbnormalStep.java
 * @Desc 异常检测
 * @date 2022/10/20 15:57
 */
public class AbnormalCheckStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        AbnormalCheckNode node = (AbnormalCheckNode) step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId(StepPluginType.class, KettleDataFlowNodeType.AbnormalCheck.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass(sp);
        AbnormalCheckMeta abnormalCheckMeta = (AbnormalCheckMeta) stepMetaInterface;
        String judgeName = node.getJudgeName();
        List<AbnormalCheckNode.CheckField> fields = node.getFields();
        AbnormalCheckConfig[] abnormalCheckConfigs = new AbnormalCheckConfig[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            AbnormalCheckNode.CheckField field = fields.get(i);
            String fieldName = field.getField();
            double multiple = field.getMultiple();
            String compareSymbols = field.getCompareSymbols();
            boolean medianFlag = false;
            boolean fixedFlag = false;
            boolean meanFlag = false;
            AbnormalCheckNode.CheckWay checkWay = field.getCheckWay();
            switch (checkWay) {
                case MEAN:
                    meanFlag = true;
                    break;
                case FIXED:
                    fixedFlag = true;
                    break;
                case MEDIAN:
                    medianFlag = true;
                default:
                    break;
            }
            AbnormalCheckConfig abnormalCheckConfig = new AbnormalCheckConfig(medianFlag, fixedFlag, meanFlag, fieldName
                    , compareSymbols, multiple, judgeName);
            abnormalCheckConfigs[i] = abnormalCheckConfig;
        }
        abnormalCheckMeta.setConfigs(abnormalCheckConfigs);
        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.AbnormalCheck.name(), node.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        return null;
    }
}
    
