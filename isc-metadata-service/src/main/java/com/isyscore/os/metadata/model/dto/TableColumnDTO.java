package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/30 14:30
 */
@Data
public class TableColumnDTO {
    private String name;
    private String tableName;
    private String aliasName;
    private Integer statisticsType;
    private List<Integer> calculationType;
    private String columnType;
    private String description;
}
