package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum KettleTaskStatus {

    RUNNING("running", "Running",1), FAIL("fail", "Halting",2), SUCCESS("success", "Finished",0);

    private String status;

    private String label;
    private Integer level;

    KettleTaskStatus(String status, String label,Integer level) {
        this.status = status;
        this.label = label;
        this.level = level;
    }

    public static String getTypeByLable(String label) {
        KettleTaskStatus[] carTypeEnums = values();
        for (KettleTaskStatus kettleTaskStatus : carTypeEnums) {
            if (kettleTaskStatus.getLabel().equals(label)) {
                return kettleTaskStatus.getStatus();
            }
        }
        return "fail";
    }

    public static String getTypeByLevel(Integer leve) {
        KettleTaskStatus[] carTypeEnums = values();
        for (KettleTaskStatus kettleTaskStatus : carTypeEnums) {
            if (kettleTaskStatus.getLevel().equals(leve)) {
                return kettleTaskStatus.getStatus();
            }
        }
        return "fail";
    }

    public static Integer getLevelByStatus(String status) {
        KettleTaskStatus[] carTypeEnums = values();
        for (KettleTaskStatus kettleTaskStatus : carTypeEnums) {
            if (kettleTaskStatus.getStatus().equals(status)) {
                return kettleTaskStatus.getLevel();
            }
        }
        return -1;
    }

}
