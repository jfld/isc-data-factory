package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class FlowDTO {

    @ApiModelProperty("流程ID")
    private String definitionId;

    @ApiModelProperty("任务流名称")
    @NotEmpty(message = "任务流名称不能为空")
    @Pattern(regexp = "[a-zA-Z0-9_\\u4e00-\\u9fa5]{1,20}", message = "只能包含中文、英文字母、数字、下划线，长度1~20位")
    private String flowName;

    @ApiModelProperty("分组ID")
    private String groupId;

    @ApiModelProperty("任务描述")
    private String description;

}
