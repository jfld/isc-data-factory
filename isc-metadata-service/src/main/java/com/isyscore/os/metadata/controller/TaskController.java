package com.isyscore.os.metadata.controller;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/24 20:38
 */
@RestController
@Api(tags = "异步任务")
@RequestMapping("${api-full-prefix:}/task")
public class TaskController {

    @Resource
    private TaskService taskService;

    @GetMapping("/{taskId}")
    @ApiOperation("获取任务数据")
    public JSONObject get(@PathVariable("taskId") String taskId) {
        JSONObject rst = new JSONObject();
        rst.put("progress", taskService.getProgress(taskId));
        rst.put("data", taskService.get(taskId).getData());
        return rst;
    }
}
