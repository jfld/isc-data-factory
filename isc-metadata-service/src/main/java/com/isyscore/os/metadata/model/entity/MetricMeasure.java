package com.isyscore.os.metadata.model.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <p>
 * 保存指标的所涉及的度量值，以及度量值所对应的聚合方式
 * </p>
 *
 * @author 
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("metric_measure")
public class MetricMeasure implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("度量值对应的指标id")
    private Long metricRefId;

    @ApiModelProperty("度量值对应的字段名")
    private String measureColName;

    @ApiModelProperty("度量值对应的聚合方式")
    private String measureAggType;

}
