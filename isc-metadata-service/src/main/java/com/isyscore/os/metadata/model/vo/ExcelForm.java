package com.isyscore.os.metadata.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * TODO
 *
 * @author zhangyn
 * @version 1.0
 * @date 2021/6/4 5:03 下午
 */
@Data
public class ExcelForm {
    @ApiModelProperty("Excel 的表头配置")
    private List<ExcelDataVo> excelList;
    @ApiModelProperty("数据源配置信息")
    private DataSourceVO dataSource;
}
