package com.isyscore.os.metadata.constant;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/12/14 11:14
 */
public interface DataSourceConstant {

    String MYSQL_KEY = "PRI";

    String SQLSERVER_KEY = "PRI";

    Integer CLICKHOUSE_KEY = 1;

    String IMPORT_METHOD_NEW = "NEW";
    String IMPORT_METHOD_CLEAR = "CLEAR";
    String IMPORT_METHOD_APPEND = "APPEND";

}
