package com.isyscore.os.metadata.model.dto.diop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class DiopImportReq {

    @ApiModelProperty(value = "应用id", example = "udmp")
    @NotBlank
    private String appCode;

    @ApiModelProperty(value = "文件存储路径（绝对路径）")
    @NotBlank
    private String dirPath;

    @ApiModelProperty("占位符")
    private List<Object> placeHolders;

    @ApiModelProperty(value = "导入的结果回调地址")
    @NotBlank
    private String callbackUrl;

}
