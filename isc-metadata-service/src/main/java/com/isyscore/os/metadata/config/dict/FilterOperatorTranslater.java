package com.isyscore.os.metadata.config.dict;

import com.isyscore.os.metadata.common.dict.CachedDictTranslater;
import com.isyscore.os.metadata.enums.FilterOperator;

public class FilterOperatorTranslater extends CachedDictTranslater {

    @Override
    public String doGetLabelByCode(String dictType, Object dictCode) {
        FilterOperator[] types = FilterOperator.values();
        for (FilterOperator type : types) {
            if (type.getSymbol().equals(dictCode)) {
                return type.getLabel();
            }
        }
        return "未知";
    }

}
