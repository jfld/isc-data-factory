package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MetricGroupDTO {

    @ApiModelProperty("指标分组的ID")
    private Long id;

    @ApiModelProperty("指标分组的名称")
    private String groupName;

}
