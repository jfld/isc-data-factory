package com.isyscore.os.metadata.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 提供静态方法获取spring bean实例
 *
 * @author
 */
@Component
public class SpringContextHolder implements ApplicationContextAware {

    private static ApplicationContext _applicationContext = null;

    public static <T> T getBean(Class<T> requiredType) {
        if (_applicationContext != null) {
            return _applicationContext.getBean(requiredType);
        } else {
            return null;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        _applicationContext = applicationContext;
    }

}