package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MetricJobCronConfigReq {

    @ApiModelProperty("定时任务运行的时间")
    private String taskCron;

}
