package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.kettle.dto.FlowNodeValidatedResultDTO;
import com.isyscore.os.metadata.kettle.vis.*;
import com.isyscore.os.metadata.model.dto.NodeNotAllowsDTO;
import com.isyscore.os.metadata.service.ETLFlowNodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.pentaho.di.trans.steps.reservoirsampling.ReservoirSampling;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api-full-prefix}/etlflow/node")
@Api(tags = "ETL可视化节点")
@RequiredArgsConstructor
public class ETLFlowNodeController {

    private final ETLFlowNodeService etlFlowNodeService;
    @ApiOperation("表输入")
    @GetMapping("/doc/TableInput")
    public TableInputNode tableInputNode() {
        return new TableInputNode();
    }

    @ApiOperation("表输出")
    @GetMapping("/doc/TableOutput")
    public TableOutputNode tableOutputNode() {
        return new TableOutputNode();
    }

    @ApiOperation("更新/插入")
    @GetMapping("/doc/InsertUpdate")
    public InsertUpdateNode insertUpdatetNode() {
        return new InsertUpdateNode();
    }

    @ApiOperation("行过滤")
    @GetMapping("/doc/FilterRows")
    public FilterRowsNode filterRowsNode() {
        return new FilterRowsNode();
    }

    @ApiOperation("增加常量")
    @GetMapping("/doc/Constant")
    public ConstantNode constantNode() {
        return new ConstantNode();
    }

    @ApiOperation("分组")
    @GetMapping("/doc/GroupBy")
    public GroupByNode groupByNode() {
        return new GroupByNode();
    }

    @ApiOperation("记录集连接")
    @GetMapping("/doc/MergeJoin")
    public MergeJoinNode mergeJoinNode() {
        return new MergeJoinNode();
    }

    @ApiOperation("去除重复记录")
    @GetMapping("/doc/Unique")
    public UniqueNode uniqueNode() {
        return new UniqueNode();
    }

    @ApiOperation("值映射")
    @GetMapping("/doc/ValueMapper")
    public ValueMapperNode valueMapperNode() {
        return new ValueMapperNode();
    }

    @ApiOperation("行排序")
    @GetMapping("/doc/SortRows")
    public SortRowsNode SortRows() {
        return new SortRowsNode();
    }

    @ApiOperation("空值处理")
    @GetMapping("/doc/EmptyValueDeal")
    public EmptyValueDealNode EmptyValueDeal() {
        return new EmptyValueDealNode();
    }

    @ApiOperation("数据采样")
    @GetMapping("/doc/ReservoirSampling")
    public ReservoirSamplingNode ReservoirSampling() {
        return new ReservoirSamplingNode();
    }

    @ApiOperation("计算表达式")
    @GetMapping("/doc/JavaExpression")
    public JavaExpressionNode JavaExpressionNode() {
        return new JavaExpressionNode();
    }

    @ApiOperation("异常值检测")
    @GetMapping("/doc/AbnormalCheck")
    public AbnormalCheckNode AbnormalCheckNode() {
        return new AbnormalCheckNode();
    }


    @ApiOperation("合并记录组件")
    @GetMapping("/doc/MergeRows")
    public MergeRowsNode MergeRowsNode() {
        return new MergeRowsNode();
    }

    @ApiOperation("流查询")
    @GetMapping("/doc/StreamLookup")
    public StreamLookupNode StreamLookupNode() {
        return new StreamLookupNode();
    }

    @ApiOperation("追加流")
    @GetMapping("/doc/Append")
    public AppendNode AppendNode() {
        return new AppendNode();
    }

    @ApiOperation("指标")
    @GetMapping("/doc/Metric")
    public MetricNode MetricNode() {
        return new MetricNode();
    }

    @ApiOperation("判断两种节点类型间是否能够进行连接")
    @GetMapping("/accept")
    public RespDTO<List<NodeNotAllowsDTO>> accept() {
        return RespDTO.onSuc(etlFlowNodeService.accept());
    }

    @ApiOperation("验证当前的流程图各个节点配置是否合法")
    @PostMapping("/validate")
    public RespDTO<List<FlowNodeValidatedResultDTO>> validateGraph(@RequestBody VisGraph userConfig) {
        return RespDTO.onSuc(etlFlowNodeService.validateGraph(userConfig));
    }

}

