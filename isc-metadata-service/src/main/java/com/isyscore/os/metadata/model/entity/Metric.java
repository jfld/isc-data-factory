package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import static com.isyscore.os.metadata.constant.CommonConstant.METRIC_PATH_SPLITTER;


/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("metric")
public class Metric implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("指标对应的SQL计算语句")
    private Long sqlRefId;

    @ApiModelProperty("指标分组编号")
    private Long groupRefId;

    @ApiModelProperty("指标所属的数据模型")
    private Long datamodelRefId;

    @ApiModelProperty("定义指标的派生路径，使用“/”对父指标的code值进行分割，例如：xxx/aaaa/dddd/cccc ")
    private String extendPath;

    @ApiModelProperty("指标名称")
    private String name;

    @ApiModelProperty("指标说明")
    private String description;

    @ApiModelProperty("该指标对应的统计维度列名")
    private String dimension;

    @ApiModelProperty("该指标对应的计算公式，如果是原子指标，此项为空,例如：(A1+A2)/A3")
    private String formula;

    @ApiModelProperty("该指标对应的统计周期字段列名")
    private String timePeriodDim;

    @ApiModelProperty("该指标对应的统计周期时间单位")
    private String timePeriodUnit;

    @ApiModelProperty("该指标对应的统计周期跨度")
    private Integer timePeriodNumber;

    @ApiModelProperty("指标计算任务调度表达式，采用quartz语法")
    private String taskCron;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(exist = false)
    private List<MetricMeasure> measures;

    public boolean isExtended() {
        return !METRIC_PATH_SPLITTER.equals(this.getExtendPath());
    }

}
