package com.isyscore.os.metadata.model.dto.diop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class DiopExportReq {

    @ApiModelProperty(value = "应用id", example = "udmp")
    @NotBlank
    private String appCode;

    @ApiModelProperty("需要导出的模块")
    @NotEmpty
    private List<DiopResourceDto> dataTypes;

    @ApiModelProperty(value = "导出回调地址")
    @NotBlank
    private String callbackUrl;

}
