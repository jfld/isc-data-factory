package com.isyscore.os.metadata.enums;

import lombok.Getter;

import java.sql.*;
import java.util.stream.Stream;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 16:16
 */
@Getter
@SuppressWarnings("all")
public enum DataSourceTypeEnum {

    MYSQL(0, "MySQL", "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=UTF8&autoReconnect=true&useSSL=false", "com.mysql.jdbc.Driver"),//,

    MARIADB(7, "MariaDB", "jdbc:mariadb://%s:%s/%s?useUnicode=true&characterEncoding=UTF8&autoReconnect=true&useSSL=false", "org.mariadb.jdbc.Driver"),

    ORACLE(1, "Oracle", "jdbc:oracle:thin:@%s:%s:%s,jdbc:oracle:thin:@//%s:%s/%s", "oracle.jdbc.driver.OracleDriver"),

    CLICKHOUSE(2, "ClickHouse", "jdbc:clickhouse://%s:%s/%s?useUnicode=true&characterEncoding=UTF8&autoReconnect=true&useSSL=false", "ru.yandex.clickhouse.ClickHouseDriver"),

//    SQLSERVER(3, "SQLServer", "jdbc:sqlserver://%s:%s;DatabaseName=%s", "com.microsoft.sqlserver.jdbc.SQLServerDriver"),

    SQLSERVER(3, "SQLServer", "jdbc:jtds:sqlserver://%s:%s/%s", "net.sourceforge.jtds.jdbc.Driver"),

    DM(4, "DM", "jdbc:dm://%s:%s/%s", "dm.jdbc.driver.DmDriver"),

    TD(5, "TDengine", "jdbc:TAOS-RS://%s:%s/%s", "com.taosdata.jdbc.rs.RestfulDriver"),

    PGSQL(6, "PostgreSQL", "jdbc:postgresql://%s:%s/%s", "org.postgresql.Driver"),

    KINGBASE(8, "Kingbasees", "jdbc:kingbase8://%s:%s/%s", "com.kingbase8.Driver");//人大金仓
    private Integer code;

    private String name;
    /**
     * 连接地址
     */
    private String url;
    /**
     * 驱动类
     */
    private String driverClass;

    DataSourceTypeEnum(Integer code, String name, String url, String driverClass) {
        this.code = code;
        this.name = name;
        this.url = url;
        this.driverClass = driverClass;
    }

    public static DataSourceTypeEnum getType(Integer code) {
        return Stream.of(DataSourceTypeEnum.values()).filter(bean -> bean.getCode().equals(code)).findFirst().orElse(null);
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //1.加载驱动程序
        Class.forName("oracle.jdbc.driver.OracleDriver");
        //2. 获得数据库连接
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.10.10:1521:helowin", "scott", "scott");
        //3.操作数据库，实现增删改查
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM \"PM\".\"ONLINE_MEDIA\"");
        //如果有数据，rs.next()返回true
        while (rs.next()) {
            System.out.println(rs.getInt("PRODUCT_ID"));
        }
    }

}
