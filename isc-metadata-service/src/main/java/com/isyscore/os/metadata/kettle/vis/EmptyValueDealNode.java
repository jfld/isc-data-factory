package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.EmptyValueDealFunctionType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liyang
 */
@ApiModel("空值处理")
@Data
public class EmptyValueDealNode extends VisNode{

    @ApiModelProperty("字段处理信息列表")
    private List<DealField> fields;


    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.fields = jsonObject.getJSONArray("fields").stream().map(o -> JSON.parseObject(JSON.toJSONString(o), DealField.class)).collect(Collectors.toList());
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.EmptyValueDeal;
    }


    @Data
    public static class DealField{
        @ApiModelProperty("字段")
        public String field;
        @ApiModelProperty("处理方式[MIN,\n" +
                "    MAX,\n" +
                "    MEAN, //平均\n" +
                "    MEDIAN,//中位数\n" +
                "    FIXED;//固定值]")
        public EmptyValueDealFunctionType functionType;

        private String fixedValue;

        public DealField(String field, EmptyValueDealFunctionType functionType) {
            this.field = field;
            this.functionType = functionType;
        }
    }

}
