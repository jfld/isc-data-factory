package com.isyscore.os.metadata.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.metadata.model.dto.DataSourceDTO;
import com.isyscore.os.metadata.model.dto.DataSourceItem;
import com.isyscore.os.metadata.model.dto.FormDataSourceDTO;
import com.isyscore.os.metadata.model.entity.DataSource;
import com.isyscore.os.metadata.model.query.DataSourceQuery;
import com.isyscore.os.metadata.model.query.DataSourceQueryWrapper;
import com.isyscore.os.metadata.model.query.IDataSourceQuery;
import com.isyscore.os.metadata.model.vo.DataSourceVO;
import com.isyscore.os.metadata.model.vo.ExcelDataVo;
import com.isyscore.os.metadata.model.vo.ResultVO;
import com.isyscore.os.metadata.model.vo.TableVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 14:48
 */
public interface DataSourceService extends IService<DataSource> {
    void lock(String datasourceId, String databaseName, Integer isLocked);

    void addDataSource(DataSourceDTO dataSourceDTO);

    Boolean updateDataSource(DataSourceDTO dataSourceDTO);

    Boolean delBatch(List<Long> datasourceIds);

    IPage<DataSourceDTO> getList(DataSourceQueryWrapper query);

    List<String> test(DataSourceDTO dataSourceDTO);

    List<DataSourceDTO> getAll();

    DataSourceDTO get(Long dataSourceId, boolean joinTenant);

    ResultVO getTableList(Long dataSourceId, String databaseName, boolean joinTenant);

    ResultVO getTableListWithAlias(Long dataSourceId, String databaseName, boolean joinTenant);

    ResultVO getTableStruct(Long dataSourceId, String databaseName, String tableName);

    ResultVO getTableData(Long dataSourceId, IDataSourceQuery query);

    List<ResultVO> getLinkTableList(Long dataSourceId, String databaseName, String tableName, String taskId);

    FormDataSourceDTO searchTables(Long dataSourceId, String table);

    JSONArray listDataType(Long dataSourceId);

    void renameTable(TableVO tableVO);

    List<Map<String,Object>> getTreeTable(long dataSourceId, String databaseName, boolean joinTenant);

    DataSourceDTO getDataSource(Long dataSourceId);

    DataSourceDTO getDataSourceMarkIgnore(Long dataSourceId);

    Integer getOutDataSourceCount();

    List<Map> importData(MultipartFile file, List<ExcelDataVo> params);

    List<String> schemas(Long dataSourceId, String databaseName);

    List<DataSource> exportDataSourceWithIds(Set<Long> ids);

    void importDataSources(List<DataSource> dataSources);

    IPage<DataSourceItem> pageDataSource(DataSource dataSource, PageRequest page);

}
