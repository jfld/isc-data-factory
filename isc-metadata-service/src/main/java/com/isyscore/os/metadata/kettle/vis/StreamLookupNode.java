package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author dc
 * @Type StreamLookupNode.java
 * @Desc 流查询
 * @date 2022/10/20 15:31
 */
@Data
@ApiModel("流查询")
public class StreamLookupNode extends VisNode {


    @ApiModelProperty(value = "查询关联步骤")
    private String lookupStepName;

    @ApiModelProperty(value = "查询所需关键字集合")
    private List<JoinField> joinFields;

    @ApiModelProperty(value = "接受所需关键字集合")
    private List<ReceiveField> receiveFields;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        List<JoinField> joinFields = jsonObject.getJSONArray("joinFields").toJavaList(JoinField.class);
        List<ReceiveField> receiveFields = jsonObject.getJSONArray("receiveFields").toJavaList(ReceiveField.class);
        setLookupStepName(jsonObject.getString("lookupStepName"));
        setJoinFields(joinFields);
        setReceiveFields(receiveFields);
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    @ApiModel(value = "查询所需关键字")
    @Data
    public static class JoinField {
        @ApiModelProperty(value = "key")
        private String key;
        @ApiModelProperty(value = "查询字段")
        private String lookKey;
    }

    @ApiModel(value = "接受指定字段")
    @Data
    public static class ReceiveField {
        @ApiModelProperty(value = "接收字段")
        private String value;
        @ApiModelProperty(value = "别名")
        private String renameValue;
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.StreamLookup;
    }


}
    
