package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.os.metadata.model.dto.DataModelColumnDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class DataModelCreateOrUpdateReq {

    @ApiModelProperty("数据模型的名称")
    @NotBlank
    @Size(max = 32)
    private String name;

    @ApiModelProperty("数据模型的说明信息")
    private String description;

    @ApiModelProperty("该数据模型关联的数据源ID")
    @NotNull
    private Long dsId;

    @ApiModelProperty("数据库名称")
    @NotBlank
    private String dbName;

    @ApiModelProperty("数据模型的SQL语句")
    private String sql;

    @ApiModelProperty("1: 通过SQL创建，2：通过ER图创建")
    private Integer createType = 1;

    @ApiModelProperty("数据模型的表ER数据")
    private CreateERRelationReq erRelation;

    @ApiModelProperty("数据模型的字段信息")
    @NotEmpty
    private List<DataModelColumnDTO> colums;

}
