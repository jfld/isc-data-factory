package com.isyscore.os.metadata.service.diop;

import cn.hutool.http.HttpRequest;
import com.google.common.collect.Maps;
import com.isyscore.device.common.util.JsonMapper;
import com.isyscore.os.metadata.model.dto.diop.DiopResultNotifyDto;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DiopNotifyService {




    public void notifyResult(String callbackUrl, String token, DiopResultNotifyDto notice,String tenantId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("serviceId", notice.getServiceId());
        params.put("appCode", notice.getAppCode());
        params.put("dirPath", notice.getDirPath());
        params.put("message", notice.getMessage());
        params.put("status", notice.getStatus());
        HttpRequest.post(callbackUrl)
                //头信息，多个头信息多次调用此方法即可
                .header("token", token)
                //添加多租户中租户Id
                .header("isc-tenant-id",tenantId)
                .body(JsonMapper.toNonNullJson(params))
                //超时，毫秒
                .timeout(5000)
                .execute().body();
    }

}
