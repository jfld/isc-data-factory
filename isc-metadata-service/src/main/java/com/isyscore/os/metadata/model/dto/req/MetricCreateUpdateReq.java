package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.os.metadata.model.dto.MetricDimFilterDTO;
import com.isyscore.os.metadata.model.dto.MetricMeasureDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class MetricCreateUpdateReq {

    @ApiModelProperty("如果是创建派生指标，需要传入父级指标的ID")
    private Long parentMetricId;

    @ApiModelProperty("所属数据模型ID")
    @NotNull
    private Long datamodelId;

    @ApiModelProperty("指标名称")
    @NotBlank
    private String name;

    @ApiModelProperty("指标分组ID")
    private Long groupId;

    @ApiModelProperty("指标说明")
    private String description;

    @ApiModelProperty("该指标对应的计算公式，由度量值字段名和常数，以及操作符组成，如：(a+2*b)/c，有效的操作符为：+，-，*，/，(，)")
    private String formula;

    @ApiModelProperty("指标所包含的度量值字段及其对于的聚合计算方式")
    @NotEmpty
    @Valid
    private List<MetricMeasureDTO> measures;

    @ApiModelProperty("统计维度字段")
    private String dimension;

    @ApiModelProperty("该指标对应的统计周期字段列名")
    private String timePeriodDim;

    @ApiModelProperty("该指标对应的统计周期时间单位")
    private String timePeriodUnit;

    @ApiModelProperty("该指标对应的统计周期跨度")
    private Integer timePeriodNumber;

    @ApiModelProperty("指标的过滤条件")
    @Valid
    private List<MetricDimFilterDTO> filters;

}
