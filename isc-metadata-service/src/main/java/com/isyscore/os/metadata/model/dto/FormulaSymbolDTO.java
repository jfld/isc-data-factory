package com.isyscore.os.metadata.model.dto;

import com.google.common.base.Strings;
import lombok.Data;

@Data
public class FormulaSymbolDTO {

    private String symbol;

    private String label;

    public String getLabel() {
        if (Strings.isNullOrEmpty(label)) {
            return symbol;
        }
        return label;
    }

}
