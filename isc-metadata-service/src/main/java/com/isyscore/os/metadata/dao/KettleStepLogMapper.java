package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.KettleStepLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Kettle步骤日志 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-08-11
 */
public interface KettleStepLogMapper extends BaseMapper<KettleStepLog> {
    public List<KettleStepLog> listGroup(@Param("taskId")  String taskId);
}
