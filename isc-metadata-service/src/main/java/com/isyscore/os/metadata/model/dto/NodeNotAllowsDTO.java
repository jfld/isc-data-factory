package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NodeNotAllowsDTO {
    @ApiModelProperty("源节点类型")
    String sourceNodeType;
    @ApiModelProperty("目标节点类型")
    String targetNodeType;
}
