package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.ConstantField;
import com.isyscore.os.metadata.kettle.vis.ConstantNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.constant.ConstantMeta;

import java.util.Arrays;
import java.util.List;

public class ConstantStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        ConstantNode addConstantNode =(ConstantNode)step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId( StepPluginType.class, KettleDataFlowNodeType.Constant.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass( sp );
        ConstantMeta constantMeta = (ConstantMeta) stepMetaInterface;

        int size = addConstantNode.getConstantFields().size();

        String[] fieldName = new String[size];
        String[] fieldType = new String[size];
        String[] fieldFormat = new String[size];
        String[] currency = new String[size];
        String[] decimal = new String[size];
        String[] group = new String[size];
        String[] nullif = new String[size];
        int[] fieldLength = new int[size];
        int[] fieldPrecision = new int[size];
        boolean[] setEmptyString = new boolean[size];
        for(int i=0; i<size; i++) {
            ConstantField constantField = addConstantNode.getConstantFields().get(i);
            fieldName[i] = constantField.getName();
            fieldType[i] = "String";
            nullif[i] = constantField.getValue();
            fieldLength[i] = -1;
            fieldPrecision[i] = -1;
            setEmptyString[i] = false;
        }
        constantMeta.setFieldName(fieldName);
        constantMeta.setFieldType(fieldType);
        constantMeta.setFieldFormat(fieldFormat);
        constantMeta.setCurrency(currency);
        constantMeta.setDecimal(decimal);
        constantMeta.setGroup(group);
        constantMeta.setValue(nullif);
        constantMeta.setFieldLength(fieldLength);
        constantMeta.setFieldPrecision(fieldPrecision);
        constantMeta.setEmptyString(setEmptyString);


        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.Constant.name(), addConstantNode.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws KettleStepException {
        return null;
    }

}
