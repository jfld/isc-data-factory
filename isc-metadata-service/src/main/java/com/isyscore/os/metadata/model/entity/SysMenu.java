package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统功能菜单表
 *
 * @author qiany@isyscore.com
 * @since 2021-06-22
 */
@Data
@TableName("sys_menu")
@ApiModel(description = "系统功能菜单表")
public class SysMenu extends Model<SysMenu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "权限编码")
    @NotNull
    @TableId(type = IdType.NONE)
    private String code;

    @ApiModelProperty(value = "父级编码")
    private String parentCode;

    @ApiModelProperty(value = "权限名称")
    private String name;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "访问前端地址")
    private String url;

    @ApiModelProperty(value = "权限类型： 1.菜单 2.按钮 3.其他")
    private Integer type;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;

    @Override
    protected Serializable pkVal() {
        return this.code;
    }

}
