package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TimePeriodDTO {

    @ApiModelProperty("统计周期的key，每个指标计算结果对于同一个key仅保存一份")
    private String key;

    @ApiModelProperty("该统计周期的开始时间")
    private Date startTime;

    @ApiModelProperty("该统计周期的结束时间")
    private Date endTime;

}
