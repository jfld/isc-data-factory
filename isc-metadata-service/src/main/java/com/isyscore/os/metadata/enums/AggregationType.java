package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum AggregationType {

    max("max", "求最大值"),
    min("min", "求最小值"),
    count("count", "计数"),
    distinctCount("distinctCount", "去重计数"),
    sum("sum", "求和"),
    avg("avg", "求平均值");

    private String symbol;

    private String label;

    AggregationType(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;
    }

    public String getAggregationEl(String column) {
        StringBuilder el = new StringBuilder();
        if (symbol.equals(distinctCount.getSymbol())) {
            el.append(count.getSymbol());
            el.append("(DISTINCT ");
            el.append(column);
            el.append(")");
        } else {
            el.append(symbol);
            el.append("(");
            el.append(column);
            el.append(")");
        }
        return el.toString();
    }

}
