package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetIncreTablesReqDTO {

    @ApiModelProperty("sql")
    private String sql;

}
