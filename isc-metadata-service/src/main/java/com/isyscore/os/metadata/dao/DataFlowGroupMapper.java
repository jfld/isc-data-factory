package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.core.model.entity.DataFlowGroup;
import org.mapstruct.Mapper;

/**
 * @author dc
 * @Type DataFlowGroupMapper.java
 * @Desc
 * @date 2022/10/21 11:20
 */
@Mapper
public interface DataFlowGroupMapper extends BaseMapper<DataFlowGroup> {

}
    
