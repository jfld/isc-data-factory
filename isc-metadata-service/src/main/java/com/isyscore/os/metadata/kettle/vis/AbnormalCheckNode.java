package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dc
 * @Type AbnormalCheckNode.java
 * @Desc 异常值处理
 * @date 2022/10/20 15:31
 */
@Data
@ApiModel("异常值处理")
public class AbnormalCheckNode extends VisNode {

    @ApiModelProperty("字段处理信息列表")
    private List<CheckField> fields;

    @ApiModelProperty("标识字段名称")
    private String judgeName;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.fields = jsonObject.getJSONArray("fields").stream().map(o -> JSON.parseObject
                (JSON.toJSONString(o), AbnormalCheckNode.CheckField.class)).collect(Collectors.toList());
        this.setJudgeName(jsonObject.getString("judgeName"));
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.AbnormalCheck;
    }

    @Data
    public static class CheckField {
        @ApiModelProperty("字段")
        public String field;
        @ApiModelProperty("处理方式[" +
                "MEAN, //平均\n" +
                "    MEDIAN,//中位数\n" +
                "    FIXED;//固定值]")
        private CheckWay checkWay;
        @ApiModelProperty("比较符号")
        private String compareSymbols;
        @ApiModelProperty("倍数或者是固定值")
        private double multiple;

        public CheckField(String field, CheckWay checkWay, String compareSymbols, double multiple) {
            this.field = field;
            this.checkWay = checkWay;
            this.compareSymbols = compareSymbols;
            this.multiple = multiple;
        }
    }

    public enum CheckWay {
        MEAN, MEDIAN, FIXED
    }
}
    
