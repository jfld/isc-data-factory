package com.isyscore.os.metadata.enums;

/**
 * @author liy
 * @date 2022-10-20 16:03
 */
public enum JavaExpressionErrorDealType {
    SKIP, //跳过数据行
    IGNORE,//忽略错误设置为NULL
    INTERRUPT;//中断程序
}
