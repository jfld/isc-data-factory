package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author liyang
 */
@ApiModel("数据采样")
@Data
@EqualsAndHashCode(callSuper = false)
public class ReservoirSamplingNode extends VisNode{
    @ApiModelProperty("样本大小")
    @NotNull(message="样本大小")
    private Long sampleSize;



    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.sampleSize = jsonObject.getLong("sampleSize");
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }


    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.ReservoirSampling;
    }
}
