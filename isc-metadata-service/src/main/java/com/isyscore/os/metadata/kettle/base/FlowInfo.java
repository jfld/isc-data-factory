package com.isyscore.os.metadata.kettle.base;


import lombok.Data;

@Data
public class FlowInfo {
    private String name;
}
