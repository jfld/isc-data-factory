package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.AppendNode;
import com.isyscore.os.metadata.kettle.vis.StreamLookupNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.step.errorhandling.StreamInterface;
import org.pentaho.di.trans.steps.append.AppendMeta;
import org.pentaho.di.trans.steps.streamlookup.StreamLookupMeta;

import java.util.List;

/**
 * @author dc
 * @Type AppendStep.java
 * @Desc 追加流
 * @date 2022/10/20 15:57
 */
public class AppendStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        AppendNode node = (AppendNode) step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId(StepPluginType.class, KettleDataFlowNodeType.Append.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass(sp);
        AppendMeta appendMeta = (AppendMeta) stepMetaInterface;

        List<StreamInterface> infoStreams = appendMeta.getStepIOMeta().getInfoStreams();
        StreamInterface headStream = infoStreams.get( 0 );
        StreamInterface tailStream = infoStreams.get( 1 );

        StepMeta head = new StepMeta();
        head.setName(node.getHeadStepname());
        headStream.setStepMeta(head);

        StepMeta tail = new StepMeta();
        tail.setName(node.getTailStepname());
        tailStream.setStepMeta(tail);

        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.Append.name(), node.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        return null;
    }
}
    
