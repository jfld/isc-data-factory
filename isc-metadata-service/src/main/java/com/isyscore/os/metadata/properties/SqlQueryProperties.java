package com.isyscore.os.metadata.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wuwx
 * @since 2022.04.26
 */
@Data
@Component
@ConfigurationProperties(prefix = "sql-query")
public class SqlQueryProperties {

    /**
     * 查询超时时间
     */
    private Integer queryTimeOut=5;

}


