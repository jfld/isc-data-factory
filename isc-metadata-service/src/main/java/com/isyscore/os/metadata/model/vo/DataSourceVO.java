package com.isyscore.os.metadata.model.vo;

import com.isyscore.os.metadata.model.dto.DatabasePair;
import com.isyscore.os.metadata.model.vo.grpups.Insert;
import com.isyscore.os.metadata.model.vo.grpups.Test;
import com.isyscore.os.metadata.model.vo.grpups.Update;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 14:49
 */
@Data
public class DataSourceVO {
    @NotNull(message = "数据源ID不能为空", groups = Update.class)
    private Long id;
    private String userId;
    @NotEmpty(message = "数据源名称不能为空", groups = {Insert.class})
    @Length(min = 1,max = 40, message = "数据源名称长度不符合要求", groups = Insert.class)
    private String name;
    @NotEmpty(message = "ip地址不能为空", groups = {Insert.class, Test.class})
    private String ip;
    @NotEmpty(message = "端口号不能为空", groups = {Insert.class, Test.class})
    private String port;
//    @NotEmpty(message = "必须指定数据库", groups = Insert.class)
    private String databaseName;
    @NotEmpty(message = "数据源用户名不能为空", groups = {Insert.class, Test.class})
    private String userName;
    @NotEmpty(message = "数据源密码不能为空", groups = {Insert.class, Test.class})
    private String password;
    @NotNull(message = "数据源类型不能为空", groups = {Insert.class, Test.class})
    private Integer type;
    private Integer IsDelete;
    private Date createTime;
    private Date updateTime;
    private Long dataSourceId;
    private List<Long> dataSourceIds;
    @NotEmpty(message = "数据库名称不能为空", groups = {Insert.class, Test.class})
    private List<String>databaseNameList;
    private String tableName;


    @ApiModelProperty(value = "连接类型", allowableValues = "BASIC,TNS")
    private String connectType;
    @ApiModelProperty(value = "BASIC下的连接类型", allowableValues = "ServiceName,SID")
    private String basicType;
    @ApiModelProperty(value = "ServiceName,SID的值")
    private String basicValue;

    private List<DatabasePair> databaseList;
}
