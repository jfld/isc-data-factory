package com.isyscore.os.metadata.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Strings;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataModelColumnDTO {

    @ApiModelProperty("字段所属表名")
    private String tableName;

    @ApiModelProperty("字段列名")
    @NotBlank
    private String colName;

    @ApiModelProperty("字段展示名称")
    private String colLabel;

    @ApiModelProperty("数据列类型：1：维度列  2：度量列")
    @NotNull
    private Integer colType;

    @ApiModelProperty("字段在数据库中的字段类型，例如varchar,datetime等等")
    @NotBlank
    private String colDataType;

    public String getColLabel() {
        if (Strings.isNullOrEmpty(colLabel)) {
            return colName;
        }
        return colLabel;
    }

}
