package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum KettleFilterOperator {
    and(2, "并且"),
    or(1, "或者");

    private Integer symbol;

    private String label;

    KettleFilterOperator(Integer symbol, String label) {
        this.symbol = symbol;
        this.label = label;

    }


}
