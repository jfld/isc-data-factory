package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class SQLConfigDTO {
    private DataSourceDTO dataSourceDTO;
    private List<TableColumnDTO> tableColumnDTOList;
    private String sqlText;
}
