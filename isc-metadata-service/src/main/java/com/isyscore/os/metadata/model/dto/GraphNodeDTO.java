package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GraphNodeDTO {

    @ApiModelProperty("节点的唯一标识")
    private String id;

    @ApiModelProperty("节点的展示名称")
    private String label;

    @ApiModelProperty("节点的描述")
    private String description;

    @ApiModelProperty("节点的类型")
    private String type;

}
