package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.metadata.common.page.CompareOperator;
import com.isyscore.os.metadata.common.page.QueryField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetricJobBatchQuery extends PageRequest {

    @ApiModelProperty("查询条件范围，数据指标执行时间-起始")
    @QueryField(name = "start_time", operator = CompareOperator.GE)
    private String startTime;

    @ApiModelProperty("查询条件范围，数据指标执行时间-结束")
    @QueryField(name = "start_time", operator = CompareOperator.LE)
    private String endTime;

    @ApiModelProperty("查询条件，指标名称支持模糊查询")
    @QueryField(operator = CompareOperator.LIKE)
    private String metricName;

    @ApiModelProperty("查询条件，任务状态允许的状态：0:执行中，1：成功，-1：失败")
    @QueryField(operator = CompareOperator.EQ)
    private Integer status;

}
