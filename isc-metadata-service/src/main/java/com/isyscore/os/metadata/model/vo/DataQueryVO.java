package com.isyscore.os.metadata.model.vo;

import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.metadata.model.dto.ConditionDTO;
import com.isyscore.os.metadata.model.dto.OrderByDTO;
import com.isyscore.os.metadata.model.query.IDataSourceQuery;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DataQueryVO extends PageRequest implements IDataSourceQuery {

    private Long dataSourceId;

    private String sqlText;

    private String databaseName;

    private String tableName;

    private List<ConditionDTO> condition;

    private List<OrderByDTO> orderBy;

    private Map<String,String> params;
}
