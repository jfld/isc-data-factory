package com.isyscore.os.metadata.utils;

import com.isyscore.os.core.exception.DataFactoryException;
import com.isyscore.os.core.exception.ErrorCode;

import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/24 20:08
 */
public class UdmpUtils {

    public static String Guid(){
        return UUID.randomUUID().toString().replace("-","");
    }

    /**
     * 解析SQLSERVER的数据库和模式拼接字符串 databaseName.schema -> databaseName
     * @param str
     * @return
     */
    public static String getDatabaseName(String str) {
        if (!str.contains(".")) {
            return str;
        }
        return str.split("\\.")[0];
    }


    /**
     * 解析SQLSERVER的数据库和模式拼接字符串 databaseName.schema -> schema
     * @param dbName
     * @return
     */
    public static String getSchema(String dbName) {
        if (!dbName.contains(".")) {
            //默认返回guest模式
            return null;
        }
        return dbName.split("\\.")[1];
    }


    /**
     * databaseName.scheme -> [databaseName].[schema].[tableName]
     * @param dbName
     * @param tableName
     * @return
     */
    public static String getTableName(String dbName, String tableName) {
        if (!dbName.contains(".")) {
            throw new DataFactoryException(ErrorCode.INPUT_PARAM_ILLEGAL);
        }
        return Arrays.stream(dbName.split("\\.")).map(s -> "[" + s + "]").collect(Collectors.joining(".")) + ".[" + tableName + "]";
    }

}
