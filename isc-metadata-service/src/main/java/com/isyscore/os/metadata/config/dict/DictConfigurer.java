package com.isyscore.os.metadata.config.dict;

import com.isyscore.os.metadata.common.dict.DictTranslaterManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DictConfigurer {

    /**
     * 定义所有的字典解析组件
     */
    @Bean
    public DictTranslaterManager dictTranslaterManager() {
        DictTranslaterManager dictTranslaterManager = new DictTranslaterManager();
        dictTranslaterManager.registerTranslater(DictTypeConst.DATA_MODEL_CREATE_TYPE, new DataModelCreateTypeDictTranslater());
        dictTranslaterManager.registerTranslater(DictTypeConst.GROUP_TYPE, new GroupTypeTranslater());
        dictTranslaterManager.registerTranslater(DictTypeConst.DATA_MODEL_CREATE_TYPE, new DataModelCreateTypeDictTranslater());
        dictTranslaterManager.registerTranslater(DictTypeConst.TIME_PERIOD_UNIT, new TimePeriodUnitTranslater());
        dictTranslaterManager.registerTranslater(DictTypeConst.METRIC_JOB_TASK_STATUS, new MetricJobTaskStatusTranslater());
        return dictTranslaterManager;
    }

}
