package com.isyscore.os.metadata.model.dto;

import com.isyscore.os.metadata.model.entity.DataSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormDataSourceDTO extends DataSource {
    private String name;
    private Integer isBusiness;
    private Long dataSourceId;
    private List<DatabaseDTO> databaseList;
}
