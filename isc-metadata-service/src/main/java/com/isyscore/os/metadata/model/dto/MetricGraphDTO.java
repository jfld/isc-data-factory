package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class MetricGraphDTO {

    @ApiModelProperty("数据血缘中的节点")
    private List<GraphNodeDTO> nodes;

    @ApiModelProperty("节点间的关系（边）")
    private List<GraphEdgeDTO> edges;

}
