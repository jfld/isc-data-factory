package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.JoinType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 数据关联
 */

@ApiModel("数据连接组件")
@Data
@EqualsAndHashCode(callSuper = false)
public class MergeJoinNode extends  VisNode {
    @ApiModelProperty("连接类型")
    @NotNull(message="连接类型不能为空")
    private JoinType joinType;
    @ApiModelProperty("第一个步骤")
    @NotBlank(message="第一个步骤不能为空")
    private String stepOne;
    @ApiModelProperty("第二个步骤")
    @NotBlank(message="第二个步骤不能为空")
    String stepTwo;
    @ApiModelProperty("连接字段信息")
    @NotEmpty(message = "连接字段信息不能为空")
    @Valid
    List<FieldTuple> connectFields;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.joinType = JoinType.valueOf(jsonObject.getString("joinType"));
        this.stepOne = jsonObject.getString("stepOne");
        this.stepTwo = jsonObject.getString("stepTwo");

        this.connectFields = Optional.ofNullable(jsonObject.getJSONArray("connectFields")).orElse(new JSONArray()).stream().map(o ->
                JSON.parseObject(JSON.toJSONString(o), FieldTuple.class)).collect(Collectors.toList());
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.MergeJoin;
    }
}
