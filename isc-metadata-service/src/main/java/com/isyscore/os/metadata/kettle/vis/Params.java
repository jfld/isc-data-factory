package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dc
 * @Type Params.java
 * @Desc get请求对应k-v
 * @date 2022/9/13 10:06
 */
@Data
@ApiModel("GET请求参数")
@NoArgsConstructor
@AllArgsConstructor
public class Params {
    @ApiModelProperty(value = "key")
    private String key;
    @ApiModelProperty(value = "value")
    private String value;

}
    
