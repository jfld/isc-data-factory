package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.core.model.entity.DataFlowDefinition;
import com.isyscore.os.metadata.kettle.dto.*;
import com.isyscore.os.metadata.kettle.vis.VisGraph;
import com.isyscore.os.metadata.model.dto.req.FlowDTO;
import com.isyscore.os.metadata.model.dto.req.FlowDefinitionModifyDTO;
import com.isyscore.os.metadata.model.dto.req.FlowScheduleModifyDTO;

import java.util.List;

public interface DataFlowDefinitionService extends IService<DataFlowDefinition> {
    String createOrUpdateFlowDefinition(FlowDefinitionModifyDTO definition);

    String publishFlowDefinition(FlowDefinitionModifyDTO definition);

    String updateFlowDefinition(String definitionId, FlowDefinitionModifyDTO definition);
    void updateFlowStatus(FlowScheduleModifyDTO definition);

    void deleteFlowDefinition(String definitionId);

    FlowDefinitionDTO getFlowDefinition(String definitionId);

    IPage<FlowDefinitionDTO> pageFlowDefinition(FlowDefinitionPageReqDTO pageRequest);

    List<String> checkFlowDefinition(VisGraph userConfig);

    List<String> tableFields(GetTableFieldsDTO getTableFieldsDTO,boolean escape);

    String getTableName(GetTableFieldsDTO getTableFieldsDTO);

    List<String> inputOutputFields(GetInputOutputFieldsDTO dto);

    void startAllDataJobFromDatabase();

    String previewSql(GetTableFieldsDTO dto);

    List<String> getDataSourceIdFromFlow(String definitionId);

    List<String> getDataSourceIdFromFlow();

    List<String>  increTables(GetIncreTablesReqDTO getIncreTablesReqDTO);

    Long createFlow(FlowDTO flowDTO);

    void copy(FlowDTO flowDTO);

    void updateFlowBase(FlowDTO flowDTO);
}
