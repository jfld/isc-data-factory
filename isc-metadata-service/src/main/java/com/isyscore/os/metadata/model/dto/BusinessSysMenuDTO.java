package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 菜单对象
 * @author admin
 */
@Data
public class BusinessSysMenuDTO {

        @ApiModelProperty("权限编码")
        private String code;

        @ApiModelProperty("父级编码")
        private String parentCode;

        @ApiModelProperty("父级编码")
        private String name;

        @ApiModelProperty("序号")
        private Integer seq;

        @ApiModelProperty("访问前端地址")
        private String url;

        @ApiModelProperty("权限类型 1.菜单 2.按钮 3.其他")
        private Integer type;

        @ApiModelProperty("备注")
        private String remark;

        @ApiModelProperty("子节点")
        private List<BusinessSysMenuDTO> aclList;

        @ApiModelProperty("非子节点")
        private List<BusinessSysMenuDTO> aclModuleList;

        public BusinessSysMenuDTO() { }

        public BusinessSysMenuDTO(String code, String parentCode, String name, String url, Integer type) {
                this.code = code;
                this.parentCode = parentCode;
                this.name = name;
                this.url = url;
                this.type = type;
        }
}
