package com.isyscore.os.metadata.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/24 19:57
 */
@Data
public class TaskDTO implements Serializable {
    private String taskId;
    private Double progress;
    private Integer isStop;
    private String data;
    private String errorMsg;
}

