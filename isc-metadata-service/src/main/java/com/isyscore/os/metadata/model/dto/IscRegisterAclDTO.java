package com.isyscore.os.metadata.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 菜单权限注册请求参数
 */
@Data
@AllArgsConstructor
public class IscRegisterAclDTO {

    /**
     * 应用code
     */
    private String code;

    /**
     * 功能权限地址
     */
    private String aclReturnUrl;

}
