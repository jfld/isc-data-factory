package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.BlockingNode;
import com.isyscore.os.metadata.kettle.vis.ConstantField;
import com.isyscore.os.metadata.kettle.vis.ConstantNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.BlockingRowSet;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.blockingstep.BlockingStepMeta;
import org.pentaho.di.trans.steps.constant.ConstantMeta;

import java.util.List;

public class BlockingStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        BlockingNode blockingNode =(BlockingNode)step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId( StepPluginType.class, KettleDataFlowNodeType.BlockingStep.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass( sp );

        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.BlockingStep.name(), blockingNode.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws KettleStepException {
        return null;
    }

}
