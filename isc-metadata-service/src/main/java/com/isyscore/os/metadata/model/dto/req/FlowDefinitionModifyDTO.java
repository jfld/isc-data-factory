package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.os.metadata.kettle.vis.VisGraph;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class FlowDefinitionModifyDTO {

    private Long definitionId;

//    @ApiModelProperty("任务流名称")
//    @NotEmpty(message = "任务流名称不能为空")
//    @Pattern(regexp = "[a-zA-Z0-9_\\u4e00-\\u9fa5]{1,20}", message = "只能包含中文、英文字母、数字、下划线，长度1~20位")
    private String flowName;

    @ApiModelProperty("日志大小")
    private Integer logSize;

    @ApiModelProperty("是否开启定时调度")
    private Boolean scheduled =Boolean.FALSE;

    @ApiModelProperty("是否开启重复运行")
    private Boolean repeatRun =Boolean.FALSE;

    @ApiModelProperty("定时调度的起始时间")
    private LocalDateTime scheduleStartTime;

    @ApiModelProperty("定时调度的间隔时间")
    private Integer scheduleInterval;

    @ApiModelProperty("定时调度的间隔时间单位,包含year,month,day,hour,minute")
    private String scheduleIntervalTimeUnit;

    @ApiModelProperty("流程图配置信息")
    @NotNull(message = "流程图配置信息不能为空")
    private VisGraph userConfig;

    @ApiModelProperty("是否开启任务重试")
    private Integer retryRun;

}
