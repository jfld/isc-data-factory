package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @author dc
 * @Type AppendNode.java
 * @Desc 追加流
 * @date 2022/10/20 15:31
 */
@Data
@ApiModel("追加流")
public class AppendNode extends VisNode {

    @ApiModelProperty("字段处理信息列表")
    private String headStepname;

    @ApiModelProperty("标识字段名称")
    private String tailStepname;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        setHeadStepname(jsonObject.getString("headStepname"));
        setTailStepname(jsonObject.getString("tialStepname"));
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.Append;
    }

}
    
