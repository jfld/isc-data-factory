package com.isyscore.os.metadata.model.vo;


import lombok.Data;

@Data
public class ColumnInfoVo {
    /**
     * 中文名称
     */
    private String cnName;
    /**
     * 英文名称
     */
    private String enName;
    /**
     * 类据类型
     */
    private String dataType;
    /**
     * 评论
     */
    private String comment;
    /**
     * 长度
     */
    private Integer length;

    /**
     * 用户自定义的名称
     */
    private String aliasName;

    /**
     * 用户是否勾选字段,默认勾选
     */
    private boolean selected = true;

    public void setMaxLength(Integer newLength) {
        if (null == this.length) {
            if (null == newLength) {
                return;
            } else {
                this.length = newLength;
            }
        } else {
            if (null == newLength) {
                return;
            } else if (0 > this.length.compareTo(newLength)) {
                this.length = newLength;
            }
        }
    }
}
