package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FlowNodeValidatedResultDTO {

    @ApiModelProperty("节点ID")
    private String nodeId;

    @ApiModelProperty("节点名称")
    private String nodeName;

    @ApiModelProperty("错误信息")
    private String errorMsg;

    @ApiModelProperty("是否通过校验")
    private Boolean passCheck;
}
