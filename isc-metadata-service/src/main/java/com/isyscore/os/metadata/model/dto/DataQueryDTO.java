package com.isyscore.os.metadata.model.dto;

import com.isyscore.os.metadata.model.vo.ResultVO;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataQueryDTO {
    private ResultVO data;
    private String sqlLog;
    private Integer isSuccess;
}
