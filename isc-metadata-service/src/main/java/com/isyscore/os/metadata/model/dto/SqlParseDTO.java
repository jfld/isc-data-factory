package com.isyscore.os.metadata.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SqlParseDTO {
    private String sourceSql;
    private String parseSql;
    private List<String> sqlVariables;
}
