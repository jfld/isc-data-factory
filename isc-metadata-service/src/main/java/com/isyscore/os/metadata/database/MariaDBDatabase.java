package com.isyscore.os.metadata.database;

import com.isyscore.os.core.sqlcore.MySQLBuilder;
import com.isyscore.os.metadata.enums.DataSourceTypeEnum;

/**
 * @Description :
 * @Author: qkc
 * @Date: 2021/9/29 11:35
 */
public class MariaDBDatabase extends AbstractDatabase {

    {
        dbType = DataSourceTypeEnum.MARIADB;
        //时间类型
        DATE_TYPE.add("DATE");
        DATE_TYPE.add("DATETIME");
        DATE_TYPE.add("TIMESTAMP");
        DATE_TYPE.add("TIME");

        //字段类型
        COLUMN_TYPE.put("varchar", "varchar(255)");
        COLUMN_TYPE.put("char", "char");
        COLUMN_TYPE.put("tinytext", "tinytext");
        COLUMN_TYPE.put("text", "text");
        COLUMN_TYPE.put("mediumtext", "mediumtext");
        COLUMN_TYPE.put("longtext", "longtext");
        COLUMN_TYPE.put("tinyint", "tinyint");
        COLUMN_TYPE.put("smallint", "smallint");
        COLUMN_TYPE.put("mediumint", "mediumint");
        COLUMN_TYPE.put("int", "int");
        COLUMN_TYPE.put("bigint", "bigint");
        COLUMN_TYPE.put("float", "float");
        COLUMN_TYPE.put("double", "double");
        COLUMN_TYPE.put("decimal", "decimal");
        COLUMN_TYPE.put("date", "date");
        COLUMN_TYPE.put("datetime", "datetime");
        COLUMN_TYPE.put("timestamp", "timestamp");
        COLUMN_TYPE.put("time", "time");
        COLUMN_TYPE.put("blob", "blob");

        builder = new MySQLBuilder();
    }

    private final String QUOTE = "`";

    @Override
    public String getQuote() {
        return QUOTE;
    }

    @Override
    public String escapeTbName(String originTableName, String databaseName) {
        return this.QUOTE + originTableName + QUOTE;
    }

    @Override
    public String escapeColName(String originColName) {
        return this.QUOTE + originColName + QUOTE;
    }

    @Override
    public String withOutEscapeColName(String escapeColName) {
        return escapeColName.replaceAll(QUOTE,"");
    }

    @Override
    public String withOutEscapeTableName(String escapeTableName) {
        return escapeTableName.replaceAll(QUOTE,"");
    }

    @Override
    public String escapeColAliasName(String originAliasName) {
        return this.QUOTE + originAliasName + QUOTE;
    }

    @Override
    public String getParseStr2DateEl(String dateVal) {
        return "str_to_date(" + dateVal + ", '%Y-%m-%d %H:%i:%s')";
    }

    @Override
    public String getParseDate2StrEl(String dateVal) {
        return dateVal;
    }

    @Override
    public String getParseStr2IntEl(String dateVal) {
        return "CONVERT("+dateVal+",UNSIGNED)";
    }

    @Override
    public String jdbcUrl(String ip, int port, String dbName, String basicType, String basicValue) {
        return String.format(DataSourceTypeEnum.MARIADB.getUrl(), ip, port, dbName);
    }

    /**
     * <code>
     *     SELECT
     * 	TABLE_COMMENT AS tableComment,
     * 	column_name AS 'columnName',
     * 	column_name AS 'oldName',
     * CASE
     *
     * 	WHEN LENGTH( column_comment ) > 0 THEN
     * 	column_comment ELSE column_name
     * 	END columnComment,
     * 	data_type AS 'dataType',
     * 	column_key AS 'columnKey',
     * 	column_type AS 'columnType',
     * 	character_maximum_length AS 'characterMaximumLength',
     * 	c.table_name AS 'tableName'
     * FROM
     * 	information_schema.COLUMNS c,
     * 	information_schema.TABLES t
     * WHERE
     * 	c.table_name = '%s'
     * 	AND c.TABLE_SCHEMA = '%s'
     * 	AND '%s' = t.table_name
     * 	AND '%s' = t.TABLE_SCHEMA
     * </code>
     * @author zhan9yn
     * @date 2021/11/12 4:17 下午
    */
    @Override
    public String tableStructSql(String tableName, String dbName) {
        String SELECT_TABLE_COL = "SELECT\n" +
                "\tTABLE_COMMENT AS tableComment,\n" +
                "\tcolumn_name AS 'columnName',\n" +
                "\tcolumn_name AS 'oldName',\n" +
                "CASE\n" +
                "\t\n" +
                "\tWHEN LENGTH( column_comment ) > 0 THEN\n" +
                "\tcolumn_comment ELSE column_name \n" +
                "\tEND columnComment,\n" +
                "\tdata_type AS 'dataType',\n" +
                "\tcolumn_key AS 'columnKey',\n" +
                "\tcolumn_type AS 'columnType',\n" +
                "\tcharacter_maximum_length AS 'characterMaximumLength',\n" +
                "\tc.table_name AS 'tableName' \n" +
                "FROM\n" +
                "\tinformation_schema.COLUMNS c,\n" +
                "\tinformation_schema.TABLES t \n" +
                "WHERE\n" +
                "\tc.table_name = '%s' \n" +
                "\tAND c.TABLE_SCHEMA = '%s' \n" +
                "\tAND '%s' = t.table_name \n" +
                "\tAND '%s' = t.TABLE_SCHEMA";

        return String.format(SELECT_TABLE_COL, tableName, dbName, tableName, dbName);
    }

    @Override
    public String tableListSql(String dbName) {
        String SELECT_TABLE = "SELECT " +
                " CASE " +
                "WHEN LENGTH(table_comment) > 0 THEN " +
                " table_comment " +
                "ELSE " +
                " table_name " +
                "END tableComment, " +
                " table_name AS 'tableName' " +
                "FROM " +
                " information_schema. TABLES " +
                "WHERE " +
                " TABLE_SCHEMA = '%s'";
        return String.format(SELECT_TABLE, dbName);
    }

    @Override
    public String linkTableSql(String columns, String dbName) {
        String SELECT_TABLE_COLUMN_LINK = "SELECT " +
                " column_name AS 'columnName', " +
                " CASE " +
                "WHEN LENGTH(column_comment) > 0 THEN " +
                " column_comment " +
                "ELSE " +
                " column_name " +
                "END columnComment, " +
                " data_type AS 'dataType', " +
                " column_key AS 'columnKey', " +
                " column_type AS 'columnType', " +
                " character_maximum_length AS 'characterMaximumLength', " +
                " table_name AS 'tableName' " +
                "FROM " +
                " information_schema. COLUMNS " +
                "WHERE " +
                " table_name IN ( " +
                "   SELECT " +
                "     table_name " +
                "   FROM " +
                "     information_schema. COLUMNS " +
                "   WHERE " +
                "     COLUMN_NAME IN (%s) " +
                " ) " +
                "AND TABLE_SCHEMA = '%s'";
        return String.format(SELECT_TABLE_COLUMN_LINK, columns, dbName);
    }

    @Override
    public String renameTableSql(String oldTableName, String newTableName) {
        return "ALTER TABLE " + oldTableName + " RENAME TO " + escapeTbName(newTableName, "");
    }

    @Override
    public String dropTableSql(String tableName) {
        return "DROP TABLE " + tableName;
    }



}
