package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class MetricGroupCreateReq {

    @ApiModelProperty("分组名称")
    @NotBlank(groups = {ValidateCreate.class, ValidateUpdate.class}, message = "分组名称不能为空")
    @Length(max = 32,message="分组名应为1~32个字符")
    private String groupName;

}
