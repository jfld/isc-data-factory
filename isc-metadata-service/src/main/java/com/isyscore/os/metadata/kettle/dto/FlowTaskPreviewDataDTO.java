package com.isyscore.os.metadata.kettle.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
public class FlowTaskPreviewDataDTO {

    @ApiModelProperty("字段")
    private List<String> fields;

    @ApiModelProperty("数据记录")
    private List<List<String>> data;



}
