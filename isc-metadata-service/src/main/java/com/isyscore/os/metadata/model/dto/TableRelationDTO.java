package com.isyscore.os.metadata.model.dto;

import lombok.Data;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/25 11:47
 */
@Data
public class TableRelationDTO {
    private String centerTable;
    private String linkTable;
    private String centerColumn;
    private String linkColumn;
}
