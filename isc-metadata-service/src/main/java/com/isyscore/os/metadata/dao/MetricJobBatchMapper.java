package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.MetricJobBatch;

/**
 * <p>
 * 记录指标每一次计算的批次信息 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-08-11
 */
public interface MetricJobBatchMapper extends BaseMapper<MetricJobBatch> {

}
