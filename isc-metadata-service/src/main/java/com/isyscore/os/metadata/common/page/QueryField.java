package com.isyscore.os.metadata.common.page;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.isyscore.os.metadata.common.page.CompareOperator.EQ;


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QueryField {

    CompareOperator operator() default EQ;

    String name() default "";

}
