package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.core.entity.SqlQuery;
import com.isyscore.os.metadata.model.vo.ResultVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *  服务类
 *
 * @author starzyn
 * @since 2022-03-16
 */
public interface SqlQueryService extends IService<SqlQuery> {
    RespDTO add(SqlQuery sqlQuery);

    RespDTO updateDataText(SqlQuery sqlQuery);

    ResultVO getData(String id, PageRequest page, HttpServletRequest httpRequest);

    void importData(List<SqlQuery> dataSources);

}
