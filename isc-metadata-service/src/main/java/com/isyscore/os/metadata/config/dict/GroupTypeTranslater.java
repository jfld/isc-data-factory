package com.isyscore.os.metadata.config.dict;

import com.google.common.base.Strings;
import com.isyscore.os.metadata.common.dict.CachedDictTranslater;
import com.isyscore.os.metadata.enums.AggregationType;

public class GroupTypeTranslater extends CachedDictTranslater {

    @Override
    public String doGetLabelByCode(String dictType, Object dictCode) {
        if (dictCode == null || Strings.isNullOrEmpty(dictCode.toString())) {
            return "无";
        }
        AggregationType[] types = AggregationType.values();
        for (AggregationType type : types) {
            if (type.getSymbol().equals(dictCode)) {
                return type.getLabel();
            }
        }
        return "未知";
    }

}
