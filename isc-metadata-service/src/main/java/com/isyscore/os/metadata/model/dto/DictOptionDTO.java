package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DictOptionDTO {

    @ApiModelProperty("字典显示名称")
    private String label;

    @ApiModelProperty("字典值")
    private String value;

}
