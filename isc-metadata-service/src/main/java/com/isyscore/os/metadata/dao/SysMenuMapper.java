package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.isyscore.os.metadata.model.dto.BusinessSysMenuDTO;
import com.isyscore.os.metadata.model.entity.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统功能菜单表 Mapper 接口
 *
 * @author liyuxy@isyscore.com
 * @since 2021-09-10
 */
@Component
public interface SysMenuMapper {

    /**
     * 获取系统菜单树
     * @return
     */
    List<BusinessSysMenuDTO> getMenuTree();


}
