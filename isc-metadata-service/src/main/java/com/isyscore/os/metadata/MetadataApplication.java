package com.isyscore.os.metadata;

import com.isyscore.os.core.config.CustomBeanNameGenerator;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.isyscore.os")
@EnableTransactionManagement
@EnableScheduling
@MapperScan(basePackages = {"com.isyscore.os.metadata.dao", "com.isyscore.os.core.mapper"})
@EnableFeignClients(basePackages = {"com.isyscore.os.metadata"})
public class MetadataApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MetadataApplication.class);
        app.setBeanNameGenerator(new CustomBeanNameGenerator());
        app.run(args);
    }
}
