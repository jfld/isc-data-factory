package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FlowScheduleModifyDTO {
    @ApiModelProperty("任务定义ID")
    @NotEmpty
    private String definitionId;


    @ApiModelProperty("是否开启定时调度")
    @NotNull
    private Boolean scheduled =Boolean.FALSE;

}
