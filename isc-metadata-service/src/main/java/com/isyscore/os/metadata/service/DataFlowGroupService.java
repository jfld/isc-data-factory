package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.core.model.entity.DataFlowGroup;
import com.isyscore.os.metadata.model.dto.req.GroupDTO;

import java.util.List;

/**
 * @author dc
 * @Type DataFlowGroupService.java
 * @Desc
 * @date 2022/10/21 10:37
 */
public interface DataFlowGroupService extends IService<DataFlowGroup> {

    /**
     * 新增分组或者新增子分组
     * @param groupDTO
     * @return
     */
    DataFlowGroup addGroup(GroupDTO groupDTO);

    /**
     * 重命名分组
     * @param groupDTO
     * @return
     */
    Boolean updateGroup(GroupDTO groupDTO);

    /**
     * 删除分组
     * @param id 分组id
     * @return
     */
    Boolean deleteGroup(String id);

    /**
     * 查询分组信息
     * @param parentId
     * @return
     */
    List<DataFlowGroup> getGroup(String parentId);

    /**
     * 查询树
     * @return
     */
    List<DataFlowGroup> listTreeGroup();
}
    