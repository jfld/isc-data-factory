package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class TableAlias {
    @ApiModelProperty("ID")
    private Long id;
    @ApiModelProperty(name = "数据源ID")
    private Long sourceId;
    @ApiModelProperty(name = "数据库")
    private String databaseName;
    @ApiModelProperty(name = "表名称")
    private String tableName;
    @ApiModelProperty(name = "別名")
    private String alias;

}
