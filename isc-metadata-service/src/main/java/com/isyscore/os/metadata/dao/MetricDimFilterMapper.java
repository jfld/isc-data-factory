package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.MetricDimFilter;

/**
 * <p>
 * 保存指标的维度过滤条件,该表仅保存指标自身设定的过滤条件，对于派生指标需要进行级联查询 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface MetricDimFilterMapper extends BaseMapper<MetricDimFilter> {

}
