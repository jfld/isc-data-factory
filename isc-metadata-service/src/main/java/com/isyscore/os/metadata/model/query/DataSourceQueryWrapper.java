package com.isyscore.os.metadata.model.query;

import lombok.Data;

/**
 * @author zhangyn
 * @version 1.0
 * @date 2021/6/22 2:14 下午
 */
@Data
public class DataSourceQueryWrapper extends BaseQuery{
    private String name;
    private String type;
    private String userId;
    private String databaseName;
    private String tableNameList;
    private String tableName;
}
