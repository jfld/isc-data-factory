package com.isyscore.os.metadata.service.executor;

/**
 * 任务重试间隔时间生成器
 *
 * @author liy
 * @date 2023-09-08 11:27
 */
public class RetryIntervalGenerator {
    private Integer multiple;
    private Long current;
    private int times;
    private int currentTimes = 0;

    public RetryIntervalGenerator(Long base, Integer multiple, Integer times) {
        this.current = base;
        this.multiple = multiple;
        this.times = times;
    }

    public Long next() {
        currentTimes++;
        if (currentTimes > 1) {
            current = current * multiple;
        }
        return current;
    }

    public boolean hasNext() {
        if (currentTimes == times) {
            return false;
        }
        return true;
    }
}
