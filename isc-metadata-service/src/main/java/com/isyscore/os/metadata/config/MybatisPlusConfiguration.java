package com.isyscore.os.metadata.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.isyscore.boot.login.LoginUserManager;
import com.isyscore.boot.login.properties.LoginProperties;
import com.isyscore.boot.mybatis.IsyscoreMybatisProperties;
import com.isyscore.os.core.config.MetaDataTenantLineHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author felixu
 * @since 2020.11.23
 */
@Configuration
public class MybatisPlusConfiguration {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(IsyscoreMybatisProperties mybatisProperties,
                                                         LoginProperties loginProperties,
                                                         LoginUserManager loginUserManager) {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor pageInterceptor = new PaginationInnerInterceptor();
        pageInterceptor.setDbType(mybatisProperties.getDbType());
        pageInterceptor.setMaxLimit(mybatisProperties.getMaxLimit());
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new MetaDataTenantLineHandler(loginProperties, loginUserManager)));
        interceptor.addInnerInterceptor(pageInterceptor);
        return interceptor;
    }

}
