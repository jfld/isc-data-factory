package com.isyscore.os.metadata.model.dto.diop;

import lombok.Data;

@Data
public class DiopResultNotifyDto {

    private String appCode;

    private String serviceId;

    private Integer status;

    private String message;

    private String dirPath;

}
