package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * Kettle转换日志
 * </p>
 *
 * @author wuwx
 * @since 2022-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("kettle_trans_log_all")
public class KettleTransLog implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("批次ID")
    @TableField("ID_BATCH")
    private Long idBatch;

    @ApiModelProperty("转换名")
    @TableField("TRANSNAME")
    private String transName;

    @ApiModelProperty("状态")
    @TableField("STATUS")
    private String status;


    @ApiModelProperty("日志")
    @TableField("LOG_FIELD")
    private String logField;
}
