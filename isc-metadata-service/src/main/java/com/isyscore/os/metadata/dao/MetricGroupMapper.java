package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.MetricGroup;

/**
 * <p>
 * 数据指标分组 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface MetricGroupMapper extends BaseMapper<MetricGroup> {

}
