package com.isyscore.os.metadata.enums;

import lombok.Getter;

import java.util.stream.Stream;

/**
 * 任务配置状态，仅作为runConfig状态枚举,不参与flink端状态条件判断
 * @author wuwx
 */

@Getter
public enum KettleJobStatus {
    FAILED(0, "失败"),

    RUNNING(1, "运行中"),

    FINISHED(2, "已完成");

    private Integer code;

    private String desc;

    KettleJobStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static KettleJobStatus get(String name) {
        return KettleJobStatus.valueOf(name);
    }

    public static KettleJobStatus getByCode(Integer code) {
        return Stream.of(KettleJobStatus.values()).filter(bean -> bean.getCode()==code).findFirst().orElse(null);
    }

}
