package com.isyscore.os.metadata.listener;

import com.isyscore.device.common.util.JsonMapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * DMC 发送 MQ 消息到 shadow 以同步数据库数据。
 * @author 陈增辉 on 2020/8/31
 */
@Data
public class DatabaseMqData {

    @ApiModelProperty("类名,使用mqtt推送时用不上")
    private String simpleClassName;

    @ApiModelProperty("ADD, UPDATE, DELETE")
    private Operation operation;

    @ApiModelProperty("具体数据内容")
    private String data;

    public enum Operation {
        //操作类型
        ADD, UPDATE, DELETE, START, END
    }
}
