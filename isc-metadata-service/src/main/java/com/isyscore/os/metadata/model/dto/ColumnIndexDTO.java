package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/20 14:24
 */
@Data
public class ColumnIndexDTO implements Serializable {

    private Integer index;

    private String name;

    private Integer isNumber;

    private String columnName;

    private Integer statisticsType;

    private String tableName;

    private String dataType;
}