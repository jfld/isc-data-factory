package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.os.metadata.model.dto.MetricGroupDTO;
import com.isyscore.os.metadata.model.dto.req.ChangeMetricGroupNameReq;
import com.isyscore.os.metadata.model.dto.req.MetricGroupCreateReq;
import com.isyscore.os.metadata.model.entity.MetricGroup;
import com.isyscore.os.metadata.service.impl.MetricGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 数据指标分组 前端控制器
 * </p>
 *
 * @author wany
 * @since 2021-08-11
 */
@RestController
@RequestMapping("${api-full-prefix}/metricGroup")
@Api(tags = "指标分组")
public class MetricGroupController {

    @Autowired
    private MetricGroupService metricGroupService;

    @ApiOperation("指标分组列表")
    @GetMapping()
    public RespDTO<List<MetricGroupDTO>> listMetricGroup() {
        List<MetricGroup> metricGroups = metricGroupService.list();
        List<MetricGroupDTO> list = metricGroups.stream().map(mg -> {
            MetricGroupDTO mgDetail = new MetricGroupDTO();
            mgDetail.setGroupName(mg.getGroupName());
            mgDetail.setId(mg.getId());
            return mgDetail;
        }).collect(Collectors.toList());
        return RespDTO.onSuc(list);
    }

    @ApiOperation("指标分组详情查询")
    @GetMapping("/{groupId}")
    public RespDTO<MetricGroupDTO> listMetricGroup(@PathVariable("groupId") Long groupId) {
        MetricGroup metricGroup = metricGroupService.getGroupById(groupId);
        MetricGroupDTO mgDetail = new MetricGroupDTO();
        mgDetail.setGroupName(metricGroup.getGroupName());
        mgDetail.setId(metricGroup.getId());
        return RespDTO.onSuc(mgDetail);
    }

    @ApiOperation("新增指标分组")
    @PostMapping()
    public RespDTO<Long> add(@RequestBody @Validated({ValidateCreate.class, Default.class}) MetricGroupCreateReq metricGroupCreateReq) {
        MetricGroup group = metricGroupService.createMetricGroup(metricGroupCreateReq.getGroupName());
        return RespDTO.onSuc(group.getId());
    }

    @ApiOperation("修改指标分组")
    @PutMapping("/{groupId}")
    public RespDTO<?> updateMetricGroup(@PathVariable("groupId") Long groupId, @Validated @RequestBody ChangeMetricGroupNameReq req) {
        MetricGroup metricGroup = metricGroupService.getGroupById(groupId);
        metricGroupService.updateMetricGroupName(metricGroup, req);
        return RespDTO.onSuc();
    }

    @ApiOperation("删除指标分组")
    @DeleteMapping("/{groupId}")
    public RespDTO<?> deleteMetricGroup(@PathVariable("groupId") Long groupId) {
        MetricGroup metricGroup = metricGroupService.getGroupById(groupId);
        metricGroupService.deleteMetricGroup(metricGroup);
        return RespDTO.onSuc();
    }

}
