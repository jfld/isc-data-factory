package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.AbstractStep;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.AbnormalCheckNode;
import com.isyscore.os.metadata.kettle.vis.SwitchCaseNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.switchcase.SwitchCaseMeta;
import org.pentaho.di.trans.steps.switchcase.SwitchCaseTarget;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dc
 * @Type SwitchCaseStep.java
 * @Desc
 * @date 2022/10/20 17:36
 */
public class SwitchCaseStep  extends AbstractStep {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        SwitchCaseNode node = (SwitchCaseNode) step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId(StepPluginType.class, KettleDataFlowNodeType.SwitchCase.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass(sp);
        SwitchCaseMeta switchCaseMeta = (SwitchCaseMeta) stepMetaInterface;
        switchCaseMeta.setCaseValueType(2);
        switchCaseMeta.setContains(false);
        switchCaseMeta.setDefaultTargetStepname(node.getDefaultTargetStepName());
        switchCaseMeta.setCaseTargets(node.getCaseTargets());
        switchCaseMeta.setFieldname(node.getFieldName());
        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.SwitchCase.name(), node.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        //将case的stepId转换成stepName
        Map<String, String> collect = transGraph.getSteps().stream().collect(Collectors.toMap(VisNode::getId, VisNode::getNodeName));
        StepMeta stepMeta = getStep(transMeta, step.getNodeName());
        SwitchCaseMeta switchCaseMeta = (SwitchCaseMeta) stepMeta.getStepMetaInterface();
        switchCaseMeta.setDefaultTargetStepname(collect.get(switchCaseMeta.getDefaultTargetStepname()));
        List<SwitchCaseTarget> caseTargets = switchCaseMeta.getCaseTargets();
        for (SwitchCaseTarget caseTarget : caseTargets) {
            caseTarget.caseTargetStepname = collect.get(caseTarget.caseTargetStepname);
        }
        return null;
    }
}
    
