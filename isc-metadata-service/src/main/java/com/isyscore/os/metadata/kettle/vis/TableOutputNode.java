package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 批量输出
 */

@ApiModel("表输出组件")
@Data
public class TableOutputNode extends  VisNode {


    @ApiModelProperty("输出表名")
    @NotBlank(message="输出表名不能为空")
    private String tableName;
    @ApiModelProperty("数据库链接标识")
    @NotNull(message="数据库链接标识不能为空")
    private Long dataSourceId;

    @ApiModelProperty("数据库名")
    @NotBlank(message="数据库名不能为空")
    private String databaseName;

    @ApiModelProperty("是否裁剪表")
    @NotNull(message="是否裁剪表不能为空")
    private Boolean truncate = false;
    @ApiModelProperty("来源字段列表")
    @NotEmpty(message = "来源字段列表不能为空")
    @Valid
    List<FieldTuple> fieldMapping;

    @ApiModelProperty("并行度")
    private Integer copies;

    @ApiModelProperty("批次大小")
    private Integer batchSize;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.tableName = jsonObject.getString("tableName");
        this.truncate = jsonObject.getBoolean("truncate");

        this.dataSourceId = jsonObject.getLong("dataSourceId");
        this.databaseName = jsonObject.getString("databaseName");

        this.fieldMapping = jsonObject.getJSONArray("fieldMapping").stream().map(o ->
                JSON.parseObject(JSON.toJSONString(o),FieldTuple.class)).collect(Collectors.toList());

        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));

        this.copies =  jsonObject.getInteger("copies");
        this.batchSize =  jsonObject.getInteger("batchSize");

    }

    @Override
    public Long getDbSourceId() {
        return this.dataSourceId;
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.OUTPUTS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.TableOutput;
    }
}
