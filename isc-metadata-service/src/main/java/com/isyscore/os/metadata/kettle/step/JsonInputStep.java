package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.AbstractStep;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.vis.JsonInputNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.jsoninput.JsonInputField;
import org.pentaho.di.trans.steps.jsoninput.JsonInputMeta;

import java.util.List;

/**
 * @author dc
 * @Type JsoninputStep.java
 * @Desc
 * @date 2022/9/14 11:29
 */
public class JsonInputStep extends AbstractStep {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        JsonInputNode node=(JsonInputNode) step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId(StepPluginType.class, KettleDataFlowNodeType.JsonInput.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass(sp);

        JsonInputMeta jsonInputMeta = (JsonInputMeta) stepMetaInterface;
        String sourceInfo = node.getSourceInfo();
        //源定义在一个字段里
        jsonInputMeta.setInFields(true);
        jsonInputMeta.setFieldValue(sourceInfo);
        List<com.isyscore.os.metadata.kettle.vis.JsonInputField> fields = node.getFields();
        JsonInputField[] inputFields=new JsonInputField[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            com.isyscore.os.metadata.kettle.vis.JsonInputField pageInput = fields.get(i);
            JsonInputField jsonInputField = new JsonInputField();
            jsonInputField.setPath(pageInput.getFieldPath());
            jsonInputField.setName(pageInput.getFieldName());
            inputFields[i]=jsonInputField;
        }
        jsonInputMeta.setInputFields(inputFields);
        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.JsonInput.name(), node.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        return null;
    }
}
    
