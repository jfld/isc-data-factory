package com.isyscore.os.metadata.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.isyscore.os.metadata.common.CommonService;
import com.isyscore.os.metadata.dao.KettleTransLogMapper;
import com.isyscore.os.metadata.model.entity.KettleTransLog;
import org.springframework.stereotype.Service;

import static com.isyscore.os.metadata.constant.CommonConstant.DS_CK_METADATA;

/**
 * <p>
 * Kettle转换日志 服务实现类
 * </p>
 *
 * @author wany
 * @since 2021-08-11
 */
@Service
@DS(DS_CK_METADATA)
public class KettleTransLogService extends CommonService<KettleTransLogMapper, KettleTransLog> {
}
