package com.isyscore.os.metadata.controller;

import com.google.common.collect.Lists;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.enums.AggregationType;
import com.isyscore.os.metadata.enums.FilterOperator;
import com.isyscore.os.metadata.enums.TimePeriodUnit;
import com.isyscore.os.metadata.database.AbstractDatabase;
import com.isyscore.os.metadata.manager.DatabaseManager;
import com.isyscore.os.metadata.model.dto.DataSourceDTO;
import com.isyscore.os.metadata.model.dto.DictOptionDTO;
import com.isyscore.os.metadata.model.entity.Datamodel;
import com.isyscore.os.metadata.service.impl.DataSourceServiceImpl;
import com.isyscore.os.metadata.service.impl.DatamodelService;
import com.isyscore.os.metadata.service.impl.FormulaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("${api-full-prefix}/dict")
@Api(tags = "公共字典项")
public class DictController {

    @Autowired
    private DatamodelService datamodelService;

    @Autowired
    private DataSourceServiceImpl dataSourceService;

    @ApiOperation("（内部API）维度过滤比较操作符字典项")
    @GetMapping("/filter-operator")
    public RespDTO<List<DictOptionDTO>> getFliterOperator() {
        List<DictOptionDTO> dicts = Lists.newArrayList();
        for (FilterOperator filterOperator : FilterOperator.values()) {
            DictOptionDTO op = new DictOptionDTO();
            op.setLabel(filterOperator.getLabel());
            op.setValue(filterOperator.name());
            dicts.add(op);
        }
        return RespDTO.onSuc(dicts);
    }

    @ApiOperation("（内部API）聚合方式字典项")
    @GetMapping("/aggregation-type")
    public RespDTO<List<DictOptionDTO>> getAggregationType() {
        List<DictOptionDTO> dicts = Lists.newArrayList();
        for (AggregationType aggregationType : AggregationType.values()) {
            DictOptionDTO op = new DictOptionDTO();
            op.setLabel(aggregationType.getLabel());
            op.setValue(aggregationType.name());
            dicts.add(op);
        }
        return RespDTO.onSuc(dicts);
    }

    @ApiOperation("（内部API）日期范围单位字典项")
    @GetMapping("/time-period")
    public RespDTO<List<DictOptionDTO>> getTimePeriod() {
        List<DictOptionDTO> dicts = Lists.newArrayList();
        for (TimePeriodUnit timePeriodUnit : TimePeriodUnit.values()) {
            DictOptionDTO op = new DictOptionDTO();
            op.setLabel(timePeriodUnit.getLabel());
            op.setValue(timePeriodUnit.name());
            dicts.add(op);
        }
        return RespDTO.onSuc(dicts);
    }

    @ApiOperation("（内部API）计算公式中可用的符号")
    @GetMapping("/formula-symbol")
    public RespDTO<Set<String>> getFormulaSymbol() {
        return RespDTO.onSuc(FormulaService.formulaSymbols);
    }

    @ApiOperation("（内部API）当前受支持的日期时间数据类型")
    @GetMapping("/legal-datetime-type")
    public RespDTO<Set<String>> getLegalDatetimeType(@RequestParam("dataModelId") Long dataModelId) {
        Datamodel datamodel = datamodelService.getDatamodelById(dataModelId);
        DataSourceDTO ds = dataSourceService.getDataSource(datamodel.getDsRefId());
        AbstractDatabase db = DatabaseManager.findDb(ds);
        return RespDTO.onSuc(db.getLegalDateTypes());
    }

}
