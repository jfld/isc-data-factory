package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dc
 * @Type JosnInputField.java
 * @Desc json输入字段名
 * @date 2022/9/14 11:16
 */
@Data
public class JsonInputField {
    @ApiModelProperty(value = "字段名")
    @NotBlank(message = "字段名不为空")
    private String fieldName;

    @NotBlank(message = "字段路径不能为空")
    @ApiModelProperty(value = "字段路径（jsonpath语法）")
    private String fieldPath;
}
    
