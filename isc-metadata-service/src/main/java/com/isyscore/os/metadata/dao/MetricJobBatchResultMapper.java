package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.MetricJobBatchResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-08-11
 */
public interface MetricJobBatchResultMapper extends BaseMapper<MetricJobBatchResult> {

//    List<String> getAllPeriodsOfMetricResults(@Param("metricId") Long metricId);

    List<MetricJobBatchResult> getMetricResultsByCodeAndPeriod(@Param("metricId") Long metricId, @Param("timePeriod") String timePeriod, @Param("dimension") String dimension, @Param("tenantId") String tenantId);

}
