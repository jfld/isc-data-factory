package com.isyscore.os.metadata.model.dto;

import lombok.Data;

/**
 * 应用注册请求参数
 */
@Data
public class IscRegisterAppDTO {
    public final static String APP_CODE = "udmp";

    /**
     * 应用code
     */
    private String code = APP_CODE;

    /**
     * 应用名称
     */
    private String name;
    /**
     * 应用类型(0.默认应用(系统应用) 3.第三方应用 5.外嵌应用 9.(isyscore OS)桌面应用 10.嵌入式应用)
     */
    private Integer type = 0;

    /**
     * 应用版本
     */
    private String version;

    public IscRegisterAppDTO(String name, String version) {
        this.name = name;
        this.version = version;
    }
}
