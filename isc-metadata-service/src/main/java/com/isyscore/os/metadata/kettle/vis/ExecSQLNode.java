package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author liyang
 */
@ApiModel("sql脚本")
@Data
@EqualsAndHashCode(callSuper = false)
public class ExecSQLNode extends VisNode{
    @ApiModelProperty("sql语句")
    @NotBlank(message="sql语句不能为空")
    private String sql;

    @ApiModelProperty("数据库链接标识")
    @NotNull(message="数据库链接标识不能为空")
    private Long dataSourceId;

    @ApiModelProperty("数据库名")
    @NotBlank(message="数据库名不能为空")
    private String databaseName;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.sql = jsonObject.getString("sql");
        this.dataSourceId = jsonObject.getLong("dataSourceId");
        this.databaseName = jsonObject.getString("databaseName");
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    @Override
    public Long getDbSourceId() {
        return dataSourceId;
    }

    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.OUTPUTS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.ExecSQL;
    }
}
