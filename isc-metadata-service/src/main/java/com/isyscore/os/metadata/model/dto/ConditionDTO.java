package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;


@Data
public class ConditionDTO {
    @ApiModelProperty("条件字段名称")
    @NotEmpty
    private String colName;

    @ApiModelProperty("设定的条件值")
    @NotEmpty
    private String conditionValue;

    @ApiModelProperty("比较操作符，支持lt, gt, lte，gte，eq，neq，like，in")
    @NotEmpty
    private String operator;

    public MetricDimFilterDTO transferTO() {
        final MetricDimFilterDTO metricDimFilterDTO = new MetricDimFilterDTO();
        metricDimFilterDTO.setDimColName(colName);
        metricDimFilterDTO.setDimConditionValue(conditionValue);
        metricDimFilterDTO.setOperator(operator);
        return metricDimFilterDTO;
    }
}
