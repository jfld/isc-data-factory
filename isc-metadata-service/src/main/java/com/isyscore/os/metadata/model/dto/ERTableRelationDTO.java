package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ERTableRelationDTO {

    @ApiModelProperty("主表表名")
    @NotBlank
    private String centerTable;

    @ApiModelProperty("从表表名")
    @NotBlank
    private String linkTable;

    @ApiModelProperty("主表连接字段")
    @NotBlank
    private String centerColumn;

    @ApiModelProperty("从表连接字段")
    @NotBlank
    private String linkColumn;

}
