package com.isyscore.os.metadata.config.dict;

import com.isyscore.os.metadata.common.dict.CachedDictTranslater;
import com.isyscore.os.metadata.enums.MetricJobTaskStatus;

public class MetricJobTaskStatusTranslater extends CachedDictTranslater {
    @Override
    public String doGetLabelByCode(String dictType, Object dictCode) {
        MetricJobTaskStatus[] types = MetricJobTaskStatus.values();
        for (MetricJobTaskStatus type : types) {
            if (type.getStatus().equals(dictCode)) {
                return type.getLabel();
            }
        }
        return "未知";
    }
}
