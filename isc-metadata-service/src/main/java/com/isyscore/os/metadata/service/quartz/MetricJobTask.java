package com.isyscore.os.metadata.service.quartz;


import com.isyscore.os.metadata.service.executor.MetricJobTaskExecutor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 用于定时执行指标的计算任务
 *
 * @author wany
 */
@Slf4j
public class MetricJobTask extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        Long metricId = Long.parseLong(jobExecutionContext.getJobDetail().getJobDataMap().get("id").toString());
        new MetricJobTaskExecutor(metricId).execute(false);
    }
}
