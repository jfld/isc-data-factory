package com.isyscore.os.metadata.utils;


import cn.hutool.core.util.StrUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.isyscore.os.metadata.model.dto.DataModelColumnDTO;
import com.isyscore.os.metadata.model.dto.ERTableInfoDTO;

import java.util.List;
import java.util.Set;

public class ColumnUtil {

    public static String getColName(Set<String> duplicateColumns, String columnName, String tableName) {
        if (duplicateColumns.contains(columnName) && !Strings.isNullOrEmpty(tableName)) {
            return StrUtil.toCamelCase(tableName) + "__" + columnName;
        } else {
            return columnName;
        }
    }

    public static Set<String> getDuplicateColumnsForDataModelColumn(List<DataModelColumnDTO> columns) {
        Set<String> handledColumns = Sets.newHashSet();
        Set<String> duplicateColumns = Sets.newHashSet();
        for (DataModelColumnDTO col : columns) {
            if (handledColumns.contains(col.getColName())) {
                duplicateColumns.add(col.getColName());
            }
            handledColumns.add(col.getColName());
        }
        return duplicateColumns;
    }

    public static Set<String> getDuplicateColumnsForErTable(List<ERTableInfoDTO> tableInfos) {
        Set<String> columns = Sets.newHashSet();
        Set<String> duplicateColumns = Sets.newHashSet();
        for (ERTableInfoDTO tableInfo : tableInfos) {
            for (String column : tableInfo.getColumns()) {
                if (columns.contains(column)) {
                    duplicateColumns.add(column);
                }
                columns.add(column);
            }
        }
        return duplicateColumns;
    }

}
