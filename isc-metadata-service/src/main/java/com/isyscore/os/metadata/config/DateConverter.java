package com.isyscore.os.metadata.config;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String s) {
        if (Strings.isNullOrEmpty(s)) {
            return null;
        }
        return DateUtil.parse(s);
    }

}
