package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.AbstractStep;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.ValueMapperField;
import com.isyscore.os.metadata.kettle.vis.ValueMapperNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.core.row.value.ValueMetaBase;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.filterrows.FilterRowsMeta;
import org.pentaho.di.trans.steps.valuemapper.ValueMapperMeta;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ValueMapperStep extends AbstractStep {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        ValueMapperNode valueMapperNode =(ValueMapperNode)step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId( StepPluginType.class, KettleDataFlowNodeType.ValueMapper.name() );
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass( sp );
        ValueMapperMeta valueMapperMeta = (ValueMapperMeta) stepMetaInterface;
        valueMapperMeta.setFieldToUse(valueMapperNode.getFieldToUse());
        valueMapperMeta.setNonMatchDefault(valueMapperNode.getNonMatchDefault());
        valueMapperMeta.setSourceValue(valueMapperNode.getMapperFields().stream().map(ValueMapperField::getSourceValue).toArray(String[]::new));
        valueMapperMeta.setTargetValue(valueMapperNode.getMapperFields().stream().map(ValueMapperField::getTargetValue).toArray(String[]::new));
        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.ValueMapper.name(), valueMapperNode.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
//        transMeta.addStep(stepMeta);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        StepMeta stepMeta = getStep(transMeta,step.getNodeName());
        ValueMapperMeta valueMapperMeta = (ValueMapperMeta) stepMeta.getStepMetaInterface();

        RowMetaInterface prevStepFields = transMeta.getPrevStepFields(stepMeta);
        List<ValueMetaInterface> valueMetaList = prevStepFields.getValueMetaList();
        List<String> fields = valueMetaList.stream().map(v -> v.getName()).collect(Collectors.toList());

        //将时间类型的字符串转换成kettle默认识别的格式
        if(fields.contains(valueMapperMeta.getFieldToUse()) && "Date".equals(getFiledType(valueMetaList,(valueMapperMeta.getFieldToUse())))){
            String[] sourceValue = valueMapperMeta.getSourceValue();
            for (int i = 0; i < sourceValue.length; i++) {
                Date dateValue = isDate(String.valueOf(sourceValue[i]));
                if(dateValue!=null){
                    sourceValue[i] = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").format(dateValue);
                }
            }
            valueMapperMeta.setSourceValue(sourceValue);
        }

        return null;
    }
}
