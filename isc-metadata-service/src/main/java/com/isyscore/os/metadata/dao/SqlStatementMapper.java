package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.SqlStatement;

/**
 * <p>
 * 存储数据模型和数据指标对应的SQL语句 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface SqlStatementMapper extends BaseMapper<SqlStatement> {

}
