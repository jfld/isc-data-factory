package com.isyscore.os.metadata.service.diop;

public class DiopConstant {

    protected static final String METRIC_RESOURCE_NAME = "metric";

    protected static final String DATAMODEL_RESOURCE_NAME = "datamodel";


    protected static final String SQL_QUERY_RESOURCE_NAME = "sqlQuery";

    protected static final String SELF_DESCRIPTION_FILENAME = "_selfdescription";

    protected static final String LIST_FILE_FILENAME = "_listfile";

    protected static final String DATAMODLE_FILE_NAME = "datamodel";

    protected static final String ER_RELATION_FILE_NAME = "er-relation";

    protected static final String DATAMODEL_COLUMN_FILE_NAME = "datamodel-column";

    protected static final String SQL_FILE_NAME = "sql-statement";

    protected static final String DATASOURCE_FILE_NAME = "datasource";

    protected static final String METRIC_FILE_NAME = "metric";

    protected static final String METRIC_GROUP_FILE_NAME = "metric-group";

    protected static final String METRIC_DIM_FILTER_FILE_NAME = "metric-dim-filter";

    protected static final String METRIC_MEASURE_FILE_NAME = "metric-measure";

    protected static final String SQL_QUERY_FILE_NAME = "sql_query";

    protected static final String SQL_QUERY_GROUP_FILE_NAME = "sql_query_group";

}
