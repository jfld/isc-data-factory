package com.isyscore.os.metadata.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MetricTreeItemDTO {

    private List<MetricTreeItemDTO> children;

    @ApiModelProperty("节点唯一编码")
    private Long id;

    @ApiModelProperty("节点名称")
    private String label;

    @ApiModelProperty("节点类型：1：分组，2：指标")
    private Integer type;

}
