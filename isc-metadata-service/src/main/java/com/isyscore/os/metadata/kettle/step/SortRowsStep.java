package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.SortField;
import com.isyscore.os.metadata.kettle.vis.SortRowsNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.sort.SortRowsMeta;

import java.util.Arrays;
import java.util.List;

public class SortRowsStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        SortRowsNode sortRowsNode =(SortRowsNode)step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId( StepPluginType.class, KettleDataFlowNodeType.SortRows.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass( sp );
        SortRowsMeta sortRowsMeta = (SortRowsMeta) stepMetaInterface;
        //字段
        sortRowsMeta.setFieldName(sortRowsNode.getSortFields().stream().map(SortField::getName).toArray(String[]::new));
        //排序
        boolean[] ascending= new boolean[sortRowsNode.getSortFields().size()];
        for(int x =0;x<sortRowsNode.getSortFields().size();x++){
            ascending[x]=sortRowsNode.getSortFields().get(x).getAscending();
        }
        sortRowsMeta.setAscending(ascending);
        //是否大小写敏感，默认敏感
        boolean[] caseInsensitive= new boolean[sortRowsNode.getSortFields().size()];
        Arrays.fill(caseInsensitive, Boolean.TRUE);
        sortRowsMeta.setCaseSensitive(caseInsensitive);

        boolean[] collatorEnabled= new boolean[sortRowsNode.getSortFields().size()];
        Arrays.fill(collatorEnabled, Boolean.FALSE);
        sortRowsMeta.setCollatorEnabled(collatorEnabled);

        int[] collatorStrength= new int[sortRowsNode.getSortFields().size()];
        Arrays.fill(collatorStrength, 0);
        sortRowsMeta.setCollatorStrength(collatorStrength);

        boolean[] preSortedField= new boolean[sortRowsNode.getSortFields().size()];
        Arrays.fill(preSortedField, false);
        sortRowsMeta.setPreSortedField(preSortedField);

        sortRowsMeta.setSortSize("1000000");
        sortRowsMeta.setDirectory("%%java.io.tmpdir%%");
        sortRowsMeta.setPrefix("out");
        sortRowsMeta.setCompressFiles(false);
        sortRowsMeta.setOnlyPassingUniqueRows(false);
        StepMeta stepMeta = new StepMeta( KettleDataFlowNodeType.SortRows.name(), sortRowsNode.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
//        transMeta.addStep(stepMeta);
        return stepMeta;
    }

    @Override
    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        return null;
    }
}
