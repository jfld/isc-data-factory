package com.isyscore.os.metadata.common.page;

public enum CompareOperator {
    LIKE,
    NOT_LIKE,
    EQ,
    NE,
    GT,
    GE,
    LT,
    LE,
    IS_NULL,
    IS_NOT_NULL
}
