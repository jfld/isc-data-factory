package com.isyscore.os.metadata.enums;

/**
 * @author liy
 * @date 2022-10-14 10:54
 */
public enum EmptyValueDealFunctionType {
    MIN,
    MAX,
    MEAN, //平均
    MEDIAN,//中位数
    FIXED;//固定值

}
