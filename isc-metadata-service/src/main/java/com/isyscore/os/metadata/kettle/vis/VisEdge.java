package com.isyscore.os.metadata.kettle.vis;

import com.google.common.base.Strings;
import com.isyscore.os.core.exception.DataFactoryException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

import static com.isyscore.os.core.exception.ErrorCode.VIS_CONFIG_PARSE_ERROR;

@Data
public class VisEdge {

    @ApiModelProperty("源任务节点的ID")
    private String sourceId;

    @ApiModelProperty("目标任务节点的ID")
    private String targetId;

    public void valueOf(Map<String, Object> params) {
        String sourceId = (String) params.get("sourceId");
        String targetId = (String) params.get("targetId");
        if (Strings.isNullOrEmpty(sourceId)
                || Strings.isNullOrEmpty(targetId)) {
            throw new DataFactoryException(VIS_CONFIG_PARSE_ERROR, "edge", "源节点和目标节点不能为空");
        }
        this.setSourceId(sourceId);
        this.setTargetId(targetId);
    }

}
