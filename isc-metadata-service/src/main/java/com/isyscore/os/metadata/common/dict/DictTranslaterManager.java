package com.isyscore.os.metadata.common.dict;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import java.util.Map;

public class DictTranslaterManager {

    private Map<String, DictTranslater> dictTranslaterMap = Maps.newHashMap();

    private DictTranslater wildcardDictTranslater;

    /**
     * 注册一个字典翻译器，用于将字典编码转换为字典文本
     *
     * @param dictType       字典类型
     * @param dictTranslater 字典翻译器
     */
    public void registerTranslater(String dictType, DictTranslater dictTranslater) {
        if (Strings.isNullOrEmpty(dictType) || dictTranslater == null) {
            throw new IllegalArgumentException("dictType和dictTranslater不能为空");
        }
        dictTranslaterMap.put(dictType, dictTranslater);
    }

    /**
     * 注册一个通用字典翻译器，用于将字典编码转换为字典文本
     *
     * @param dictTranslater 字典翻译器
     */
    public void registerWildcardTranslater(DictTranslater dictTranslater) {
        if (dictTranslater == null) {
            throw new IllegalArgumentException("dictTranslater不能为空");
        }
        this.wildcardDictTranslater = dictTranslater;
    }


    public DictTranslater getDictTranslaterByType(String dictType) {
        if (Strings.isNullOrEmpty(dictType)) {
            return null;
        }
        DictTranslater dt = dictTranslaterMap.get(dictType);
        if (dt == null) {
            dt = this.wildcardDictTranslater;
        }
        return dt;
    }


}
