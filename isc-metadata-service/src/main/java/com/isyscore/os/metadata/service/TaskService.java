package com.isyscore.os.metadata.service;

import com.isyscore.os.metadata.model.dto.TaskDTO;
import java.util.concurrent.TimeUnit;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/24 19:54
 */
public interface TaskService {
    String creatTask(Long time, TimeUnit t, String data);

    Double getProgress(String taskId);

    boolean stopTask(String taskId, String errorMsg);

    boolean isTaskStop(String taskId);

    void setTaskProgress(String taskId, Double progress);


    TaskDTO get(String taskId);

    void update(TaskDTO taskDTO);

}
