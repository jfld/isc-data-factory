package com.isyscore.os.metadata.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.model.dto.DataModelDTO;
import com.isyscore.os.metadata.model.dto.MetricGraphDTO;
import com.isyscore.os.metadata.model.dto.req.DataModelCreateOrUpdateReq;
import com.isyscore.os.metadata.model.dto.req.DataModelQuery;
import com.isyscore.os.metadata.model.entity.Datamodel;
import com.isyscore.os.metadata.service.impl.DatamodelService;
import com.isyscore.os.metadata.service.impl.MetricGraphService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * <p>
 * 数据模型的前端控制器
 * </p>
 *
 * @author wany
 * @since 2021-08-11
 */
@RestController
@RequestMapping("${api-full-prefix}/datamodel")
@Api(tags = "数据模型")
public class DatamodelController {

    @Autowired
    private DatamodelService datamodelService;

    @Autowired
    private MetricGraphService metricGraphService;


    @ApiOperation("分页查询数据模型列表")
    @GetMapping()
    public RespDTO<IPage<DataModelDTO>> pageDataModel(DataModelQuery pageQuery) {
        IPage<Datamodel> datamodels = datamodelService.pageWithPageParam(pageQuery);
        IPage<DataModelDTO> pageData = datamodels.convert((datamodel) -> {
            DataModelDTO dataModelDetail = new DataModelDTO();
            dataModelDetail.fromDataModleEntity(datamodel);
            return dataModelDetail;
        });
        return RespDTO.onSuc(pageData);
    }

    @ApiOperation("查询数据模型的详情")
    @GetMapping("/{dataModelId}")
    public RespDTO<DataModelDTO> getDataModelDetail(@PathVariable("dataModelId") Long dataModelId) {
        DataModelDTO detail = datamodelService.getDatamodelDetailById(dataModelId);
        return RespDTO.onSuc(detail);
    }

    @ApiOperation("新增数据模型")
    @PostMapping()
    public RespDTO<Long> createDataModel(@Validated @RequestBody DataModelCreateOrUpdateReq dataModelCreateOrUpdateReq) {
        Long id = datamodelService.addDatamodel(dataModelCreateOrUpdateReq);
        return RespDTO.onSuc(id);
    }

    @ApiOperation("更新数据模型")
    @PutMapping("/{dataModelId}")
    public RespDTO<?> updateDataModel(@PathVariable("dataModelId") Long dataModelId, @Validated @RequestBody DataModelCreateOrUpdateReq dataModelCreateOrUpdateReq) {
        datamodelService.updateDatamodel(dataModelId, dataModelCreateOrUpdateReq);
        return RespDTO.onSuc();
    }

    @ApiOperation("删除数据模型")
    @DeleteMapping("/{dataModelId}")
    public RespDTO<?> deleteDataModel(@PathVariable("dataModelId") Long dataModelId) {
        datamodelService.removeDatamodel(dataModelId);
        return RespDTO.onSuc();
    }

    @ApiOperation("按数据模型查看指标血缘")
    @GetMapping("/graph/{dataModelId}")
    public RespDTO<MetricGraphDTO> getDatamodelGrath(@PathVariable("dataModelId") Long dataModelId) {
        Datamodel datamodel = datamodelService.getDatamodelById(dataModelId);
        return RespDTO.onSuc(metricGraphService.getDatamodelGraph(datamodel));
    }

    @ApiOperation("(内部API)执行数据模型所定义的查询语句,进行分页查询")
    @GetMapping("/result/{dataModelId}")
    public RespDTO<IPage<Map<String, Object>>> queryDatamodelResult(@PathVariable("dataModelId") Long dataModelId, PageRequest pr) {
        IPage<Map<String, Object>> page = datamodelService.queryDatamodelResult(dataModelId, pr);
        return RespDTO.onSuc(page);
    }

}