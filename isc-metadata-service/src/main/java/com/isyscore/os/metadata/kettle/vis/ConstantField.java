package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ConstantField{
    @ApiModelProperty("名称")
    @NotBlank(message = "常量字段名称不能为空")
    private String name;
    @ApiModelProperty("值")
    @NotBlank(message = "常量字段值不能为空")
    private String value;
}
