package com.isyscore.os.metadata.model.query;

import lombok.Data;

import javax.validation.constraints.Min;

import static com.isyscore.os.metadata.constant.CommonConstant.DEFAULT_PAGE_NO;
import static com.isyscore.os.metadata.constant.CommonConstant.DEFAULT_PAGE_SIZE;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 15:55
 */
@Data
public class BaseQuery {

    @Min(value = 1)
    private Integer pageSize;

    @Min(value = 1)
    private Integer pageNo;


    public Integer getPageSize() {
        if (pageSize == null || pageSize <= 0) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }

    public Integer getPageNo() {
        if (pageNo == null || pageNo <= 0) {
            pageNo = DEFAULT_PAGE_NO;
        }
        return pageNo;
    }

}
