package com.isyscore.os.metadata.constant;


/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 15:44
 */
public interface CommonConstant {

    Integer DEL = 1;

    Integer NOT_DEL = 0;

    Integer DEFAULT_PAGE_SIZE = 2000;

    Integer DEFAULT_PAGE_NO = 1;

    Integer STATISTICS_TYPE_DIMENSION = 1;

    Integer STATISTICS_TYPE_MEASURE = 2;

    String MYSQL_DEFAULT_DATABASE = "information_schema";

    String CLICKHOUSE_DEFAULT_DATABASE = "system";

    Integer IS_WAREHOUSE = 1;

    Integer NOT_WAREHOUSE = 0;

    Integer NOT_DATA_WAREHOUSE = 0;

    Integer IS_NUMBER = 1;

    Integer NOT_NUMBER = 0;

    String DIM_TABLE_PREFIX = "dim_";

    Integer LICENSE_EXTERNAL_DATABASE = 5;

    Integer DEFAULT_INDEX = 0;

    String TOTAL_COUNT = "total";

    Integer DEFAULT_USER_ID = -1;

    Long MAX_FILE_SIZE = 1024 * 1024 * 10L;

    Integer LOCKED = 1;

    Integer NOT_LOCKED = 0;

    String METRIC_PATH_SPLITTER = "/";

    Integer MAX_METRIC_DATA_RESULT_LIMIT = 1000;

    String METRIC_TIME_PERIOD_START_TIME_PARAMNAME = "#{startTime}";

    String METRIC_TIME_PERIOD_END_TIME_PARAMNAME = "#{endTime}";

    String METRIC_SQL_DYNAMIC_FILTERS = "${dynamic_filters_placeholder}";

    String METRIC_CALCULATE_JOB_GROUP = "metric_calculate_job_group";

    String DATA_FLOW_JOB_GROUP = "data_flow_job_group";

    /**
     * 对于没有设定统计周期的指标的默认统计周期标识
     */
    String DEFAULT_METRIC_DATE_PERIOD_KEY = "latest";

    /**
     * 新增指标的默认分组ID
     */
    long DEFAULT_METRIC_GROUP_ID = -1;

    long DEFAULT_SQL_GROUP_ID = -1;

    String SORT_DESC = "DESC";

    String SORT_ASC = "ASC";


    String DS_CK_METADATA = "clickhouse-metadata";

    String DEPLOY_MODE_STANDALONE = "standalone";

    String DEPLOY_MODE_STANDBY = "standby";

    Integer TRUE = 1;

    Integer FALSE = 0;

    /**
     *  分组枚举
     */
    Integer FOLDER = 0;

    /**
     *  分组枚举
     */
    Integer DATA_TEXT = 1;

    String PRIVATE = "PRIVATE";

    String PUBLIC = "PUBLIC";

    String APP_CODE = "udmp";

}
