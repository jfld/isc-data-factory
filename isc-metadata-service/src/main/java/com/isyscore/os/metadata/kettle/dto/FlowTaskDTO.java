package com.isyscore.os.metadata.kettle.dto;

import com.isyscore.os.core.model.entity.DataFlowTask;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FlowTaskDTO {

    @ApiModelProperty("任务实例ID")
    private String id;

    @ApiModelProperty("流程名称")
    private String flowName;

    @ApiModelProperty("流程定义ID")
    private String definitionId;

    @ApiModelProperty("任务状态：0：失败  1：运行中  2：已完成")
    private Integer status;

    @ApiModelProperty("触发方式:1：手动触发，2：自动触发 ")
    private Integer triggerMethod;

    @ApiModelProperty("任务开始运行时间")
    private LocalDateTime startTime;

    @ApiModelProperty("任务结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty("kettle端的任务id")
    private String taskId;

    public void valueOf(DataFlowTask dataFlowTask,String flowName){
        this.setId(String.valueOf(dataFlowTask.getId()));
        this.setFlowName(flowName);
        this.setDefinitionId(String.valueOf(dataFlowTask.getDefinitionId()));
        this.setStatus(dataFlowTask.getStatus());
        this.setStartTime(dataFlowTask.getStartTime());
        this.setEndTime(dataFlowTask.getEndTime());
        this.setTaskId(dataFlowTask.getKettleBatchId());
        this.setTriggerMethod(dataFlowTask.getTriggerMethod());
    }
}
