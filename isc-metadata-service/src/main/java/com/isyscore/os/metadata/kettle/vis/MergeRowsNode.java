package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.JoinType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 合并记录
 */

@ApiModel("合并记录组件")
@Data
@EqualsAndHashCode(callSuper = false)
public class MergeRowsNode extends  VisNode {

    @ApiModelProperty("状态标识字段")
    @NotNull(message="标识字段不能为空")
    private String flagField;

    @ApiModelProperty("旧数据源")
    @NotNull(message="旧数据源不能为空")
    private String oldStepName; //reference

    @ApiModelProperty("新数据源")
    @NotNull(message="新数据源不能为空")
    private String newStepName; //compare

    @ApiModelProperty("对比字段列表")
    @NotNull(message="对比字段列表能为空")
    private List<String> compareFileds;

    @ApiModelProperty("数据字段列表")
    @NotNull(message="数据字段列表不能为空")
    private List<String> dataFileds;

    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.flagField = jsonObject.getString("flagField");
        this.oldStepName = jsonObject.getString("oldStepName");
        this.newStepName = jsonObject.getString("newStepName");

        this.compareFileds = Optional.ofNullable(jsonObject.getJSONArray("compareFileds")).orElse(new JSONArray()).stream().map(String::valueOf).collect(Collectors.toList());
        this.dataFileds = Optional.ofNullable(jsonObject.getJSONArray("dataFileds")).orElse(new JSONArray()).stream().map(String::valueOf).collect(Collectors.toList());


        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.MergeRows;
    }
}
