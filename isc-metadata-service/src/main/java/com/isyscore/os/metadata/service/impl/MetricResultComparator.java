package com.isyscore.os.metadata.service.impl;

import cn.hutool.core.util.NumberUtil;
import com.google.common.base.Strings;
import com.isyscore.os.metadata.model.entity.MetricJobBatchResult;

import java.util.Comparator;

public class MetricResultComparator implements Comparator<MetricJobBatchResult> {

    @Override
    public int compare(MetricJobBatchResult o1, MetricJobBatchResult o2) {
        if (o1 == null || Strings.isNullOrEmpty(o1.getDim())) {
            return -1;
        }
        if (o2 == null || Strings.isNullOrEmpty(o2.getDim())) {
            return 1;
        }
        if (NumberUtil.isNumber(o1.getDim())
                && NumberUtil.isNumber(o2.getDim())) {
            Number number1 = NumberUtil.parseNumber(o1.getDim());
            Number number2 = NumberUtil.parseNumber(o2.getDim());
            return NumberUtil.compare(number1.doubleValue(), number2.doubleValue());
        }
        return o1.getDim().compareToIgnoreCase(o2.getDim());
    }


}
