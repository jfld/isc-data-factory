package com.isyscore.os.metadata.kettle.step;

import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import com.isyscore.os.metadata.kettle.base.FlowConfig;
import com.isyscore.os.metadata.kettle.base.Step;
import com.isyscore.os.metadata.kettle.vis.BlockingNode;
import com.isyscore.os.metadata.kettle.vis.EmptyValueDealNode;
import com.isyscore.os.metadata.kettle.vis.VisNode;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.plugins.PluginInterface;
import org.pentaho.di.core.plugins.PluginRegistry;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.emptyValueDeal.EmptyValueDeal;
import org.pentaho.di.trans.steps.emptyValueDeal.EmptyValueDealFunction;
import org.pentaho.di.trans.steps.emptyValueDeal.EmptyValueDealMeta;
import org.pentaho.di.trans.steps.sql.ExecSQLMeta;
import org.w3c.dom.Node;

import java.util.List;

public class EmptyValueDealStep implements Step {
    @Override
    public StepMeta decode(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws Exception {
        EmptyValueDealNode emptyValueDealNode =(EmptyValueDealNode)step;
        PluginRegistry registry = PluginRegistry.getInstance();
        PluginInterface sp = registry.findPluginWithId( StepPluginType.class, KettleDataFlowNodeType.EmptyValueDeal.name());
        StepMetaInterface stepMetaInterface = (StepMetaInterface) registry.loadClass( sp );

        EmptyValueDealMeta emptyValueDealMeta = (EmptyValueDealMeta) stepMetaInterface;
        //参数封装
        EmptyValueDealFunction[] functions = new EmptyValueDealFunction[emptyValueDealNode.getFields().size()];
        for (int i = 0; i < emptyValueDealNode.getFields().size(); i++) {
            EmptyValueDealNode.DealField field = emptyValueDealNode.getFields().get(i);
            EmptyValueDealFunction function = new EmptyValueDealFunction(field.getField(), field.getFunctionType());
            function.setFixedValue(field.getFixedValue());
            functions[i] = function;
        }

        emptyValueDealMeta.setInputFieldMetaFunctions(functions);

        StepMeta stepMeta = new StepMeta(KettleDataFlowNodeType.EmptyValueDeal.name(), emptyValueDealNode.getNodeName(), stepMetaInterface);
        stepMeta.setDraw(true);
        return stepMeta;
    }

    public StepMeta after(VisNode step, List<DatabaseMeta> databases, TransMeta transMeta, FlowConfig transGraph) throws KettleStepException {
        return null;
    }

}
