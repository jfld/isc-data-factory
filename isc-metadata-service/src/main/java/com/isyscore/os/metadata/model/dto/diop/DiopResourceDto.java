package com.isyscore.os.metadata.model.dto.diop;

import lombok.Data;

import java.util.List;

@Data
public class DiopResourceDto {

    private String name;

    private String desc;

    private List<DiopResourceDto> extra;

}
