package com.isyscore.os.metadata.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/30 17:52
 */
@Data
public class TableInfoDTO {
    private String tableName;
    private List<TableColumnDTO> tableColumnDTOList;
    private List<TableRelationDTO> tableRelationDTOList;
    private List<TableInfoDTO> tableInfoNodeList;
}
