package com.isyscore.os.metadata.kettle.base;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FlowHup {
    private String from;
    private String to;
}
