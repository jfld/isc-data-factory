package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.Datamodel;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface DatamodelMapper extends BaseMapper<Datamodel> {

}
