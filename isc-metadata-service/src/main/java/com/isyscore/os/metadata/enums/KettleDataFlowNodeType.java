package com.isyscore.os.metadata.enums;

public enum KettleDataFlowNodeType {

    /**
     * 表输入
     */
    TableInput,
    /**
     * 数据关联
     */
    MergeJoin,
    /**
     * 分组
     */
    GroupBy,
    /**
     * 内存分组
     */
    MemoryGroupBy,

    /**
     * 过滤
     */
    FilterRows,
    /**
     * 表输出
     */
    TableOutput,
    /**
     * 插入更新
     */
    InsertUpdate,
    /**
     * 增加常量
     */
    Constant,
    /**
     * 值映射
     */
    ValueMapper,
    /**
     * 去重
     */
    Unique,
    /**
     * 排序
     */
    SortRows,
    /**
     * 阻塞
     */
    BlockingStep,
    /**
     * sql脚本
     */
    ExecSQL,
    /**
     * Rest请求
     */
    Rest,
    /**
     * json输入
     */
    JsonInput,

    /**
     * 随机值
     */
    RandomValue,

    /**
     * 空值处理
     */
    EmptyValueDeal,

    /**
     * 数据采样
     */
    ReservoirSampling,

    /**
     * java表达式
     */
    JavaExpression,
    /**
     * switchcase
     */
    SwitchCase,
    /**
     * 异常值检测
     */
    AbnormalCheck,

    /**
     * 行合并
     */
    MergeRows,
    /**
     * 流查询
     */
    StreamLookup,

    /**
     * 追加流
     */
    Append,
    /**
     * 指标
     */
    Metric
    ;
}
