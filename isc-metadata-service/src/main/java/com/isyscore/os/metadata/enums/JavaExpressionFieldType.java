package com.isyscore.os.metadata.enums;

/**
 * @author liy
 * @date 2022-10-14 10:54
 */
public enum JavaExpressionFieldType {
    BigNumber,
    Number,
    Boolean,
    Integer,
    String,
    Date,
    Timestamp;
}
