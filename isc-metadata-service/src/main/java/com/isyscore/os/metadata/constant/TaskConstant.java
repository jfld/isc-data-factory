package com.isyscore.os.metadata.constant;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/11/24 20:03
 */
public interface TaskConstant {

    Integer NOT_STOP = 0;

    Integer STOP = 1;

    String TASK_NAME = "UDMP_TASK_ID_";
    String UDMP_REDIS_KEY = "udmp:";
}
