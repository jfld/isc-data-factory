package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ChangeMetricNameReq {

    @ApiModelProperty("指标id")
    @NotNull
    private Long metricId;

    @ApiModelProperty("指标的新名称")
    @NotEmpty
    private String newName;

}
