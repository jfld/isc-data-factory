package com.isyscore.os.metadata.enums;

import lombok.Getter;

import java.util.stream.Stream;


@Getter
@SuppressWarnings("all")
public enum KettleDataSourceType {

    MYSQL(0, "MYSQL"),//,
    MARIADB(7, "MYSQL"), //kettle使用maraisb的时候有问题。用mysql替换

    ORACLE(1, "ORACLE"),

    CLICKHOUSE(2, "CLICKHOUSE"),

    SQLSERVER(3, "MSSQL"),

    DM(4, "DM"),

    PGSQL(6, "POSTGRESQL"),

    KINGBASE(8, "POSTGRESQL"),

    //通用类型
    GENERIC(-1, "GENERIC");

    private Integer code;

    private String name;

    KettleDataSourceType(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static KettleDataSourceType getType(Integer code) {
        return Stream.of(KettleDataSourceType.values()).filter(bean -> bean.getCode().equals(code)).findFirst().orElse(null);
    }
    public static KettleDataSourceType getType(String name) {
        return Stream.of(KettleDataSourceType.values()).filter(bean -> bean.getName().equals(name)).findFirst().orElse(null);
    }

}
