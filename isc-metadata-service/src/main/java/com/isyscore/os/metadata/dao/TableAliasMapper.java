package com.isyscore.os.metadata.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.TableAlias;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TableAliasMapper extends BaseMapper<TableAlias> {
}
