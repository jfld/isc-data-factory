package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum FilterOperator {
    lt("<", "小于"),
    gt(">", "大于"),

    lte("<=", "小于等于"),
    gte(">=", "大于等于"),
    eq("=", "等于"),
    neq("!=", "不等于"),
    like("like", "类似于"),
    in("in", "包含");

    private String symbol;

    private String label;

    FilterOperator(String symbol, String label) {
        this.symbol = symbol;
        this.label = label;

    }


}
