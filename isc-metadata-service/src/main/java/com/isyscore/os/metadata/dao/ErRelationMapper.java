package com.isyscore.os.metadata.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.isyscore.os.metadata.model.entity.ErRelation;

/**
 * <p>
 * 存储ER关系 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-08-11
 */
public interface ErRelationMapper extends BaseMapper<ErRelation> {

}
