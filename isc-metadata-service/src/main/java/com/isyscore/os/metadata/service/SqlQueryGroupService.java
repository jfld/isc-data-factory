package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.core.entity.SqlQueryGroup;
import com.isyscore.os.metadata.model.dto.SqlTreeItemDTO;
import com.isyscore.os.metadata.model.vo.SqlQueryVO;

import java.util.List;

/**
 *  服务类
 *
 * @author starzyn
 * @since 2022-03-16
 */
public interface SqlQueryGroupService extends IService<SqlQueryGroup> {
    List<SqlQueryGroup> listWithInitially();
    void removeGroup(String id);
    List<SqlTreeItemDTO> getSqlQueryTree(String sqlName);
    SqlQueryVO getSqlQueryVO(String id);
    void importData(List<SqlQueryGroup> sqlQueryGroups);

}
