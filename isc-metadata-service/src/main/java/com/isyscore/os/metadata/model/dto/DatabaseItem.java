package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2022/2/24 3:50 下午
 */
@Data
public class DatabaseItem {
    /**
     *  对于四级结构而言，继续沿用 database.schema 的形式
     */
    @ApiModelProperty("数据库名称")
    private String databaseName;

    @ApiModelProperty("数据库下面的数据表")
    private List<String> tableList;

    @ApiModelProperty("当前数据库下面数据是否完全获取到")
    private Integer isAllSuccess;
}
