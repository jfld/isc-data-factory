package com.isyscore.os.metadata.enums;

import lombok.Getter;

@Getter
public enum MetricJobTaskStatus {

    running(0, "执行中"), fail(-1, "失败"), success(1, "成功");

    private Integer status;

    private String label;

    MetricJobTaskStatus(Integer status, String label) {
        this.status = status;
        this.label = label;
    }

}
