package com.isyscore.os.metadata.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.isyscore.os.metadata.model.entity.DataSource;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 15:15
 */
@Data
public class DataSourceDTO extends DataSource {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dataSourceId;
    private List<String> databaseNameList;
    private String selectDatabaseName;
    private String tableName;
    private List<String> tableNameList;
    private String columnName;
    private List<String> columnNameList;
    private String centerTable;

    private List<DatabasePair> databaseList;

    public String getSelectDatabaseName(){
        String dbName = selectDatabaseName;
        if (StringUtils.isBlank(dbName)){
            if (!CollectionUtils.isEmpty(databaseNameList))
                dbName = databaseNameList.get(0);
        }
        return dbName;
    }
}
