package com.isyscore.os.metadata.config.dict;

public class DictTypeConst {

    //数据模型创建方式名称转换
    public static final String DATA_MODEL_CREATE_TYPE = "DATA_MODEL_CREATE_TYPE";

    //将过滤条件中的比较操作符转换为适当的名称
    public static final String FILTER_OPERATOR = "FILTER_OPERATOR";

    //聚合模式字典翻译
    public static final String GROUP_TYPE = "GROUP_TYPE";

    //时间范围单位
    public static final String TIME_PERIOD_UNIT = "TIME_PERIOD_UNIT";

    //JOB执行状态
    public static final String METRIC_JOB_TASK_STATUS = "METRIC_JOB_TASK_STATUS";

}
