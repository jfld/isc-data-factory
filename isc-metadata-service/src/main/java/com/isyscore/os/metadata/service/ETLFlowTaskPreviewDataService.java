package com.isyscore.os.metadata.service;

import com.isyscore.os.metadata.kettle.dto.FlowTaskPreviewDataDTO;

/**
 * @author liy
 * @date 2022-07-13 10:50
 */
public interface ETLFlowTaskPreviewDataService {

    FlowTaskPreviewDataDTO getTaskStepPreviewData(String taskId, String stepName);

    FlowTaskPreviewDataDTO getTaskLastStepPreviewData(String taskId);

    FlowTaskPreviewDataDTO previewData(String sql,Long dataSourceId,String databaseName);


}

