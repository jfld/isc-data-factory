package com.isyscore.os.metadata.enums;

/**
 * @author wuwx
 *    节点分组类型
 */
public enum KettleDataFlowNodeGroupType {
    //输入
    INPUTS,
    //转换
    TRANS,
    //输出
    OUTPUTS,
    //其他
    OTHER
}
