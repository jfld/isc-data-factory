package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dc
 * @Type GroupDTO.java
 * @Desc
 * @date 2022/10/21 13:58
 */

@Data
public class GroupDTO {
    @ApiModelProperty("父节点id")
    private String parentId;

    @ApiModelProperty("分组名称")
    @NotBlank(message = "分组名称不能为空")
    private String name;

    @ApiModelProperty("分组id")
    private String id;
}
    
