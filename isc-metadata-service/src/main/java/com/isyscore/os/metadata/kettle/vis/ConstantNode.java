package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author wuwx
 */
@ApiModel("常量组件")
@Data
@EqualsAndHashCode(callSuper = false)
public class ConstantNode extends VisNode{

    @ApiModelProperty("常量字段")
    @NotEmpty(message = "常量字段不能为空")
    @Valid
    private List<ConstantField> constantFields;


    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.constantFields = Optional.ofNullable(jsonObject.getJSONArray("constantFields")).orElse(new JSONArray()).stream().map(o ->
                JSON.parseObject(JSON.toJSONString(o), ConstantField.class)).collect(Collectors.toList());
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.TRANS;
    }

    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.Constant;
    }
}
