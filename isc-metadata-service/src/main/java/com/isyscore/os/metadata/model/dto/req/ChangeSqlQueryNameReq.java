package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ChangeSqlQueryNameReq {

    @ApiModelProperty("SQL查询ID")
    @NotNull
    private Long sqlQueryId;

    @ApiModelProperty("SQL查询新名称")
    @NotEmpty
    private String sqlQueryName;

}
