package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class OrderByDTO {
    @NotEmpty
    private String colName;

    @ApiModelProperty("排序规则，升序或降序，ASC / DESC")
    private String rule = "ASC";
}
