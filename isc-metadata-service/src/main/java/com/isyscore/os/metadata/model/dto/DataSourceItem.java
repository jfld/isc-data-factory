package com.isyscore.os.metadata.model.dto;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author : zhan9yn
 * @version : 1.0
 * @date : 2022/2/24 3:00 下午
 */
@Data
public class DataSourceItem {

    @ApiModelProperty("数据源ID")
    private Long id;

    @ApiModelProperty("数据源名称")
    private String name;

    @ApiModelProperty("数据源IP")
    private String ip;
    @ApiModelProperty("数据源端口")
    private String port;

    @ApiModelProperty("数据库列表")
    private List<DatabaseItem> databaseList;

    @ApiModelProperty(name = "数据源类型", allowableValues = "0,1,2,3,4,5,6",
        notes = "0 mysql;1 oracle;2 clickhouse; 3 sqlserver; 4 dm; 5 td; 6 pg")
    private Integer type;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    @ApiModelProperty(value = "连接类型", allowableValues = "BASIC,TNS")
    private String connectType;

    @ApiModelProperty(value = "BASIC下的连接类型", allowableValues = "ServiceName,SID")
    private String basicType;

    @ApiModelProperty(value = "ServiceName,SID的值")
    private String basicValue;

    @ApiModelProperty("当前数据源下面数据是否完全获取到")
    private Integer isAllSuccess;
}
