package com.isyscore.os.metadata.kettle.dto;

import com.isyscore.boot.mybatis.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FlowDefinitionPageReqDTO extends PageRequest {
    @ApiModelProperty(value = "流程ID",hidden = true)
    String id;

    @ApiModelProperty("查询条件，任务名称（模糊查询）")
    String flowName;
    @ApiModelProperty("查询条件，任务说明（模糊查询）")
    String description;
    @ApiModelProperty("查询条件，分组名（模糊查询）")
    String groupName;
    @ApiModelProperty("分组ID")
    String groupId;

}
