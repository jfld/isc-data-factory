package com.isyscore.os.metadata.kettle.vis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class FieldTuple {
    @ApiModelProperty("来源字段")
    @NotBlank(message = "来源字段不能为空")
    String fromField;
    @ApiModelProperty("目标字段")
    @NotBlank(message = "目标字段不能为空")
    String toField;
}
