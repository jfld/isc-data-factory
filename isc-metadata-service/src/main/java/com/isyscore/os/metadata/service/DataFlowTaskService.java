package com.isyscore.os.metadata.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.isyscore.os.core.model.entity.DataFlowTask;
import com.isyscore.os.metadata.controller.OpenApiController;
import com.isyscore.os.metadata.enums.KettleJobTriggerType;
import com.isyscore.os.metadata.kettle.dto.FlowTaskDTO;
import com.isyscore.os.metadata.kettle.dto.FlowTaskPageReqDTO;

import java.util.List;
import java.util.Map;

public interface DataFlowTaskService extends IService<DataFlowTask> {
    public FlowTaskDTO getFlowTask(String id);
    public String runTask(String definitionId, Map<String, String> params, KettleJobTriggerType triggerType,  OpenApiController.CallBack callBack);
    public IPage<FlowTaskDTO> page(FlowTaskPageReqDTO req);

    public void stopTask(String definitionId);

    List<String> runningFlows();

    String taskStatus(String definitionId);
}
