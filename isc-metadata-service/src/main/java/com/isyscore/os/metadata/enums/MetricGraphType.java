package com.isyscore.os.metadata.enums;

public enum MetricGraphType {

    table, datamodel, measure, metric;

}
