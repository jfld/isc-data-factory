package com.isyscore.os.metadata.kettle.base;

import org.pentaho.di.base.AbstractMeta;

public interface GraphCodec {

	public AbstractMeta decode(String xml) throws Exception;
	public AbstractMeta decode(FlowConfig graph,boolean doAfter) throws Exception;
	
	public static final String TRANS_CODEC = "TransGraph";
//	public static final String JOB_CODEC = "JobGraph";
	
}
