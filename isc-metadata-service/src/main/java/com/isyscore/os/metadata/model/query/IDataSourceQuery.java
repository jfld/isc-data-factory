package com.isyscore.os.metadata.model.query;

/**
 *  用来统一 {@link com.isyscore.os.metadata.service.impl.DataSourceServiceImpl#getTableData(Long, DataSourceQuery)} 中的参数
 *  由于目前的分页查询相关字段使用的是两套规则：pageNo/pageSize 和 current/size
 *  为了兼容两套而尽量目前不改动业务层代码，采取临时措施，通过规范入参的形式来减少改动，后面在合适的时间节点统一为一套分页规则
 *  @author: zhan9yn
 *  @Date: 2022/2/17 10:58 上午
 */
public interface IDataSourceQuery {
}
