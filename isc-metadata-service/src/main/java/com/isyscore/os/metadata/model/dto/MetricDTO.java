package com.isyscore.os.metadata.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class MetricDTO {

    @ApiModelProperty("数据指标的code")
    private Long id;

    @ApiModelProperty("所属数据模型编号")
    private Long datamodelId;

    @ApiModelProperty("指标分组ID")
    private Long groupId;

    @ApiModelProperty("所属数据模型名称")
    private String datamodelName;

    @ApiModelProperty("指标名称")
    private String name;

    @ApiModelProperty("指标说明")
    private String description;

    @ApiModelProperty("指标对应的SQL计算语句")
    private String sql;

    @ApiModelProperty("该指标对应的计算公式")
    private String formula;

    @ApiModelProperty("指标所包含的度量值字段及其对于的聚合计算方式")
    private List<MetricMeasureDTO> measures;

    @ApiModelProperty("统计维度字段")
    private String dimension;

    @ApiModelProperty("该指标对应的统计周期字段列名")
    private String timePeriodDim;

    @ApiModelProperty("该指标对应的统计周期时间单位")
    private String timePeriodUnit;

    @ApiModelProperty("该指标对应的统计周期跨度")
    private Integer timePeriodNumber;

    @ApiModelProperty("数据指标的任务调度表达式，采用Quartz cron格式")
    private String taskCron;

    @ApiModelProperty("数据指标计算结果的查询API地址")
    private String metricJobResultApiPath;

    @ApiModelProperty("该指标是否为派生指标")
    private Boolean extended;

    @ApiModelProperty("指标的过滤条件")
    private List<MetricDimFilterDTO> filters;

}
