package com.isyscore.os.metadata.service;

import com.isyscore.os.metadata.model.dto.DataQueryDTO;
import com.isyscore.os.metadata.model.dto.QuerySqlResultDTO;
import com.isyscore.os.metadata.model.vo.DataQueryVO;
import com.isyscore.os.metadata.model.vo.ResultVO;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/12/4 14:00
 */
public interface DataBuildService {

    DataQueryDTO executeSQL(DataQueryVO dataQueryVO, String taskId);

    /**
     * 通过sql获取表结构和表数据
     * @param dataQueryVO 例子 DataQueryVO(dataSourceId=1472466791465689089, sqlText=select * from device_dim, databaseName=Test-demo3, tableName=null, condition=null, orderBy=null, params=null)
     * @return 返回表结构和表数据
     */
    ResultVO getStructureBySql(DataQueryVO dataQueryVO);


    QuerySqlResultDTO executeQuerySqlSync(DataQueryVO dataQueryVO);

}
