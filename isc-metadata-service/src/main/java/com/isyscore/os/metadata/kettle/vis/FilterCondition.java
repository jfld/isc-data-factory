package com.isyscore.os.metadata.kettle.vis;

import com.isyscore.os.metadata.enums.KettleFilterFunction;
import com.isyscore.os.metadata.enums.KettleFilterOperator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FilterCondition {
    @ApiModelProperty("字段")
    @NotBlank(message = "字段不能为空")
    private String filed;
    @ApiModelProperty("函数")
    @NotNull(message = "函数不能为空")
    private KettleFilterFunction function;
    @ApiModelProperty("值")
    private String value;
    @ApiModelProperty("操作")
    private KettleFilterOperator operator;
}
