package com.isyscore.os.metadata.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.device.common.model.RespDTO;
import com.isyscore.os.metadata.kettle.dto.*;
import com.isyscore.os.metadata.kettle.vis.VisGraph;
import com.isyscore.os.metadata.model.dto.req.FlowDTO;
import com.isyscore.os.metadata.model.dto.req.FlowDefinitionModifyDTO;
import com.isyscore.os.metadata.model.dto.req.FlowScheduleModifyDTO;
import com.isyscore.os.metadata.service.DataFlowDefinitionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api-full-prefix}/etlflow/definition")
@Api(tags = "ETL流程定义")
@Slf4j
@RequiredArgsConstructor
public class ETLFlowDefinitionController {

    private final DataFlowDefinitionService dataFlowDefinitionService;


    @ApiOperation("任务新增")
    @PostMapping("saveFlow")
    public RespDTO<Long> saveFlowBase(@RequestBody @Validated FlowDTO flowDTO) {
        return RespDTO.onSuc(dataFlowDefinitionService.createFlow(flowDTO));
    }

    @ApiOperation("任务编辑")
    @PutMapping("updateFlow")
    public RespDTO<String> updateFlowBase(@RequestBody @Validated FlowDTO flowDTO) {
        dataFlowDefinitionService.updateFlowBase(flowDTO);
        return RespDTO.onSuc();
    }

    @ApiOperation("复制流程")
    @PostMapping("copy")
    public RespDTO<String> copy(@RequestBody @Validated FlowDTO flowDTO) {
        dataFlowDefinitionService.copy(flowDTO);
        return RespDTO.onSuc();
    }

    @ApiOperation("保存ETL流程定义")
    @PostMapping()
    public RespDTO<String> saveFlowDefinition(@RequestBody @Validated FlowDefinitionModifyDTO definition) {
        return RespDTO.onSuc(dataFlowDefinitionService.createOrUpdateFlowDefinition(definition));
    }

    @ApiOperation("发布ETL流程")
    @PostMapping("publish")
    public RespDTO<String> publishFlowDefinition(@RequestBody @Validated FlowDefinitionModifyDTO definition) {
        return RespDTO.onSuc(dataFlowDefinitionService.publishFlowDefinition(definition));
    }

//    @ApiOperation("验证的指定的流程定义是否合法")
//    @PostMapping("/check")
//    public RespDTO<List<String>> checkFlowDefinition(@RequestBody VisGraph userConfig) {
//        return RespDTO.onSuc(dataFlowDefinitionService.checkFlowDefinition(userConfig));
//    }

//    @ApiOperation("修改指定的ETL流程配置定义")
//    @PutMapping("/{definitionId}")
//    public RespDTO<String> updateFlowDefinition(@PathVariable("definitionId") String definitionId, @RequestBody @Validated FlowDefinitionModifyDTO definition) {
//        return RespDTO.onSuc(dataFlowDefinitionService.updateFlowDefinition(definitionId, definition));
//    }

    @ApiOperation("修改指定的ETL流程调度状态")
    @PutMapping("/status")
    public RespDTO<?> updateFlowDefinition(@RequestBody @Validated FlowScheduleModifyDTO definition) {
        dataFlowDefinitionService.updateFlowStatus(definition);
        return RespDTO.onSuc();
    }

    @ApiOperation("得到指定的ETL流程配置定义的详细信息")
    @GetMapping("/{definitionId}")
    public RespDTO<FlowDefinitionDTO> getFlowDefinition(@PathVariable("definitionId") String definitionId) {
        return RespDTO.onSuc(dataFlowDefinitionService.getFlowDefinition(definitionId));
    }

    @ApiOperation("分页查询当前所有的ETL流程定义")
    @GetMapping("/page")
    public RespDTO<IPage<FlowDefinitionDTO>> pageFlowDefinition(FlowDefinitionPageReqDTO pageRequest) {
        return RespDTO.onSuc(dataFlowDefinitionService.pageFlowDefinition(pageRequest));
    }

    @ApiOperation("删除ETL流程定义")
    @DeleteMapping("/{definitionId}")
    public RespDTO<?> deleteFlowDefinition(@PathVariable("definitionId") String definitionId) {
        dataFlowDefinitionService.deleteFlowDefinition(definitionId);
        return RespDTO.onSuc();
    }

    @ApiOperation("获取指定表的字段列表")
    @PostMapping("/tableFields")
    public RespDTO<List<String>> tableFields(@RequestBody GetTableFieldsDTO getTableFieldsDTO) {
        return RespDTO.onSuc(dataFlowDefinitionService.tableFields(getTableFieldsDTO,false));
    }

    @ApiOperation("获取步骤输入输出字段")
    @PostMapping("/inputOutputFields")
    public RespDTO<List<String>> inputOutputFields(@RequestBody GetInputOutputFieldsDTO dto) {
        return RespDTO.onSuc(dataFlowDefinitionService.inputOutputFields(dto));
    }

    @ApiOperation("获得快速创建sql")
    @PostMapping("/getFastSql")
    public RespDTO<String> getFastSql(@RequestBody GetTableFieldsDTO dto) {
        return RespDTO.onSuc(dataFlowDefinitionService.previewSql(dto));
    }

    @ApiOperation("获取增量表列表")
    @PostMapping("/increTables")
    public RespDTO<List<String>> increTables(@RequestBody GetIncreTablesReqDTO getIncreTablesReqDTO) {
        return RespDTO.onSuc(dataFlowDefinitionService.increTables(getIncreTablesReqDTO));
    }
}
