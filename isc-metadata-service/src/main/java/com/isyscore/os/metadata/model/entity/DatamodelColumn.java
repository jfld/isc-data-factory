package com.isyscore.os.metadata.model.entity;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <p>
 * 定义数据模型中包含的数据列
 * </p>
 *
 * @author 
 * @since 2021-10-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("datamodel_column")
public class DatamodelColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("租户ID")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("字段所属的数据模型")
    private Long datamodelRefId;

    @ApiModelProperty("字段列名")
    private String colName;

    @ApiModelProperty("字段展示名")
    private String colLabel;

    @ApiModelProperty("数据列类型：1：维度列  2：度量列  3: 时间范围")
    private Integer colType;

    @ApiModelProperty("字段的数据类型，例如string,integer等等")
    private String colDataType;

}
