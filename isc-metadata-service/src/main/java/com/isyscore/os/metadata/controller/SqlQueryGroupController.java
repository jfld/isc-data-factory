package com.isyscore.os.metadata.controller;

import com.isyscore.device.common.model.RespDTO;
import com.isyscore.device.common.validation.ValidateCreate;
import com.isyscore.device.common.validation.ValidateUpdate;
import com.isyscore.os.core.entity.SqlQueryGroup;
import com.isyscore.os.metadata.constant.CommonConstant;
import com.isyscore.os.metadata.service.SqlQueryGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.util.List;

/**
 *
 *
 * @author starzyn
 * @since 2022-03-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("${api-full-prefix}/sql-query-group")
@Api(tags = "SQL分组接口")
public class SqlQueryGroupController {

    private final SqlQueryGroupService sqlQueryGroupService;

    @GetMapping
    @ApiOperation("分组")
    public List<SqlQueryGroup> list() {
        return sqlQueryGroupService.listWithInitially();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("录入分组")
    public boolean create(@Validated({ValidateCreate.class, Default.class}) @RequestBody SqlQueryGroup sqlQueryGroup) {
        sqlQueryGroup.setType(CommonConstant.FOLDER);
        return sqlQueryGroupService.save(sqlQueryGroup);
    }

    @ApiOperation("更新分组")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean update(@Validated({ValidateUpdate.class, Default.class}) @RequestBody SqlQueryGroup sqlQueryGroup) {
        return sqlQueryGroupService.updateById(sqlQueryGroup);
    }

    @ApiOperation("删除分组")
    @DeleteMapping("/{id}")
    public RespDTO<Void> delete(@PathVariable(name = "id") String id) {
        sqlQueryGroupService.removeGroup(id);
        return RespDTO.onSuc();
    }

}
