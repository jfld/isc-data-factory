package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChangeSqlQueryGroupReq {

    @ApiModelProperty("SQL查询ID")
    @NotNull
    private Long sqlQueryId;

    @ApiModelProperty("新的分组ID，如果为空，则指标移入到默认分组中")
    private Long groupId;

}
