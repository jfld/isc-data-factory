package com.isyscore.os.metadata.model.dto.diop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * DIOP注册结构体
 *
 * @author liyx@isyscore.com
 * @date 2021/8/14
 */
@Data
public class DiopRegisterReq {

    @ApiModelProperty("服务名")
    private String serviceId;

    @ApiModelProperty("导入url")
    private String importUrl;

    @ApiModelProperty("导出url")
    private String exportUrl;

    @ApiModelProperty("导出模块列表url")
    private String dataTypesUrl;

    @ApiModelProperty("占位符")
    private List<String> placeHolders;

    @ApiModelProperty("数据类型")
    private List<DiopResourceDto> dataTypes;

}
