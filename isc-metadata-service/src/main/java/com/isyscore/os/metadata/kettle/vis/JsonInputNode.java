package com.isyscore.os.metadata.kettle.vis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author dc
 * @Type JsonInputNode.java
 * @Desc
 * @date 2022/9/14 10:54
 */
@EqualsAndHashCode(callSuper = false)
@Data
@ApiModel("json输入")
public class JsonInputNode extends VisNode {


    @ApiModelProperty(value = "源信息")
    @NotBlank(message = "源信息不能为空")
    private String sourceInfo;

    @ApiModelProperty(value = "字段信息")
    @NotEmpty(message = "字段信息不能为空")
    private List<JsonInputField> fields;

    /**
     * 将前端传入的节点配置值转换为实际的节点属性，每个不同的节点类型应该根据自身的
     * 属性情况覆盖该方法
     *
     * @param params 前端传入的某个节点的配置值
     */
    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.setSourceInfo(jsonObject.getString("sourceInfo"));
        Object field = jsonObject.get("fields");
        if(Objects.nonNull(field)){
           String fieldStr= JSONObject.toJSONString(field);
            List<JsonInputField> fields = JSONArray.parseArray(fieldStr, JsonInputField.class);
            this.setFields(fields);
        }
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }

    /**
     * 获得节点分组类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.INPUTS;
    }

    /**
     * 获得节点类型
     *
     * @return
     */
    @Override
    public KettleDataFlowNodeType getNodeType() {
        return KettleDataFlowNodeType.JsonInput;
    }
}
    
