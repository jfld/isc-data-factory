package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.boot.mybatis.PageRequest;
import com.isyscore.os.metadata.common.page.CompareOperator;
import com.isyscore.os.metadata.common.page.QueryField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DataModelQuery extends PageRequest {

    @ApiModelProperty("查询条件，数据模型名称，支持模糊查询")
    @QueryField(operator = CompareOperator.LIKE)
    private String name;

    @ApiModelProperty("查询条件，数据模型所属的数据源ID")
    @QueryField(name = "ds_ref_id", operator = CompareOperator.EQ)
    private String dsId;

    @ApiModelProperty("查询条件，该模型创建时间范围-起点")
    @QueryField(name = "create_time", operator = CompareOperator.GE)
    private LocalDateTime startTime;

    @ApiModelProperty("查询条件，该模型创建时间范围-终点")
    @QueryField(name = "create_time", operator = CompareOperator.LE)
    private LocalDateTime endTime;

}
