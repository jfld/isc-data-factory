package com.isyscore.os.metadata.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ChangeMetricGroupNameReq {

    @ApiModelProperty("新的分组名称")
    @NotEmpty
    private String groupName;

}
