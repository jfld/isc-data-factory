package com.isyscore.os.metadata.kettle.base;

import com.isyscore.os.metadata.enums.KettleDataSourceType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "kettle.log.clickhouse")
@Component
public class FlowLogDbConfiguration {

    private String host;

    private String port;

    private String username;

    private String password;

    private String database;

    private KettleDataSourceType dbType = KettleDataSourceType.CLICKHOUSE;

}
