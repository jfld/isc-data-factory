package com.isyscore.os.metadata.kettle.vis;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeGroupType;
import com.isyscore.os.metadata.enums.KettleDataFlowNodeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * 表输入
 */
@ApiModel("表输入组件")
@Data
@EqualsAndHashCode(callSuper = false)
public class TableInputNode extends VisNode {

    @ApiModelProperty("sql语句")
    @NotBlank(message="sql语句不能为空")
    private String sql;

    @ApiModelProperty("数据库链接标识")
    @NotNull(message="数据库链接标识不能为空")
    private Long dataSourceId;

    @ApiModelProperty("数据库名")
    @NotBlank(message="数据库名不能为空")
    private String databaseName;


    @ApiModelProperty("是否为增量任务")
    private String isIncre;
    @ApiModelProperty("增量表")
    private String incrTable;
    @ApiModelProperty("增量字段")
    private String increField;
    @ApiModelProperty("当前增量值")
    private String increValue;
    @ApiModelProperty("增量字段类型")
    private String increFieldType;


    @Override
    public void valueOf(Map<String, Object> params) {
        JSONObject jsonObject = new JSONObject(params);
        this.sql = jsonObject.getString("sql");
        this.dataSourceId = jsonObject.getLong("dataSourceId");
        this.databaseName = jsonObject.getString("databaseName");
        setId(jsonObject.getString("id"));
        setNodeName(jsonObject.getString("nodeName"));
    }


    @Override
    public String validate() {
        String va= super.validate();
        if(StrUtil.isBlank(va)){
            if (!sql.trim().toUpperCase().startsWith("SELECT")) {
                return "表输入仅支持Select语句";
            }
        }
        return va;
    }

    @Override
    public Long getDbSourceId() {
        return dataSourceId;
    }
    @Override
    public String getDatabaseName() {
        return databaseName;
    }
    @Override
    public KettleDataFlowNodeGroupType getGroupType() {
        return KettleDataFlowNodeGroupType.INPUTS;
    }
    @Override
    public KettleDataFlowNodeType getNodeType(){
        return KettleDataFlowNodeType.TableInput;
    }
}
