package com.isyscore.os.metadata.model.dto.req;

import com.isyscore.os.metadata.model.dto.ERTableInfoDTO;
import com.isyscore.os.metadata.model.dto.ERTableRelationDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class CreateERRelationReq {

    @ApiModelProperty("主表表名")
    @NotBlank
    private String centerTable;

    @ApiModelProperty("ER图中表与表间的关联信息")
    @Valid
    public List<ERTableRelationDTO> tableRelations;

    @ApiModelProperty("ER图中每张表所包含的字段信息")
    @NotEmpty
    @Valid
    public List<ERTableInfoDTO> tableInfos;

    @ApiModelProperty("用于展示ER图的JSON数据，格式完全由前端自行定义和处理，修改数据模型时前端可根据该值复原ER视图")
    private String erLayoutJSON;

}
