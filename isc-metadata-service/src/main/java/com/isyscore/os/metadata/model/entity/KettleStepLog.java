package com.isyscore.os.metadata.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * Kettle步骤日志
 * </p>
 *
 * @author wuwx
 * @since 2022-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("kettle_step_log_all")
public class KettleStepLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty("批次ID")
    @TableField("ID_BATCH")
    private String batchId;

    @ApiModelProperty("步骤名称")
    @TableField("TRANSNAME")
    private String stepName;

    @ApiModelProperty("记录时间")
    @TableField("LOG_DATE")
    private LocalDateTime logDate;

    @ApiModelProperty("读记录条数")
    @TableField("LINES_READ")
    private Long readLines;

    @ApiModelProperty("写记录条数")
    @TableField("LINES_WRITTEN")
    private Long writtenLines;

    @ApiModelProperty("更新条数")
    @TableField("LINES_UPDATED")
    private Long updatedLines;

    @ApiModelProperty("输入记录条数")
    @TableField("LINES_INPUT")
    private Long inputLines;

    @ApiModelProperty("输出记录条数")
    @TableField("LINES_OUTPUT")
    private Long outputLines;

    @ApiModelProperty("舍弃的记录条数")
    @TableField("LINES_REJECTED")
    private Long rejectedLines;

    @ApiModelProperty("错误数")
    @TableField("ERRORS")
    private Long errors;
}
