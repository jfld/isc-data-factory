package com.isyscore.os.metadata.model.query;

import com.isyscore.os.metadata.model.dto.ConditionDTO;
import com.isyscore.os.metadata.model.dto.OrderByDTO;
import lombok.Data;

import java.util.List;

/**
 * @author fjZheng
 * @version 1.0
 * @date 2020/10/21 15:36
 */
@Data
public class DataSourceQuery extends BaseQuery implements IDataSourceQuery{
    private String name;
    private Integer type;
    private String userId;
    private String databaseName;
    private String tableNameList;
    private String tableName;
}
