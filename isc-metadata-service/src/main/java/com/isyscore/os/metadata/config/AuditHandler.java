package com.isyscore.os.metadata.config;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.isyscore.boot.login.cache.LoginUserHolder;
import com.isyscore.boot.mybatis.AuditTimeMetaObjectHandler;
import com.isyscore.boot.mybatis.IsyscoreMybatisProperties;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 *
 * @author zhangyn
 * @version 1.0
 * @date 2021/6/10 6:41 下午
 */
@Primary
@Component("auditTimeMetaObjectHandler")
public class AuditHandler extends AuditTimeMetaObjectHandler implements MetaObjectHandler {


    public AuditHandler(IsyscoreMybatisProperties mybatisProperties) {
        super(mybatisProperties);
    }

    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime now = LocalDateTime.now();
        final String tenantId = LoginUserHolder.getTenantId();
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, now);
        this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, now);
        this.strictInsertFill(metaObject, "tenantId", String.class, tenantId);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
    }

}
