create table IF NOT EXISTS isc_metadata.metric_job_batch_result_local
(
    id            Int32 comment 'ID',
    metric_ref_id Int64 comment '指标ID',
    dim           String comment '维度',
    measure       String comment '度量值',
    data_period   String comment '度量值',
    tenant_id     String comment '租户ID',
    create_time   DateTime64(3) comment '创建时间'
)engine = ReplacingMergeTree
PARTITION BY metric_ref_id
PRIMARY KEY (metric_ref_id,data_period,tenant_id,dim)
ORDER BY (metric_ref_id,data_period,tenant_id,dim);

create table IF NOT EXISTS isc_metadata.metric_job_batch_result_all
(
    id            Int32 comment 'ID',
    metric_ref_id Int64 comment '指标ID',
    dim           String comment '维度',
    measure       String comment '度量值',
    data_period   String comment '度量值',
    tenant_id     String comment '租户ID',
    create_time   DateTime64(3) comment '创建时间'
)engine=Distributed(replicated,isc_metadata,metric_job_batch_result_local, metric_ref_id);