CREATE table IF NOT EXISTS isc_metadata.metric_job_batch_local on cluster replicated
(
    `id` UInt64,
    `tenant_id` String COMMENT '租户ID',
    `metric_ref_id` UInt64 COMMENT '关联的指标',
    `metric_name` String COMMENT '关联的指标名称',
    `data_period` String COMMENT '批次对应的时间期',
    `status` Int8 COMMENT '执行结果：0:执行中,1：成功,-1：失败',
    `message` Nullable(String),
    `executed_sql` String COMMENT 'JOB任务实际执行的SQL',
    `start_time` Datetime,
    `end_time` Datetime,
    `type` Int8 COMMENT '任务执行的方式：1：自动执行  2：手动执行',
    `result_quantity` Nullable(UInt64) COMMENT '本次计算生成的结果集数据量'
    )    engine = ReplicatedMergeTree('/clickhouse/tables/{shard}/metric_job_batch_local', '{replica}')
    PRIMARY KEY (tenant_id,metric_ref_id,start_time)
    ORDER BY (tenant_id,metric_ref_id,start_time);

CREATE TABLE IF NOT EXISTS isc_metadata.metric_job_batch_all ON CLUSTER replicated
(
    id            UInt64,
    tenant_id     String comment '租户ID',
    metric_ref_id UInt64 comment '关联的指标',
    metric_name   String comment '关联的指标名称',
    data_period   String comment '批次对应的时间期',
    status        Int8 comment '执行结果：0:执行中,1：成功,-1：失败',
    message Nullable(String),
    executed_sql  String comment 'JOB任务实际执行的SQL',
    start_time    DateTime,
    end_time      DateTime,
    type          Int8 comment '任务执行的方式：1：自动执行  2：手动执行',
    result_quantity Nullable(UInt64) comment '本次计算生成的结果集数据量'
    )
    ENGINE = Distributed(replicated, isc_metadata, metric_job_batch_local, id);
