CREATE table IF NOT EXISTS isc_metadata.kettle_trans_log_local on cluster replicated
(
    ID_BATCH       Int32 comment '批次ID',
    CHANNEL_ID     String comment '频道ID',
    TRANSNAME      String comment '转换名',
    STATUS         String comment '状态 ',
    LINES_READ     Int64 comment '读',
    LINES_WRITTEN  Int64 comment '写',
    LINES_UPDATED  Int64 comment '更新',
    LINES_INPUT    Int64 comment '输入',
    LINES_OUTPUT   Int64 comment '输出',
    LINES_REJECTED Int64 comment '舍弃',
    ERRORS         Int64 comment '错误',
    STARTDATE      DateTime64(3) comment '启动时间',
    ENDDATE        DateTime64(3) comment '结束时间',
    LOGDATE        DateTime64(3) comment '日志时间',
    DEPDATE        DateTime64(3) comment '依赖时间',
    REPLAYDATE     DateTime64(3) comment '',
    LOG_FIELD      String comment '日志'
    )   engine = ReplicatedMergeTree('/clickhouse/tables/{shard}/kettle_trans_log_local', '{replica}')
    PRIMARY KEY ID_BATCH
    ORDER BY ID_BATCH;

CREATE TABLE IF NOT EXISTS isc_metadata.kettle_trans_log_all ON CLUSTER replicated
(
    ID_BATCH       Int32 comment '批次ID',
    CHANNEL_ID     String comment '频道ID',
    TRANSNAME      String comment '转换名',
    STATUS         String comment '状态 ',
    LINES_READ     Int64 comment '读',
    LINES_WRITTEN  Int64 comment '写',
    LINES_UPDATED  Int64 comment '更新',
    LINES_INPUT    Int64 comment '输入',
    LINES_OUTPUT   Int64 comment '输出',
    LINES_REJECTED Int64 comment '舍弃',
    ERRORS         Int64 comment '错误',
    STARTDATE      DateTime64(3) comment '启动时间',
    ENDDATE        DateTime64(3) comment '结束时间',
    LOGDATE        DateTime64(3) comment '日志时间',
    DEPDATE        DateTime64(3) comment '依赖时间',
    REPLAYDATE     DateTime64(3) comment '',
    LOG_FIELD      String comment '日志'
    )
    ENGINE = Distributed(replicated, isc_metadata, kettle_trans_log_local, ID_BATCH);



CREATE table IF NOT EXISTS isc_metadata.kettle_step_log_local on cluster replicated
(
    ID_BATCH       Int32 comment '批次ID',
    CHANNEL_ID     String comment '频道ID',
    LOG_DATE       DateTime64(3) comment '日志时间',
    TRANSNAME      String comment '转换名',
    STEPNAME       String comment '步骤名',
    STEP_COPY      Int32 comment '复制',
    LINES_READ     Int64 comment '读',
    LINES_WRITTEN  Int64 comment '写',
    LINES_UPDATED  Int64 comment '更新',
    LINES_INPUT    Int64 comment '输入',
    LINES_OUTPUT   Int64 comment '输出',
    LINES_REJECTED Int64 comment '舍弃',
    ERRORS         Int64 comment '错误',
    LOG_FIELD Nullable(String) comment '日志'
    )   engine = ReplicatedMergeTree('/clickhouse/tables/{shard}/kettle_step_log_local', '{replica}')
    PRIMARY KEY ID_BATCH
    ORDER BY ID_BATCH;

CREATE TABLE IF NOT EXISTS isc_metadata.kettle_step_log_all ON CLUSTER replicated
(
    ID_BATCH       Int32 comment '批次ID',
    CHANNEL_ID     String comment '频道ID',
    LOG_DATE       DateTime64(3) comment '日志时间',
    TRANSNAME      String comment '转换名',
    STEPNAME       String comment '步骤名',
    STEP_COPY      Int32 comment '复制',
    LINES_READ     Int64 comment '读',
    LINES_WRITTEN  Int64 comment '写',
    LINES_UPDATED  Int64 comment '更新',
    LINES_INPUT    Int64 comment '输入',
    LINES_OUTPUT   Int64 comment '输出',
    LINES_REJECTED Int64 comment '舍弃',
    ERRORS         Int64 comment '错误',
    LOG_FIELD Nullable(String) comment '日志'
    )
    ENGINE = Distributed(replicated, isc_metadata, kettle_step_log_local, ID_BATCH);

