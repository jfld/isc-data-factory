package com.isyscore.os.flinksql.enums;

import lombok.Getter;

/**
 * @author wany
 * sql job的类型
 */
@Getter
public enum JobTypeEnum {

    /**
     * 流式任务
     */
    SQL_STREAMING(0),
    /**
     * 批处理任务
     */
    SQL_BATCH(2);

    private final int code;

    JobTypeEnum(int code) {
        this.code = code;
    }

    public static JobTypeEnum getJobTypeEnum(Integer code) {
        if (code == null) {
            return null;
        }
        for (JobTypeEnum jobTypeEnum : JobTypeEnum.values()) {
            if (code == jobTypeEnum.getCode()) {
                return jobTypeEnum;
            }
        }
        return null;
    }

}
