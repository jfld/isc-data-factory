package com.isyscore.os.flinksql;

import java.util.regex.Pattern;

/**
 * @author wany
 * 系统常量
 */
public class Constant {

    public final static String COMMENT_SYMBOL = "--";

    public final static String SEMICOLON = ";";

    public final static String LINE_FEED = "\n";

    public final static String SPACE = "";

    public static final int DEFAULT_PATTERN_FLAGS = Pattern.CASE_INSENSITIVE | Pattern.DOTALL;

}
