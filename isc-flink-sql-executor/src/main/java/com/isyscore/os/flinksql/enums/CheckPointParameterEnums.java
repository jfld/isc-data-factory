package com.isyscore.os.flinksql.enums;

import java.util.Set;


/**
 * @author wany
 * CheckPoint参数枚举
 */
public enum CheckPointParameterEnums {

    /**
     * 路径
     */
    checkpointDir,
    /**
     * 检查点的模式
     */
    checkpointingMode,
    /**
     * 记录检查点的间隔周期
     */
    checkpointInterval,
    /**
     * 检查点的超时时间
     */
    checkpointTimeout,
    /**
     * 容忍检查点失败的次数
     */
    tolerableCheckpointFailureNumber,
    /**
     * 异步快照
     */
    asynchronousSnapshots,
    /**
     * 取消作业时是否删除检查点
     */
    externalizedCheckpointCleanup,
    /**
     * 检查点的状态后端的类型
     */
    stateBackendType,
    /**
     * 检查点允许增量记录
     */
    enableIncremental;

    public static void isExits(Set<String> keys) {
        for (String key : keys) {
            boolean exits = false;
            for (CheckPointParameterEnums checkPointParameterEnums : CheckPointParameterEnums.values()) {
                if (checkPointParameterEnums.name().equalsIgnoreCase(key)) {
                    exits = true;
                    continue;
                }
            }
            if (!exits) {
                throw new RuntimeException(key + " 暂时不支持使用");
            }
        }
    }
}
