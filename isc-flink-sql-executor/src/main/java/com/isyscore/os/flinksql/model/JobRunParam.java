package com.isyscore.os.flinksql.model;

import com.isyscore.os.flinksql.enums.JobTypeEnum;
import com.isyscore.os.flinksql.model.CheckPointParam;
import lombok.Data;

import java.util.List;

/**
 * @author zhuhuipei
 * @Description:
 * @date 2020-08-21
 * @time 02:10
 */
@Data
public class JobRunParam {
    /**
     * 待执行的sql语句
     */
    private String sql;

    /**
     * SQL任务依赖的jar包路径
     */
    private List<String> depClassPaths;

    /**
     * 任务类型
     */
    private JobTypeEnum jobTypeEnum;

    /**
     * CheckPoint 参数
     */
    private CheckPointParam checkPointParam;


}
